/*
Simple OpenID Plugin
http://code.google.com/p/openid-selector/

This code is licenced under the New BSD License.
*/

var providers_large = {
    google: {name: "google", url: "https://www.google.com/accounts/o8/id", x: 0, y: 0},
    yahoo: {name: "yahoo", url: "http://yahoo.com/", x: -100, y: 0},
    steam: {name: "steam", url: "http://steamcommunity.com/openid/", x: -200, y: 0},
    myopenid: {name: "myopenid", url: "http://myopenid.com/", x: -300, y: 0},
    flickr: {name: "flickr", url: "http://yahoo.com/", x: -400, y: 0}
};

var providers_small = {
    blogger: {name: "blogger", label: "enter your blogger account", url: "http://{username}.blogspot.com/", x: -24, y: -60},
    chimp: { name: "chimp", url: "http://chi.mp", x: -48, y: -60},
    claimid: {name: "claimid", label: "enter your claimid username", url: "http://openid.claimid.com/{username}", x: -72, y: -60},
    google_profile: {name: "google_profile", label: "enter your google profile username", url: "http://www.google.com/profiles/{username}", x: -96, y: -60},
    clickpass: {name: "clickpass", label: "enter your clickpass username", url: "http://clickpass.com/public/{username}", x: -120, y: -60},
    livejournal: {name: "livejournal", label: "enter your livejournal username", url: "http://{username}.livejournal.com/", x: -144, y: -60},
    myid: {name: "myid", url: "http://myid.net", x: -168, y: -60},
    myspace: {name: "myspace", label: "enter your myspace username", url: "http://myspace.com/{username}", x: -192, y: -60},
    wordpress: {name: "wordpress", label: "enter your wordpress.com username", url: "http://{username}.wordpress.com/", x: -216, y: -60},
    verisign: {name: "verisign", label: "enter your verisign username", url: "http://{username}.pip.verisignlabs.com/", x: -240, y: -60},
    yiid: {name: "yiid", url: "http://yiid.com", x: -264, y: -60}
};

var providers = $.extend({}, providers_large, providers_small);

var openid = {
	img_path: '/img/',
	input_id: null,
	provider_url: null,
	
	init: function(input_id) {
		// turn off hourglass
		$('body').css('cursor', 'default');
		$('#openid_submit').css('cursor', 'default');

		var openid_btns = $('#openid_btns');
		
		this.input_id = input_id;
		
		$('#openid_choice').show();
		$('#openid_input_area').empty();
		
		// add box for each provider
		for (id in providers_large) {
			openid_btns.append(this.getBoxHTML(providers_large[id], 'large'));
		}
		if (providers_small) {
			openid_btns.append('<br/>');
			for (id in providers_small) {
				openid_btns.append(this.getBoxHTML(providers_small[id], 'small'));
			}
		}
		$('#openid_form').submit(this.submit);
	},

	getBoxHTML: function(provider, box_size) {
		var box_id = provider["name"].toLowerCase();
		return '<a title="log in with ' + provider["name"] + '" href="javascript:openid.signin(\'' + box_id + '\');"' +
			' style="background: #FFF url(' + this.img_path + 'openid-providers-en.png); background-position: ' + provider["x"] + 'px ' + provider["y"] + 'px" ' +
			'class="' + box_id + ' openid_' + box_size + '_btn"></a>';
	},

	    /* Provider image click */
	signin: function(box_id, onload) {
		var provider = providers[box_id];
		if (! provider) return;
		this.highlight(box_id);

		if (box_id == 'openid') {
			$('#openid_input_area').empty();
			this.setOpenIdUrl("");
			$("#openid_identifier").focus();
			return;
		}

		// comment these two lines?
		this.provider_id = box_id;
		this.provider_url = provider['url'];

		// prompt user for input?
		if (provider['label']) {
			this.useInputBox(provider);
			this.provider_url = provider['url'];
		} else {
			$("." + box_id).css("cursor", "wait");
			this.setOpenIdUrl(provider['url']);

			//$('#openid_input_area').empty();
			this.provider_url = null;
			if (! onload) {
				$('#openid_form').submit();
			}
		}
	},

    /* Sign-in button click */
    submit: function() {
        if ($('#openid_username').val() == "") return true;

        // set up hourglass on body
        $('body').css('cursor', 'wait');
        $('#openid_submit').css('cursor', 'wait');

        // disable submit button on form
        $('input[type=submit]', this).attr('disabled', 'disabled');

    	var url = openid.provider_url;
    	if (url) {
    		url = url.replace('{username}', $('#openid_username').val());
    		openid.setOpenIdUrl(url);
    	}
    	return true;
    },
    setOpenIdUrl: function (url) { var hidden = $('#' + this.input_id); hidden.val(url); },

    highlight: function (box_id) {
    	// remove previous highlight.
    	var highlight = $('#openid_highlight');
    	if (highlight) {
    		highlight.replaceWith($('#openid_highlight a')[0]);
    	}
    	// add new highlight.
    	$('.'+box_id).wrap('<div id="openid_highlight"></div>');
    },
    useInputBox: function (provider) {
   	
		var input_area = $('#openid_input_area');
		
		var html = '';
		var id = 'openid_username';
		var value = '';
		var label = provider['label'];
		var style = '';
		
		if (label) {
			html = '<p>' + label + '</p>';
		}
		html += '<input id="'+id+'" type="text" style="'+style+'" name="'+id+'" value="'+value+'" />' + 
					'<input id="openid_submit" type="submit" value="sign in"/>';
		
		input_area.empty();
		input_area.append(html);

		$('#'+id).focus();
    }
};

$(document).ready(function() { openid.init('openid_identifier'); });
