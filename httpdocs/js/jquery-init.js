$(document).ready(function() {
	$("textarea.resizable:not(.processed)").TextAreaResizer();

	var showdown = new Showdown.converter();
	$("div.showdown").each(function() {
		$(this).html(showdown.makeHtml($(this).html().replace(/&amp;/g, "&").replace(/&quot;/g, "\"").replace(/&lt;/g, "<").replace(/&gt;/g, ">")));
	});	
	$("textarea.showdown").each(function() {
		if ($(this).attr("id")) {
			$(this).parent().append("<div id='"+$(this).attr("id")+"_preview'></div>");
		}
	});
	$("textarea.showdown").keyup(function(event){
		if ($(this).attr("id")) {
			$("div#"+$(this).attr("id")+"_preview").html(showdown.makeHtml($(this).val()));
		}
	}).keyup();
});