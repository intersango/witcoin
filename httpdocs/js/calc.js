$(document).ready(function() {
	$("input.calc").keyup(function() { calc(); });

	function calc() {
		var cost_pr = Math.abs(parseFloat($("input#cost_pr").val()));
		var cost_uv = Math.abs(parseFloat($("input#cost_uv").val()));
		var upvotes = parseInt($("input#upvotes").val());

		if (!isNumeric(cost_pr)) cost_pr = 0.01;
		if (!isNumeric(cost_uv)) cost_uv = 0.01;
		if (upvotes < 100 || !isNumeric(upvotes)) upvotes = 100;

		var last, votes = [];
		for (i=0; i<=upvotes; i++) {
			var evt = {};
			if (i == 0) {
				evt.title = "initial post or reply";
				evt.balance = cost_pr * -1;
			} else {
				evt.title = "upvote #" + i;
				evt.balance = last.balance + cost_uv / (i + 4);
			}

			last = evt;

			if (i % Math.floor(upvotes / 100) == 0) votes.push(evt);
		}

		var html = "";
		$.each(votes, function(index, vote) {
			html += "<tr>";
			html += "<td class='event'>"+vote.title+"</td>";
			html += "<td class='balance'>"+vote.balance.toFixed(8)+"</td>";
			html += "</tr>";
		});
		$("table#calc tbody").html(html);
	}

	// http://stackoverflow.com/questions/18082/validate-numbers-in-javascript-isnumeric
	function isNumeric(n) { return !isNaN(parseFloat(n)) && isFinite(n); }

	calc();
});