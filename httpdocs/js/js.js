var witcoin_balance;

$(document).ready(function() {
	$("#popup").jqm();

	witcoin_balance = parseFloat($("#witcoin_balance").text().replace(/,/g, ""));

	$("textarea.resizable:not(.processed)").TextAreaResizer();

	$("select#status_filter").change(function(event) {
		var q = []; var query = getQueryString();
		query["status"] = $(this).val();
		if (query["sort"]) q.push("sort=" + query["sort"]);
		if (query["status"]) q.push("status=" + query["status"]);
		window.location.href = window.location.protocol + "//" + window.location.host + window.location.pathname + "?" + q.join("&");
	});

	$("textarea[maxlength]").each(function() { maxlength(this); });
	
	$("textarea[maxlength]").keypress(function() { var $self = this; setTimeout(function() { maxlength($self) }, 10); });

	$("#category_filter").keyup(function() {
		var catfil = $(this).val();
		$("#category_select .cat").each(function() {
			if ($(this).text().match(new RegExp("^.*"+catfil+".*$", "ig"))) {
				var elem = $(this);
				elem.show();
				while (elem.parent().attr("class") == "indented") {
					elem = elem.parent().prev();
					elem.show();
				}
			}
			else $(this).hide();
		});
	});

	$("body.p_newcat input.category").focus();

	var showdown = new Showdown.converter();
	$("div.showdown").each(function() { $(this).html(filter($(this).html())); });
	$("input#tags").keyup(function() {
		var html = "", preview = [], tags = $(this).val().split(",");
		$.each(tags, function(i, v) {
			var tag = $.trim(v).toLowerCase().replace(/[^a-z0-9 #+._-]*/g, "");
			if (tag.split(" ").length > 3) {
				tags = tag.split(" ");
				$.each(tags, function(i, v) {
					var tag = $.trim(v).toLowerCase().replace(/[^a-z0-9 #+._-]*/g, "");
					if (preview.indexOf(tag) == -1 && tag != "") {
						preview.push(tag);
						html += '<a class="tag" href="'+window.location.protocol+'//'+window.location.host+'/p/t/'+tag+'" rel="tag" title="'+tag+'">'+tag+'</a>';
					}
				});
			} else {
				if (preview.indexOf(tag) == -1 && tag != "") {
					preview.push(tag);
					html += '<a class="tag" href="'+window.location.protocol+'//'+window.location.host+'/p/t/'+tag+'" rel="tag" title="'+tag+'">'+tag+'</a>';
				}
			}
		});
		$("#tag-preview").html(html);
	}); $("input#tags").keyup();
	$("textarea.showdown").each(function() { $(this).wmd(); });
	$("textarea.showdown").keyup(function(event){
		if ($(this).attr("id")) {
			$("div#"+$(this).attr("id")+"_preview").html(filter($(this).val()));
		}
	}).keyup();

	$(document).delegate("a.vote-up", "click", function(event){
		if ($(this).attr("class").indexOf("voted") > -1) { showerror("already voted"); return false; }
		
		var elem = this;
		data="";
		if ($(this).attr("id").match(/^vote_up_post_[0-9]*$/)) {
			var postid = /^vote_up_post_([0-9]*)$/.exec($(this).attr("id"))[1];
			var data = "postid="+postid;
		} else if ($(this).attr("id").match(/^vote_up_reply_[0-9]*$/)) {
			var replyid = /^vote_up_reply_([0-9]*)$/.exec($(this).attr("id"))[1];
			var data = "replyid="+replyid;
		}
		if (data != "") {
			var elem = this;
			$.ajax({
				type: "POST",
				url: "/vote",
				data: data,
				success: function(response){
					response = $.parseJSON(response);
					if (response.error) showerror(response.error);
					else {
						if (response.balance)  $(elem).parent().parent().children("div.profit").html("profit: "+response.balance);
						if (response.upvotes) $(elem).next().html(response.upvotes);
						$("#witcoin_balance").html(response.witcoins);
						witcoin_balance = parseFloat(response.witcoins.replace(/,/g, ""));
						$(elem).addClass("voted");
					}
				}
			});
		}
	});

	$("div.reply-collapse").click(function(event){
		if ($(this).html() == "[+]") {
			var reply = $(this).html("[&ndash;]").next().show().next().show().parent().css("background-color", "#F3F3F3").attr("reply");
			if ($("blockquote.threaded[reply="+reply+"]").length > 0) $("blockquote.threaded[reply="+reply+"]").show();
		}
		else {
			var reply = $(this).html("[+]").next().hide().next().hide().parent().css("background-color", "#FFFFFF").attr("reply");
			if ($("blockquote.threaded[reply="+reply+"]").length > 0) $("blockquote.threaded[reply="+reply+"]").hide();
		}
	});

	$("body.p_post form").submit(function(event){ $("input[type=submit]", this).attr("disabled", "disabled"); });

	$(document).delegate("body.p_viewpost form#submit_reply, form.reply_to_reply", "submit", function(event){
		var thisform = this;$(thisform).find("input[type=submit]").attr("disabled", "disabled");
		data = "json=1&reply=" + encodeURIComponent($(this).find("textarea.showdown").val());
		if ($(this).find("input.upvote").is(":checked") == true) data += "&upvote=1";
		$.ajax({
			type: "POST",
			url: $(this).attr("action"),
			data: data,
			success: function(response){
				response = $.parseJSON(response);
				if (response.error) showerror(response.error);
				else {
					$("#witcoin_balance").html(response.witcoins);
					$("#post_title").html(response.post.title);
					$("div#post_"+response.post.postid).find("div.vote-count").text(response.post.post_votes);
					if (response.post.post_voted > 0) $("a#vote_up_post_"+response.post.postid).addClass("voted");
					if (response.post.profit) $("div#post_"+response.post.postid).find("div.profit").html("profit: "+response.post.profit);
					if ($("#post_duration").attr("title") != response.post.posted) {
						$("#post_body").html(response.post.post);
						$("#post_duration").attr("title", response.post.posted);
					}
					$("#post_duration").html(response.post.duration);

					if ($("div.replies").length == 0) {
						var html = '<div class="replies">'
							+ '<a name="replies"></a>'
							+ '<div class="subheaderf">'
								+ '<h3 id="replies_num"></h3>'
								+ '<div class="sort">'
									+ '<a href="'+location.pathname+'?sort=newest#replies" title="in reverse chronological order">newest</a>'
									+ '<a href="'+location.pathname+'?sort=oldest#replies" title="in chronological order">oldest</a>'
									+ '<a class="active" href="'+location.pathname+'?sort=topvotes#replies" title="in descending order of votes">top votes</a>'
						+ '</div></div></div><div id="reply_list"></div>'
						$("div.post").after(html);
					}
					$("#replies_num").html(response.num_replies);
					var lreplydepth = 0, lreplyid = "", lreplypid = "";
					$.each(response.replies, function(index, reply) {
						if ($("div#reply_"+reply.replyid).length == 0) {
							if (reply.replypid && $("blockquote[reply="+lreplyid+"]").length == 0) $("div#reply_"+lreplyid).after("<blockquote class='threaded' reply='"+lreplyid+"'></blockquote>");

							var html = "";
							html += '<div class="reply" id="reply_'+reply.replyid+'" reply="'+reply.replyid+'">';
							html += '<div class="reply-collapse" reply="'+reply.replyid+'">[&ndash;]</div>';
							html += '<div class="reply-header">';
							html += '<div class="reply-header-left">';
							html += '<div class="reply-votes">';
							html += '<a class="vote-up'+(reply.reply_voted > 0 ? " voted" : "")+'" id="vote_up_reply_'+reply.replyid+'" title="vote up">vote up</a>';
							html += '<div class="vote-count">'+reply.reply_votes+'</div>';
							html += '</div>';
							html += '<div class="reply-links">';
							if (response.userid == reply.reply_userid) html += '<a href="'+location.protocol+'//'+location.host+'/r/'+reply.replyid+'/edit" title="edit">edit</a>';
							html += ' <a href="'+location.protocol+'//'+location.host+'/r/'+reply.replyid+'/reply#reply" title="reply">reply</a>';
							if (response.userid) html += ' <a href="'+location.protocol+'//'+location.host+'/support?r='+reply.replyid+'" title="support">support</a>';
							html += ' <a href="#r-'+reply.replyid+'" title="link">link</a>';
							html += '</div>';
							if (reply.profit != "") html += '<div class="profit">profit: '+reply.profit+'</div>';
							html += '</div>';
							html += '<div class="reply-info">';
							html += '<div class="reply-time"><span class="time" title="'+reply.replied+'">'+reply.duration+'</span> by</div>';
							html += '<div class="userinfo"><a href="/witizens/'+reply.reply_userid+'" title="'+reply.displayname+'">'+reply.displayname+'</a></div>';
							html += '</div>';
							if (reply.gravatar != "") {
								html += '<span class="gravatar"><a href="/witizens/'+reply.reply_userid+'" title="gravatar">';
								html += '<img alt="'+reply.displayname+'" class="gravatar" src="'+reply.gravatar+'"/>';
								html += '</a></span>';
							}
							if (reply.reply_revs > 0) {
								html += '<span class="revinfo">';
								html += '<a href="'+location.protocol+'//'+location.href+'/r/'+reply.replyid+'/rev'+(reply.reply_revs == 1 ? "" : "s")+'" title="revisions">';
								html += reply.reply_revs+' rev'+(reply.reply_revs == 1 ? "" : "s");
								html += '</a>';
								html += '</span>';
							}
							html += '</div>';
							html += '<div class="reply-content">'+reply.reply+'</div>';
							html += '</div>';

							if (lreplyid == "") $("div#reply_list").prepend(html);
							else if (reply.replypid) {
								if (reply.replypid == lreplypid) $("div#reply_"+lreplyid).after(html);
								else if (reply.depth > lreplydepth) $("blockquote[reply="+lreplyid+"]").append(html);
								else {
									var elem = $("blockquote[reply="+lreplypid+"]");
									while (--lreplydepth > reply.depth) elem = elem.parent();
									$(elem).after(html);
								}
							} else if (reply.depth == lreplydepth) $("div#reply_"+lreplyid).after(html);
							else {
								var elem = $("blockquote[reply="+lreplypid+"]");
								while (--lreplydepth > reply.depth) elem = elem.parent();
								$(elem).after(html);
							}
						} else {
							if (reply.reply_voted > 0) $("a#vote_up_reply_"+reply.replyid).addClass("voted");
							$("div#reply_"+reply.replyid).find("div.vote-count").text(reply.reply_votes);
							if (reply.profit) $("#reply_"+reply.replyid).find("div.profit").html("profit: "+reply.profit);
							if ($("#reply_"+reply.replyid+"_duration").attr("title") != reply.replied) {
								$("#reply_"+reply.replyid+"_duration").attr("title", reply.replied);
								$("div#reply_"+reply.replyid).find("div.reply-content").html(reply.reply);
							}
							$("#reply_"+reply.replyid+"_duration").html(reply.duration);
						}
						lreplydepth = reply.depth;
						lreplyid = reply.replyid;
						if (typeof reply.replypid === "undefined") lreplypid = ""; else lreplypid = reply.replypid;
						$("#wmd-input").val("");
					});
				}
				$(thisform).find("input[type=submit]").removeAttr("disabled");
			}
		});
		$("div.reply-ajax").remove();
		return false;
	});

	$(document).delegate("a[title=reply]", "click", function(event){
		var replyelem = $(this).parent().parent().parent().parent();
		var replyid = replyelem.attr("reply");0
		var html = "<div class='reply-ajax'><form action='/r/"+replyid+"/reply' class='reply_to_reply' method='post'>"
				+ "<div><textarea class='resizable showdown' cols='1' name='reply' rows='1'></textarea></div>"
				+ "<div><label><input class='upvote' name='upvote' type='checkbox'/> upvote</label></div>"
				+ "<div><input type='submit' value='reply'/></div>"
			+ "</form></div>";
		if (replyelem.children("div.reply-ajax").length == 0) {
			replyelem.children("div.reply-content").after(html);
			replyelem.find("div.reply-ajax textarea").TextAreaResizer();
			replyelem.find("div.reply-ajax textarea").wmd();
		}
		replyelem.find("div.reply-ajax textarea").focus();
		return false;
	});

	$("div.cat-collapse").click(function(event){ catce($(this)); });

	function filter(str) {
		str = showdown.makeHtml(str);
		str = str.replace(/(^|\s)(https?|ftp)(:\/\/[-A-Z0-9+&@#\/%?=~_|\[\]\(\)!:,\.;]*[-A-Z0-9+&@#\/%=~_|\[\]])($|\W)/gi,"$1<$2$3>$4");
		str = str.replace(/<((https?|ftp):[^'">\s]+)>/gi,'<a href="$1">$1</a>');
		return str;
	}

	function showerror(error) {
		if (error == "not logged in") {
			if ($("#getwitit").html()) error += "<br/>" + $("#getwitit").html();
			else error += "<br/>" + '<a title="get wit it" href="'+window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/getwitit?dest='+window.location.href+'">get wit it</a>';
		}
		$("#popup").html("<div class='error'>"+error+"</div>").jqmShow();
	}
	$(document).resize();
});

$(document).resize(function() {
	$("iframe#chatframe").height("1px");
	$("iframe#chatframe").height(
		$(document).height()
			- $(".maincontent").position().top
			- $(".maincontent").css("padding-bottom").replace(/px/, "")
			- $(".main").css("padding-bottom").replace(/px/, "")
			- $("#content").css("padding-bottom").replace(/px/, "")
			- $("#container").css("margin-bottom").replace(/px/, "")
	);
});

function catce(elem) {
	if (typeof($.cookie("catcol")) == "string") var catcol = $.cookie("catcol").split(","); else var catcol = [];
	var catsd = elem.parent().attr("cat");
	if (elem.html() == "+") {
		elem.html("&ndash;").parent().next().show();
		if (catcol.indexOf(catsd) !== -1) { catcol.splice(catcol.indexOf(catsd), 1); $.cookie('catcol', catcol, {domain:'witcoin.com', path:'/'}); }
	}
	else {
		elem.html("+").parent().next().hide();
		if (catcol.indexOf(catsd) === -1) { catcol.push(catsd); $.cookie('catcol', catcol.join(','), {domain:'witcoin.com', path:'/'}); }
	}
}

function catceable() {
	$("div#category_select div.cat").each(function() {
		if ($(this).next().hasClass("indented") == true) $(this).prepend("<div class='cat-collapse'>&ndash;</div>&nbsp;");
		else $(this).prepend("<div class='cat-collapse'>&nbsp;</div>&nbsp;");
		if (typeof($.cookie("catcol")) == "string") {
			var catcol = $.cookie("catcol").split(",");
			if (catcol.indexOf($(this).attr("cat")) !== -1) catce($("div.cat[cat="+$(this).attr("cat")+"] div.cat-collapse"));
		}
	});
}

// http://stackoverflow.com/questions/18082/validate-numbers-in-javascript-isnumeric/1830844#1830844
function isNumber(n) { return !isNaN(parseFloat(n)) && isFinite(n); }

function maxlength(ta) {
	var length = $(ta).val().length;
	var maxlength = $(ta).attr("maxlength");
	if ($(ta).parent().children("div.remaining").length > 0) $(ta).parent().children("div.remaining").text(maxlength - length);
	else $(ta).parent().prepend("<div class='remaining'>"+(maxlength - length)+"</div>");
}

// http://stackoverflow.com/questions/647259/javascript-query-string/647272#647272
function getQueryString() {
	var result = {}, queryString = location.search.substring(1), re = /([^&=]+)=([^&]*)/g, m;
	while (m = re.exec(queryString)) result[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
	return result;
}