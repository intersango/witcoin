<?php
	global $category, $path, $title;

	include "_head.php";
?>
   <div class="header">
    <h2><?php echo implode(" - ", $title); ?></h2>
   </div>
   <div class="maincontent">
<?php foreach ($errmsg as $msg) echo "<div class='error'>$msg</div>"; ?>
<?php foreach ($successmsg as $msg) echo "<div class='success'>$msg</div>"; ?>
    <form action="<?php echo "/admin/categories", (isset($_GET["sort"]) ? "?sort=".$_GET["sort"] : ""); ?>" method="post"><table class="categories"><thead>
     <tr>
      <th rowspan="2"><a href="?sort=category">category</a></th>
      <th class="group" colspan="4">prices</th>
     </tr>
     <tr>
      <th><a href="?sort=post">post</a></th>
      <th><a href="?sort=reply">reply</a></th>
      <th><a href="?sort=edit">edit</a></th>
      <th><a href="?sort=upvote">upvote</a></th>
     </tr>
    </thead><tbody>
<?php foreach ($categories as $categoryid => $category) { ?>
     <tr>
      <td><?php echo $category["category"]; ?></td>
      <td<?php echo isset($err["cat_".$categoryid."_cost_post"]) ? " class='error'" : ""; ?>>
       <input name="<?php echo "cat_", $categoryid, "_cost_post"; ?>" value="<?php echo isset($_POST["cat_".$categoryid."_cost_post"]) ? $_POST["cat_".$categoryid."_cost_post"] : $category["costs"]["post"]; ?>"/>
      </td>
      <td<?php echo isset($err["cat_".$categoryid."_cost_reply"]) ? " class='error'" : ""; ?>>
       <input name="<?php echo "cat_", $categoryid, "_cost_reply"; ?>" value="<?php echo isset($_POST["cat_".$categoryid."_cost_reply"]) ? $_POST["cat_".$categoryid."_cost_reply"] : $category["costs"]["reply"]; ?>"/>
      </td>
      <td<?php echo isset($err["cat_".$categoryid."_cost_edit"]) ? " class='error'" : ""; ?>>
       <input name="<?php echo "cat_", $categoryid, "_cost_edit"; ?>" value="<?php echo isset($_POST["cat_".$categoryid."_cost_edit"]) ? $_POST["cat_".$categoryid."_cost_edit"] : $category["costs"]["edit"]; ?>"/>
      </td>
      <td<?php echo isset($err["cat_".$categoryid."_cost_upvote"]) ? " class='error'" : ""; ?>>
       <input name="<?php echo "cat_", $categoryid, "_cost_upvote"; ?>" value="<?php echo isset($_POST["cat_".$categoryid."_cost_upvote"]) ? $_POST["cat_".$categoryid."_cost_upvote"] : $category["costs"]["upvote"]; ?>"/>
      </td>
     </tr>
<?php } ?>
    </tbody></table>
    <div><input id="submit" style="width: auto;" type="submit" value="update"/></div>
    </form>
   </div>
<?php include "_foot.php"; ?>