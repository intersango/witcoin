<?php
	global $bodyclass, $category, $debug, $head, $tags, $title;
	$title = array_reverse($title);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
 <head>
<?php foreach ($head["style"] as $name => $style) { ?>
  <link href="<?php echo $style; ?>" media="all" rel="stylesheet" <?php if (!is_numeric($name)) echo "title='",$name,"' "; ?>type="text/css"  />
<?php } ?>
  <link href="/favicon.ico" rel="shortcut icon" type="image/x-icon" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php foreach ($head["script"] as $script) { ?>
  <script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
  <title><?php echo strip_tags(implode(" - ", $title)); ?></title>
 </head>
 <body<?php echo (count($bodyclass) > 0 ? " class='".join(" ",$bodyclass)."'" : ""); ?>>
   <div id="header">
    <div id="top">
     <div id="logo"><a href="<?php echo getdomain(); ?>"><img alt="witcoin" src="<?php echo getdomainlogo(); ?>"/></a></div>
     <div id="special"><a href="<?php echo getdomain(), "charities"; ?>"><img alt="charitable organizations" src="/img/pot.png" title="charitable organizations"/></a></div>
     <div id="menu">
      <div class="menuitem"><a href="<?php echo getdomain(); ?>help" title="help">help</a></div>
      <div class="menuitem"><a href="<?php echo getdomain(); ?>chat" title="chat">chat</a></div>
      <?php if (!loggedin()) { ?><div class="menuitem"><a href="<?php echo getdomain(),"getwitit?dest=",getdest(); ?>" title="get wit it">get wit it</a></div><?php } ?>
      <?php if (loggedin()) { ?><div class="menuitem"><a href="/witout?dest=<?php echo urldecode(getdest()); ?>" title="wit out">log out</a></div><?php } ?>
      <div class="menuitem"><a href="<?php echo getdomain(); ?>categories" title="categories">categories</a></div>
<?php if (loggedin()) { ?>
      <div class="menuitem">
       <a href="<?php echo getdomain(), "witizens/", $_SESSION["userid"]; ?>"><?php echo $_SESSION["displayname"]; ?></a>
<?php $unread = getunreadmessages(); ?>
       <a href="/inbox"><img alt="inbox" class="<?php echo $unread == 0 ? "inbox_ntsh" : "inbox_stsh" ; ?>" src="/img/mail.png" title="<?php echo $unread; ?> unread"/></a>
      </div>
<?php } ?>
      <?php if (loggedin()) { ?><div class="menuitem"><a href="/witup" title="get witcoins">wit up</a></div><?php } ?>
      <?php if (loggedin()) { ?><div class="menuitem"><a href="/transactions">witcoins: <span id="witcoin_balance"><?php echo clean_num(getbalance($_SESSION["userid"])); ?></span></a></div><?php } ?>
     </div>
    </div>
   </div>
   <div id="content">
<?php array_pop($title); ?>