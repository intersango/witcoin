<?php
	global $err, $errmsg, $path, $title, $witizen;
	include "_head.php";
?>
   <div class="main">
    <div class="header">
     <h2><?php $title = array_reverse($title); echo implode(" - ", $title); ?></h2>
    </div>
    <div class="maincontent">
     <div class="witizen_gravatar">
      <img alt="" class="gravatar" height="128" src="<?php echo $witizen["gravatar"]; ?>" width="128"/><br/>
      <div class="center"><a href="http://gravatar.com/" target="_blank" title="your gravatar is associated with your email address">change picture</a></div>
     </div>
     <div class="witizen_details_edit">
<?php if ($err && isset($errmsg)) foreach ($errmsg as $msg) echo "<div class='error'>$msg</div>"; ?>
      <h2><?php echo $witizen["rank"]; ?></h2>
      <form action="<?php echo $path; ?>" method="post"><table><tbody>
<?php if ($witizen["username"]) { ?>
       <tr>
        <td class="field_label">username</td>
        <td class="field_input"><?php echo $witizen["username"]; ?></td>
       </tr>
       <tr>
        <td class="field_label">password</td>
        <td class="field_input"><a href="/password">change password</a></td>
       </tr>
<?php }?>
       <tr>
        <td class="field_label<?php echo isset($err["displayname"]) ? " error" : ""; ?>">display name</td>
        <td class="field_input"><input name="displayname" type="text" value="<?php echo $witizen["displayname"]; ?>"/></td>
       </tr>
       <tr>
        <td class="field_label<?php echo isset($err["bitcoinaddress"]) ? " error" : ""; ?>">bitcoin address</td>
	<td class="field_input"><input name="bitcoinaddress" type="text" value="<?php echo $witizen["bitcoin_address_user"]; ?>"/></td>
       </tr>
       <tr>
        <td class="field_label<?php echo isset($err["emailaddress"]) ? " error" : ""; ?>">email address</td>
        <td class="field_input"><input name="emailaddress" type="text" value="<?php echo $witizen["email_address"]; ?>"/></td>
       </tr>
       <tr>
        <td class="field_label<?php echo isset($err["website"]) ? " error" : ""; ?>">website</td>
        <td class="field_input"><input name="website" type="text" value="<?php echo $witizen["website"]; ?>"/></td>
       </tr>
       <tr>
        <td class="field_label">location</td>
        <td class="field_input"><input name="location" type="text" value="<?php echo $witizen["location"]; ?>"/></td>
       </tr>
       <tr>
        <td class="field_label">about me</td>
        <td class="field_input"><textarea class="resizable showdown" cols="1" id="wmd-input" name="aboutme" rows="1"><?php echo $witizen["aboutme"]; ?></textarea></td>
       </tr>
       <tr>
	<td class="field_label">valid elements</td>
	<td class="field_input">address, a, big, blockquote, br, b, center, cite, code, dfn, div, em, h1, h2, h3, h4, h5, h6, hr, img, i, kbd, li, ol, pre, p, samp, small, strike, strong, sub, sup, tt, u, ul, var</td>
       </tr>
       <tr>
	<td class="field_label">&nbsp;</td>
	<td class="field_input"><input id="submit-button" type="submit" value="update"/>
	<a href="/witizens/<?php echo $_SESSION["userid"]; ?>" title="cancel">cancel</a>
	</td>
       </tr>
      </tbody></table></form>
     </div>
    </div>
   </div>
<?php include "_foot.php"; ?>