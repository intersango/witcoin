<?php
	global $title;

	include "_head.php";
?>
    <div class="main">
     <div class="header">
      <h2><?php echo implode(" - ", $title); ?></h2>
     </div>
     <div class="maincontent"><div class="help">
      <h3>qu'est-ce que witcoin?</h3>
      <p>witcoin est un site social baséé sur le micro-paiement et utilisant <a href="http://bitcoin.org" title="bitcoin">bitcoin</a>. poster, répondre, éditer, voter, supporter et créer / louer une catégorie nécessite un micro-paiement. En plus de payer pour fournir du contenu, les utilisateurs peuvent aussi mettre en place des profits pour eux-mêmes.</p>

      <h3>comment faire des profits?</h3>
      <p>les utilisateurs peuvent faire des profits en postant, répondant, créant / louant une catégorie, ou simplement en ayant un compte.
       <ul>
        <li>si un utilisateur poste, le cout du post sera équitablement redistribué entre le site, le locataire de la catégorie ainsi qu'à toutes les oeuvres de charité sélectionnées par cet utilisateur.</li>
        <li>si un utilisateur répond à un post, le cout de la réponse sera équitablement redistribué entre le site, le locataire de la catégorie, le posteur, ainsi qu'à toutes les oeuvres de charité sélectionnées par cet utilisateur.</li>
        <li>si un utilisateur répond à une réponse, le cout de la réponse sera équitablement redistribué entre le site, le locataire de la catégorie, la personne ayant répondu précédemment, ainsi qu'à toutes les oeuvres de charité sélectionnées par cet utilisateur.</li>
        <li>si un utilisateur édite un post, le cout de l'édition sera équitablement redistribué entre le site, le locataire de la catégorie, ainsi qu'à toutes les oeuvres de charité sélectionnées par cet utilisateur.</li>
        <li>si un utilisateur édite une réponse, le cout de l'édition sera équitablement redistribué entre le site, le locataire de la catégorie, le posteur ou personne ayant répondu précédemment (celui dont le message est immédiatement précédent), ainsi qu'à toutes les oeuvres de charité sélectionnées par cet utilisateur.</li>
        <li>si un utilisateur vote positivement ("upvote") pour un postage ou une réponse, le cout du vote sera équitablement distribué entre le site, le locataire de la catégorie, le posteur ou personne ayant répondu, et tous les up-voteurs précédents s'il y en a ainsi que toutes les oeuvres de charité sélectionnées par cet utilisateur.</li>
        <li>si un utilisateur supporte un post ou une réponse, 90% du montant supporté sera distribué au posteur ou à la personne ayant répondu et les 10% restants seront redistribués équitablement entre le site, le locataire de la catégorie, toutes les personnes ayant précédemment voté positivement s'il y en a ainsi qu'à toutes les oeuvres de charité sélectionnées par cet utilisateur.</li>
	<li>si un utilisateur crée / loue une catégorie, le cout de création / location de la catégorie sera redistribué au site ainsi qu'à toutes les oeuvres de charité sélectionnées par cet utilisateur.</li>
	<li>si un utilisateur donne à un autre utilisateur, 100% du montant sera distribué à l'utilisateur destinataire.</li>
       </ul>
      </p>

      <h3>comment rejoindre witcoin?</h3>
      <p><a href="<?php echo getdomain(),"getwitit?dest=",getdest(); ?>" title="obtenez wit it">obtenez wit it</a>.</p>

      <h3>comment puis-je obtenir des bitcoins?</h3>
      <p>pour participer au site, vous devez créditer votre compte en <a href="http://bitcoin.org" title="bitcoins">bitcoins</a>. voyez les sections <a href="https://en.bitcoin.it/wiki/getting_started" title="mise en route">"mise en route"</a> et <a href="https://en.bitcoin.it/wiki/buying_bitcoins" title="acheter des bitcoins">"acheter des bitcoins"</a> sur le wiki de bitcoin pour des informations sur la manière d'obtenir des bitcoins. vous pouvez obtenir 0.05 bitcoins gratuitement sur <a href="https://freebitcoins.appspot.com/" title="bitcoin faucet">bitcoin faucet</a>.</p>

      <h3>comment puis-je obtenir des witcoins?</h3>
      <p><a href="/witup" title="wittez">"wittez"</a> c'est-à-dire convertissez des bitcoins en witcoins en précisant le montant désiré sur cette page. vous pouvez aussi rafraichir la page des <a href="/transactions" title="transactions">"transactions"</a> lorsque la transaction reçoit une confirmation. votre solde est mis à jour seulement lorsque vous rafraichissez l'une de ces deux pages.  un witcoin sera toujours convertit en un bitcoin; vous n'avez pas à vous inquiéter de perdre de l'argent à cause des taux de change.</p>

      <h3>comment puis-je poster?</h3>
      <p>naviguez vers la catégorie voulue et cliquez sur le lien "post" (poster) présent en haut à droite de la page (juste en dessous du lien "help" (aide)). le texte de ce lien dépend de la catégorie.</p>
      <p>il n'est pas possible de poster à partir de la page principale.</p>
      <p>le cout du postage dépend de la catégorie. le cout par défaut est de 0.01 witcoins.</p>
      <p>vous devez être authentifié.</p>

      <h3>comment puis-je réponde à un post?</h3>
      <p>naviguez vers le post désiré et faites défiler la fenêtre vers le bas.</p>
      <p>le cout de la réponse dépend de la catégorie. le cout par défaut est de 0.01 witcoins.</p>
      <p>vous devez être authentifié.</p>

      <h3>comment puis-je répondre à une réponse?</h3>
      <p>naviguez vers la réponse désirée et cliquez "reply" (répondre).</p>
      <p>le cout de la réponse dépend de la catégorie. le cout par défaut est de 0.01 witcoins.</p>
      <p>vous devez être authentifié.</p>

      <h3>comment puis-je éditer mon post ou ma réponse?</h3>
      <p>naviguez vers le post ou la réponse désirée et cliquez sur "edit" (éditer).</p>
      <p>le cout d'édition dépend de la catégorie. le cout par défaut est de 0.01 witcoins.</p>
      <p>vous devez être authentifié.</p>

      <h3>comment puis-je voter positivement (upvote)?</h3>
      <p>naviguez vers le post ou la réponse désirée et cliquez sur la flèche pointant vers le haut à coté du post ou de la réponse.</p>
      <p>le cout d'un vote positif dépend de la catégorie. le cout par défaut est de 0.01 witcoins.</p>
      <p>vous devez être authentifié.</p>

      <h3>comment puis-je voter négativement (downvote)?</h3>
      <p>cela n'est pas possible.</p>

      <h3>comment puis-je créer / louer une catégorie?</h3>
      <p>naviguez vers la catégorie parente désirée et sélectionnez "new subcategory" (nouvelle sous-catégorie).</p>
      <p>le cout de création / location d'une catégorie est de 1 witcoin. lisez <a href="http://meta.witcoin.com/p/56/user-generated-categories" title="user-generated categories">ceci</a>.</p>
      <p>vous devez être authentifié.</p>

      <h3>comment puis-je faire un don à ou payer un autre utilisateur?</h3>
      <p>naviguez vers le profil de l'utilisateur désiré et sélectionnez "donate witcoins" (donner des witcoins).</p>
      <p>vous devez être authentifié.</p>

      <h3>comment puis-je [dé]selectionner des oeuvres de charité à inclure dans la redistribution des profits?</h3>
      <p>naviguez vers votre <?php echo isset($_SESSION["userid"]) ? '<a href="/witizens/'.$_SESSION["userid"].'" title="profil utilisateur">profil utilisateur</a>' : 'profil utilisateur'; ?> et sélectionnez <?php echo isset($_SESSION["userid"]) ? '<a href="/witizens/charities/'.$_SESSION["userid"].'" title="charities">"charities"</a>' : '"charities"'; ?> (oeuvres de charité).</p>

      <h3>comment la donation à des oeuvres de charité fonctionne-t-elle?</h3>
      <p>sélectionnez une oeuvre de charité ou plus et elles seront incluses dans la redistribution des profits pour tous les paiements distribués.</p>
      <p>par exemple, si vous avez sélectionné 8 oeuvres de charité et postez pour un cout de 0.01 witcoins, le cout du post sera équitablement réparti entre le site, le locataire de la catégorie, et chacune des huit oeuvres de charité. ainsi, chacune de ces entités recevra 0.001 witcoins.</p>

      <h3>comment puis-je convertir à nouveau mes witcoins en bitcoins?</h3>
      <p>premièrement, assurez vous que votre adresse bitcoin est correctement renseignée dans votre <?php echo isset($_SESSION["userid"]) ? '<a href="/witizens/edit/'.$_SESSION["userid"].'" title="profil utilisateur">profil utilisateur</a>' : 'profil utilisateur'; ?>.  ensuite, saisissez le montant à retirer sur la page <a href="http://witcoin.com/witdraw" title="witdraw">"witdraw"</a> (retrait) et validez. les bitcoins seront directement envoyés à l'adresse bitcoin précisée dans votre profil utilisateur.</p>

      <h3>markdown</h3>
      <p><a href="http://en.wikipedia.org/wiki/markdown" title="markdown">"markdown"</a> ainsi que partiellement <a href="http://en.wikipedia.org/wiki/html" title="html">"html"</a> sont supportés dans les posts, réponses, descriptions de catégories et le "about me" (à propos de moi) du profil utilisateur. de surcroît, les urls sont converties en liens.</p>
      <p>les tags html valides sont: address, a, big, blockquote, br, b, center, cite, code, dfn, div, em, h1, h2, h3, h4, h5, h6, hr, img, i, kbd, li, object, ol, pre, p, samp, small, strike, strong, sub, sup, tt, u, ul, var</p>

      <h3>support et rapport de bugs</h3>
      <p>posez vos questions dans la catégorie <a href="http://faq.witcoin.com/" title="faq">"faq"</a>.</p>
      <p>rapportez des bugs et cherchez du support dans la catégorie <a href="http://meta.witcoin.com/" title="meta">"meta"</a>.</p>
      <p>rendez-nous visite sur <a href="/chat" title="internet relay chat">irc</a>.</p>
     </div></div>
    </div>
<?php include "_foot.php"; ?>