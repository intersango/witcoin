<?php
	global $category, $title;
	include "_head.php";
?>
   <div class="main">
    <div class="headerf">
     <h2><?php echo implode(" - ", $title); ?></h2>
<?php if (isset($_SESSION["userid"])) { ?>
     <div class="sort">
      <a href="/newcharity" title="apply to participate in receiving charitable donations">apply</a>
      <a href="/witizens/charities/<?php echo $_SESSION["userid"]; ?>" title="select which charities to include in distribution of profits">select</a>
     </div>
<?php } ?>
    </div>
    <div class="maincontent">
     <table class="charities">
      <tr>
       <th>entity</th>
       <th>witcoins donated</th>
       <th>bitcoin address</th>
      </tr>
<?php foreach ($charities as $charityid => $charity) { ?>
      <tr>
       <td class="name"><a href="/charity/<?php echo $charity["path"]; ?>">
        <?php if ($charity["logo"] != "") { ?><img alt="<?php echo $charity["charity"]; ?>" src="<?php echo $charity["logo"]; ?>" title="<?php echo $charity["charity"]; ?>"/><?php } else echo $charity["charity"]; ?>
       </a></td>
       <td class="donated"><?php echo clean_num($charity["donated"]); ?></td>
       <td class="bitcoinaddress"><?php echo $charity["bitcoin_address_charity"]; ?></td>
      </tr>
<?php } ?>
     </table>
    </div>
   </div>
<?php include "_foot.php"; ?>