<?php
	global $category, $costs, $err, $errmsg, $post, $title, $witizen;
	include "_head.php";
?>
   <div class="main">
    <div class="header">
     <h2><?php echo implode(" - ", $title); ?></h2>
    </div>
    <div class="maincontent">
<?php if (isset($_SESSION["userid"])) { ?>
<?php	if ($witizen["witcoins"] < $category["costs"]["post"]) { ?>
     <div class="error lh29">
      you do not have enough witcoins to post<br>
     <a href="/witup" title="get witcoins">wit up</a> to get witcoins</a>
     </div>
<?php	} else { ?>
<?php		foreach ($errmsg as $msg) echo "<div class='error'>$msg</div>"; ?>
     <form action="/p" method="post">
      <h3<?php echo isset($err["title"]) ? " class='error'" : ""; ?>>title</h3>
      <div><input id="title" maxlength="300" name="title" type="text" value="<?php echo $post["title"]; ?>"/></div>
      <h3<?php echo isset($err["post"]) ? " class='error'" : ""; ?>><?php echo $category["lang"]["noun_post"]["singular"]; ?></h3>
      <div><textarea class="resizable showdown" cols="1" id="wmd-input" name="post" rows="1"><?php echo $post["post"]; ?></textarea></div>
      <h3<?php echo isset($err["tags"]) ? " class='error'" : ""; ?>>tags (comma delimited):</h3>
      <div class="tags"><div id="tag-preview"></div></div>
      <div><input id="tags" name="tags" type="text" value="<?php echo addslashes($post["taglist"]); ?>"/></div>
      <div><label><input name="upvote" type="checkbox"<?php if ($post["upvote"] == "t") echo " checked"; ?>/> upvote</label></div>
<?php		if (isset($category["status"])) { ?>
      <h3<?php echo isset($err["status"]) ? " class='error'" : ""; ?>>status</h3>
      <div><?php echo $select_status; ?></div>
<?php		} ?>
      <div><input id="submit" type="submit" value="<?php echo $category["lang"]["verb_post"]["present"]; ?>"/></div>
     </form>
<?php	} ?>
<?php } else { ?>
     <div class="center">you must <a href="<?php echo getdomain(),"getwitit?dest=",getdest(); ?>" title="get wit it">get wit it</a> to <?php echo $category["lang"]["verb_post"]["present"]; ?></div>
<?php } ?>
    </div>
   </div>
<?php include "_foot.php"; ?>
