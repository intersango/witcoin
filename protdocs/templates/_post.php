<?php
	global $category, $err, $path, $post, $replies;
	include "_head.php";
?>
   <div class="main">
    <div class="header">
     <h2 id="post_title"><?php if (isset($post["category_status"]) && isset($post["post_status"])) { ?><span class="status">(<?php echo $post["post_status"]; ?>)</span> <?php } ?><?php echo implode(" - ", $title); ?></h2>
    </div>
    <div class="maincontent">
     <div class="post" id="<?php echo "post_", $post["postid"]; ?>">
      <div class="post-header">
       <div class="post-header-left">
        <div class="post-votes">
         <a class="vote-up<?php if (isset($post["post_voted"]) && $post["post_voted"] > 0) echo " voted"; ?>" id="vote_up_post_<?php echo $post["postid"]; ?>" title="vote up">vote up</a>
         <div class="vote-count"><?php echo $post["post_votes"]; ?></div>
        </div>
        <div class="post-links">
<?php if (loggedin() && $post["post_userid"] == $_SESSION["userid"]) { ?>
         <a href="<?php echo getdomain($post["category_sd"]),"p/",$post["postid"],"/edit"; ?>" title="edit">edit</a>
<?php } ?>
         <a href="#reply" title="reply">reply</a>
         <a href="<?php echo getdomain($post["category_sd"]),"support?p=",$post["postid"]; ?>" title="support">support</a>
        </div>
        <div class="profit">
<?php if ($post["post_balance"] >= 0.00000001) { ?>
         profit: <?php echo clean_num($post["post_balance"]); ?>
<?php } ?>
        </div>
<?php if (is_rank("moderator") && $post["post_balance"] <= 0) { ?>
        <div class="loss">balance: <?php echo clean_num($post["post_balance"]); ?></div>
<?php } ?>
       </div>
       <div class="post-info">
        <div class="post-time"><span class="time" id="post_duration" title="<?php echo $post["posted"]; ?>"><?php echo time_duration(strtotime($post["posted"])); ?></span></div>
        <div class="userinfo">
         <a href="<?php echo getdomain(), "witizens/", $post["post_userid"]; ?>" title="<?php echo $post["displayname"]; ?>"><?php echo $post["displayname"]; ?></a>
        </div>
       </div>
<?php if ($post["gravatar"] != "") { ?>
       <span class="gravatar"><a href="<?php echo getdomain(), "witizens/", $post["post_userid"]; ?>" title="gravatar">
        <img alt="<?php echo $post["displayname"]; ?>" class="gravatar" src="<?php echo $post["gravatar"]; ?>"/>
       </a></span>
<?php } ?>
<?php if ($post["post_revs"] > 0) { ?>
        <div class="revinfo">
        <a href="<?php echo getdomain($post["category_sd"]),"p/",$post["postid"],"/rev",$post["post_revs"] == 1 ? "" : "s"; ?>" title="revisions">
         <?php echo $post["post_revs"]," rev",$post["post_revs"] == 1 ? "" : "s"; ?>
        </a>
       </div>
<?php } ?>
      </div>
      <div class="post-content">
       <div class="post-body" id="post_body"><?php echo $raw ? htmlspecialchars($post["post"]) : $post["post"]; ?></div>
<?php if ($post["taglist"]) { ?>
       <div class="tags">
<?php	foreach (tags_delimited($post["taglist"]) as $tag) { ?>
        <a class="tag" href="/p/t/<?php echo $tag; ?>" rel="tag" title="<?php echo $tag; ?>"><?php echo $tag; ?></a>
<?php	} ?>
       </div>
<?php } ?>
<?php if (isset($post["category_status"]) && is_rank("moderator")) { ?>
       <div>
        <form action="<?php echo $path; ?>" method="post">
         <div><?php echo $select_status; ?></div>
         <input class="status-button" type="submit" value="update status"/>
        </form>
       </div>
<?php } ?>
      </div>
     </div>
<?php if (count($replies) > 0) { ?>
     <div class="replies">
      <a name="replies"></a>
      <div class="subheaderf">
       <h3 id="replies_num"><?php echo count($replies), " ", $category["lang"]["noun_reply"][count($replies) == 1 ? "singular" : "plural"]; ?></h3>
       <div class="sort">
        <a <?php if ($sort == "newest") echo 'class="active" '; ?>href="<?php echo $path; ?>?sort=newest#replies" title="in reverse chronological order">newest</a>
        <a <?php if ($sort == "oldest") echo 'class="active" '; ?>href="<?php echo $path; ?>?sort=oldest#replies" title="in chronological order">oldest</a>
<?php /*        <a <?php if ($sort == "toprank") echo 'class="active" '; ?>href="<?php echo $path; ?>?sort=toprank#replies" title="in descending order of rank">top rank</a> */ ?>
        <a <?php if ($sort == "topvotes") echo 'class="active" '; ?>href="<?php echo $path; ?>?sort=topvotes#replies" title="in descending order of votes">top votes</a>
       </div>
      </div>
      <div id="reply_list">
<?php
		$lastdepth = 0;
		foreach ($replies as $replyid => $reply) {
			while ($reply["depth"] > $lastdepth) { echo "<blockquote class='threaded' reply='".$lreplyid."'>"; $lastdepth++; }
			while ($reply["depth"] < $lastdepth) { echo "</blockquote>"; $lastdepth--; }
			$lreplyid = $replyid;
			if (strtotime($reply["replied"]) == strtotime($post["lastactivity"])) {
?>
       <a name="lastactivity"></a>
<?php		} ?>
       <a name="r-<?php echo $replyid; ?>"></a>
       <div class="reply" id="<?php echo "reply_", $replyid; ?>" reply="<?php echo $replyid; ?>">
        <div class="reply-collapse" reply="<?php echo $replyid; ?>">[&ndash;]</div>
        <div class="reply-header">
         <div class="reply-header-left">
          <div class="reply-votes">
           <a class="vote-up<?php if (isset($reply["reply_voted"]) && $reply["reply_voted"] > 0) echo " voted"; ?>" id="vote_up_reply_<?php echo $replyid; ?>" title="vote up">vote up</a>
           <div class="vote-count"><?php echo $reply["reply_votes"]; ?></div>
          </div>
          <div class="reply-links">
<?php		if (loggedin() && $reply["reply_userid"] == $_SESSION["userid"]) { ?>
           <a href="<?php echo getdomain($post["category_sd"]),"r/",$replyid,"/edit"; ?>" title="edit">edit</a>
<?php		} ?>
           <a href="<?php echo getdomain($post["category_sd"]),"r/",$replyid,"/reply#reply"; ?>" title="reply">reply</a>
<?php		if (loggedin()) { ?>
           <a href="<?php echo getdomain($post["category_sd"]),"support?r=",$replyid; ?>" title="support">support</a>
<?php		} ?>
           <a href="#r-<?php echo $replyid; ?>" title="link">link</a>
          </div>
          <div class="profit">
<?php if ($reply["reply_balance"] >= 0.00000001) { ?>
           profit: <?php echo clean_num($reply["reply_balance"]); ?>
<?php } ?>
          </div>
<?php if (is_rank("moderator") && $reply["reply_balance"] <= 0) { ?>
          <div class="loss">balance: <?php echo clean_num($reply["reply_balance"]); ?></div>
<?php } ?>
         </div>
         <div class="reply-info">
          <div class="reply-time"><span class="time" id="<?php echo "reply_", $replyid, "_duration"; ?>" title="<?php echo $reply["replied"]; ?>"><?php echo time_duration(strtotime($reply["replied"])); ?></span> by</div>
          <div class="userinfo"><a href="/witizens/<?php echo $reply["reply_userid"]; ?>" title="<?php echo $reply["displayname"]; ?>"><?php echo $reply["displayname"]; ?></a></div>
         </div>
<?php if ($reply["gravatar"] != "") { ?>
         <span class="gravatar"><a href="/witizens/<?php echo $reply["reply_userid"]; ?>" title="gravatar">
          <img alt="<?php echo $reply["displayname"]; ?>" class="gravatar" src="<?php echo $reply["gravatar"]; ?>"/>
         </a></span>
<?php } ?>
<?php		if ($reply["reply_revs"] > 0) { ?>
         <span class="revinfo">
          <a href="<?php echo getdomain($post["category_sd"]),"r/",$replyid,"/rev",$reply["reply_revs"] == 1 ? "" : "s"; ?>" title="revisions">
           <?php echo $reply["reply_revs"]," rev",$reply["reply_revs"] == 1 ? "" : "s"; ?>
          </a>
         </span>
<?php		} ?>
        </div>
        <div class="reply-content"><?php echo $raw ? htmlspecialchars($reply["reply"]) : $reply["reply"]; ?></div>
       </div>
<?php	} ?>
      </div>
     </div>
<?php
	} /* if (count($replies) > 0) */ else {
	}
?>
     <a name="reply"></a>
     <div class="submitreply">
<?php
	if (isset($_SESSION["userid"])) {
		if ($err) foreach ($errmsg as $msg) echo "<div class='error'>$msg</div>";
?>
      <form action="<?php echo $path; ?>" id="submit_reply" method="post">
       <h3>your <?php echo $category["lang"]["noun_reply"]["singular"]; ?></h3>
       <div><textarea class="resizable showdown" cols="1" id="wmd-input" name="reply" rows="1"><?php echo $preply["reply"]; ?></textarea></div>
       <div><label><input class="upvote" name="upvote" type="checkbox"<?php if ($preply["upvote"] == "t") echo " checked"; ?>/> upvote</label></div>
       <div><input type="submit" value="<?php echo $category["lang"]["verb_reply"]["present"]; ?>"/></div>
      </form>
<?php } else { ?>
     <div class="center">you must <a href="<?php echo getdomain(),"getwitit?dest=",getdest(); ?>" title="get wit it">get wit it</a> to <?php echo $category["lang"]["noun_reply"]["singular"]; ?></div>
<?php } ?>
     </div>
    </div>
   </div>
<?php include "_foot.php"; ?>
