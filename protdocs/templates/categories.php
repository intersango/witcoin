<?php
	global $categories, $category, $title;
	include "_head.php";
?>
   <div class="main">
    <div class="headerf">
     <h2><?php echo implode(" - ", $title); ?></h2>
     <div class="sort">
      <a <?php if ($sort == "newest") echo 'class="active" '; ?>href="<?php echo $path; ?>?sort=newest" title="in reverse chronological order">newest</a>
      <a <?php if ($sort == "oldest") echo 'class="active" '; ?>href="<?php echo $path; ?>?sort=oldest" title="in chronological order">oldest</a>
      <a <?php if ($sort == "topcategories") echo 'class="active" '; ?>href="<?php echo $path; ?>?sort=topcategories" title="in descending order of categories">top categories</a>
     </div>
    </div>
    <div class="maincontent">
<?php foreach ($categories as $categoryid => $cat) { ?>
     <div class="categories-outer"><div class="categories">
      <div class="stats">
       <div class="posts"><strong><?php echo $cat["categories_posts"]; ?></strong>posts</div>
      </div>
      <div class="catsum">
       <div class="category">
        <h3><a href="<?php echo getdomain($cat["categorysd"]); ?>" title="<?php echo $cat["category"]; ?>"><?php echo $cat["category"]; ?></a></h3>
       </div>
       <div>
<?php if ($cat["allowposts"] == "t") { ?>
        <a href="<?php echo getdomain($cat["categorysd"]),"p"; ?>" title="<?php echo lang($cat["lang"], "verb_post", "present"); ?>"><?php echo lang($cat["lang"], "verb_post", "present"); ?></a>
<?php } ?>
<?php if (isset($_SESSION["userid"]) && $cat["renter"] == $_SESSION["userid"]) { ?>
        <a href="<?php echo getdomain($cat["categorysd"]), "admin"; ?>" title="configure">configure</a>
<?php } ?>
       </div>
      </div>
     </div></div>
<?php } ?>
    </div>
   </div>
<?php include "_foot.php"; ?>