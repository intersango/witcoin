<?php
	global $err, $path, $post, $replies, $title;
	include "_head.php";
?>
   <div class="main">
    <div class="header">
     <h2>
      <?php if (isset($post["category_status"])) { ?><span class="status">(<?php echo $post["post_status"]; ?>)</span><?php } ?>
      <a href="<?php echo getdomain($post["category_sd"]),"p/",$post["postid"],"/",urlfriendly($post["title"]); ?>" title="<?php echo $post["title"]; ?>">
       <?php echo implode(" - ", $title); ?>
      </a>
     </h2>
    </div>
    <div class="maincontent">
     <div class="postrev">
<?php
	$lastrev = $post;
	$lastrev["status"] = isset($post["post_status"]) ? $post["post_status"] : NULL;
	$diff = new Diff(); $rev = count($post["postrevs"]);
	foreach ($post["postrevs"] as $postrev) {
?>
      <div class="postrev-content">
       <div class="revision-number"><?php echo $rev--; ?></div>
       <div class="revision">
<?php if ($lastrev["title"] != $postrev["title"]) {	?>
        <div><h3>title</h3><?php echo filter($diff->renderDiff($diff->stringDiff($postrev["title"], $lastrev["title"])), FILTER_WORDS | FILTER_PURIFY); ?></div>
<?php } if ($lastrev["post"] != $postrev["post"]) { ?>
        <div><h3><?php echo lang($category["lang"], "noun_post", "singular"); ?></h3><?php echo filter($diff->renderDiff($diff->stringDiff($postrev["post"], $lastrev["post"]))); ?></div>
<?php } if ($lastrev["taglist"] != $postrev["taglist"]) { ?>
        <div><h3>tags</h3><?php echo filter($diff->renderDiff($diff->stringDiff(str_replace(",", ", ", $postrev["taglist"]), str_replace(",", ", ", $lastrev["taglist"])), " ,.\n")); ?></div>
<?php } if ($lastrev["status"] != $postrev["status"]) { ?>
        <div><h3>status</h3><?php echo $diff->renderDiff($diff->stringDiff($postrev["status"], $lastrev["status"])); ?></div>
<?php } if ($lastrev["syndicateupto"] != $postrev["syndicateupto"]) { ?>
        <div><h3>syndicate up to</h3><?php echo $diff->renderDiff($diff->stringDiff($postrev["syndicateupto"], $lastrev["syndicateupto"])); ?></div>
<?php } ?>
       </div>
       <div class="post-info">
        <div class="post-time">
	 <span class="time" title="<?php echo $postrev["posted"]; ?>"><?php echo time_duration(strtotime($postrev["posted"])); ?></span>
	</div>
<?php if ($post["gravatar"] != "") { ?>
	<div class="gravatar">
	 <a href="/witizens/<?php echo $postrev["userid"]; ?>" title="gravatar"><img alt="<?php echo $postrev["displayname"]; ?>" class="gravatar" src="<?php echo $postrev["gravatar"]; ?>"/></a>
	</div>
<?php } ?>
	<div class="userinfo"><a href="/witizens/<?php echo $postrev["userid"]; ?>" title="<?php echo $postrev["displayname"]; ?>"><?php echo $postrev["displayname"]; ?></a></div>
       </div>
      </div>
<?php
		$lastrev = $postrev;
	}
?>
     </div>
    </div>
   </div>
<?php include "_foot.php"; ?>