<?php
	global $title, $witizen;

	include "_head.php";
?>
   <div class="main">
    <div class="headerf">
     <h2><?php echo implode(" - ", $title); ?></h2>
     <div class="sort"><a href="/outbox" title="outbox">outbox</a></div>
    </div>
    <div class="maincontent">
     <table class="messages">
      <thead><tr>
       <th>from</th>
       <th>witcoins</th>
       <th>subject</th>
       <th>date</th>
      </tr></thead>
      <tbody>
<?php foreach ($messages as $message) { ?>
       <tr>
        <td><a href="/witizens/<?php echo $message["fromuser"]; ?>" title="<?php echo filter(htmlspecialchars($message["fromuserdisplayname"]), FILTER_WORDS); ?>"><?php echo filter(htmlspecialchars($message["fromuserdisplayname"]), FILTER_WORDS); ?></a></td>
	<td class="witcoins"><?php echo clean_num($message["amount"]); ?></td>
	<td><a href="<?php echo "/inbox/msg/", $message["messageid"]; ?>"><?php echo filter(htmlspecialchars($message["subject"]), FILTER_WORDS); ?></a></td>
	<td><?php echo nicedate($message["date"]); ?></td>
       </tr>
<?php } ?>
      </tbody>
     </table>
    </div>
   </div>
<?php include "_foot.php"; ?>
