<?php
	global $category, $title;
	include "_head.php";
?>
   <div class="main">
    <div class="headerf">
     <h2><?php array_pop($title); echo implode(" - ", $title); ?></h2>
    </div>
    <div class="maincontent">
     <div class="charity">
      <?php if ($charity["website"] != "") { ?><a href="<?php echo $charity["website"]; ?>"><?php } ?>
       <?php if ($charity["logo"] != "") { ?><img alt="<?php echo $charity["charity"]; ?>" src="<?php echo $charity["logo"]; ?>"/><?php } else echo $charity["charity"]; ?>
      <?php if ($charity["website"] != "") { ?></a><?php } ?>
      <div class="charity_stats">
       <div>witcoins donated<br><span class="success"><?php echo clean_num($charity["donated"]); ?></span></div>
       <div>bitcoin address<br><span class="charity_bitcoinaddress"><?php echo $charity["bitcoin_address_charity"]; ?></span></div>
      </div>
     </div>
    </div>
   </div>
<?php include "_foot.php"; ?>