<?php
	global $blocks, $bodyclass, $category, $debug, $head, $tags, $title;
	$title = array_reverse($title);
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
 <head>
<?php foreach ($head["style"] as $name => $style) { ?>
  <link href="<?php echo $style; ?>" media="all" rel="stylesheet" <?php if (!is_numeric($name)) echo "title='",$name,"' "; ?>type="text/css"  />
<?php } ?>
  <link href="/favicon.ico" rel="shortcut icon" type="image/x-icon" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php foreach ($head["script"] as $script) { ?>
  <script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php if (isset($error) || count($blocks) == 0) { ?>
  <style type="text/css">
	.main {
		margin-right: auto;
		width: auto;
	}
  </style>
<?php } ?>
  <title><?php echo strip_tags(implode(" - ", $title)); ?></title>
 </head>
 <body<?php echo (count($bodyclass) > 0 ? " class='".join(" ",$bodyclass)."'" : ""); ?>>
  <?php if (isset($debug)) { ?><div class="debug"><?php while (count($debug["out"]) > 0) { $out = array_shift($debug["out"]); echo "<div>", $out, "</div>"; } ?></div><?php } ?>
  <div id="popup"></div>
  <div id="container">
   <div id="header">
    <div id="top">
     <div id="logo"><a href="<?php echo getdomain(); ?>"><img alt="witcoin" src="<?php echo getdomainlogo(); ?>"/></a></div>
     <div id="menu">
      <div class="menuitem"><a href="<?php echo getdomain(); ?>help" title="help">help</a></div>
      <div class="menuitem"><a href="<?php echo getdomain(); ?>chat" title="chat">chat</a></div>
      <?php if (!loggedin()) { ?><div class="menuitem"><a href="<?php echo getdomain(),"getwitit?dest=",getdest(); ?>" title="get wit it">get wit it</a></div><?php } ?>
      <?php if (loggedin()) { ?><div class="menuitem"><a href="/witout?dest=<?php echo urldecode(getdest()); ?>" title="wit out">log out</a></div><?php } ?>
      <div class="menuitem"><a href="<?php echo getdomain(); ?>categories" title="categories">categories</a></div>
<?php if (loggedin()) { ?>
      <div class="menuitem">
       <a href="<?php echo getdomain(), "witizens/", $_SESSION["userid"]; ?>"><?php echo $_SESSION["displayname"]; ?></a>
<?php $unread = getunreadmessages(); ?>
       <a href="/inbox"><img alt="inbox" class="<?php echo $unread == 0 ? "inbox_ntsh" : "inbox_stsh" ; ?>" src="/img/mail.png" title="<?php echo $unread; ?> unread"/></a>
      </div>
<?php } ?>
      <?php if (loggedin()) { ?><div class="menuitem"><a href="/witup" title="get witcoins">wit up</a></div><?php } ?>
      <?php if (loggedin()) { ?><div class="menuitem"><a href="/transactions">witcoins: <span id="witcoin_balance"><?php echo clean_num(getbalance($_SESSION["userid"])); ?></span></a></div><?php } ?>
     </div>
     <div id="special">
      <a href="<?php echo getdomain(), "charities"; ?>"><img alt="charitable organizations" src="/img/pot.png" title="charitable organizations"/></a>
     </div>
    </div>
<?php if ($category["categoryid"] != 0) { ?>
    <div id="category">
     <h1><a href="<?php echo getdomain($category["categorysd"]); ?>"><?php echo $category["category"]; ?></a></h1>
     <div id="nav">
<?php	if ($category["allowposts"] != "f") { ?>
<?php		if ($path == "/p") { ?>
       <span class="here"><?php echo lang($category["lang"], "verb_post", "present"); ?></span>
<?php		} else { ?>
       <a href="/p" title="<?php echo lang($category["lang"], "verb_post", "present"); ?>"><?php echo lang($category["lang"], "verb_post", "present"); ?></a>
<?php		} ?>
<?php	} ?>
     </div>
    </div>
<?php } ?>
   </div>
   <div id="content">
<?php if (!isset($error) && count($blocks) > 0) { ?>
    <div class="side"><div class="side-inner">
<?php	if (in_array("links", $blocks)) { ?>
     <div class="sideitem">
      <a href="<?php echo ($category["categoryid"] == 0 ? getdomain() : getdomain($category["category"])),"atom",($status != "all" ? "?status=".$status : ""); ?>"><img alt="atom" src="http://witcoin.com/img/atom.png" title="atom"></a>
      <a href="<?php echo ($category["categoryid"] == 0 ? getdomain() : getdomain($category["category"])),"rss",($status != "all" ? "?status=".$status : ""); ?>"><img alt="rss" src="http://witcoin.com/img/rss.png" title="rss"></a>
     </div>
<?php	} ?>
<?php	if (in_array("description", $blocks) && ((isset($_SESSION["userid"]) && $category["renter"] == $_SESSION["userid"]) || $category["description"] != "")) { ?>
     <div class="sideitem">
      <div class="header">
       <h2>description</h2>
       <?php if (isset($_SESSION["userid"]) && $category["renter"] == $_SESSION["userid"]) { ?>&nbsp;<a href="/admin">edit</a><?php } ?>
      </div>
      <?php if (isset($category["description"])) echo filter($category["description"]); ?>
     </div>
<?php	} ?>
<?php	if (in_array("prices", $blocks) && $category["costs"]) { ?>
     <div class="sideitem">
      <div class="header"><h2>prices</h2></div>
      <div class="costs">
<?php		foreach ($category["costs"] as $action => $witcoins) { ?>
       <div><span class="action"><?php echo $action; ?>:</span><span class="cost"><?php echo $witcoins; ?></span></div>
<?php		} ?>
      </div>
     </div>
<?php	} ?>
<?php	if (in_array("category", $blocks)) { ?>
     <div class="sideitem">
      <div class="header"><h2>categories</h2></div>
      <input id="category_filter" type="text"/>
      <div id="category_select">
<?php
			require_once "../protdocs/includes/witcoin_category_select.php";
			foreach ($category_select as $cat) {
				while ($cat["depth"] > $lastdepth) { echo "<div class='indented'>"; $lastdepth++; }
				while ($cat["depth"] < $lastdepth) { echo "</div>"; $lastdepth--; }
				if ($cat["categorysd"] == $category["categorysd"]) echo "<div cat='", $cat["categorysd"], "' class='cat'>", $cat["category"], "</div>";
				else echo "<div cat='", $cat["categorysd"], "' class='cat'><a href='", getdomain($cat["categorysd"]), "'>", $cat["category"], "</a></div>";
			}
			// If last item isn't at 0 depth
			while (0 < $lastdepth) { echo "</div>"; $lastdepth--; }
?>
      </div>
      <script type="text/javascript">catceable();</script>
     </div>
<?php
	}
	if (in_array("tags", $blocks)) {
		require_once "../protdocs/includes/witcoin_tags.php";
?>
     <div class="sideitem">
      <div class="header"><h2>recent tags</h2></div>
      <div class="tags">
<?php		foreach ($tags as $tag) { ?>
       <a class="tag" href="/p/t/<?php echo $tag; ?>"><?php echo $tag; ?></a>
<?php		} ?>
      </div>
     </div>
<?php	} ?>
<?php	if (in_array("profile", $blocks)) include "blocks/user.php"; ?>
    </div></div>
<?php
	}
	array_pop($title);
?>