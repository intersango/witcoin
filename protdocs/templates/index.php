<?php
	global $category, $posts, $sort, $title;
	include "_head.php";
?>
   <div class="main">
    <div class="headerf">
     <h2><?php echo implode(" - ", $title); ?></h2>
     <div class="sort">
      <a <?php if ($sort == "lastactivity") echo 'class="active" '; ?>href="<?php echo $path, "?sort=lastactivity", ($status != "all" ? "&status=$status" : ""); ?>" title="latest activity">last activity</a>
      <a <?php if ($sort == "newest") echo 'class="active" '; ?>href="<?php echo $path, "?sort=newest", ($status != "all" ? "&status=$status" : ""); ?>" title="in reverse chronological order">newest</a>
      <a <?php if ($sort == "oldest") echo 'class="active" '; ?>href="<?php echo $path, "?sort=oldest", ($status != "all" ? "&status=$status" : ""); ?>" title="in chronological order">oldest</a>
      <a <?php if ($sort == "toprank") echo 'class="active" '; ?>href="<?php echo $path, "?sort=toprank", ($status != "all" ? "&status=$status" : ""); ?>" title="in descending order of rank">top rank</a>
      <a <?php if ($sort == "topreplies") echo 'class="active" '; ?>href="<?php echo $path, "?sort=topreplies", ($status != "all" ? "&status=$status" : ""); ?>" title="in descending order of replies">top replies</a>
      <a <?php if ($sort == "topvotes") echo 'class="active" '; ?>href="<?php echo $path, "?sort=topvotes", ($status != "all" ? "&status=$status" : ""); ?>" title="in descending order of votes">top votes</a>
     </div>
    </div>
    <div class="maincontent">
     <div class="listheader">
<?php
	if (isset($_SESSION["userid"])) {
		if ($category["categoryid"] != 0) {
			if ((isset($_SESSION["rank"]) && $_SESSION["rank"] == "moderator") || (!in_array($category["categoryid"], array(2,3,4,5,6,88,198)))) {
?>
      <div class="newcat"><a href="newcat">new subcategory</a></div>
<?php
			}
		} else if (isset($_SESSION["rank"]) && $_SESSION["rank"] == "moderator") {
?>
      <div class="newcat"><a href="newcat">new category</a></div>
<?php
		}
	}
	if (isset($category["status"])) {
?>
      <div class="status_filter">status: <?php echo $status_filter; ?></div>
<?php } ?>
     </div>
<?php if ($category["categoryid"] != 0 && $category["allowposts"] != "f" && count($posts) == 0) { ?>
     <div class="post">be the first to <a href="/p" title="<?php echo $category["lang"]["verb_post"]["present"]; ?>"><?php echo $category["lang"]["verb_post"]["present"]; ?></a></div>
<?php } else include "_listposts.php"; ?>
    </div>
   </div>
<?php include "_foot.php"; ?>