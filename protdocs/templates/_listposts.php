     <div class="posts">
<?php foreach ($posts as $postid => $post) { ?>
      <div class="post-short">
       <div class="stats">
        <div class="replies"><strong><?php echo $post["posts_replies"]; ?></strong>replies</div>
        <div class="views"><strong><?php echo $post["views"]; ?></strong>views</div>
        <div class="votes"><strong><?php echo $post["posts_votes"] > 99 ? "<small>" : "",$post["posts_votes"],$post["posts_votes"] > 99 ? "</small>" : ""; ?></strong>votes</div>
       </div>
<?php	if (isset($post["thumb"])) { ?>
       <div class="thumb">
        <img alt="" src="<?php echo $post["thumb"]; ?>"/>
       </div>
<?php	} ?>
       <div class="summary">
<?php	if ($debug["type"] == "rank") { ?>
        <div class="debug">timestamp: <?php echo strtotime($post["lastactivity"]); ?></div>
        <div class="debug">rank: <?php echo $post["posts_rank"]; ?></div>
<?php	} ?>
        <h3>
<?php	if ($category["categoryid"] == 0 || $category["categoryid"] != $post["category"]) { ?>
         <a href="<?php echo getdomain($post["category_sd"]); ?>" title="<?php echo $post["category_name"]; ?>"><?php echo $post["category_name"]; ?></a>:
<?php	} ?>
         <?php if (isset($post["category_status"]) && isset($post["posts_status"])) { ?><span class="status">(<?php echo $post["posts_status"]; ?>)</span><?php } ?>
         <a href="<?php echo getdomain($post["category_sd"]),"p/",$postid,"/",urlfriendly($post["title"]); ?>" title="<?php echo $post["title"]; ?>">
          <?php echo $post["title"]; ?>
         </a>
        </h3>
<?php /*       <div class="excerpt"><?php echo filter($post["post"]); ?></div>*/ ?>
        <div class="tags">
<?php	foreach (tags_delimited($post["taglist"]) as $tag) { if ($tag != "") {?>
         <a class="tag" href="/p/t/<?php echo $tag; ?>" rel="tag" title="<?php echo $tag; ?>"><?php echo $tag; ?></a>
<?php	} } ?>
        </div>
        <div class="posted">
<?php	if ($post["posted"] == $post["lastactivity"] || $sort == "newest" || $sort == "oldest") { ?>
         <span class="lastactivity" title="<?php echo $post["posted"]; ?>"><?php echo ($post["lastevent"] == "" ? "posted" : $post["lastevent"]), " ", time_duration(strtotime($post["posted"])); ?></span>
<?php	} else { ?>
         <a href="<?php echo getdomain($post["category_sd"]),"p/",$postid,"/",urlfriendly($post["title"]),"#lastactivity"; ?>" title="<?php echo $post["lastactivity"]; ?>">
          <span class="lastactivity"><?php echo ($post["lastevent"] != "" ? $post["lastevent"]." " : ""), time_duration(strtotime($post["lastactivity"])); ?></span>
         </a>
<?php	} ?>
         <span class="lastuser">by <a href="/witizens/<?php echo $post["lastuserid"]; ?>" title="<?php echo $post["displayname"]; ?>"><?php echo $post["displayname"]; ?></a></span>
        </div>
       </div>
      </div>
<?php } ?>
     </div>
     <div class="more">
<?php if (isset($postid) && count($posts) == $limit) { ?>
      <a href="?<?php echo ($sort != "toprank" ? "sort=" . $sort . "&" : "") , "a=", urlencode(rtrim(base64_encode($postid), "=")); ?>" rel="next">next</a>
<?php } ?>
     </div>