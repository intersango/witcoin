<?php
	global $btc, $title, $tx;

	include "_head.php";
?>
   <div class="main">
    <div class="header">
     <h2><?php echo implode(" - ", $title); ?></h2>
    </div>
    <div class="maincontent">
     <div class="transactions">
<?php if (!$btc) { ?>
      <div class="error">bitcoin daemon is offline</div>
<?php } if (count($txs) == 0) { ?>
      no transactions
<?php } else { ?>
<?php	if (isset($rcvd1) && $rcvd1 > 0) { ?>
      <div class="success">your bitcoins have been converted into witcoins<br>you now have <?php echo clean_num($witcoins); ?> witcoins</div>
<?php	} else if (isset($rcvd0) && $rcvd0 > 0) { ?>
      <div class="success">detected <?php echo clean_num($rcvd0); ?> bitcoins<br>please wait at least one confirmation before reloading this page<br>after one confirmation your bitcoins will be converted into witcoins</div>
<?php	} ?>
      <table cellpadding="0" cellspacing="0">
       <thead><tr><th>date</th><th>type</th><th>witcoins</th><th>confirmations</th><th>bitcoin address</th></tr>
       <tbody>
<?php	foreach ($txs as $tx) { ?>
        <tr>
	 <td><?php echo $tx["txed"]; ?></td>
	 <td><?php echo $tx["type"]; ?></td>
	 <td><?php echo clean_num($tx["witcoins"]); ?></td>
	 <td><?php echo $tx["confirmations"] == 0 ? 0 : "1+"; ?></td>
	 <td><?php echo $tx["type"] == "deposit" ? $tx["bitcoin_address_witcoin"] : $tx["bitcoin_address_user"]; ?></td>
	</tr>
<?php	} ?>
       </tbody>
      </table>
<?php } ?>
     </div>
    </div>
   </div>
<?php include "_foot.php"; ?>