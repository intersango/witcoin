<?php
	global $err, $path, $reply, $title;
	include "_head.php";
?>
   <div class="main">
    <div class="header">
     <h2>
      <?php if (isset($reply["category_status"])) { ?><span class="status">(<?php echo $reply["post_status"]; ?>)</span><?php } ?>
      <a href="<?php echo getdomain($reply["category_sd"]),"p/",$reply["reply_postid"],"/",urlfriendly($reply["post_title"]); ?>" title="<?php echo $reply["post_title"]; ?>">
       <?php echo $reply["post_title"]; ?>
      </a>
     </h2>
    </div>
    <div class="maincontent">
     <div class="replyrev">
<?php
	$lastrev = $reply; $diff = new Diff(); $rev = count($reply["replyrevs"]);
	foreach ($reply["replyrevs"] as $replyrev) {
?>
      <div class="replyrev-content">
       <div class="revision-number"><?php echo $rev--; ?></div>
       <div class="revision"><?php echo filter($diff->renderDiff($diff->stringDiff($replyrev["reply"], $lastrev["reply"]))); ?></div>
       <div class="reply-info">
        <div class="reply-time">
	 <span class="time" title="<?php echo $replyrev["replied"]; ?>"><?php echo time_duration(strtotime($replyrev["replied"])); ?></span>
	</div>
<?php if ($reply["gravatar"] != "") { ?>
	<div class="gravatar">
	 <a href="/witizens/<?php echo $reply["reply_userid"]; ?>" title="gravatar"><img alt="<?php echo $reply["displayname"]; ?>" class="gravatar" src="<?php echo $reply["gravatar"]; ?>"/></a>
	</div>
<?php } ?>
	<div class="userinfo"><a href="/witizens/<?php echo $reply["reply_userid"]; ?>" title="<?php echo $reply["displayname"]; ?>"><?php echo $reply["displayname"]; ?></a></div>
       </div>
      </div>
<?php
		$lastrev = $replyrev;
	}
?>
     </div>
    </div>
   </div>
<?php include "_foot.php"; ?>