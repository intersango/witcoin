<?php
	global $category, $err, $path, $reply;
	include "_head.php";
?>
   <div class="main">
    <div class="header">
     <h2><?php echo implode(" - ", $title); ?></h2>
    </div>
    <div class="maincontent">
<?php if (isset($_SESSION["userid"])) { ?>
<?php	if ($witizen["witcoins"] < $category["costs"]["reply"]) { ?>
     <div class="error lh29">
      you do not have enough witcoins to reply<br>
     <a href="/witup" title="get witcoins">wit up</a> to get witcoins</a>
     </div>
<?php	} else { ?>
     <div class="reply">
      <div class="reply-header">
       <div class="reply-votes">
        <a class="vote-up<?php if (isset($reply["reply_voted"]) && $reply["reply_voted"] > 0) echo " voted"; ?>" id="vote_up_reply_<?php echo $reply["replyid"]; ?>" title="vote up">vote up</a>
        <span class="vote-count"><?php echo $reply["reply_votes"]; ?></span>
       </div>
       <span class="reply-links">
<?php		if (isset($_SESSION["userid"]) && $reply["reply_userid"] == $_SESSION["userid"]) { ?>
        <a href="<?php echo getdomain($reply["category_sd"]),"r/",$reply["replyid"],"/edit"; ?>" title="edit">edit</a>
<?php		} ?>
        <a href="#reply" title="reply">reply</a>
       </span>
       <div class="reply-info">
        <div class="reply-time"><span class="time" title="<?php echo $reply["replied"]; ?>"><?php echo time_duration(strtotime($reply["replied"])); ?></span></div>
	<div class="userinfo"><a href="<?php echo getdomain() ,"witizens/", $reply["reply_userid"]; ?>" title="<?php echo $reply["displayname"]; ?>"><?php echo $reply["displayname"]; ?></a></div>
       </div>
<?php		if ($reply["gravatar"] != "") { ?>
       <span class="gravatar"><a href="<?php echo getdomain(), "witizens/", $reply["reply_userid"]; ?>"><img alt="<?php echo $reply["displayname"]; ?>" class="gravatar" src="<?php echo $reply["gravatar"]; ?>"/></a></span>
<?php		} ?>
<?php		if ($reply["reply_revs"] > 0) { ?>
        <div class="revinfo">
        <a href="<?php echo getdomain($reply["category_sd"]),"r/",$reply["replyid"],"/rev",$reply["reply_revs"] == 1 ? "" : "s"; ?>" title="revisions">
         <?php echo $reply["reply_revs"]," rev",$reply["reply_revs"] == 1 ? "" : "s"; ?>
        </a>
       </div>
<?php		} ?>
      </div> 
      <div class="reply-content">
       <div class="reply">
        <?php echo $reply["reply"]; ?>
       </div>
      </div>
     </div>
     <a name="reply"></a>
     <div class="submitreply">
<?php		if ($err) { foreach ($errmsg as $msg) echo "<div class='error'>$msg</div>"; } ?>
      <form action="<?php echo $path; ?>" method="post">
       <h3>your <?php echo $category["lang"]["noun_reply"]["singular"]; ?></h3>
       <div><textarea class="resizable showdown" cols="1" id="wmd-input" name="reply" rows="1"><?php echo $preply["reply"]; ?></textarea></div>
       <div><label><input name="upvote" type="checkbox"<?php if ($preply["upvote"] == "t") echo " checked"; ?>/> upvote</label></div>
       <div><input type="submit" value="<?php echo $category["lang"]["verb_reply"]["present"]; ?>"/></div>
      </form>
     </div>
<?php	} ?>
<?php } else { ?>
     <div class="center">you must <a href="<?php echo getdomain(),"getwitit?dest=",getdest(); ?>" title="get wit it">get wit it</a> to <?php echo $category["lang"]["noun_reply"]["singular"]; ?></div>
<?php } ?>
    </div>
   </div>
<?php include "_foot.php"; ?>