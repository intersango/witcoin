<?php
	global $title;
	include "_head.php";
?>
   <div class="main">
    <div class="header">
     <h2><?php echo implode(" - ", $title); ?></h2>
    </div>
    <div class="maincontent">
     # of upvotes: <input class="calc" id="upvotes" type="text" value="100"/>
     cost to post/reply: <input class="calc" id="cost_pr" type="text" value="0.01"/>
     cost to upvote: <input class="calc" id="cost_uv" type="text" value="0.01"/>
     <table id="calc">
      <thead>
       <tr>
        <th class="event">event</th>
	<th class="balance">balance</th>
       </tr>
      </thead>
      <tbody>
      </tbody>
     </table>
    </div>
   </div>
<?php include "_foot.php"; ?>