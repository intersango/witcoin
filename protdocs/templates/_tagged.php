<?php
	global $posts, $sort, $title;
	include "_head.php";
?>
   <div class="main">
    <div class="headerf">
     <h2>tagged posts</h2>
     <div class="sort">
      <a <?php if ($sort == "lastactivity") echo 'class="active" '; ?>href="<?php echo $path, "?sort=lastactivity", ($status != "all" ? "&status=$status" : ""); ?>" title="latest activity">last activity</a>
      <a <?php if ($sort == "newest") echo 'class="active" '; ?>href="<?php echo $path, "?sort=newest", ($status != "all" ? "&status=$status" : ""); ?>" title="in reverse chronological order">newest</a>
      <a <?php if ($sort == "oldest") echo 'class="active" '; ?>href="<?php echo $path, "?sort=oldest", ($status != "all" ? "&status=$status" : ""); ?>" title="in chronological order">oldest</a>
      <a <?php if ($sort == "toprank") echo 'class="active" '; ?>href="<?php echo $path, "?sort=toprank", ($status != "all" ? "&status=$status" : ""); ?>" title="in descending order of rank">top rank</a>
      <a <?php if ($sort == "topreplies") echo 'class="active" '; ?>href="<?php echo $path, "?sort=topreplies", ($status != "all" ? "&status=$status" : ""); ?>" title="in descending order of replies">top replies</a>
      <a <?php if ($sort == "topvotes") echo 'class="active" '; ?>href="<?php echo $path, "?sort=topvotes", ($status != "all" ? "&status=$status" : ""); ?>" title="in descending order of votes">top votes</a>
     </div>
    </div>
    <div class="maincontent">
<?php if (isset($category["status"])) { ?>
     <div class="listheader"><div class="status_filter">status: <?php echo $status_filter; ?></div></div>
<?php } ?>
<?php include "_listposts.php"; ?>
    </div>
   </div>
<?php include "_foot.php"; ?>
