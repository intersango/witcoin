<?php
	global $title;

	include "_head.php";
?>
    <div class="main">
     <div class="header">
      <h2><?php echo implode(" - ", $title); ?></h2>
     </div>
     <div class="maincontent"><div class="help">
      <div class="center"><?php foreach ($languages as $en => $language) { ?><a class="language" href="<?php echo getdomain(), "help?lang=", $en; ?>" title="<?php echo $en; ?>"><?php echo $language; ?></a><?php } ?></div>

      <br>

      <h3><?php echo $help["q:what is witcoin?"][$lang]; ?></h3>
      <p><?php echo $help["a:what is witcoin?"][$lang]; ?></p>

      <h3><?php echo $help["q:how can i profit?"][$lang]; ?></h3>
      <p><?php echo $help["a1:how can i profit?"][$lang]; ?>
       <ul>
        <li><?php echo $help["a2:how can i profit?"][$lang]; ?></li>
        <li><?php echo $help["a3:how can i profit?"][$lang]; ?></li>
        <li><?php echo $help["a4:how can i profit?"][$lang]; ?></li>
        <li><?php echo $help["a5:how can i profit?"][$lang]; ?></li>
        <li><?php echo $help["a6:how can i profit?"][$lang]; ?></li>
        <li><?php echo $help["a7:how can i profit?"][$lang]; ?></li>
        <li><?php echo $help["a8:how can i profit?"][$lang]; ?></li>
        <li><?php echo $help["a9:how can i profit?"][$lang]; ?></li>
        <li><?php echo $help["a10:how can i profit?"][$lang]; ?></li>
       </ul>
      </p>

      <h3><?php echo $help["q:how do i log in?"][$lang]; ?></h3>
      <p><?php echo $help["a:how do i log in?"][$lang]; ?></p>

      <h3><?php echo $help["q:how do i get bitcoins?"][$lang]; ?></h3>
      <p><?php echo $help["a:how do i get bitcoins?"][$lang]; ?></p>

      <h3><?php echo $help["q:how do i get witcoins?"][$lang]; ?></h3>
      <p><?php echo $help["a:how do i get witcoins?"][$lang]; ?></p>

      <h3><?php echo $help["q:how do i post?"][$lang]; ?></h3>
      <p><?php echo $help["a1:how do i post?"][$lang]; ?></p>
      <p><?php echo $help["a2:how do i post?"][$lang]; ?></p>
      <p><?php echo $help["a3:how do i post?"][$lang]; ?></p>
      <p><?php echo $help["a4:how do i post?"][$lang]; ?></p>

      <h3><?php echo $help["q:how do i reply to a post?"][$lang]; ?></h3>
      <p><?php echo $help["a1:how do i reply to a post?"][$lang]; ?></p>
      <p><?php echo $help["a2:how do i reply to a post?"][$lang]; ?></p>
      <p><?php echo $help["a3:how do i reply to a post?"][$lang]; ?></p>

      <h3><?php echo $help["q:how do i reply to a reply?"][$lang]; ?></h3>
      <p><?php echo $help["a1:how do i reply to a reply?"][$lang]; ?></p>
      <p><?php echo $help["a2:how do i reply to a reply?"][$lang]; ?></p>
      <p><?php echo $help["a3:how do i reply to a reply?"][$lang]; ?></p>

      <h3><?php echo $help["q:how do i edit my post or reply?"][$lang]; ?></h3>
      <p><?php echo $help["a1:how do i edit my post or reply?"][$lang]; ?></p>
      <p><?php echo $help["a2:how do i edit my post or reply?"][$lang]; ?></p>
      <p><?php echo $help["a3:how do i edit my post or reply?"][$lang]; ?></p>

      <h3><?php echo $help["q:how do i upvote?"][$lang]; ?></h3>
      <p><?php echo $help["a1:how do i upvote?"][$lang]; ?></p>
      <p><?php echo $help["a2:how do i upvote?"][$lang]; ?></p>
      <p><?php echo $help["a3:how do i upvote?"][$lang]; ?></p>

      <h3><?php echo $help["q:how do i downvote?"][$lang]; ?></h3>
      <p><?php echo $help["a:how do i downvote?"][$lang]; ?></p>

      <h3><?php echo $help["q:how do i create/rent a category?"][$lang]; ?></h3>
      <p><?php echo $help["a1:how do i create/rent a category?"][$lang]; ?></p>
      <p><?php echo $help["a2:how do i create/rent a category?"][$lang]; ?></p>
      <p><?php echo $help["a3:how do i create/rent a category?"][$lang]; ?></p>

      <h3><?php echo $help["q:how do i donate to or pay another user?"][$lang]; ?></h3>
      <p><?php echo $help["a1:how do i donate to or pay another user?"][$lang]; ?></p>
      <p><?php echo $help["a2:how do i donate to or pay another user?"][$lang]; ?></p>

      <h3><?php echo $help["q:how do i [de]select charities to include in distribution of profits?"][$lang]; ?></h3>
      <p><?php echo $help["a:how do i [de]select charities to include in distribution of profits?"][$lang]; ?></p>

      <h3><?php echo $help["q:how does donating to charities work?"][$lang]; ?></h3>
      <p><?php echo $help["a1:how does donating to charities work?"][$lang]; ?></p>
      <p><?php echo $help["a2:how does donating to charities work?"][$lang]; ?></p>

      <h3><?php echo $help["q:how do i convert my witcoins back into bitcoins?"][$lang]; ?></h3>
      <p><?php echo $help["a:how do i convert my witcoins back into bitcoins?"][$lang]; ?></p>

      <h3><?php echo $help["q:markdown"][$lang]; ?></h3>
      <p><?php echo $help["a1:markdown"][$lang]; ?></p>
      <p><?php echo $help["a2:markdown"][$lang]; ?></p>

      <h3><?php echo $help["q:support and reporting bugs"][$lang]; ?></h3>
      <p><?php echo $help["a1:support and reporting bugs"][$lang]; ?></p>
      <p><?php echo $help["a2:support and reporting bugs"][$lang]; ?></p>
      <p><?php echo $help["a3:support and reporting bugs"][$lang]; ?></p>
     </div></div>
    </div>
<?php include "_foot.php"; ?>