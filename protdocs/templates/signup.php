<?php
	global $bitcoin_address_witcoin, $rcvd0, $rcvd1, $title;

	include "_head.php";
?>
   <div class="main">
    <div class="header">
     <h2><?php echo implode(" - ", $title); ?></h2>
    </div>
    <div class="maincontent">
<?php if ($rcvd0 == 0) { ?>
      send any amount of bitcoins to<br>
      <span class="bitcoin_address"><?php echo $bitcoin_address_witcoin; ?></span><br>
      this is a single use bitcoin address<br>
      1 bitcoin == 1 witcoin, always<br>
      <small>wait for one confirmation, then refresh. your bitcoin payment will be credited to your witcoin account.</small>
<?php } else if ($rcvd1 == 0) { ?>
      detected <?php echo clean_num($rcvd0); ?> bitcoins<br>
      please wait for at least one confirmation before reloading this page<br>
      you may sign up after receiving one confirmation.<br><br>
      chat while you wait:<br>
      <iframe src="http://irc.witcoin.com:9090?<?php echo isset($_SESSION["userid"]) ? "nick=witizen_".$_SESSION["userid"] : "randomnick=1"; ?>&channels=witcoin&uio=d4" width="100%" height="400"></iframe>
<?php
	} else {
		if ($err && isset($errmsg)) foreach ($errmsg as $msg) echo "<div class='error'>$msg</div>";
?>
     <form action="/signup<?php if ($query) echo "?",implode("&", $query); ?>" method="post"><table><tbody>
      <tr>
       <td class="field_label<?php echo isset($err["username"]) ? " error" : ""; ?>">username:</td>
       <td class="field_input"><input class="logreg" maxlength="43" name="username" type="text" value="<?php echo isset($_POST["username"]) ? $_POST["username"] : ""; ?>" /></td>
      </tr>
<?php if (!isset($_GET["get"])) { ?>
      <tr>
       <td class="field_label<?php echo isset($err["display_name"]) ? " error" : ""; ?>">display name:</td>
       <td class="field_input"><input class="logreg" maxlength="43" name="display_name" type="text" value="<?php echo isset($_POST["display_name"]) ? $_POST["display_name"] : ""; ?>" /></td>
      </tr>
      <tr>
       <td class="field_label<?php echo isset($err["email_address"]) ? " error" : ""; ?>">email address:</td>
       <td class="field_input"><input class="logreg" name="email_address" type="text" value="<?php echo isset($_POST["email_address"]) ? $_POST["email_address"] : ""; ?>" /></td>
      </tr>
      <tr>
       <td class="field_label">&nbsp;</td>
       <td class="field_input">important email messages will be sent to you using this email address.</td>
      </tr>
<?php } ?>
      <tr>
       <td class="field_label<?php echo isset($err["password"]) ? " error" : ""; ?>">password:</td>
       <td class="field_input"><input class="logreg" name="password" type="password"/></td>
      </tr>
      <tr>
       <td class="field_label<?php echo isset($err["password_confirm"]) ? " error" : ""; ?>">confirm:</td>
       <td class="field_input"><input class="logreg" name="password_confirm" type="password"/></td>
      </tr>
      <tr>
       <td class="field_label">&nbsp;</td>
       <td class="field_input">password must be minimum of 12 characters</td>
      </tr>
      <tr>
       <td class="field_label">&nbsp;</td>
       <td class="field_input"><input class="logreg" type="submit" value="<?php echo isset($_GET["get"]) ? "create account" : "sign up"; ?>"/></td>
      </tr>
     </tbody></table></form>
<?php } ?>
    </div>
   </div>
<?php include "_foot.php"; ?>