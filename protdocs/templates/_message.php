<?php
	global $title, $witizen;

	include "_head.php";
?>
   <div class="main">
    <div class="header">
     <h2><?php echo implode(" - ", $title); ?></h2>
    </div>
    <div class="maincontent">
     <h3><?php echo filter(htmlspecialchars($message["subject"]), FILTER_WORDS); ?></h3>
     <h4><?php echo nicedate($message["date"]); ?></h4>
     <div><?php echo $raw ? htmlspecialchars(filter($message["message"])) : filter($message["message"]); ?></div>
    </div>
   </div>
<?php include "_foot.php"; ?>
