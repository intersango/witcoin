<?php
	global $category, $title;
	include "_head.php";
?>
   <div class="main">
    <div class="headerf">
     <h2><?php echo implode(" - ", $title); ?></h2>
    </div>
    <div class="maincontent">
<?php foreach ($errmsg as $msg) echo "<div class='error'>$msg</div>"; ?>
     <form action="/newcharity" method="post">
      <h3<?php echo isset($err["name"]) ? " class='error'" : ""; ?>>organization</h3>
      <div><input maxlength="64" name="name" type="text" value="<?php echo $newcharity["name"]; ?>"/></div>
      <h3<?php echo isset($err["website"]) ? " class='error'" : ""; ?>>website</h3>
      <div><input maxlength="128" name="website" type="text" value="<?php echo $newcharity["website"]; ?>"/></div>
      <h3<?php echo isset($err["logo"]) ? " class='error'" : ""; ?>>logo url</h3>
      <div><input maxlength="128" name="logo" type="text" value="<?php echo $newcharity["logo"]; ?>"/></div>
      <h3<?php echo isset($err["email"]) ? " class='error'" : ""; ?>>email address</h3>
      <div><input maxlength="128" name="email" type="text" value="<?php echo $newcharity["email"]; ?>"/></div>
      <h3<?php echo isset($err["bitcoint_address_charity"]) ? " class='error'" : ""; ?>>bitcoin address</h3>
      <div><input maxlength="43" name="bitcoin_address" type="text" value="<?php echo $newcharity["bitcoin_address_charity"]; ?>"/></div>
      <div><input id="submit" type="submit" value="add"/></div>
     </form>
<?php if ($_SESSION["rank"] != "moderator") { ?>
     <small>* <?php echo $costs["new charity"]; ?> witcoins required to add.  come <a href="/chat">chat</a> with us if you would like a refund.</small>
<?php } ?>
    </div>
   </div>
<?php include "_foot.php"; ?>