<?php
	global $err, $errmsg, $title;
	include "_head.php";
?>
   <div class="main">
    <div class="header">
     <h2><?php echo implode(" - ", $title); ?></h2>
    </div>
    <div class="maincontent">
     <div class="error"></div>
     <div class="auth_bitcoin"><h3>bitcoin</h3></div>
     <blockquote>
      <p>login</p>
      <blockquote>
<?php foreach ($errmsg as $msg) echo "<div class='error'>$msg</div>"; ?>
       <form action="/getwitit<?php if ($query) echo "?",implode("&", $query); ?>" method="post"><table><tbody>
        <tr>
         <td class="field_label<?php echo isset($err["username"]) ? " error" : ""; ?>">username:</td>
         <td class="field_input"><input class="logreg" maxlength="43" name="username" type="text" value="<?php echo isset($_POST["username"]) ? $_POST["username"] : ""; ?>" /></td>
        </tr>
        <tr>
         <td class="field_label<?php echo isset($err["password"]) ? " error" : ""; ?>">password:</td>
         <td class="field_input"><input class="logreg" name="password" type="password"/></td>
        </tr>
        <tr>
         <td class="field_label">&nbsp;</td>
         <td class="field_input"><input class="logreg" type="submit" value="log in"/></td>
        </tr>
       </tbody></table></form>
      </blockquote>
      <p><a href="/signup<?php if ($query) echo "?",implode("&", $query); ?>" title="sign up">sign up</a></p>
     </blockquote>
     <div class="auth_openid"><h3>openid</h3></div>
     <blockquote>
      <form action="/authopenid<?php if ($query) echo "?",implode("&", $query); ?>" id="openid_form" method="post">
       <input type="hidden" name="action" value="verify" />
       <div id="openid_choice"><p>do you already have an account on one of these sites? click the logo to <b>log in</b> with it here:</p><div id="openid_btns"></div></div>
       <div id="openid_input_area"></div>
       <div><noscript><p>openid is a service that allows you to log-on to many different websites using a single indentity.  find out <a href="http://openid.net/what/" title="more about openid">more about openid</a> and <a href="http://openid.net/get/" title="get openid">how to get an openid enabled account</a>.</p></noscript></div>
       <p>if you don't already have an account on any of the above</p>
       <p><a href="https://www.myopenid.com/signup?affiliate_id=65139" title="sign up">click here to sign up</a></p>
       <p>if you've forgotten or lost your login information</p>
       <p><a href="/recover" title="recover">click here to recover your account</a></p>
       <p>or, you can manually enter your openid</p>
       <div>
        <input class="openid-identifier" id="openid_identifier" name="openid_identifier" type="text"/>
        <input id="openid_submit" type="submit" value="Log in"/>
       </div>
      </form>
     </blockquote>
    </div>
   </div>
<?php include "_foot.php"; ?>
