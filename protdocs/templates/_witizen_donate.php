<?php
	global $err, $title, $witizen, $witizenfrom;

	include "_head.php";
	$title = array_reverse($title);
?>
   <div class="main">
    <div class="header">
     <h2><?php echo implode(" - ", $title); ?></h2>
    </div>
    <div class="maincontent">
<?php
	if (!isset($_SESSION["userid"])) {
?>
     <div class="center">you must <a href="<?php echo getdomain(),"getwitit?dest=",getdest(); ?>" title="get wit it">get wit it</a> to donate</div>
<?php
	} else {
		if ($userid == $_SESSION["userid"]) {
?>
     <div>send <a href="/witizens/donate/<?php echo $userid; ?>">this link</a> to others so they may donate witcoins to you.</div>
<?php
		} else {
			if (isset($_POST["witcoins"]) && !$err) {
?>
     <div class="success">
      successfully donated <?php echo $_POST["witcoins"]; ?> witcoins to
      <a href="/witizens/<?php echo $userid; ?>" title="<?php echo $witizen["displayname"]; ?>"><?php echo $witizen["displayname"]; ?></a>
     </div>
<?php
				 unset($_POST["message"]); unset($_POST["subject"]); unset($_POST["witcoins"]);
			}
?>
<?php if ($err && isset($errmsg)) foreach ($errmsg as $msg) echo "<div class='error'>$msg</div>"; ?>
    <form action="<?php echo $path; ?>" method="post">
     <h3<?php echo isset($err["witcoins"]) ? " class='error'" : ""; ?>>witcoins</h3>
     <div><input name="witcoins" type="text" value="<?php echo isset($_POST["witcoins"]) ? $_POST["witcoins"] : 0; ?>"/></div>
     <h3<?php echo isset($err["subject"]) ? " class='error'" : ""; ?>>subject</h3>
     <div><input maxlength="64" name="subject" type="text" value="<?php echo isset($_POST["subject"]) ? $_POST["subject"] : ""; ?>"/></div>
     <h3<?php echo isset($err["message"]) ? " class='error'" : ""; ?>>message</h3>
     <div><textarea class="resizable showdown" cols="1" name="message" rows="1"><?php if (isset($_POST["message"])) echo $_POST["message"]; ?></textarea></div>
     <div><input id="submit-button" type="submit" value="donate"/><a href="/witizens/<?php echo $_SESSION["userid"]; ?>" title="cancel">cancel</a></div>
    </form>
<?php
		}
	}
?>
    </div>
   </div>
<?php include "_foot.php"; ?>