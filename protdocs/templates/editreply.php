<?php
	global $category, $costs, $err, $errmsg, $path, $reply, $title, $witizen;

	include "_head.php";

	preg_match("/^\/r\/([0-9]*)\/edit?$/", $path, $matches);
?>
   <div class="main">
    <div class="header">
     <h2><?php echo implode(" - ", $title); ?></h2>
    </div>
    <div class="maincontent">
<?php if (isset($_SESSION["userid"])) { ?>
<?php	if ($witizen["witcoins"] < $costs["edit"]) { ?>
     <div class="error lh29">
      you do not have enough witcoins to edit your reply<br>
     <a href="/witup" title="get witcoins">wit up</a> to get witcoins</a>
     </div>
<?php	} else { ?>
<?php
			foreach ($errmsg as $msg) echo "<div class='error'>$msg</div>";
?>
     <form action="<?php echo getdomain($category["categorysd"]),"r/",$matches[1],"/edit"; ?>" method="post">
      <h3<?php echo isset($err["reply"]) ? " class='error'" : ""; ?>><?php echo $category["lang"]["noun_reply"]["singular"]; ?></h3>
      <div><textarea class="resizable showdown" cols="1" id="wmd-input" name="reply" rows="1"><?php echo $reply["reply"]; ?></textarea></div>
      <div><input id="submit" style="width: auto;" type="submit" value="edit <?php echo $category["lang"]["noun_reply"]["singular"]; ?>"/></div>
     </form>
<?php	} ?>
<?php } else { ?>
     <div class="center">you must <a href="<?php echo getdomain(),"getwitit?dest=",getdest(); ?>" title="get wit it">get wit it</a> to edit <?php echo $category["lang"]["noun_reply"]["singular"]; ?></div>
<?php } ?>
    </div>
   </div>
<?php include "_foot.php"; ?>