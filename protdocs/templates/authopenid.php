<?php
	global $errmsg, $title;

	include "_head.php";
?>
   <div class="main">
    <div class="header">
     <h2><?php echo implode(" - ", $title); ?></h2>
    </div>
    <div class="maincontent">
     <div class="error"><?php echo $errmsg; ?></div>
     <form action="authopenid<?php if ($query) echo "?",implode("&", $query); ?>" id="openid_form" method="post">
      <input type="hidden" name="action" value="verify" />
      <div id="openid_choice"><p>do you already have an account on one of these sites? click the logo to <b>log in</b> with it here:</p><div id="openid_btns"></div></div>
      <div id="openid_input_area"></div>
      <div><noscript><p>openid is a service that allows you to log-on to many different websites using a single indentity.  find out <a href="http://openid.net/what/" title="more about openid">more about openid</a> and <a href="http://openid.net/get/" title="how to get openid">how to get an openid enabled account</a>.</p></noscript></div>
      <p>if you don't already have an account on any of the above</p>
      <p><a href="https://www.myopenid.com/signup?affiliate_id=65139" title="sign up">click here to sign up</a></p>
      <p>if you've forgotten or lost your login information</p>
      <p><a href="/recover" title="recover">click here to recover your account</a></p>
      <p>or, you can manually enter your openid</p>
      <div>
       <input class="openid-identifier" id="openid_identifier" name="openid_identifier" type="text"/>
       <input id="openid_submit" type="submit" value="Log in"/>
      </div>
     </form>
    </div>
   </div>
<?php include "_foot.php"; ?>
