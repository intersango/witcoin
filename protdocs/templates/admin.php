<?php
	global $category, $err, $errmsg, $path, $title;

	include "_head.php";
?>
   <div class="main">
    <div class="header">
     <h2><?php echo implode(" - ", $title); ?></h2>
    </div>
    <div class="maincontent">
<?php foreach ($errmsg as $msg) echo "<div class='error'>$msg</div>"; ?>
<?php foreach ($successmsg as $msg) echo "<div class='success'>$msg</div>"; ?>
     <form action="/admin" method="post">
<?php /*      <h3>costs</h3>
      <table class="costs"><tbody>
       <tr>
        <td class="field_label<?php echo isset($err["cost_post"]) ? " error" : ""; ?>">post</td>
        <td class="field_input"><input class="lang" name="cost_post" type="text" value="<?php echo $admincat["cost_post"]; ?>"/></td>
       </tr>
       <tr>
        <td class="field_label<?php echo isset($err["cost_reply"]) ? " error" : ""; ?>">reply</td>
        <td class="field_input"><input class="lang" name="cost_reply" type="text" value="<?php echo $admincat["cost_reply"]; ?>"/></td>
       </tr>
       <tr>
        <td class="field_label<?php echo isset($err["cost_edit"]) ? " error" : ""; ?>">edit</td>
        <td class="field_input"><input class="lang" name="cost_edit" type="text" value="<?php echo $admincat["cost_edit"]; ?>"/></td>
       </tr>
       <tr>
        <td class="field_label<?php echo isset($err["cost_upvote"]) ? " error" : ""; ?>">upvote</td>
        <td class="field_input"><input class="lang" name="cost_upvote" type="text" value="<?php echo $admincat["cost_upvote"]; ?>"/></td>
       </tr>
      </tbody></table>
*/ ?>
      <h3<?php echo isset($err["description"]) ? " class='error'" : ""; ?>>description</h3>
      <div><textarea class="resizable showdown" cols="1" id="description" name="description" rows="1"><?php echo $admincat["description"]; ?></textarea></div>

      <h3<?php echo isset($err["faq"]) ? " class='error'" : ""; ?>>faq</h3>
      <div><textarea class="resizable showdown" cols="1" id="faq" name="faq" rows="1"><?php echo $admincat["faq"]; ?></textarea></div>

      <h3>language</h3>
      <table class="lang"><tbody>
       <tr>
        <td class="field_label" colspan="2"><h4>adjectives</h4></td>
       </tr>
       <tr>
        <td class="field_label<?php echo isset($err["lang_adj_newest"]) ? " error" : ""; ?>">newest</td>
        <td class="field_input"><input class="lang" name="adj_newest" type="text" value="<?php echo $admincat["lang"]["adj_newest"]; ?>"/></td>
       </tr>
       <tr>
        <td class="field_label<?php echo isset($err["lang_adj_oldest"]) ? " error" : ""; ?>">oldest</td>
        <td class="field_input"><input class="lang" name="adj_oldest" type="text" value="<?php echo $admincat["lang"]["adj_oldest"]; ?>"/></td>
       </tr>
       <tr>
        <td class="field_label" colspan="2"><h4>nouns</h4></td>
       </tr>
       <tr>
        <td class="field_label<?php echo isset($err["lang_noun_post"]) ? " error" : ""; ?>">post</td>
        <td class="field_input">
	 singular: <input class="lang" name="noun_post_singular" type="text" value="<?php echo $admincat["lang"]["noun_post"]["singular"]; ?>"/>
	 plural: <input class="lang" name="noun_post_plural" type="text" value="<?php echo $admincat["lang"]["noun_post"]["plural"]; ?>"/>
	</td>
       </tr>
       <tr>
        <td class="field_label<?php echo isset($err["lang_noun_recenttags"]) ? " error" : ""; ?>">recent tags</td>
        <td class="field_input"><input class="lang" name="noun_recenttags" type="text" value="<?php echo $admincat["lang"]["noun_recenttags"]; ?>"/></td>
       </tr>
       <tr>
        <td class="field_label<?php echo isset($err["lang_noun_reply"]) ? " error" : ""; ?>">reply</td>
        <td class="field_input">
	 singular: <input class="lang" name="noun_reply_singular" type="text" value="<?php echo $admincat["lang"]["noun_reply"]["singular"]; ?>"/>
	 plural: <input class="lang" name="noun_reply_plural" type="text" value="<?php echo $admincat["lang"]["noun_reply"]["plural"]; ?>"/>
	</td>
       </tr>
       <tr>
        <td class="field_label<?php echo isset($err["lang_noun_vote"]) ? " error" : ""; ?>">vote</td>
        <td class="field_input">
	 singular: <input class="lang" name="noun_vote_singular" type="text" value="<?php echo $admincat["lang"]["noun_vote"]["singular"]; ?>"/>
	 plural: <input class="lang" name="noun_vote_plural" type="text" value="<?php echo $admincat["lang"]["noun_vote"]["plural"]; ?>"/>
	</td>
       </tr>
       <tr>
        <td class="field_label" colspan="2"><h4>verbs</h4></td>
       </tr>
       <tr>
        <td class="field_label<?php echo isset($err["lang_verb_post"]) ? " error" : ""; ?>">post</td>
        <td class="field_input">
	 past: <input class="lang" name="verb_post_past" type="text" value="<?php echo $admincat["lang"]["verb_post"]["past"]; ?>"/>
	 present: <input class="lang" name="verb_post_present" type="text" value="<?php echo $admincat["lang"]["verb_post"]["present"]; ?>"/>
	 participle: <input class="lang" name="verb_post_participle" type="text" value="<?php echo $admincat["lang"]["verb_post"]["participle"]; ?>"/>
	</td>
       </tr>
       <tr>
        <td class="field_label<?php echo isset($err["lang_verb_reply"]) ? " error" : ""; ?>">reply</td>
        <td class="field_input">
	 past: <input class="lang" name="verb_reply_past" type="text" value="<?php echo $admincat["lang"]["verb_reply"]["past"]; ?>"/>
	 present: <input class="lang" name="verb_reply_present" type="text" value="<?php echo $admincat["lang"]["verb_reply"]["present"]; ?>"/>
	 participle: <input class="lang" name="verb_reply_participle" type="text" value="<?php echo $admincat["lang"]["verb_reply"]["participle"]; ?>"/>
	</td>
       </tr>
       <tr>
        <td class="field_label<?php echo isset($err["lang_verb_tag"]) ? " error" : ""; ?>">tag</td>
        <td class="field_input">
	 past: <input class="lang" name="verb_tag_past" type="text" value="<?php echo $admincat["lang"]["verb_tag"]["past"]; ?>"/>
	 present: <input class="lang" name="verb_tag_present" type="text" value="<?php echo $admincat["lang"]["verb_tag"]["present"]; ?>"/>
	 participle: <input class="lang" name="verb_tag_participle" type="text" value="<?php echo $admincat["lang"]["verb_tag"]["participle"]; ?>"/>
	</td>
       </tr>
      </tbody></table>

      <h3>post settings</h3>
      <table class="costs"><tbody>
       <tr>
        <td class="field_label<?php echo isset($err["status"]) ? " error" : ""; ?>">status</td>
        <td class="field_input"><input name="status" type="text" value="<?php echo isset($admincat["status"]) ? $admincat["status"] : ""; ?>"/></td>
       </tr>
       <tr>
        <td class="field_label"></td>
        <td class="field_input">comma-delimited list of possible statuses</td>
       </tr>
       <tr>
        <td class="field_label">unique titles</td>
        <td class="field_input"><input name="p_uniquetitles" type="checkbox"<?php if ($category["p_uniquetitles"] == "t") echo " checked"; ?>/></td>
       </tr>
      </tbody></table>
      <div><input id="submit" style="width: auto;" type="submit" value="update"/></div>
     </form>
    </div>
   </div>
<?php include "_foot.php"; ?>