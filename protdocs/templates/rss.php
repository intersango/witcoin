<?php
	header("content-type: application/rss+xml");
?><?xml version="1.0"  encoding="UTF-8"?><?php /*http://feeds.feedburner.com/witcoin*/ ?><rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom"><channel>
<atom:link href="<?php echo ($category["categoryid"] == 0 ? getdomain() : getdomain($category["category"])), "rss"; ?>" rel="self" type="application/rss+xml" />
<title>witcoin</title>
<link><?php echo $category["categoryid"] == 0 ? getdomain() : getdomain($category["category"]); ?></link>
<description>witcoin</description>
<?php foreach ($posts as $postid => $post) { ?>
<item>
<title><?php echo (isset($post["category_status"]) ? "(".$post["posts_status"].") " : ""), $post["title"]; ?></title>
<link><?php echo getdomain($post["category_sd"]),"p/",$postid,"/",urlfriendly($post["title"]); ?></link>
<pubDate><?php echo date("r", strtotime($post["lastactivity"])); ?></pubDate>
<description><?php echo $post["lastevent"], " in ", $post["category_name"], " ", time_duration(strtotime($post["lastactivity"])), " by ", $post["displayname"]; ?></description>
<guid isPermaLink="false"><?php echo "witcoin.com,", date("Y-m-d", strtotime($post["posted"])), ":", $postid; ?></guid>
</item>
<?php } ?>
</channel></rss>