<?php
	global $category, $title;
	include "_head.php";
?>
   <div class="main">
    <div class="headerf">
     <h2><?php $title = array_reverse($title); echo implode(" - ", $title); ?></h2>
    </div>
    <div class="maincontent">
     <?php if (count($_POST) > 0) { ?><div class="success">updated</div><?php } ?>
     <form action="/witizens/charities/<?php echo $_SESSION["userid"]; ?>" method="post">
      <input name="charity" type="hidden" value="1"/>
      <table class="charities">
       <tr>
        <th></th>
        <th>charity</th>
        <th>witcoins donated</th>
        <th>bitcoin address</th>
       </tr>
<?php foreach ($charities as $charityid => $charity) { ?>
       <tr>
        <td><input name="charity_<?php echo $charity["charityid"]; ?>" type="checkbox"<?php if (in_array($charity["charityid"], explode(",", $witizen["charities"]))) echo " checked"; ?>/></td>
        <td class="name"><a href="/charity/<?php echo $charity["path"]; ?>">
	 <?php if ($charity["logo"] != "") { ?><img alt="<?php echo $charity["charity"]; ?>" src="<?php echo $charity["logo"]; ?>" title="<?php echo $charity["charity"]; ?>"/><?php } else echo $charity["charity"]; ?>
	</a></td>
        <td class="donated"><?php echo clean_num($charity["donated"]); ?></td>
        <td class="bitcoinaddress"><?php echo $charity["bitcoin_address_charity"]; ?></td>
       </tr>
<?php } ?>
      </table>
      <div><input id="submit" type="submit" value="include selected charities in distribution of profits"/></div>
     </form>
    </div>
   </div>
<?php include "_foot.php"; ?>