<?php
	global $err, $errmsg, $path, $title, $witizen;
	include "_head.php";
?>
   <div class="main">
    <div class="header">
     <h2><?php $title = array_reverse($title); echo implode(" - ", $title); ?></h2>
    </div>
    <div class="maincontent">
<?php if ($err && isset($errmsg)) foreach ($errmsg as $msg) echo "<div class='error'>$msg</div>"; ?>
     <form action="<?php echo $path; ?>" method="post">
      <h3>voting</h3>
      <div><label><input name="autoupvote" type="checkbox"<?php if ($post["autoupvote"] == "t") echo " checked"; ?>/> auto upvote when posting or replying</label></div>
      </form>
     </div>
    </div>
   </div>
<?php include "_foot.php"; ?>