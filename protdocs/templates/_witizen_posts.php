<?php
	global $title, $witizen;
	include "_head.php";
?>
   <div class="main">
    <div class="header">
     <h2><?php echo implode(" - ", $title); ?></h2>
    </div>
    <div class="maincontent">
<?php if (count($posts) > 0) { ?>
     <div class="subheaderf">
      <h3>posts</h3>
      <div class="sort">
       <a <?php if ($sort == "lastactivity") echo 'class="active" '; ?>href="<?php echo $path, "?sort=lastactivity"; ?>" title="latest activity">last activity</a>
       <a <?php if ($sort == "newest") echo 'class="active" '; ?>href="<?php echo $path, "?sort=newest"; ?>" title="in reverse chronological order">newest</a>
       <a <?php if ($sort == "oldest") echo 'class="active" '; ?>href="<?php echo $path, "?sort=oldest"; ?>" title="in chronological order">oldest</a>
       <a <?php if ($sort == "toprank") echo 'class="active" '; ?>href="<?php echo $path, "?sort=toprank"; ?>" title="in descending order of rank">top rank</a>
       <a <?php if ($sort == "topreplies") echo 'class="active" '; ?>href="<?php echo $path, "?sort=topreplies"; ?>" title="in descending order of replies">top replies</a>
       <a <?php if ($sort == "topvotes") echo 'class="active" '; ?>href="<?php echo $path, "?sort=topvotes"; ?>" title="in descending order of votes">top votes</a>
      </div>
     </div>
<?php include "_listposts.php"; ?>
<?php } ?>
    </div>
   </div>
<?php include "_foot.php"; ?>
