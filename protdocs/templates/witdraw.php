<?php
	global $err, $success, $title, $witizen;

	include "_head.php";
?>
   <div class="main">
    <div class="header">
     <h2><?php echo implode(" - ", $title); ?></h2>
    </div>
    <div class="maincontent">
     <div class="witdraw">
<?php if ($err && isset($errmsg)) foreach ($errmsg as $msg) echo "<div class='error'>$msg</div>"; ?>
<?php if ($success && isset($successmsg)) foreach ($successmsg as $msg) echo "<div class='success'>$msg</div>"; ?>
      <form action="/witdraw" method="post"><table><tbody>
       <tr>
        <td class="field_label<?php echo isset($err["witcoins"]) ? " error" : ""; ?>">witcoins:</td>
        <td class="field_input"><input id="witcoins" maxlength="64" name="witcoins" type="text" value="<?php echo isset($_POST["witcoins"]) ? $_POST["witcoins"] : ""; ?>"/></td>
       </tr>
       <tr>
        <td class="field_label<?php echo isset($err["bitcoin_address_user"]) ? " error" : ""; ?>">bitcoin address:</td>
        <td class="field_input"><?php echo $witizen["bitcoin_address_user"]; ?></td>
       </tr>
       <tr>
        <td class="field_input" colspan="2"><input id="submit" type="submit" value="witdraw"/></td>
       </tr>
      </tbody></table></form>
     </div>
    </div>
   </div>
<?php include "_foot.php"; ?>
