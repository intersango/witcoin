     <div class="witizen">
<?php if ($witizen["gravatar"] != "") { ?>
      <div class="witizen_gravatar">
       <img alt="" class="gravatar" src="<?php echo $witizen["gravatar"]; ?>"/>
      </div>
<?php } ?>
      <div class="witizen_details">
       <h2><?php echo $witizen["rank"]; ?></h2>
<?php if (isset($_SESSION["userid"]) && $witizen["userid"] == $_SESSION["userid"]) { ?>
       <div><a href="/witizens/edit/<?php echo $_SESSION["userid"]; ?>">edit</a></div>
<?php } ?>
       <div class="witizen_details">
<?php if (isset($_SESSION["userid"]) && $witizen["userid"] == $_SESSION["userid"]) { ?>
        <div class="field_label">username</div>
<?php	if ($witizen["username"]) { ?>
        <div class="field_value"><?php echo $witizen["username"]; ?></div>
<?php	} else { ?>
        <div class="field_value"><a href="/signup?get&ne">get username</a></div>
<?php
		}
	}
?>
        <div class="field_label">display name</div>
        <div class="field_value"><?php echo filter(htmlspecialchars($witizen["displayname"]), FILTER_WORDS); ?></div>
        <div class="field_label">member for</div>
        <div class="field_value"><?php echo $witizen["member_for"]; ?></div>
        <div class="field_label">seen</div>
        <div class="field_value"><?php echo $witizen["seen"]; ?></div>
<?php if ($witizen["website"] != "") { ?>
        <div class="field_label">website</div>
        <div class="field_value"><a href="<?php echo $witizen["website"]; ?>">click here</a></div>
<?php } ?>
<?php if ($witizen["location"] != "") { ?>
        <div class="field_label">location</div>
        <div class="field_value"><?php echo $witizen["location"]; ?></div>
<?php } ?>
<?php if (isset($_SESSION["userid"]) && $witizen["userid"] == $_SESSION["userid"]) { ?>
<?php	if ($witizen["bitcoin_address_user"] != "") { ?>
        <div class="field_label">bitcoin address</div>
        <div class="field_value"><?php echo substr($witizen["bitcoin_address_user"], 0, 12)."..."; ?></div>
<?php	} ?>
        <div class="field_label">witcoins</div>
        <div class="field_value"><?php echo clean_num($witizen["witcoins"]); ?></div>
<?php } ?>
       </div>
       <div class="witizen_actions">
<?php if (isset($_SESSION["userid"]) && $witizen["userid"] == $_SESSION["userid"]) { ?>
         <a href="/witizens/charities/<?php echo $_SESSION["userid"]; ?>">charitable organizations</a><br/>
<?php	if (checkAddress($witizen["bitcoin_address_user"])) { ?>
	 <a href="/transactions">transactions</a><br/>
	 <?php if ($witizen["witcoins"] > 0) { ?><a href="/witdraw">witdraw</a><br/><?php } ?>
<?php	} ?>
<?php } ?>
        <a href="/witizens/donate/<?php echo $witizen["userid"]; ?>">donate witcoins</a>
       </div>
      </div>
      <div class="showdown witizen_aboutme"><?php echo filter($witizen["aboutme"]); ?></div>
     </div>
