<?php
	global $err, $post, $title;

	include "_head.php";
?>
   <div class="main">
    <div class="header">
     <h2><?php echo implode(" - ", $title); ?></h2>
    </div>
    <div class="maincontent">
<?php
	if (isset($_SESSION["userid"])) {
		if (isset($_POST["witcoins"]) && !$err) {
?>
     <div class="success">
      successfully supported with <?php echo $_POST["witcoins"]; ?> witcoins
     </div>
<?php
			unset($_POST["witcoins"]);
		}
?>
<?php	if ($err && isset($errmsg)) foreach ($errmsg as $msg) echo "<div class='error'>$msg</div>"; ?>
    <form action="/support?<?php echo implode("&", $query); ?>" method="post">
     <h3 class="<?php echo isset($err["witcoins"]) ? "error " : ""; ?>middle">witcoins</h3>
     <div><input name="witcoins" type="text" value="<?php echo isset($_POST["witcoins"]) ? $_POST["witcoins"] : 0; ?>"/></div>
     <div><input id="submit-button" type="submit" value="send"/><a href="<?php echo getdomain($post["category_sd"]),"p/",$post["postid"],"/",urlfriendly($post["title"]); ?>" title="cancel">cancel</a></div>
    </form>
    <br/>
    <div>95% of proceeds will go to <?php echo isset($_GET["p"]) ? "poster" : "replier"; ?>.  the remaining 5% of proceeds will be equally distributed to site, category renter and upvoters.</div>
<?php } else { ?>
     <div class="center">you must <a href="<?php echo getdomain(),"getwitit?dest=",getdest(); ?>" title="get wit it">get wit it</a> to support</div>
<?php } ?>
    </div>
   </div>
<?php include "_foot.php"; ?>