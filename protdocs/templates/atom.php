<?php
	header("content-type: application/atom+xml");
?><?xml version="1.0" encoding="utf-8"?><feed xmlns="http://www.w3.org/2005/Atom" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:thr="http://purl.org/syndication/thread/1.0">
<title>witcoin</title>
<id><?php echo $category["categoryid"] == 0 ? getdomain() : getdomain($category["category"]); ?></id>
<link rel="self" type="application/atom+xml" href="<?php echo ($category["categoryid"] == 0 ? getdomain() : getdomain($category["category"])), "atom"; ?>" />
<link rel="alternate" type="text/html" href="<?php echo $category["categoryid"] == 0 ? getdomain() : getdomain($category["category"]); ?>" />
<updated><?php echo date("c", strtotime($lastactivity["date"])); ?></updated>
<?php foreach ($posts as $postid => $post) { ?>
<entry>
<title><?php echo (isset($post["category_status"]) ? "(".$post["posts_status"].") " : ""), $post["title"]; ?></title>
<link rel="alternate" type="text/html" href="<?php echo getdomain($post["category_sd"]),"p/",$postid,"/",urlfriendly($post["title"]); ?>" />
<link rel="replies" type="text/html" href="<?php echo getdomain($post["category_sd"]),"p/",$postid,"/",urlfriendly($post["title"]); ?>" />
<id><?php echo "tag:witcoin.com,", date("Y-m-d", strtotime($post["posted"])), ":", $postid; ?></id>
<published><?php echo date("c", strtotime($post["posted"])); ?></published>
<updated><?php echo date("c", strtotime($post["lastactivity"])); ?></updated>
<summary><?php echo strip_tags($post["post"]); ?></summary>
<author><name><?php echo $post["displayname"]; ?></name></author>
<content type="xhtml" xml:lang="en-US" xml:base="<?php echo getdomain($post["category_sd"]); ?>"><div xmlns="http://www.w3.org/1999/xhtml"><?php echo $post["post"]; ?></div></content>
</entry>
<?php } ?>
</feed>
