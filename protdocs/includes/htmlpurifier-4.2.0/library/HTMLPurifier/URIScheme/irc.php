<?php

/**
 * Validates irc (Internet Relay Chat) URIs as defined by generic mIRC, see http://www.mirc.com/mirclink.html
 */
class HTMLPurifier_URIScheme_irc extends HTMLPurifier_URIScheme {

    public $browsable = false;
    public $default_port = 6667;

    public function validate(&$uri, $config, &$context) {
        parent::validate($uri, $config, $context);
        // strip unused parts
        $uri->userinfo = null;
        $uri->fragment = null;
	// channel (path) or server (host) can be empty, but not both
        if($uri->host == "" && ($uri->path == "/" || $uri->path == "")) return false;
        return true;
    }
}

// vim: et sw=4 sts=4
