<?php
	global $domain;

	if ($site == "default") ini_set("session.cookie_domain", ".witcoin.com");
	else if ($site == "i2p") ini_set("session.cookie_domain", ".witcoin.i2p");
	else if ($site == "3rd") ini_set("session.cookie_domain", $domain["address"]);
	ini_set("session.cookie_httponly", 1);
	session_start();

	// Since devel and production sites use same domain, they share cookies.
	// Previously it was possibly for a user to create a user account at devel site
	// and log in as another user with same userid on production site.
	// userids in devel site now begin at 1,000,000,001 so as to not exist for production site
	// If cookie contains userid which does not exist, clear all cookies
	if (isset($_SESSION["userid"]) && db_result(db_query("SELECT userid FROM users WHERE userid = %d", $_SESSION["userid"])) === FALSE)
		foreach ($_SESSION as $key => $value) { unset($_SESSION[$key]); }

	function isLoggedIn() { return isset($_SESSION["username"]); }

	if (isset($_SESSION["userid"])) {
		$sql = "UPDATE users SET access = %s WHERE userid = %d";
		if (!isset($_SESSION["rank"]) || is_numeric($_SESSION["rank"])) $sql .= " RETURNING rank";

		$result = db_result(db_query($sql, "to_timestamp(".time().")", $_SESSION["userid"]));
		if ($result) $_SESSION["rank"] = $result;
	}
?>