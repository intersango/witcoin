<?php
	global $category, $debug, $domain, $lang, $site;

	date_default_timezone_set("GMT");
	require_once "config.php";
	require_once "bitcoin_validator.php";
	require_once "constants.php";
	require_once "head.php";
	require_once "postgresql.php";
	require_once "user.php";
	require_once "vars.php";

	if (isset($_GET["debug"])) {
		$debug = array("out" => array(), "type" => $_GET["debug"]);
		if ($debug["type"] == "mem") $debug["out"][] = "000 memory: emalloc(" . memory_get_usage() . ") real(" . memory_get_usage(TRUE) . ")";
	}

	// example:
	// $_SERVER["HOST_NAME"] == faq.witcoin.com:2980
	// $_SERVER["SERVER_NAME"] == faq.witcoin.com
	$category = getcategorydata($domain["cat"]);
	if ($category["lang"] != "") $category["lang"] = category_lang($category["lang"]);
	else $category["lang"] = $lang;

	if (isset($_SERVER["REQUEST_URI"])) {
		global $path;
		$req = $_SERVER["REQUEST_URI"];
		if (preg_match("|^https?://[^/]+(/.*)|", $req, $matches)) $req = $matches[1];
		if (preg_match("/([^?]*)\?.*/", $req, $matches)) $path = str_replace("%20", " ", $matches[1]);
		else $path = str_replace("%20", " ", $req);
		while (strpos($path, "//") !== FALSE) $path = str_replace("//", "/", $path);
	}

	function bitcoins($num) { return "$num witcoin" . ($num == "1" ? "" : "s"); }

	function category_lang($category) {
		global $lang;
		$defaults = $lang;
		foreach ($category as $k1 => $v1) {
			if (is_array($v1)) {
				foreach ($category[$k1] as $k2 => $v2) {
					if (is_array($v2)) {
						foreach ($category[$k1][$k2] as $k3 => $v3) {
							if (!is_array($v3)) $defaults[$k1][$k2][$k3] = $v3;
						}
					} else $defaults[$k1][$k2] = $v2;
				}
			} else $defaults[$k1] = $v1;
		}
		return $defaults;
	}

	function clean_num($num) {
		// Don't use this.  It rounds up making users think they have more witcoins than they really have.
		//return number_format($num, 2);

		if (!is_numeric($num)) return "0.00000000";
		if (strpos($num, ".") === FALSE) return "$num.00000000";
		$num = rtrim(rtrim($num, "0"), ".");
		if (strpos($num, ".") === FALSE) return "$num.00000000";
		$integer = substr($num, 0, strpos($num, "."));
		$decimal = substr($num, strpos($num, ".")+1, 8); // Eight decimal places only
		if (strlen($decimal) < 8) $decimal .= str_repeat("0", 8 - strlen($decimal));
		if ($integer === "-0") return "-0.$decimal";
		else return number_format($integer).".$decimal";
	}

	function filter($str, $filter = FILTER_ALL) {
		if ($filter & FILTER_WORDS) {
			$str = str_replace("BitCoin", "Bitcoin", $str);
			$str = str_replace("Bit Coin", "Bitcoin", $str);
			$str = str_replace("Bit coin", "Bitcoin", $str);
			$str = str_replace("bit coin", "bitcoin", $str);
			$str = str_ireplace("witcoin", "witcoin", $str);
			$str = str_ireplace("wit coin", "witcoin", $str);
		}
		if ($filter & FILTER_URLS) {
			// Liveleak
				$str = preg_replace("`(?:^|\n) {0,3}(?:[^ ].*\s)?http://(?:www\.)?liveleak\.com/view\?i=([a-z0-9_]*)(?:&.*)?`", '<object height="605" width="736"><param name="movie" value="http://www.liveleak.com/e/$1"></param><param name="wmode" value="transparent"></param><param name="allowscriptaccess" value="always"></param><param name="wmode" value="transparent"></param></object><a href="http://www.liveleak.com/view?i=$1">http://www.liveleak.com/view?i=$1</a>', $str);
			// Livestream
				$str = preg_replace("`(?:^|\n) {0,3}(?:[^ ].*\s)?http://(?:www\.)?livestream.com/([a-zA-Z0-9_]*)`", '<object height="446" width="736"><param name="movie" value="http://cdn.livestream.com/grid/LSPlayer.swf?channel=$1&amp;autoPlay=false"></param><param name="allowScriptAccess" value="always"></param><param name="allowFullScreen" value="true"></param><param name="wmode" value="transparent"></param></object><a href="http://www.livestream.com/$1">http://www.livestream.com/rebelliontv$1</a>', $str);
			// Vimeo
				$str = preg_replace("`(?:^|\n) {0,3}(?:[^ ].*\s)?http://(?:www\.)?vimeo\.com/([0-9]*)(?:\?(autoplay=[0-9]))?`", '<object width="736" height="414"><param name="movie" value="http://vimeo.com/moogaloop.swf?clip_id=$1&$2" /><param name="wmode" value="transparent"></param></object><a href="http://vimeo.com/$1">http://vimeo.com/$1</a>', $str);
			// YouTube
				$str = preg_replace("`(?:^|\n) {0,3}(?:[^ ].*\s)?https?://(?:www\.)?youtube\.com/watch\?v=([a-zA-z0-9_-]*)(&autoplay=[0-9])?`", '<object width="736" height="444"><param name="movie" value="http://www.youtube.com/v/$1?fs=1&amp;hl=en_US&amp;rel=0$2"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="wmode" value="transparent"></param></object><a href="http://youtube.com/watch?v=$1">http://youtube.com/watch?v=$1</a>', $str);

			// Convert urls to markdown
			//$str = preg_replace("`((?:^|\n) {0,3}(?:[^ ](?:https?|ftp)*)?|[^'\"<>\w]+)(https?|ftp)(:\/\/[-A-Z0-9+&@#/%?=~_|()!:,.;[\]]*[-A-Z0-9+&@#/%=~_|[\]])($|\W)`i", "$1<$2$3>$4", $str);
			$str = preg_replace("`((?:^|\n) {0,3}(?:[^ ](?:https?|ftp)*)?|[^\">])(https?|ftp)(:\/\/[-A-Z0-9+&@#/%?=~_|()!:,.;[\]]*[-A-Z0-9+&@#/%=~_|[\]])($|[^ \"<]?)`i", "$1<$2$3>$4", $str);

			// Changes since last save.  Figure out wtf I did and maybe use the following?  Above is old since last save.
			// Convert urls to markdown
//			//$str = preg_replace("`((?:^|\n) {0,3}(?:[^ ](?:https?|ftp)*)?|[^'\"<>\w]+)(https?|ftp)(:\/\/[-A-Z0-9+&@#/%?=~_|()!:,.;[\]]*[-A-Z0-9+&@#/%=~_|[\]])($|\W)`i", "$1<$2$3>$4", $str);
//			$str = preg_replace("`((?:^|\n) {0,3}(?:[^ ](?:https?|ftp)*)?|[^\">])(https?|ftp)(:\/\/[-A-Z0-9+&@#/%?=~_|()!:,.;[\]]*[-A-Z0-9+&@#/%=~_|[\]])($|[^ \"<]?)`i", "$1<$2$3>$4", $str);
//			$str = preg_replace("`([^\">]*)(https?|ftp)(:\/\/[-A-Z0-9+&@#/%?=~_|()!:,.;[\]]*[-A-Z0-9+&@#/%=~_|[\]])($|[^ \"<]?)`i", ",$1.<$2$3>:$4;", $str);
		}
		if ($filter & FILTER_MARKDOWN) {
			require_once "markdown.php";
			$str = Markdown($str);
			//$str = SmartyPants($str);
		}
		if ($filter & FILTER_PURIFY) {
			global $purifier;

			if (!isset($purifier)) {
				require_once "htmlpurifier-4.2.0/library/HTMLPurifier.standalone.php";
				$purifier_config = HTMLPurifier_Config::createDefault();
				$purifier_config->set("HTML.Doctype", "XHTML 1.0 Strict");
				$purifier_config->set("HTML.SafeObject", true);
				//$purifier_config->set("HTML.SafeEmbed", true);
				$purifier = new HTMLPurifier($purifier_config);
			}

			$str = trim($purifier->purify($str));
		}
		if ($filter & FILTER_GESHI) {
			require_once "geshi.php";

			// http://stackoverflow.com/questions/4217581/geshi-with-markdown
			// I am not sure about the following code.  Needs review - miz
			// Must establish a way to detect language to use in place of php
			$str = preg_replace_callback("/<pre.*?><code.*?>(.*?[<pre.*?><code.*?>.*<\/code><\/pre>]*)<\/code><\/pre>/ism",
				create_function('$matches', '
					$matches[1] = htmlspecialchars_decode($matches[1]); // Revert HTML entities
					$geshi = new GeSHi($matches[1], \'php\');
					return $geshi->parse_code();
				'),
				$str);
		}
		return $str;
	}

	function getbalance($userid) { return db_result(db_query("SELECT witcoins FROM users WHERE userid = %d", $userid)); }

	function getcategorydata($cat) {
		global $blocks, $domain;

		if ($cat == "*") return array("allowposts" => FALSE, "category" => "*", "categoryid" => 0, "categorysd" => FALSE, "costs" => array(), "description" => "", "lang" => NULL, "renter" => NULL);
		$sql = "SELECT categories.allowposts, categories.category, categories.categoryid AS category_categoryid, categories.categorypid, categories.categorysd, categories.created, categories.creator,
				categories.description, categories.faq, categories.lang, categories.p_uniquetitles, categories.privacy, categories.renter, categories.status,
			costs.edit AS cost_edit, costs.post AS cost_post, costs.reply AS cost_reply, costs.upvote AS cost_upvote
			FROM categories LEFT OUTER JOIN costs ON (categories.categoryid = costs.categoryid) WHERE categorysd = '%s'";
		$categories = db_prepare_categories(db_query($sql, $cat));
		if ($categories) {
			$keys = array_keys($categories);
			$category = array("categoryid" => $keys[0]);
			foreach ($categories[$keys[0]] as $key => $value) $category[$key] = $value;

			//	$sql = "WITH RECURSIVE hcategories(category, categoryid, categorypid, depth) AS (
			//		(SELECT category, categoryid, categorypid, 0 FROM categories WHERE categoryid = %d)
			//		UNION ALL (SELECT c.category, c.categoryid, c.categorypid, hcategories.depth+1 FROM categories c JOIN hcategories ON (c.categoryid = hcategories.categorypid))
			//	) SELECT category, categoryid FROM hcategories WHERE categorypid IS NULL";
			//	$category["parent"] = db_fetch_array(db_query($sql, $category["categoryid"]));

			return $category;
		}
		else { header("Location: ".getdomain()); exit; }
	}

	function getdest() {
		global $path;
		if (count($_GET) > 0) {
			$query = array();
			foreach ($_GET as $k => $v) if ($k != "path") $query[] = $k."=".$v;
		}
		return getprotocol().$_SERVER["HTTP_HOST"].$path.(count($_GET) > 0 ? urlencode(urlencode("?".implode("&", $query))) : "");
	}

	function getdomain($cat = "") {
		global $domain, $site;
		if ($cat == "" && ($site == "default" || $site == "3rd")) $address = $domain["address"];
		else if ($cat == "" && $site == "i2p") $address = "witcoin.i2p";
		else if ($site == "3rd" && $cat == $domain["cat"]) $address = $domain["address"];
		else if ($site == "3rd") $address = $cat.".witcoin.com";
		else $address = $cat.".".$domain["address"];
		return getprotocol().$address."/";
	}

	function getdomainlogo() {
		global $domain, $site;
		if ($site == "3rd" && $domain["logo"] != "") return $domain["logo"];
		else return "/img/witcoin.png";
	}

	function getipaddr() {
		if ($_SERVER["REMOTE_ADDR"] == "69.164.221.33" && isset($_SERVER["HTTP_X_FORWARDED_FOR"])) return $_SERVER["HTTP_X_FORWARDED_FOR"];
		else return $_SERVER["REMOTE_ADDR"];
	}

	function getprotocol() { return "http" . (isset($_SERVER["HTTPS"]) ? "s" : "") . "://"; }

	function getunreadmessages() { return db_result(db_query("SELECT count(*) FROM messages WHERE touser = %d AND read = FALSE", $_SESSION["userid"])); }

	function is_rank($rank) { return (isset($_SESSION["rank"]) && $_SESSION["rank"] == $rank); }

	function loggedin() { return isset($_SESSION["userid"]); }

	function lang($l) {
		$args = func_get_args(); array_shift($args);
		if (isset($l) && $l != "") {
			if (is_string($l)) $l = unserialize($l);
			while ($arg = array_shift($args)) $l = $l[$arg];
			return $l;
		}
		// This may no longer be needed since adding else $category["lang"] = $lang; above
		else {
			global $lang; $l = $lang;
			while ($arg = array_shift($args)) $l = $l[$arg];
			return $l;
		}
	}

	function nicedate($date) { return date("M j, Y H:i:s  T", strtotime($date)); }

	function payment($from, $to, $funds) {
		$payees = array($from => bcmul($funds, -1, 64));
		$payment = bcdiv($funds, count($to), 64);
		foreach ($to as $userid) $payees[$userid] = bcadd((isset($payees[$userid]) ? $payees[$userid] : 0), $payment, 64);

		foreach ($payees as $payee => $amount) {
			if (is_numeric($payee)) {
				if ($payee == 0) {
					$dist_payees = array();
					$dist_payees[-99] = bcmul("1", 1, 64); // witcoin
					//$dist_payees[1] = bcmul(".2", 1, 64); // mizerydearia
					//$dist_payees[2] = bcmul(".15", 1, 64); // noagendamarket
					//$dist_payees[7] = bcmul(".05", 1, 64); // ducki2p
					//$dist_payees[14] = bcmul(".05", 1, 64); // tcatm
					//$dist_payees[23] = bcmul(".05", 1, 64); // theymos
					foreach ($dist_payees as $dist_payee => $dist_perc) {
						db_query("UPDATE users SET witcoins = witcoins + '%s' WHERE userid = %d", bcmul($amount, $dist_perc, 64), $dist_payee);
					}
				} else {
					$witcoins = db_result(db_query("UPDATE users SET witcoins = witcoins + '%s' WHERE userid = %d RETURNING witcoins", $amount, $payee));
					if ($from == $payee) $balance = $witcoins;
				}
			} else if (preg_match("/^charity_([0-9]*)$/", $payee, $matches)) {
				db_query("UPDATE charities SET balance = balance + '%s', donated = donated + '%s' WHERE charityid = %d", $amount, $amount, $matches[1]);
			}
		}
		return $balance;
	}

	function prepare($path) {
		global $category;

		require_once "../protdocs/prepare/_head.php";
		require_once "../protdocs/prepare/_foot.php";
		if (preg_match("|^/p/[0-9]*/edit$|", $path)) {
			require_once "../protdocs/prepare/editpost.php";
			require_once "../protdocs/templates/editpost.php";
		}
		else if (preg_match("|^/p/[0-9]*/revs?$|", $path)) {
			require_once "../protdocs/prepare/_postrevs.php";
			require_once "../protdocs/templates/_postrevs.php";
		}
		else if (preg_match("|^/p/[0-9]*(?:/.*)?$|", $path)) {
			require_once "../protdocs/prepare/_post.php";
			require_once "../protdocs/templates/_post.php";
		}
		// Change in includes.php, p.php, editpost.php, _tagged.php
		else if (preg_match("|^/p/t/[a-zA-Z0-9 #+.,_-]*$|", $path)) {
			require_once "../protdocs/prepare/_tagged.php";
			require_once "../protdocs/templates/_tagged.php";
		}
		else if (preg_match("|^/r/[0-9]*/edit$|", $path)) {
			require_once "../protdocs/prepare/editreply.php";
			require_once "../protdocs/templates/editreply.php";
		}
		else if (preg_match("|^/r/[0-9]*/reply$|", $path)) {
			require_once "../protdocs/prepare/reply.php";
			require_once "../protdocs/templates/reply.php";
		}
		else if (preg_match("|^/r/[0-9]*/revs?$|", $path)) {
			require_once "../protdocs/prepare/_replyrevs.php";
			require_once "../protdocs/templates/_replyrevs.php";
		}
		else if (preg_match("`^/witizens?/((?:(?:charities|donate|edit|posts|replies|settings)/)?[0-9]*)$`", $path, $matches)) {
			if (preg_match("|^[0-9]*$|", $matches[1])) {
				$user = db_fetch_array(db_query("SELECT * FROM users WHERE userid = '%d'", $matches[1]));
				if ($user) {
					require_once "../protdocs/prepare/_witizen.php";
					require_once "../protdocs/templates/_witizen.php";
				} else require_once "../protdocs/templates/_error.php";
			} else if (preg_match("|^charities/([0-9]*)$|", $matches[1], $matches2)) {
				if (isset($_SESSION["userid"]) && $_SESSION["userid"] == $matches2[1]) {
					require_once "../protdocs/prepare/_witizen_charity.php";
					require_once "../protdocs/templates/_witizen_charity.php";
				} else require_once "../protdocs/templates/_error.php";
			} else if (preg_match("|^donate/[0-9]*$|", $matches[1])) {
				require_once "../protdocs/prepare/_witizen_donate.php";
				require_once "../protdocs/templates/_witizen_donate.php";
			} else if (preg_match("|^edit/([0-9]*)$|", $matches[1], $matches2)) {
				if (isset($_SESSION["userid"]) && $_SESSION["userid"] == $matches2[1]) {
					require_once "../protdocs/prepare/_witizen_edit.php";
					require_once "../protdocs/templates/_witizen_edit.php";
				} else require_once "../protdocs/templates/_error.php";
			} else if (preg_match("|^posts/[0-9]*$|", $matches[1])) {
				require_once "../protdocs/prepare/_witizen_posts.php";
				require_once "../protdocs/templates/_witizen_posts.php";
			} else if (preg_match("|^replies/[0-9]*$|", $matches[1])) {
				require_once "../protdocs/prepare/_witizen_replies.php";
				require_once "../protdocs/templates/_witizen_replies.php";
			} else if (preg_match("|^settings|/([0-9]*)$|", $matches[1], $matches2)) {
				if (isset($_SESSION["userid"]) && $_SESSION["userid"] == $matches2[1]) {
					require_once "../protdocs/prepare/_witizen_settings.php";
					require_once "../protdocs/templates/_witizen_settings.php";
				} else require_once "../protdocs/templates/_error.php";
			} else require_once "../protdocs/templates/_error.php";
		}
		else if (preg_match("|^/charity/.{3,}$|", $path)) {
			require_once "../protdocs/prepare/_charity.php";
			require_once "../protdocs/templates/_charity.php";
		}
		elseif (preg_match("|^/inbox/msg/[0-9]*$|", $path)) {
			require_once "../protdocs/prepare/_message.php";
			require_once "../protdocs/templates/_message.php";
		}
		else {
			// $category["lang"]["verb_post"]["present"] is user-generated
			// Is there any concern for the following line in the case of category owner using it
			// maliciously or in some way affecting the fucntion of the site for other users?
			if ($category["categoryid"] == 0) $file = $path;
			else if (substr($path, 1) == $category["lang"]["verb_post"]["present"]) $file = "/post";
			else $file = $path;

			if (is_file("../protdocs/prepare$file.php")) require_once "../protdocs/prepare$file.php";
			else if (is_file("../protdocs/prepare".$file."index.php")) require_once "../protdocs/prepare".$file."index.php";
			if (is_file("../protdocs/templates$file.php")) require_once "../protdocs/templates$file.php";
			else if (is_file("../protdocs/templates".$file."index.php")) require_once "../protdocs/templates".$file."index.php";
			else require_once "../protdocs/templates/_error.php";
		}
	}

	function tags_delimited($taglist) {
		$tags = array();
		foreach (explode(",", $taglist) as $tag) {
			if (count(explode(" ", $tag)) > 3) $tags = array_merge($tags, explode(" ", $tag)); else $tags[] = $tag;
		}
		return $tags;
	}

	function time_duration($time) {
		$sex = time() - $time;
		if ($sex < 60) { if ($sex == 1) return "1 sec ago"; else return "$sex secs ago"; }
		else if ($sex < 60 * 60) {
			$mins = floor($sex / 60);
			if ($mins == 1) return "1 min ago"; else return "$mins mins ago";
		}
		else if ($sex < 60 * 60 * 24) {
			$hrs = floor($sex / 60 / 60);
			if ($hrs == 1) return "1 hr ago"; else return "$hrs hrs ago";
		}
		else if ($sex < 60 * 60 * 24 * 365) {
			$days = floor($sex / 60 / 60 / 24);
			if ($days == 1) return "1 day ago"; else return "$days days ago";
		}
		else {
			return gmdate('M j \'y \a\t H:i', time() - $sex);
		}
	}

	// 1 year == 31556926 seconds
	function time_length($time) {
		$days = 0; $hrs = 0; $mins = 0; $months = 0; $out = array(); $sex = time() - round($time); $yrs = 0;
		while ($sex >= 31556926) { $yrs++; $sex -= 60 * 60 * 24 * 365; }
		while ($sex >= 31556926 / 12) { $months++; $sex -= round(31556926 / 12); }
		while ($sex >= 86400) { $days++; $sex -= 60 * 60 * 24; }
		while ($sex >= 3600) { $hrs++; $sex -= 60 * 60; }
		while ($sex >= 60) { $mins++; $sex -= 60; }
		if ($yrs > 0) return $yrs . " year" . ($yrs != 1 ? "s" : "");
		else if ($months > 0) return $months . " month" . ($months != 1 ? "s" : "");
		else if ($days > 0) return $days . " day" . ($days != 1 ? "s" : "");
		else if ($hrs > 0) return $hrs . " hour" . ($hrs != 1 ? "s" : "");
		else if ($mins > 0) return $mins . " minute" . ($mins != 1 ? "s" : "");
		else return $sex . " second" . ($sex != 1 ? "s" : "");
	}

	function urlfriendly($s, $r1 = ". ", $r2 = "-") {
		for ($x=0; $x<strlen($r1); $x++) $s = str_replace(substr($r1, $x, 1), $r2, $s);
		$s = strip_tags(html_entity_decode($s));
		return trim(preg_replace("/[^a-zA-Z0-9-]/", "", $s), $r2);
	}

	require_once "sorting.php";
?>
