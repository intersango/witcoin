<?php
	global $blocks, $bodyclass, $lang, $title;
	$blocks = array(); $bodyclass = array(); $title = array("witcoin");

	//$lang = unserialize('a:9:{s:16:"noun_newestposts";s:6:"newest";s:16:"noun_oldestposts";s:6:"oldest";s:9:"noun_post";a:2:{s:8:"singular";s:4:"post";s:6:"plural";s:5:"posts";}s:15:"noun_recenttags";s:11:"recent tags";s:15:"noun_topreplies";s:11:"top replies";s:13:"noun_topvotes";s:9:"top votes";s:11:"verb_logout";s:7:"log out";s:9:"verb_post";a:2:{s:7:"present";s:4:"post";s:4:"past";s:6:"posted";}s:8:"verb_tag";a:2:{s:7:"present";s:3:"tag";s:4:"past";s:6:"tagged";}}');
	$lang = array(
		"adj_newest" => "newest",
		"adj_oldest" => "oldest",
		"noun_post" => array("singular" => "post", "plural" => "posts"),
		"noun_recenttags" => "recent tags",
		"noun_reply" => array("singular" => "reply", "plural" => "replies"),
		"noun_vote" => array("singular" => "vote", "plural" => "votes"),
		"verb_post" => array("present" => "post", "past" => "posted", "participle" => "posting"),
		"verb_reply" => array("present" => "reply", "past" => "replied", "participle" => "replying"),
		"verb_tag" => array("present" => "tag", "past" => "tagged", "participle" => "tagging")
	);
?>