<?php
	global $db;

	define('DB_QUERY_REGEXP', '/(%d|%s|%%|%f|%b|%n)/');

	$conn_string = "host=".$db["host"]." dbname=".$db["db"]." user=".$db["user"]." password=".$db["pass"];
	//$conn_string = "host=".$db["host"]." dbname=postgres user=root";
	$pg_conn = pg_connect($conn_string) or die("Could not connect: " . pg_last_error());
	pg_query($pg_conn, "set client_encoding=\"UTF8\"");

	/**
	 * Runs a basic query in the active database.
	 *
	 * User-supplied arguments to the query should be passed in as separate
	 * parameters so that they can be properly escaped to avoid SQL injection
	 * attacks.
	 *
	 * @param $query
	 *   A string containing an SQL query.
	 * @param ...
	 *   A variable number of arguments which are substituted into the query
	 *   using printf() syntax. Instead of a variable number of query arguments,
	 *   you may also pass a single array containing the query arguments.
	 *
	 *   Valid %-modifiers are: %s, %d, %f, %b (binary data, do not enclose
	 *   in '') and %%.
	 *
	 *   NOTE: using this syntax will cast NULL and FALSE values to decimal 0,
	 *   and TRUE values to decimal 1.
	 *
	 * @return
	 *   A database query result resource, or FALSE if the query was not
	 *   executed correctly.
	 */
	function db_query($query) {
		$args = func_get_args();
		array_shift($args);
		if (isset($args[0]) and is_array($args[0])) { // 'All arguments in one array' syntax
		$args = $args[0];
		}
		_db_query_callback($args, TRUE);
		$query = preg_replace_callback(DB_QUERY_REGEXP, '_db_query_callback', $query);
		return _db_query($query);
	}

	function db_query_params($query, $params) {
		$last_result = pg_query_params($query, $params);
		if ($last_result !== FALSE) return $last_result;
		return FALSE;
	}

	/**
	* Helper function for db_query().
	*/
	function _db_query($query, $debug = 0) {
		global $pg_conn, $last_result, $queries;
		$last_result = pg_query($pg_conn, $query);
		if ($debug) echo '<p>query: '. $query .'<br />error:'. pg_last_error($pg_conn) .'</p>';
		if ($last_result !== FALSE) return $last_result;
		return FALSE;
	}

	/**
	 * Helper function for db_query().
	 */
	function _db_query_callback($match, $init = FALSE) {
		static $args = NULL;
		if ($init) { $args = $match; return; }
		switch ($match[1]) {
			case '%d': // We must use type casting to int to convert FALSE/NULL/(TRUE?)
				$value = array_shift($args);
				// Do we need special bigint handling?
				if ($value > PHP_INT_MAX) {
					$precision = ini_get('precision');
					@ini_set('precision', 16);
					$value = sprintf('%.0f', $value);
					@ini_set('precision', $precision);
				}
				else $value = (int) $value;
				// We don't need db_escape_string as numbers are db-safe.
				return $value;
			case '%s':
				return db_escape_string(array_shift($args));
			case '%n':
				// Numeric values have arbitrary precision, so can't be treated as float.
				// is_numeric() allows hex values (0xFF), but they are not valid.
				$value = trim(array_shift($args));
				return is_numeric($value) && !preg_match('/x/i', $value) ? $value : '0';
			case '%%':
				return '%';
			case '%f':
				return (float) array_shift($args);
			case '%b': // binary data
				return db_encode_blob(array_shift($args));
		}
	}

	/**
	 * Restrict a dynamic table, column or constraint name to safe characters.
	 *
	 * Only keeps alphanumeric and underscores.
	 */
	function db_escape_table($string) { return preg_replace('/[^A-Za-z0-9_]+/', '', $string); }

	/**
	* Fetch one result row from the previous query as an object.
	*
	* @param $result
	*   A database query result resource, as returned from db_query().
	* @return
	*   An object representing the next row of the result, or FALSE. The attributes
	*   of this object are the table fields selected by the query.
	*/
	function db_fetch_object($result) { if ($result) return pg_fetch_object($result); }

	/**
	 * Fetch one result row from the previous query as an array.
	 *
	 * @param $result
	 *   A database query result resource, as returned from db_query().
	 * @return
	 *   An associative array representing the next row of the result, or FALSE.
	 *   The keys of this object are the names of the table fields selected by the
	 *   query, and the values are the field values for this result row.
	 */
	function db_fetch_array($result) { if ($result) return pg_fetch_assoc($result); }

	/**
	 * Return an individual result field from the previous query.
	 *
	 * Only use this function if exactly one field is being selected; otherwise,
	 * use db_fetch_object() or db_fetch_array().
	 *
	 * @param $result
	 *   A database query result resource, as returned from db_query().
	 * @return
	 *   The resulting field or FALSE.
	 */
	function db_result($result) { if ($result && pg_num_rows($result) > 0) { $array = pg_fetch_row($result); return $array[0]; } return FALSE; }

	/**
	 * Determine whether the previous query caused an error.
	 */
	function db_error() { return pg_last_error($pg_conn); }

	/**
	 * Returns the last insert id. This function is thread safe.
	 *
	 * @param $table
	 *   The name of the table you inserted into.
	 * @param $field
	 *   The name of the autoincrement field.
	 */
	function db_last_insert_id($table, $field) { return db_result(db_query("SELECT CURRVAL('". db_escape_table($table) ."_". db_escape_table($field) ."_seq')")); }

	/**
	 * Determine the number of rows changed by the preceding query.
	 */
	function db_affected_rows() { global $last_result; return empty($last_result) ? 0 : pg_affected_rows($last_result); }

	/**
	 * Runs a limited-range query in the active database.
	 *
	 * Use this as a substitute for db_query() when a subset of the query
	 * is to be returned.
	 * User-supplied arguments to the query should be passed in as separate
	 * parameters so that they can be properly escaped to avoid SQL injection
	 * attacks.
	 *
	 * @param $query
	 *   A string containing an SQL query.
	 * @param ...
	 *   A variable number of arguments which are substituted into the query
	 *   using printf() syntax. Instead of a variable number of query arguments,
	 *   you may also pass a single array containing the query arguments.
	 *   Valid %-modifiers are: %s, %d, %f, %b (binary data, do not enclose
	 *   in '') and %%.
	 *
	 *   NOTE: using this syntax will cast NULL and FALSE values to decimal 0,
	 *   and TRUE values to decimal 1.
	 *
	 * @param $from
	 *   The first result row to return.
	 * @param $count
	 *   The maximum number of result rows to return.
	 * @return
	 *   A database query result resource, or FALSE if the query was not executed
	 *   correctly.
	 */
	function db_query_range($query) {
		$args = func_get_args();
		$count = array_pop($args);
		$from = array_pop($args);
		array_shift($args);

		if (isset($args[0]) and is_array($args[0])) { // 'All arguments in one array' syntax
		$args = $args[0];
		}
		_db_query_callback($args, TRUE);
		$query = preg_replace_callback(DB_QUERY_REGEXP, '_db_query_callback', $query);
		$query .= ' LIMIT '. (int)$count .' OFFSET '. (int)$from;
		return _db_query($query);
	}

	/**
	 * Runs a SELECT query and stores its results in a temporary table.
	 *
	 * Use this as a substitute for db_query() when the results need to stored
	 * in a temporary table. Temporary tables exist for the duration of the page
	 * request.
	 * User-supplied arguments to the query should be passed in as separate parameters
	 * so that they can be properly escaped to avoid SQL injection attacks.
	 *
	 * Note that if you need to know how many results were returned, you should do
	 * a SELECT COUNT(*) on the temporary table afterwards. db_affected_rows() does
	 * not give consistent result across different database types in this case.
	 *
	 * @param $query
	 *   A string containing a normal SELECT SQL query.
	 * @param ...
	 *   A variable number of arguments which are substituted into the query
	 *   using printf() syntax. The query arguments can be enclosed in one
	 *   array instead.
	 *   Valid %-modifiers are: %s, %d, %f, %b (binary data, do not enclose
	 *   in '') and %%.
	 *
	 *   NOTE: using this syntax will cast NULL and FALSE values to decimal 0,
	 *   and TRUE values to decimal 1.
	 *
	 * @param $table
	 *   The name of the temporary table to select into. This name will not be
	 *   prefixed as there is no risk of collision.
	 * @return
	 *   A database query result resource, or FALSE if the query was not executed
	 *   correctly.
	 */
	function db_query_temporary($query) {
		$args = func_get_args();
		$tablename = array_pop($args);
		array_shift($args);

		$query = preg_replace('/^SELECT/i', 'CREATE TEMPORARY TABLE '. $tablename .' AS SELECT', $query);
		if (isset($args[0]) and is_array($args[0])) { // 'All arguments in one array' syntax
			$args = $args[0];
		}
		_db_query_callback($args, TRUE);
		$query = preg_replace_callback(DB_QUERY_REGEXP, '_db_query_callback', $query);
		return _db_query($query);
	}

	/**
	 * Returns a properly formatted Binary Large OBject value.
	 * In case of PostgreSQL encodes data for insert into bytea field.
	 *
	 * @param $data
	 *   Data to encode.
	 * @return
	 *  Encoded data.
	 */
	function db_encode_blob($data) { return "'". pg_escape_bytea($data) ."'"; }

	/**
	 * Returns text from a Binary Large OBject value.
	 * In case of PostgreSQL decodes data after select from bytea field.
	 *
	 * @param $data
	 *   Data to decode.
	 * @return
	 *  Decoded data.
	 */
	function db_decode_blob($data) { return pg_unescape_bytea($data); }

	/**
	 * Prepare user input for use in a database query, preventing SQL injection attacks.
	 * Note: This function requires PostgreSQL 7.2 or later.
	 */
	function db_escape_string($text) { return pg_escape_string($text); }

	function db_prepare_categories($result) {
		global $costs, $lang;

		$categories = array();
		$categorycolumns = array("allowposts", "categories_posts", "category", "categorypid", "categorysd", "created", "creator", "description", "faq", "lang", "p_uniquetitles", "renter", "status");
		while ($category = db_fetch_array($result)) {
			$categoryid = isset($category["categories_categoryid"]) ? $category["categories_categoryid"] : $category["category_categoryid"];

			if (!isset($categories[$categoryid])) $categories[$categoryid] = array();

			foreach ($categorycolumns as $categorycolumn) {
				if (!isset($categories[$categoryid][$categorycolumn]) && isset($category[$categorycolumn])) {
					if ($categorycolumn == "lang") {
						if ($category[$categorycolumn] == "") $categories[$categoryid]["lang"] = $lang;
						else $categories[$categoryid]["lang"] = unserialize($category[$categorycolumn]);
					}
					else if ($categorycolumn == "category") $categories[$categoryid][$categorycolumn] = filter(htmlspecialchars($category[$categorycolumn]), FILTER_WORDS);
					else $categories[$categoryid][$categorycolumn] = $category[$categorycolumn];
				}
			}

			$categories[$categoryid]["costs"] = $costs;
			if (isset($category["cost_edit"])) $categories[$categoryid]["costs"]["edit"] = $category["cost_edit"];
			if (isset($category["cost_post"])) $categories[$categoryid]["costs"]["post"] = $category["cost_post"];
			if (isset($category["cost_reply"])) $categories[$categoryid]["costs"]["reply"] = $category["cost_reply"];
			if (isset($category["cost_upvote"])) $categories[$categoryid]["costs"]["upvote"] = $category["cost_upvote"];
		}

		return $categories;
	}

	function db_prepare_charities($result) {
		$charities = array();

		while ($charity = db_fetch_array($result)) {
			$charityid = $charity["charityid"];

			if (!isset($charities[$charityid])) $charities[$charityid] = array();

			foreach ($charity as $var => $val) {
				if ($var == "charity") $charities[$charityid][$var] = filter(htmlspecialchars($charity[$var]), FILTER_WORDS);
				else $charities[$charityid][$var] = $charity[$var];
			}
		}

		return $charities;
	}

	function db_prepare_posts($result) {
		$posts = array();
		$postcolumns = array("category", "category_lang", "category_name", "category_sd", "category_status", "displayname", "email_address", "lastactivity", "lastevent", "lastuserid", "parent", "post",
			"posted", "post_balance", "post_revs", "post_status", "post_userid", "post_voted", "post_votes", "posts_rank", "posts_replies", "posts_status", "posts_userid", "posts_voted", "posts_votes",
			"posts_voters", "syndicateupto", "taglist", "thumb", "title", "views");

		while ($post = db_fetch_array($result)) {
			$postid = isset($post["posts_postid"]) ? $post["posts_postid"] : $post["post_postid"];

			if (!isset($posts[$postid])) $posts[$postid] = array();

			foreach ($postcolumns as $postcolumn) {
				if (!isset($posts[$postid][$postcolumn]) && isset($post[$postcolumn])) {
					if ($postcolumn == "taglist")
						// http://leftofreality.net/2010/03/05/postgresql-arrays-and-phps-str_getcsv/
						$posts[$postid]["taglist"] = pg_array_to_str($post["taglist"]);
					else if ($postcolumn == "displayname" || $postcolumn == "title")
						$posts[$postid][$postcolumn] = filter(htmlspecialchars($post[$postcolumn]), FILTER_WORDS);
					else $posts[$postid][$postcolumn] = $post[$postcolumn];
				}
			}

			if (isset($post["email_address"]))
				$posts[$postid]["gravatar"] = $post["email_address"] == "" ? "" : "https://secure.gravatar.com/avatar/".md5(strtolower($post["email_address"]))."?s=32&amp;d=monsterid&amp;r=x";

			if (!isset($posts[$postid]["postrevs"])) $posts[$postid]["postrevs"] = array();
			if (isset($post["revid"])) {
				if (!isset($posts[$postid]["postrevs"][$post["revid"]])) $posts[$postid]["postrevs"][$post["revid"]] = array(
					"displayname" => $post["postrev_displayname"],
					"gravatar" => $post["postrev_emailaddress"] == "" ? "" : "https://secure.gravatar.com/avatar/".md5(strtolower($post["postrev_emailaddress"]))."?s=32&amp;d=monsterid&amp;r=x",
					"post" => $post["postrev_post"],
					"posted" => $post["postrev_posted"],
					"status" => $post["postrev_status"],
					"syndicateupto" => $post["postrev_syndicateupto"],
					"taglist" => pg_array_to_str($post["postrev_taglist"]),
					"title" => $post["postrev_title"],
					"userid" => $post["postrev_userid"]);
			}

			if (!isset($posts[$postid]["replies"])) $posts[$postid]["replies"] = array();
			if (isset($post["replyid"])) {
				if (!isset($posts[$postid]["replies"][$post["replyid"]])) {
					$posts[$postid]["replies"][$post["replyid"]] = isset($post["reply"]) ? $post["reply"] : "";
				}
			}

			if (!isset($posts[$postid]["tags"])) $posts[$postid]["tags"] = array();
			if (isset($post["tagid"])) {
				// Use for transition of production to use new taglist stored in posts table
				//if (!isset($posts[$postid]["tags"][$post["tagid"]])) $posts[$postid]["tags"][$post["tagid"]] = pg_escape_string("\"".$post["tag"]."\"");
				if (!isset($posts[$postid]["tags"][$post["tagid"]])) $posts[$postid]["tags"][$post["tagid"]] = $post["tag"];
			}
		}

//		if (count($posts) == 1) {
//			$keys = array_keys($posts);
//			$post = array("posts_postid" => $keys[0]);
//			foreach ($posts[$keys[0]] as $key => $value) $post[$key] = $value;
//			return $post;
//		}

		return $posts;
	}

	function db_prepare_replies($result) {
		$replies = array();
		$replycolumns = array("category_sd", "category_status", "depth", "displayname", "email_address", "path", "post_status", "post_title", "post_userid", "preply_userid", "replies_postid", "replies_userid", "replypid", "reply",
			"replied", "replies_voted", "replies_votes", "reply_balance", "reply_postid", "reply_revs", "reply_userid", "reply_voted", "reply_votes");

		while ($reply = db_fetch_array($result)) {
			$replyid = isset($reply["replies_replyid"]) ? $reply["replies_replyid"] : (isset($reply["reply_replyid"]) ? $reply["reply_replyid"] : $reply["replyid"]);

			if (!isset($replies[$replyid])) $replies[$replyid] = array();

			foreach ($replycolumns as $replycolumn) {
				if (!isset($replies[$replyid][$replycolumn]) && isset($reply[$replycolumn])) {
					if ($replycolumn == "displayname" || $replycolumn == "post_title")
						$replies[$replyid][$replycolumn] = filter(htmlspecialchars($reply[$replycolumn]), FILTER_WORDS);
					else $replies[$replyid][$replycolumn] = $reply[$replycolumn];
				}
			}

			if (isset($reply["email_address"]))
				$replies[$replyid]["gravatar"] = $reply["email_address"] == "" ? "" : "https://secure.gravatar.com/avatar/".md5(strtolower($reply["email_address"]))."?s=32&amp;d=monsterid&amp;r=x";

			if (!isset($replies[$replyid]["replyrevs"])) $replies[$replyid]["replyrevs"] = array();
			if (isset($reply["revid"])) {
				if (!isset($replies[$replyid]["replyrevs"][$reply["revid"]])) $replies[$replyid]["replyrevs"][$reply["revid"]] =
					array("reply" => $reply["replyrev_reply"], "replied" => $reply["replyrev_replied"]);
			}
		}

//		if (count($replies) == 1) {
//			$keys = array_keys($replies);
//			$reply = array("replies_replyid" => $keys[0]);
//			foreach ($replies[$keys[0]] as $key => $value) $reply[$key] = $value;
//			return $reply;
//		}

		return $replies;
	}

	function pg_array_to_str($str) { return str_replace("\"", "", trim($str, "{}")); }

	function pg_str_to_array($str) {
		$arr = str_getcsv($str);
		foreach ($arr as $key => $val) if ($val == "null") $arr[$key] = "\"null\"";
		$str = implode(",", $arr);
		return "{".$str."}";
	}
?>