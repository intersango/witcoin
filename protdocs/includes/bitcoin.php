<?php
	global $btc, $btcconn; $btc = TRUE;
	ini_set('default_socket_timeout', 10);
	function btcerr($e) { global $btc; $btc = FALSE; file_put_contents("../btc/err", time()."\n$e\n\n", FILE_APPEND | LOCK_EX); }
	require_once 'jsonRPCClient.php';
	$btcuser = implode("", file("../btc/user", FILE_IGNORE_NEW_LINES));
	$btcpass = implode("", file("../btc/pass", FILE_IGNORE_NEW_LINES));
	$btcconn = new jsonRPCClient("http://$btcuser:$btcpass@127.0.0.1:8332");
	try { $btcconn->getblockcount(); } catch (Exception $e) { btcerr($e); }
?>