��          ,      <       P   �   Q   X  �   �   G                     TabFocus changes the notice form behavior so that, while in the text area, pressing the tab key focuses the "Send" button, matching the behavior of Twitter. Project-Id-Version: StatusNet - TabFocus
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:45+0000
Language-Team: Norwegian (bokmål)‬ <http://translatewiki.net/wiki/Portal:no>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 19:01:01+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: no
X-Message-Group: #out-statusnet-plugin-tabfocus
Plural-Forms: nplurals=2; plural=(n != 1);
 TabFocus endrer notisskjemaets oppførsel slik at når man er tekstområdet vil et trykk på tab-knappen fokusere på «Send»-knappen, samsvarende med oppførselen til Twitter. 