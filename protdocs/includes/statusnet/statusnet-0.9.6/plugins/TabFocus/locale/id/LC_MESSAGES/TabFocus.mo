��          ,      <       P   �   Q   E  �   �   4                     TabFocus changes the notice form behavior so that, while in the text area, pressing the tab key focuses the "Send" button, matching the behavior of Twitter. Project-Id-Version: StatusNet - TabFocus
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:45+0000
Language-Team: Indonesian <http://translatewiki.net/wiki/Portal:id>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 19:01:01+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: id
X-Message-Group: #out-statusnet-plugin-tabfocus
Plural-Forms: nplurals=1; plural=0;
 TabFocus mengubah kelakuan bentuk pemberitahuan sehingga ketika berada dalam area teks, menekan tombol tab akan memunculkan tombol "Kirim", menyerupai kelakuan Twitter. 