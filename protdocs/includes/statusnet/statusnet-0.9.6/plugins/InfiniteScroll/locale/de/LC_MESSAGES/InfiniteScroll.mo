��          ,      <       P      Q   T  r  ;  �                     Infinite Scroll adds the following functionality to your StatusNet installation: When a user scrolls towards the bottom of the page, the next page of notices is automatically retrieved and appended. This means they never need to click "Next Page", which dramatically increases stickiness. Project-Id-Version: StatusNet - InfiniteScroll
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:43+0000
Language-Team: German <http://translatewiki.net/wiki/Portal:de>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:54+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: de
X-Message-Group: #out-statusnet-plugin-infinitescroll
Plural-Forms: nplurals=2; plural=(n != 1);
 Infinite Scroll fügt die folgende Funktionalität zu deiner StatusNet-Installation hinzu: Wenn ein Benutzer Richtung Seitenende scrollt, wird die nächste Nachrichtenseite automatisch geladen und angefügt. Das bedeutet, dass sie nie „Nächste Seite“ klicken müssen, was die Attraktivität dramatisch erhöht. 