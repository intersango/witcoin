��          ,      <       P      Q   c  r    �                     Infinite Scroll adds the following functionality to your StatusNet installation: When a user scrolls towards the bottom of the page, the next page of notices is automatically retrieved and appended. This means they never need to click "Next Page", which dramatically increases stickiness. Project-Id-Version: StatusNet - InfiniteScroll
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:43+0000
Language-Team: Simplified Chinese <http://translatewiki.net/wiki/Portal:zh-hans>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:54+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: zh-hans
X-Message-Group: #out-statusnet-plugin-infinitescroll
Plural-Forms: nplurals=1; plural=0;
 无限滚动（Infinite Scroll）给你的 StatusNet 网站增加了这些功能：当用户滚动页面到底部的时候，将自动获取下一页的消息并追加显示出来。这样用户就不用再去点击“下一页”按钮了，大大的增加了黏贴度。 