��          ,      <       P      Q   S  r  �  �                     Infinite Scroll adds the following functionality to your StatusNet installation: When a user scrolls towards the bottom of the page, the next page of notices is automatically retrieved and appended. This means they never need to click "Next Page", which dramatically increases stickiness. Project-Id-Version: StatusNet - InfiniteScroll
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:43+0000
Language-Team: French <http://translatewiki.net/wiki/Portal:fr>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:54+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: fr
X-Message-Group: #out-statusnet-plugin-infinitescroll
Plural-Forms: nplurals=2; plural=(n > 1);
 InfiniteScroll ajoute la fonctionnalité suivante à votre installation StatusNet : lorsqu’un utilisateur fait défiler la page jusqu’à la fin, la page d’avis suivante est automatiquement téléchargée et ajoutée à la suite. Ceci signifie que l’utilisateur n’a plus besoin de cliquer sur le bouton « Page suivante », ce qui augmente considérablement l’adhérence de l’utilisateur. 