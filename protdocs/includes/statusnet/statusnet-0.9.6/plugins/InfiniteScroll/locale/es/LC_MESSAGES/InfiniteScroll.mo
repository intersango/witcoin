��          ,      <       P      Q   U  r  c  �                     Infinite Scroll adds the following functionality to your StatusNet installation: When a user scrolls towards the bottom of the page, the next page of notices is automatically retrieved and appended. This means they never need to click "Next Page", which dramatically increases stickiness. Project-Id-Version: StatusNet - InfiniteScroll
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:43+0000
Language-Team: Spanish <http://translatewiki.net/wiki/Portal:es>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:54+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: es
X-Message-Group: #out-statusnet-plugin-infinitescroll
Plural-Forms: nplurals=2; plural=(n != 1);
 Desplazamiento Infinito añade la siguiente funcionalidad a tu instalación StatusNet: Cuando un usuario se desplaza hacia el final de la página, la próxima página de mensajes se recupera y añade automáticamente. Esto significa que no será necesario hacer clic en "Próxima página", con lo que se aumenta dramáticamente la capacidad de retención. 