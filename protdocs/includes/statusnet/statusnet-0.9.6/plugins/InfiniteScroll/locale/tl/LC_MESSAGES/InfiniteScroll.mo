��          ,      <       P      Q   U  r  �  �                     Infinite Scroll adds the following functionality to your StatusNet installation: When a user scrolls towards the bottom of the page, the next page of notices is automatically retrieved and appended. This means they never need to click "Next Page", which dramatically increases stickiness. Project-Id-Version: StatusNet - InfiniteScroll
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:43+0000
Language-Team: Tagalog <http://translatewiki.net/wiki/Portal:tl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:54+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: tl
X-Message-Group: #out-statusnet-plugin-infinitescroll
Plural-Forms: nplurals=2; plural=(n != 1);
 Nagdaragdag ang Walang Hangganang Balumbon ng sumusunod na tungkulin sa iyong instalasyon ng StatusNet: Kapag ang isang tagagamit ay nagpapadulas papunta sa ilalim ng pahina, ang susunod na pahina ng mga pabatid ay kusang muling nakukuha at nadurugtong.  Nangangahulugan ito na hindi nila kailangan kailanman na pindutin ang "Susunod na Pahina", na mabisang nagpapataas ng pagkamadikit. 