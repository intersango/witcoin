��    
      l      �       �   >   �      0  *   @  )   k     �  	   �  
   �     �  8   �  C  �  G   >     �  B   �  B   �     !  	   '  
   1     <  ?   O                      
   	                    A sample plugin to show basics of development for new hackers. A warm greeting Could not increment greeting count for %d. Could not save new greeting count for %d. Hello Hello, %s Hello, %s! Hello, stranger! I have greeted you %d time. I have greeted you %d times. Project-Id-Version: StatusNet - Sample
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:34+0000
Language-Team: Dutch <http://translatewiki.net/wiki/Portal:nl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-20 17:58:22+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: nl
X-Message-Group: #out-statusnet-plugin-sample
Plural-Forms: nplurals=2; plural=(n != 1);
 Een voorbeeldplug-in als basis voor ontwikkelingen door nieuwe hackers. Een warme begroeting Het was niet mogelijk het aantal begroetingen op te hogen voor %d. Het was niet mogelijk het aantal begroetingen op te slaan voor %d. Hallo Hallo, %s Hallo, %s! Hallo vreemdeling! Ik heb u voor de eerste keer gegroet. Ik heb u %d keer gegroet. 