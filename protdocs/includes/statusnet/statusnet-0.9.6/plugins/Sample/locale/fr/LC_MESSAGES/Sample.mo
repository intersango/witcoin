��    
      l      �       �   >   �      0  *   @  )   k     �  	   �  
   �     �  8   �  C  �  Y   >     �  G   �  M   �     D     L     X     f     {                      
   	                    A sample plugin to show basics of development for new hackers. A warm greeting Could not increment greeting count for %d. Could not save new greeting count for %d. Hello Hello, %s Hello, %s! Hello, stranger! I have greeted you %d time. I have greeted you %d times. Project-Id-Version: StatusNet - Sample
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:32+0000
Language-Team: French <http://translatewiki.net/wiki/Portal:fr>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-20 17:58:22+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: fr
X-Message-Group: #out-statusnet-plugin-sample
Plural-Forms: nplurals=2; plural=(n > 1);
 Un exemple de greffon pour montrer les bases de développement pour les nouveaux codeurs. Un accueil chaleureux Impossible d’incrémenter le compte de vœux pour l’utilisateur %d. Impossible de sauvegarder le nouveau compte de vœux pour l’utilisateur %d. Bonjour Bonjour, %s Bonjour, %s ! Bonjour, étranger ! une %d 