��    
      l      �       �   >   �      0  *   @  )   k     �  	   �  
   �     �  8   �  ]  �  �   X     �  V   �  T   R     �     �     �  !   �  H   �                      
   	                    A sample plugin to show basics of development for new hackers. A warm greeting Could not increment greeting count for %d. Could not save new greeting count for %d. Hello Hello, %s Hello, %s! Hello, stranger! I have greeted you %d time. I have greeted you %d times. Project-Id-Version: StatusNet - Sample
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:34+0000
Language-Team: Macedonian <http://translatewiki.net/wiki/Portal:mk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-20 17:58:22+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: mk
X-Message-Group: #out-statusnet-plugin-sample
Plural-Forms: nplurals=2; plural=(n == 1 || n%10 == 1) ? 0 : 1;
 Приклучок-пример за основите на развојното програмирање за нови хакери. Срдечен поздрав Не можев да го дополнам бројот на поздрави за %d. Не можев да го зачувам бројот на поздрави за %d. Здраво Здраво, %s Здраво, %s! Здрво, незнајнику! Ве поздравив еднаш. Ве поздравив %d пати. 