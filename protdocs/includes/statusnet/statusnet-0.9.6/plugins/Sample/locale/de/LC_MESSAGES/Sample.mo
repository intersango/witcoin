��    
      l      �       �   >   �      0  *   @  )   k     �  	   �  
   �     �  8   �  D  �  F   ?     �  /   �  6   �            	          A   *                      
   	                    A sample plugin to show basics of development for new hackers. A warm greeting Could not increment greeting count for %d. Could not save new greeting count for %d. Hello Hello, %s Hello, %s! Hello, stranger! I have greeted you %d time. I have greeted you %d times. Project-Id-Version: StatusNet - Sample
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:31+0000
Language-Team: German <http://translatewiki.net/wiki/Portal:de>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-20 17:58:22+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: de
X-Message-Group: #out-statusnet-plugin-sample
Plural-Forms: nplurals=2; plural=(n != 1);
 Ein Beispiel-Plugin, um die Entwicklungsbasis neuen Hackern zu zeigen. Ein herzlicher Gruß Konnte Grüßungszähler von %d nicht erhöhen. Konnte neuen Grüßungszähler von %d nicht speichern. Hallo Hallo %s Hallo %s! Hallo Fremder! Ich habe dich einmal begrüßt.  Ich habe dich %d-mal begrüßt.  