��    
      l      �       �   >   �      0  *   @  )   k     �  	   �  
   �     �  8   �  E  �  d   @     �  3   �  6   �     -     5     A     N  3   `                      
   	                    A sample plugin to show basics of development for new hackers. A warm greeting Could not increment greeting count for %d. Could not save new greeting count for %d. Hello Hello, %s Hello, %s! Hello, stranger! I have greeted you %d time. I have greeted you %d times. Project-Id-Version: StatusNet - Sample
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:34+0000
Language-Team: Tagalog <http://translatewiki.net/wiki/Portal:tl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-20 17:58:22+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: tl
X-Message-Group: #out-statusnet-plugin-sample
Plural-Forms: nplurals=2; plural=(n != 1);
 Isang halimbawang pampasak upang ipakita ang mga saligan ng kaunlaran para sa bagong mga mangunguha. Isang mainit-init na pagbati Hindi masudlungan ang bilang ng pagbati para sa %d. Hindi masagip ang bagong bilang ng pagbati para sa %d. Kumusta Kumusta, %s Kumusta, %s! Kumusta, dayuhan! Binati kita ng %d ulit. Binati kita ng %d mga ulit. 