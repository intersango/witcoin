��    
      l      �       �   >   �      0  *   @  )   k     �  	   �  
   �     �  8   �  I  �  ^   D     �  8   �  >   �     0  	   7  
   A     L  6   _                      
   	                    A sample plugin to show basics of development for new hackers. A warm greeting Could not increment greeting count for %d. Could not save new greeting count for %d. Hello Hello, %s Hello, %s! Hello, stranger! I have greeted you %d time. I have greeted you %d times. Project-Id-Version: StatusNet - Sample
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:33+0000
Language-Team: Interlingua <http://translatewiki.net/wiki/Portal:ia>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-20 17:58:22+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: ia
X-Message-Group: #out-statusnet-plugin-sample
Plural-Forms: nplurals=2; plural=(n != 1);
 Un plug-in de exemplo pro demonstrar le principios de disveloppamento pro nove programmatores. Un calide salutation Non poteva incrementar le numero de salutationes pro %d. Non poteva salveguardar le numero de nove salutationes pro %d. Salute Salute %s Salute %s! Salute estraniero! Io te ha salutate %d vice. Io te ha salutate %d vices. 