��    
      l      �       �   >   �      0  *   @  )   k     �  	   �  
   �     �  8   �  �  �  t   �       ]   +  `   �     �     �            y   8                      
   	                    A sample plugin to show basics of development for new hackers. A warm greeting Could not increment greeting count for %d. Could not save new greeting count for %d. Hello Hello, %s Hello, %s! Hello, stranger! I have greeted you %d time. I have greeted you %d times. Project-Id-Version: StatusNet - Sample
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:35+0000
Language-Team: Ukrainian <http://translatewiki.net/wiki/Portal:uk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-20 17:58:22+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: uk
X-Message-Group: #out-statusnet-plugin-sample
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 Приклад додатку для демонстрації основ розробки новим гакерам. Щирі вітання Не вдалося перерахувати лічильник привітань для %d. Не вдалося зберегти новий лічильник привітань для %d. Привіт Привіт, %s Привіт, %s! Привіт, чужинцю! Я привітав вас %d раз. Я привітав вас %d разів. Я привітав вас %d разів. 