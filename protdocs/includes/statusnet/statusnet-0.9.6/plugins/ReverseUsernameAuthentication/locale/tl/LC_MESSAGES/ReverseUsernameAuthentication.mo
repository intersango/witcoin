��          ,      <       P   �   Q   s  �   �   o                     The Reverse Username Authentication plugin allows for StatusNet to handle authentication by checking if the provided password is the same as the reverse of the username. Project-Id-Version: StatusNet - ReverseUsernameAuthentication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:29+0000
Language-Team: Tagalog <http://translatewiki.net/wiki/Portal:tl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:31:03+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: tl
X-Message-Group: #out-statusnet-plugin-reverseusernameauthentication
Plural-Forms: nplurals=2; plural=(n != 1);
 Ang pamasak na Pabaligtad na Pagpapatunay ng Pangalan ng Tagagamit ay nagpapahintulot sa StatusNet na panghawakan ang pagpapatunay sa pamamagitan ng pagsusuri kung ang ibinigay na hudyat ay katulad ng kabaligtara ng pangalan ng tagagamit. 