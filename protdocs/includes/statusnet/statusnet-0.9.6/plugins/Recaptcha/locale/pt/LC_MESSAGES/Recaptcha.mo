��          <      \       p      q      y   f   �   N  �      G  +   _  ~   �                   Captcha Captcha does not match! Uses <a href="http://recaptcha.org/">Recaptcha</a> service to add a  captcha to the registration page. Project-Id-Version: StatusNet - Recaptcha
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:24+0000
Language-Team: Portuguese <http://translatewiki.net/wiki/Portal:pt>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 19:00:37+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: pt
X-Message-Group: #out-statusnet-plugin-recaptcha
Plural-Forms: nplurals=2; plural=(n != 1);
 Imagem de verificação A imagem de verificação não corresponde! Usa o serviço <a href="http://recaptcha.org/">Recaptcha</a> para adicionar uma imagem de verificação à página de registo. 