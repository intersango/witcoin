��          <      \       p      q      y   f   �   I  �      B     J  w   e                   Captcha Captcha does not match! Uses <a href="http://recaptcha.org/">Recaptcha</a> service to add a  captcha to the registration page. Project-Id-Version: StatusNet - Recaptcha
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:24+0000
Language-Team: Dutch <http://translatewiki.net/wiki/Portal:nl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 19:00:37+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: nl
X-Message-Group: #out-statusnet-plugin-recaptcha
Plural-Forms: nplurals=2; plural=(n != 1);
 Captcha Captcha komt niet overeen! Gebruikt de dienst <a href="http://recaptcha.org/">Recaptcha</a> om een captcha toe te voegen aan de registratiepagina. 