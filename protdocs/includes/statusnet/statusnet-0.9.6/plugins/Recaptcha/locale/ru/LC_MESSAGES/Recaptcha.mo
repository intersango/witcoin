��          <      \       p      q      y   f   �   �  �   
   �  0   �  �   �                   Captcha Captcha does not match! Uses <a href="http://recaptcha.org/">Recaptcha</a> service to add a  captcha to the registration page. Project-Id-Version: StatusNet - Recaptcha
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:24+0000
Language-Team: Russian <http://translatewiki.net/wiki/Portal:ru>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 19:00:37+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: ru
X-Message-Group: #out-statusnet-plugin-recaptcha
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 Капча Код проверки не совпадает! Добавляет на страницу регистрации каптчу с использованием <a href="http://recaptcha.org/">Recaptcha</a>. 