��          ,      <       P   E   Q   c  �   d   �                     Opens external links (i.e. with rel=external) on a new window or tab. Project-Id-Version: StatusNet - OpenExternalLinkTarget
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:52+0000
Language-Team: French <http://translatewiki.net/wiki/Portal:fr>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-20 17:54:49+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: fr
X-Message-Group: #out-statusnet-plugin-openexternallinktarget
Plural-Forms: nplurals=2; plural=(n > 1);
 Ouvre les liens externes (p. ex., avec rel=external) dans une nouvelle fenêtre ou un nouvel onglet. 