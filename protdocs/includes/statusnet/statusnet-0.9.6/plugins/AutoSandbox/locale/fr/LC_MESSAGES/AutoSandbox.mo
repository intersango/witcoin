��          <      \       p   1   q   \   �   �      M  �  D   �  �   5  �   �                   Automatically sandboxes newly registered members. Note you will initially be "sandboxed" so your posts will not appear in the public timeline. Note you will initially be "sandboxed" so your posts will not appear in the public timeline. Send a message to $contactlink to speed up the unsandboxing process. Project-Id-Version: StatusNet - AutoSandbox
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:13+0000
Language-Team: French <http://translatewiki.net/wiki/Portal:fr>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:04+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: fr
X-Message-Group: #out-statusnet-plugin-autosandbox
Plural-Forms: nplurals=2; plural=(n > 1);
 Place automatiquement les nouveaux membres dans une boîte à sable. Notez que vous serez initialement placé dans un « bac à sable », ce qui signifie que vos messages n’apparaîtront pas dans le calendrier public. Notez que vous serez initialement placé dans un « bac à sable », ce qui signifie que vos messages n’apparaîtront pas dans le calendrier public. Envoyez un message à $contactlink pour accélérer le processus de sortie du bac à sable. 