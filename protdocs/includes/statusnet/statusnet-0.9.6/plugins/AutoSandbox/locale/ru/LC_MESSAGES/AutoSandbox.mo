��          <      \       p   1   q   \   �   �      �  �  u   N  �   �  k  �                   Automatically sandboxes newly registered members. Note you will initially be "sandboxed" so your posts will not appear in the public timeline. Note you will initially be "sandboxed" so your posts will not appear in the public timeline. Send a message to $contactlink to speed up the unsandboxing process. Project-Id-Version: StatusNet - AutoSandbox
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:14+0000
Language-Team: Russian <http://translatewiki.net/wiki/Portal:ru>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:04+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: ru
X-Message-Group: #out-statusnet-plugin-autosandbox
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 Автоматически отсылает всех новых пользователей в «песочницу». Обратите внимание, что сначала вы будете находиться в «песочнице», так что ваши сообщения не будут появляться в общей ленте. Обратите внимание, что сначала вы будете находиться в «песочнице», так что ваши сообщения не будут появляться в общей ленте. Отправьте сообщение на адрес $contactlink, чтобы ускорить перевод из песочницы. 