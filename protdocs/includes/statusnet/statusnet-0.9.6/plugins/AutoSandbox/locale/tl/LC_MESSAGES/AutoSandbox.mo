��          <      \       p   1   q   \   �   �      O  �  @   �  y   3  �   �                   Automatically sandboxes newly registered members. Note you will initially be "sandboxed" so your posts will not appear in the public timeline. Note you will initially be "sandboxed" so your posts will not appear in the public timeline. Send a message to $contactlink to speed up the unsandboxing process. Project-Id-Version: StatusNet - AutoSandbox
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:15+0000
Language-Team: Tagalog <http://translatewiki.net/wiki/Portal:tl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:04+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: tl
X-Message-Group: #out-statusnet-plugin-autosandbox
Plural-Forms: nplurals=2; plural=(n != 1);
 Kusang ikinakahon ng buhangin ang bagong nagpatalang mga kasapi. Taandan na "ikakahon ng buhangin" ka muna kaya't ang mga pagpapaskil mo ay hindi lilitaw sa pangmadlang guhit ng panahon. Taandan na "ikakahon ng buhangin" ka muna kaya't ang mga pagpapaskil mo ay hindi lilitaw sa pangmadlang guhit ng panahon.  Magpadala ng mensahe sa $contactlink upang mapabilis ang proseso ng hindi pagkakahong pambuhangin. 