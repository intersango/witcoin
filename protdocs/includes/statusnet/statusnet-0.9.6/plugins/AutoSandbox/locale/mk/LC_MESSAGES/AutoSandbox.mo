��          <      \       p   1   q   \   �   �      g  �  j   
  �   u  y  S                   Automatically sandboxes newly registered members. Note you will initially be "sandboxed" so your posts will not appear in the public timeline. Note you will initially be "sandboxed" so your posts will not appear in the public timeline. Send a message to $contactlink to speed up the unsandboxing process. Project-Id-Version: StatusNet - AutoSandbox
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:14+0000
Language-Team: Macedonian <http://translatewiki.net/wiki/Portal:mk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:04+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: mk
X-Message-Group: #out-statusnet-plugin-autosandbox
Plural-Forms: nplurals=2; plural=(n == 1 || n%10 == 1) ? 0 : 1;
 Автоматски става новорегистрираните членови во песочник. Напомена: во прво време ќе бидете ставени во песочникот, па така Вашите објави нема да фигурираат во јавната хронологија. Напомена: во прво време ќе бидете ставени во песочникот, па така Вашите објави нема да фигурираат во јавната хронологија.
Испратете порака на $contactlink за да ја забрзате постапката за излегување од песочникот. 