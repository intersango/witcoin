��          <      \       p   1   q   \   �   �      ]  �  -      i   .  �   �                   Automatically sandboxes newly registered members. Note you will initially be "sandboxed" so your posts will not appear in the public timeline. Note you will initially be "sandboxed" so your posts will not appear in the public timeline. Send a message to $contactlink to speed up the unsandboxing process. Project-Id-Version: StatusNet - AutoSandbox
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:15+0000
Language-Team: Simplified Chinese <http://translatewiki.net/wiki/Portal:zh-hans>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:04+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: zh-hans
X-Message-Group: #out-statusnet-plugin-autosandbox
Plural-Forms: nplurals=1; plural=0;
 自动将新注册用户添加至沙盒中。 注意：最初你将被添加至“沙盒”中，你的消息将不会在公共的时间线上显示。 注意：最初你将被添加至“沙盒”中，你的消息将不会在公共的时间线上显示。给$contactlink发消息可以加快你被移出沙盒的速度。 