��          <      \       p   1   q   \   �   �      M  �  S   �     D  �   �                   Automatically sandboxes newly registered members. Note you will initially be "sandboxed" so your posts will not appear in the public timeline. Note you will initially be "sandboxed" so your posts will not appear in the public timeline. Send a message to $contactlink to speed up the unsandboxing process. Project-Id-Version: StatusNet - AutoSandbox
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:14+0000
Language-Team: Dutch <http://translatewiki.net/wiki/Portal:nl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:04+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: nl
X-Message-Group: #out-statusnet-plugin-autosandbox
Plural-Forms: nplurals=2; plural=(n != 1);
 Voegt nieuwe gebruikers automatisch toe aan een groep met beperkte functionaliteit. Let op: In eerste instantie worden uw mogelijkheden beperkt, dus uw mededelingen worden niet zichtbaar in de publieke tijdlijn. Let op: In eerste instantie worden uw mogelijkheden beperkt, dus uw mededelingen worden niet zichtbaar in de publieke tijdlijn. Stuur een bericht naar $contactlink om het proces te versnellen. 