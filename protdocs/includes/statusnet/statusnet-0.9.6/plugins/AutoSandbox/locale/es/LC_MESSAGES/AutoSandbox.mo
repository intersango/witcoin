��          <      \       p   1   q   \   �   �      O  �  M   �  �   @  �   �                   Automatically sandboxes newly registered members. Note you will initially be "sandboxed" so your posts will not appear in the public timeline. Note you will initially be "sandboxed" so your posts will not appear in the public timeline. Send a message to $contactlink to speed up the unsandboxing process. Project-Id-Version: StatusNet - AutoSandbox
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:13+0000
Language-Team: Spanish <http://translatewiki.net/wiki/Portal:es>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:04+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: es
X-Message-Group: #out-statusnet-plugin-autosandbox
Plural-Forms: nplurals=2; plural=(n != 1);
 Envía automáticamente a zona de pruebas a los usuarios recién registrados. Ten en cuenta que inicialmente serás enviado a la zona de pruebas, así que tus mensajes no aparecerán en la línea temporal pública. Ten en cuenta que inicialmente serás enviado a la zona de pruebas, así que tus mensajes no aparecerán en la línea temporal pública. Envía un mensaje a $contactlink para acelerar el proceso de exclusión de la zona de pruebas. 