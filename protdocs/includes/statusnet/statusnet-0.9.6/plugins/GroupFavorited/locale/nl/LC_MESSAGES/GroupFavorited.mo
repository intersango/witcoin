��          L      |       �      �   &   �      �   #   �   ;     S  W     �  1   �  "   �  %   	  L   /                                         MENUPopular Popular posts in %1$s group, page %2$d Popular posts in %s group TOOLTIPPopular notices in %s group This plugin adds a menu item for popular notices in groups. Project-Id-Version: StatusNet - GroupFavorited
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:41+0000
Language-Team: Dutch <http://translatewiki.net/wiki/Portal:nl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:53+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: nl
X-Message-Group: #out-statusnet-plugin-groupfavorited
Plural-Forms: nplurals=2; plural=(n != 1);
 Populair Populaire berichten in de groep %1$s, pagina %2$d Populaire berichten in de groep %s Populaire mededelingen in de groep %s Deze plug-in voegt een menukeuze toe voor populaire mededelingen in groepen. 