��          L      |       �      �   &   �      �   #   �   ;     S  W  
   �  2   �  %   �  !     Z   1                                         MENUPopular Popular posts in %1$s group, page %2$d Popular posts in %s group TOOLTIPPopular notices in %s group This plugin adds a menu item for popular notices in groups. Project-Id-Version: StatusNet - GroupFavorited
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:41+0000
Language-Team: French <http://translatewiki.net/wiki/Portal:fr>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:53+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: fr
X-Message-Group: #out-statusnet-plugin-groupfavorited
Plural-Forms: nplurals=2; plural=(n > 1);
 Populaires Messages populaires dans le groupe %1$s, page %2$d Messages populaires dans le groupe %s Avis populaires dans le groupe %s Cette extension ajoute un élément de menu pour les messages populaires dans les groupes. 