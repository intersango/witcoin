��          t      �         E        W     j  4   n     �     �     �     �        y   >  i  �  ]   "     �     �  6   �     �  $   �          .     G  Q   c                                              
      	        (Have an account with CAS? Try our [CAS login](%%action.caslogin%%)!) Already logged in. CAS Error setting user. You are probably not authorized. Incorrect username or password. Login or register with CAS. Specifying a path is required. Specifying a port is required. Specifying a server is required. The CAS Authentication plugin allows for StatusNet to handle authentication through CAS (Central Authentication Service). Project-Id-Version: StatusNet - CasAuthentication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:23+0000
Language-Team: Simplified Chinese <http://translatewiki.net/wiki/Portal:zh-hans>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:33:30+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: zh-hans
X-Message-Group: #out-statusnet-plugin-casauthentication
Plural-Forms: nplurals=1; plural=0;
  (已有中央鉴权服务帐号？尝试使用 [中央鉴权登录](%%action.caslogin%%)！) 已登录。 中央鉴权服务 设置用户时出错。你可能没有通过鉴权。 用户名或密码错误 登录或注册到中央鉴权服务 需要指定一个路径 需要指定一个端口 需要指定一个服务器 中央鉴权插件可以使StatusNet使用中央鉴权服务进行登录鉴权。 