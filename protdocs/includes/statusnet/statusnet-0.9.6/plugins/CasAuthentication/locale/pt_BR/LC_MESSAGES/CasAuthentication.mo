��          L      |       �      �   4   �      �            0  m  Q     �  O   �  )   &  %   P  '   v                                         Already logged in. Error setting user. You are probably not authorized. Incorrect username or password. Specifying a port is required. Specifying a server is required. Project-Id-Version: StatusNet - CasAuthentication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:23+0000
Language-Team: Brazilian Portuguese <http://translatewiki.net/wiki/Portal:pt-br>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:33:30+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: pt-br
X-Message-Group: #out-statusnet-plugin-casauthentication
Plural-Forms: nplurals=2; plural=(n > 1);
 Já está autenticado. Erro na configuração do usuário. Você provavelmente não tem autorização. Nome de usuário e/ou senha incorreto(s). É necessário especificar uma porta. É necessário especificar um servidor. 