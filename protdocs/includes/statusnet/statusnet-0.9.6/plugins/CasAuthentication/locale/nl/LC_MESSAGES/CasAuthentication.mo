��          t      �         E        W     j  4   n     �     �     �     �        y   >  Y  �  L        _     t  j   x  +   �  !     $   1  &   V  '   }  |   �                                              
      	        (Have an account with CAS? Try our [CAS login](%%action.caslogin%%)!) Already logged in. CAS Error setting user. You are probably not authorized. Incorrect username or password. Login or register with CAS. Specifying a path is required. Specifying a port is required. Specifying a server is required. The CAS Authentication plugin allows for StatusNet to handle authentication through CAS (Central Authentication Service). Project-Id-Version: StatusNet - CasAuthentication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:23+0000
Language-Team: Dutch <http://translatewiki.net/wiki/Portal:nl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:33:30+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: nl
X-Message-Group: #out-statusnet-plugin-casauthentication
Plural-Forms: nplurals=2; plural=(n != 1);
 Hebt u een gebruiker met CAS? [Meld u dan aan met CAS](%%action.caslogin%%)! U bent al aangemeld. CAS Er is een fout opgetreden bij het maken van de instellingen. U hebt waarschijnlijk niet de juiste rechten. De gebruikersnaam of wachtwoord is onjuist. Aanmelden of registreren via CAS. Het aangeven van een pad is vereist. Het aangeven van een poort is vereist. Het aangeven van een server is vereist. De plugin CAS Authentication stelt StatusNet in staat authenticatie via CAS after handelen (Central Authentication Service). 