��          t      �         E        W     j  4   n     �     �     �     �        y   >  Y  �  ]        p  *   �  \   �  .   
  (   9  ;   b  .   �  1   �  �   �                                              
      	        (Have an account with CAS? Try our [CAS login](%%action.caslogin%%)!) Already logged in. CAS Error setting user. You are probably not authorized. Incorrect username or password. Login or register with CAS. Specifying a path is required. Specifying a port is required. Specifying a server is required. The CAS Authentication plugin allows for StatusNet to handle authentication through CAS (Central Authentication Service). Project-Id-Version: StatusNet - CasAuthentication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:23+0000
Language-Team: French <http://translatewiki.net/wiki/Portal:fr>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:33:30+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: fr
X-Message-Group: #out-statusnet-plugin-casauthentication
Plural-Forms: nplurals=2; plural=(n > 1);
 (Vous avez un compte authentifié SAC ? Essayez notre [connexion SAC](%%action.caslogin%%) !) Déjà connecté. Service d’authentification central (SAC) Erreur lors de la définition de l’utilisateur. Vous n’êtes probablement pas autorisé. Nom d’utilisateur ou mot de passe incorrect. Se connecter ou s’inscrire via le SAC. La spécification d’un chemin d’accès est nécessaire. La spécification d’un port est nécessaire. La spécification d’un serveur est nécessaire. Le greffon d’authentification SAC permet à StatusNet de gérer l’authentification par SAC (Service central d’authentification). 