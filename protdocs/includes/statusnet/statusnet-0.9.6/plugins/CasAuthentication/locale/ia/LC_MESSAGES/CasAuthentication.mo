��          t      �         E        W     j  4   n     �     �     �     �        y   >  _  �  Q        j     �  I   �  *   �  %   �  #   !  !   E  $   g  �   �                                              
      	        (Have an account with CAS? Try our [CAS login](%%action.caslogin%%)!) Already logged in. CAS Error setting user. You are probably not authorized. Incorrect username or password. Login or register with CAS. Specifying a path is required. Specifying a port is required. Specifying a server is required. The CAS Authentication plugin allows for StatusNet to handle authentication through CAS (Central Authentication Service). Project-Id-Version: StatusNet - CasAuthentication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:23+0000
Language-Team: Interlingua <http://translatewiki.net/wiki/Portal:ia>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:33:30+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: ia
X-Message-Group: #out-statusnet-plugin-casauthentication
Plural-Forms: nplurals=2; plural=(n != 1);
 (Tu ha un conto de CAS? Essaya nostre [authentication CAS](%%action.caslogin%%)!) Tu es jam authenticate. CAS Error de acceder al conto de usator. Tu probabilemente non es autorisate. Nomine de usator o contrasigno incorrecte. Aperir session o crear conto via CAS. Specificar un cammino es necessari. Specificar un porto es necessari. Specificar un servitor es necessari. Le plug-in de authentication CAS permitte que StatusNet manea le authentication via CAS (Central Authentication Service, servicio central de authentication). 