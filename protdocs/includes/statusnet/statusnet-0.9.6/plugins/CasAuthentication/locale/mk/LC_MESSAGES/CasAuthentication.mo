��          t      �         E        W     j  4   n     �     �     �     �        y   >  s  �  t   ,  !   �     �  |   �  P   D  6   �  /   �  -   �  7   *  �   b                                              
      	        (Have an account with CAS? Try our [CAS login](%%action.caslogin%%)!) Already logged in. CAS Error setting user. You are probably not authorized. Incorrect username or password. Login or register with CAS. Specifying a path is required. Specifying a port is required. Specifying a server is required. The CAS Authentication plugin allows for StatusNet to handle authentication through CAS (Central Authentication Service). Project-Id-Version: StatusNet - CasAuthentication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:23+0000
Language-Team: Macedonian <http://translatewiki.net/wiki/Portal:mk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:33:30+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: mk
X-Message-Group: #out-statusnet-plugin-casauthentication
Plural-Forms: nplurals=2; plural=(n == 1 || n%10 == 1) ? 0 : 1;
 (Имате сметка на CAS? Пробајте ја нашата [најава со CAS](%%action.caslogin%%)!) Веќе сте најавени. CAS Грешка при поставувањето на корисникот. Веројатно не сте потврдени. Мора да се назначи корисничко име и лозинка. Најава или регистрација со CAS. Мора да се назначи патека. Мора да се назначи порта. Мора да се назначи опслужувач. Приклучокот за потврда CAS му овозможува на StatusNet да работи со потврди преку CAS (Central Authentication Service - „Служба за централно потврдување“). 