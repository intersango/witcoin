��          t      �         E        W     j  4   n     �     �     �     �        y   >  [  �  [        p     �  E   �  ,   �  %   �  !   %  $   G  $   l  �   �                                              
      	        (Have an account with CAS? Try our [CAS login](%%action.caslogin%%)!) Already logged in. CAS Error setting user. You are probably not authorized. Incorrect username or password. Login or register with CAS. Specifying a path is required. Specifying a port is required. Specifying a server is required. The CAS Authentication plugin allows for StatusNet to handle authentication through CAS (Central Authentication Service). Project-Id-Version: StatusNet - CasAuthentication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:23+0000
Language-Team: Spanish <http://translatewiki.net/wiki/Portal:es>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:33:30+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: es
X-Message-Group: #out-statusnet-plugin-casauthentication
Plural-Forms: nplurals=2; plural=(n != 1);
 (¿Tienes una cuenta con CAS? Prueba nuestro [Inicio de usuario CAS](%%action.caslogin%%)!) Ya has iniciado sesión CAS Error al configurar el usuario. Posiblemente no tengas autorización. Nombre de usuario o contraseña incorrectos. Inicia sesión o regístrate con CAS. Se requiere especificar una ruta. Se requiere especificar un servidor. Se requiere especificar un servidor. La extensión de Autenticación CAS permite a StatusNet manejar autenticación a través de CAS (Servicio de Autenticación Central). 