��    	      d      �       �      �      �        	        &     *  f   8     �  `  �          /     <     M     ]     b  f   u     �                       	                       %1$s friends map, page %2$d %s friends map %s map, page %d Full size Map No such user. Show maps of users' and friends' notices with <a href="http://www.mapstraction.com/">Mapstraction</a>. User has no profile. Project-Id-Version: StatusNet - Mapstraction
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:47+0000
Language-Team: Norwegian (bokmål)‬ <http://translatewiki.net/wiki/Portal:no>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:55+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: no
X-Message-Group: #out-statusnet-plugin-mapstraction
Plural-Forms: nplurals=2; plural=(n != 1);
 %1$s vennekart, side %2$d %s vennekart %s kart, side %d Full størrelse Kart Ingen slik bruker. Vis kart over brukeres og venners notiser med <a href="http://www.mapstraction.com/">Mapstraction</a>. Bruker har ingen profil. 