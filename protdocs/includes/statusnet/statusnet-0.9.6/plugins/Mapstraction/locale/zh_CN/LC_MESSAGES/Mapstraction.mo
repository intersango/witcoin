��    	      d      �       �      �      �        	        &     *  f   8     �  _  �  #        8     H     _     l     s  f   �     �                       	                       %1$s friends map, page %2$d %s friends map %s map, page %d Full size Map No such user. Show maps of users' and friends' notices with <a href="http://www.mapstraction.com/">Mapstraction</a>. User has no profile. Project-Id-Version: StatusNet - Mapstraction
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:47+0000
Language-Team: Simplified Chinese <http://translatewiki.net/wiki/Portal:zh-hans>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:55+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: zh-hans
X-Message-Group: #out-statusnet-plugin-mapstraction
Plural-Forms: nplurals=1; plural=0;
 %1$s 好友地图，第 %2$d 页。 %s 好友地图 %s 地图，第 %d 页 完整尺寸 地图 没有这个用户。 使用 <a href="http://www.mapstraction.com/">Mapstraction</a> 显示用户和好友的消息地图。 用户没有个人信息。 