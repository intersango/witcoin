��    	      d      �       �      �      �        	        &     *  f   8     �  P  �  &        ,     E     X     j     p  r   �     �                       	                       %1$s friends map, page %2$d %s friends map %s map, page %d Full size Map No such user. Show maps of users' and friends' notices with <a href="http://www.mapstraction.com/">Mapstraction</a>. User has no profile. Project-Id-Version: StatusNet - Mapstraction
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:46+0000
Language-Team: German <http://translatewiki.net/wiki/Portal:de>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:55+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: de
X-Message-Group: #out-statusnet-plugin-mapstraction
Plural-Forms: nplurals=2; plural=(n != 1);
 Karte der Freunde von %1$s, Seite %2$d Karte der Freunde von %s %s-Karte, Seite %d in voller Größe Karte Unbekannter Benutzer. Zeigt Karten mit Nachrichten von Freunden und Nutzern mit <a href="http://www.mapstraction.com/">Mapstraction</a>. Benutzer hat kein Profil. 