��    	      d      �       �      �      �        	        &     *  f   8     �  O  �  (        -     F     ^     e     k  �   �  "                          	                       %1$s friends map, page %2$d %s friends map %s map, page %d Full size Map No such user. Show maps of users' and friends' notices with <a href="http://www.mapstraction.com/">Mapstraction</a>. User has no profile. Project-Id-Version: StatusNet - Mapstraction
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:47+0000
Language-Team: Dutch <http://translatewiki.net/wiki/Portal:nl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:55+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: nl
X-Message-Group: #out-statusnet-plugin-mapstraction
Plural-Forms: nplurals=2; plural=(n != 1);
 Kaart van vrienden van %1$s, pagina %2$d Kaart van %s en vrienden Kaart van %s, pagina %d Groter Kaart Deze gebruiker bestaat niet Geeft een kaart met de mededelingen van de gebruiker en vrienden weer met behulp van <a href="http://www.mapstraction.com/">Mapstraction</a>. Deze gebruiker heeft geen profiel. 