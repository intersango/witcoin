��    	      d      �       �      �      �        	        &     *  f   8     �  i  �  C     *   b     �     �  
   �  %   �  �   �  +   �                       	                       %1$s friends map, page %2$d %s friends map %s map, page %d Full size Map No such user. Show maps of users' and friends' notices with <a href="http://www.mapstraction.com/">Mapstraction</a>. User has no profile. Project-Id-Version: StatusNet - Mapstraction
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:47+0000
Language-Team: Macedonian <http://translatewiki.net/wiki/Portal:mk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:55+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: mk
X-Message-Group: #out-statusnet-plugin-mapstraction
Plural-Forms: nplurals=2; plural=(n == 1 || n%10 == 1) ? 0 : 1;
 Карта на пријатели на %1$s, страница %2$d Карта на пријатели на %s Карта на %s, стр. %d Полна големина Карта Нема таков корисник. Прикажувај карти со забелешките на корисниците и пријателите со <a href="http://www.mapstraction.com/">Mapstraction</a> Корисникот нема профил. 