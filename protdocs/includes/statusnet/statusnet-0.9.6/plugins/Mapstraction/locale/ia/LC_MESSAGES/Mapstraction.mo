��    	      d      �       �      �      �        	        &     *  f   8     �  U  �  $   
     /     F     ]     p     v  i   �     �                       	                       %1$s friends map, page %2$d %s friends map %s map, page %d Full size Map No such user. Show maps of users' and friends' notices with <a href="http://www.mapstraction.com/">Mapstraction</a>. User has no profile. Project-Id-Version: StatusNet - Mapstraction
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:47+0000
Language-Team: Interlingua <http://translatewiki.net/wiki/Portal:ia>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:55+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: ia
X-Message-Group: #out-statusnet-plugin-mapstraction
Plural-Forms: nplurals=2; plural=(n != 1);
 Mappa de amicos de %1$s, pagina %2$d Mappa del amicos de %s Mappa de %s, pagina %d Dimension complete Mappa Iste usator non existe. Monstra mappas de notas de usatores e amicos con <a href="http://www.mapstraction.com/">Mapstraction</a>. Le usator non ha un profilo. 