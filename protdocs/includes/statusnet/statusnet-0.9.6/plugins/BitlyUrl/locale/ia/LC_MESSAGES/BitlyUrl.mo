��          �      �       H     I     Q  >   ]  .   �  ,   �  4   �  
   -     8  �   M  ;   %  4   a     �     �  M  �  	          C   #  9   g  @   �  I   �     ,  &   <  �   c  7   U  9   �     �     �     	                               
                             API key Credentials If you leave these empty, bit.ly will be unavailable to users. Invalid API key. Max length is 255 characters. Invalid login. Max length is 255 characters. Leave these empty to use global default credentials. Login name Save bit.ly settings URL shortening with bit.ly requires [a bit.ly account and API key](http://bit.ly/a/your_api_key). This verifies that this is an authorized account, and allow you to use bit.ly's tracking features and custom domains. Uses <a href="http://%1$s/">%1$s</a> URL-shortener service. You must specify a serviceUrl for bit.ly shortening. bit.ly bit.ly URL shortening Project-Id-Version: StatusNet - BitlyUrl
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:16+0000
Language-Team: Interlingua <http://translatewiki.net/wiki/Portal:ia>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-20 17:58:21+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: ia
X-Message-Group: #out-statusnet-plugin-bitlyurl
Plural-Forms: nplurals=2; plural=(n != 1);
 Clave API Datos de authentication Si tu lassa istes vacue, bit.ly non essera disponibile al usatores. Clave API invalide. Longitude maximal es 255 characteres. Nomine de usator invalide. Longitude maximal es 255 characteres. Lassa istes vacue pro usar le datos de authentication global predefinite. Nomine de conto Salveguardar configurationes de bit.ly Le accurtamento de URL con bit.ly require [un conto de bit.ly e un clave API](http://bit.ly/a/your_api_key). Isto verifica que isto es un conto autorisate, e permitte usar le functionalitate de traciamento e dominios personalisate de bit.ly. Usa abbreviator de URL <a href="http://%1$s/">%1$s</a>. Tu debe specificar un serviceUrl pro accurtamento bit.ly. bit.ly Accurtamento de URL con bit.ly 