��          �      �       H     I     Q  >   ]  .   �  ,   �  4   �  
   -     8  �   M  ;   %  4   a     �     �  �  �     [     h  �   �  ^   "  \   �  �   �  
   �  0   �  �  �  d   N
  w   �
     +  *   2     	                               
                             API key Credentials If you leave these empty, bit.ly will be unavailable to users. Invalid API key. Max length is 255 characters. Invalid login. Max length is 255 characters. Leave these empty to use global default credentials. Login name Save bit.ly settings URL shortening with bit.ly requires [a bit.ly account and API key](http://bit.ly/a/your_api_key). This verifies that this is an authorized account, and allow you to use bit.ly's tracking features and custom domains. Uses <a href="http://%1$s/">%1$s</a> URL-shortener service. You must specify a serviceUrl for bit.ly shortening. bit.ly bit.ly URL shortening Project-Id-Version: StatusNet - BitlyUrl
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:16+0000
Language-Team: Ukrainian <http://translatewiki.net/wiki/Portal:uk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-20 17:58:21+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: uk
X-Message-Group: #out-statusnet-plugin-bitlyurl
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 API-ключ Повноваження Якщо ви залишите це поле порожнім, сервіс bit.ly стане недоступним для інших користувачів. Невірний API-ключ. Максимальна довжина — 255 символів. Невірний лоґін. Максимальна довжина — 255 символів. Залиште це поле порожнім, щоб користуватися загальними повноваженнями за замовчуванням. Лоґін Зберегти налаштування bit.ly Скорочення URL-адрес за допомогою bit.ly вимагає [акаунт bit.ly та API-ключ](http://bit.ly/a/your_api_key). Це підтвердить те, що даний акаунт є авторизованим і дозволить користуватися функцією відстеження bit.ly, а також доменами користувачів. Використання <a href="http://%1$s/">%1$s</a> для скорочення URL-адрес. Ви мусите зазначити URL-адресу для сервісу скорочення URL-адрес bit.ly. bit.ly Скорочення URL-адрес bit.ly 