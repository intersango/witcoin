��          �      �       0     1     9  >   E  .   �  ,   �  4   �  
           �   5  ;        I     P  X  f     �     �  G   �  -     ,   J  ;   w     �     �  �   �  >   �     �          
                                       	              API key Credentials If you leave these empty, bit.ly will be unavailable to users. Invalid API key. Max length is 255 characters. Invalid login. Max length is 255 characters. Leave these empty to use global default credentials. Login name Save bit.ly settings URL shortening with bit.ly requires [a bit.ly account and API key](http://bit.ly/a/your_api_key). This verifies that this is an authorized account, and allow you to use bit.ly's tracking features and custom domains. Uses <a href="http://%1$s/">%1$s</a> URL-shortener service. bit.ly bit.ly URL shortening Project-Id-Version: StatusNet - BitlyUrl
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:16+0000
Language-Team: Norwegian (bokmål)‬ <http://translatewiki.net/wiki/Portal:no>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-20 17:58:21+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: no
X-Message-Group: #out-statusnet-plugin-bitlyurl
Plural-Forms: nplurals=2; plural=(n != 1);
 API-nøkkel Attester Om du lar disse være tomme vil bit.ly være utilgjengelig for brukere. Ugyldig API-nøkkel. Maks lengde er 255 tegn. Ugyldig pålogging. Maks lengde er 255 tegn. La disse være tomme for å bruke globale standardattester. Innloggingsnavn Lagre bit.ly-innstillinger URL-forkortelse med bit.ly krever [en bit.ly-konto og API-nøkkel](http://bit.ly/a/your_api_key). Denne bekrefter at dette er en autorisert konto og tillater deg å bruke bit.lys sporingsfunksjoner og egendefinerte domener. Bruker URL-forkortertjenesten <a href="http://%1$s/">%1$s</a>. bit.ly bit.ly URL-forkortelse 