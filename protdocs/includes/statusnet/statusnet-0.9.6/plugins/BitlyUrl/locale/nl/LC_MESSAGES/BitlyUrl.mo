��          �      �       H     I     Q  >   ]  .   �  ,   �  4   �  
   -     8  �   M  ;   %  4   a     �     �  G  �     �       N     =   i  <   �  D   �     )     8  t   T  L   �  ?        V     ]     	                               
                             API key Credentials If you leave these empty, bit.ly will be unavailable to users. Invalid API key. Max length is 255 characters. Invalid login. Max length is 255 characters. Leave these empty to use global default credentials. Login name Save bit.ly settings URL shortening with bit.ly requires [a bit.ly account and API key](http://bit.ly/a/your_api_key). This verifies that this is an authorized account, and allow you to use bit.ly's tracking features and custom domains. Uses <a href="http://%1$s/">%1$s</a> URL-shortener service. You must specify a serviceUrl for bit.ly shortening. bit.ly bit.ly URL shortening Project-Id-Version: StatusNet - BitlyUrl
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:16+0000
Language-Team: Dutch <http://translatewiki.net/wiki/Portal:nl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-20 17:58:21+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: nl
X-Message-Group: #out-statusnet-plugin-bitlyurl
Plural-Forms: nplurals=2; plural=(n != 1);
 API-sleutel Gebruikersgegevens Als u deze velden oningevuld laat, is bit.ly niet beschikbaar voor gebruikers. De API-sleutel is ongeldig. De maximale lengte is 255 tekens. Ongeldige aanmeldgegevens. De maximale lengte is 255 tekens. Laat deze leeg om globale standaard gebruikersgegevens te gebruiken. Gebruikersnaam bit.ly-instellingen opslaan Het inkorten van URL's via bit.ly vereist een [account bij bit.ly en een API-sleutel](http://bit.ly/a/your_api_key). Gebruikt de dienst <a href="http://%1$s/">%1$s</a> om URL's korter te maken. U moet een serviceUrl opgeven om URL's in te korten via bit.ly. bit.ly URL's inkorten via bit.ly 