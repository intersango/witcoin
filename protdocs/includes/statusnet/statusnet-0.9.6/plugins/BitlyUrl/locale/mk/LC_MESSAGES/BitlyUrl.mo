��          �      �       H     I     Q  >   ]  .   �  ,   �  4   �  
   -     8  �   M  ;   %  4   a     �     �  a  �          "  x   5  W   �  f     �   m     �  /     �  @  n   �	  V   G
     �
  )   �
     	                               
                             API key Credentials If you leave these empty, bit.ly will be unavailable to users. Invalid API key. Max length is 255 characters. Invalid login. Max length is 255 characters. Leave these empty to use global default credentials. Login name Save bit.ly settings URL shortening with bit.ly requires [a bit.ly account and API key](http://bit.ly/a/your_api_key). This verifies that this is an authorized account, and allow you to use bit.ly's tracking features and custom domains. Uses <a href="http://%1$s/">%1$s</a> URL-shortener service. You must specify a serviceUrl for bit.ly shortening. bit.ly bit.ly URL shortening Project-Id-Version: StatusNet - BitlyUrl
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:16+0000
Language-Team: Macedonian <http://translatewiki.net/wiki/Portal:mk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-20 17:58:21+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: mk
X-Message-Group: #out-statusnet-plugin-bitlyurl
Plural-Forms: nplurals=2; plural=(n == 1 || n%10 == 1) ? 0 : 1;
 API-клуч Уверенија Ако ова го оставите празно, bit.ly ќе биде недостапен за корисниците. Неважечки API-клуч. Дозволени се највеќе 255 знаци. Неважечко корисничко име. Дозволени се највеќе 255 знаци. Оставете го ова празно за да го користите глобалното уверение по основно. Корисничко име Зачувај нагодувања на bit.ly Скратувањето на URL-адреси со bit.ly бара [сметка и API-клуч за bit.ly](http://bit.ly/a/your_api_key). Со ова се потврдува дека ова е овластена сметка, и Ви овозможува да ги користите можностите за следење и прилагодување на домени што ги нуди bit.ly's. Користи <a href="http://%1$s/">%1$s</a> - служба за скратување на URL-адреса. Мора да наведете URL-адреса за скратување со bit.ly. bit.ly Скратување на URL со bit.ly 