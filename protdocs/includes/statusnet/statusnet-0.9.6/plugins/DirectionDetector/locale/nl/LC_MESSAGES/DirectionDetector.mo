��          ,      <       P   >   Q   �  �   i   6                     Shows notices with right-to-left content in correct direction. Project-Id-Version: StatusNet - DirectionDetector
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:25+0000
Last-Translator: Siebrand Mazeland <s.mazeland@xs4all.nl>
Language-Team: Dutch <http://translatewiki.net/wiki/Portal:nl>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-POT-Import-Date: 2010-10-19 23:49:52+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: nl
X-Message-Group: #out-statusnet-plugin-directiondetector
 Geeft mededelingen met inhoud in een van rechts naar links geschreven schrift in de juiste richting weer. 