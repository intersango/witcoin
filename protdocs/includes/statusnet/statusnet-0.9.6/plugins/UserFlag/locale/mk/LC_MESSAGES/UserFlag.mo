��          �      ,      �     �     �     �  &   �  &   �     �     �          (  >   0     o     }     �     �     �  R   �  a       m  +   ~     �  ]   �  W        u  '   �  .   �     �  k   �     V  $   o     �  *   �     �  �   �                                               	                           
    Clear Clear all flags Cleared Couldn't clear flags for profile "%s". Couldn't flag profile "%d" for review. Flag Flag already exists. Flag profile for review. Flagged Flagged by %1$s and %2$d other Flagged by %1$s and %2$d others Flagged by %s Flagged for review Flagged profiles Flags cleared Moderate This plugin allows flagging of profiles for review and reviewing flagged profiles. Project-Id-Version: StatusNet - UserFlag
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:54+0000
Language-Team: Macedonian <http://translatewiki.net/wiki/Portal:mk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 19:01:50+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: mk
X-Message-Group: #out-statusnet-plugin-userflag
Plural-Forms: nplurals=2; plural=(n == 1 || n%10 == 1) ? 0 : 1;
 Отстрани Отстрани ги сите ознаки Отстрането Не можев да ги отстранам ознаките за профилот „%s“. Не можев да го означам профилот „%d“ за преглед. Означи Ознаката веќе постои. Означи профил за преглед. Означено Означено од %1$s и уште %2$d друг Означено од %1$s и уште %2$d други Означено од %s Означено за преглед Означени профили Ознаките се отстранети Модерирај Овој приклучок овозможува означување на профили за преглед и прегледување на означени профили. 