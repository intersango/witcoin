��          �      ,      �     �     �     �  &   �  &   �     �     �          (  >   0     o     }     �     �     �  R   �  G       S     [     v  >   ~  <   �     �       %        8  >   @          �     �     �     �  f   �                                               	                           
    Clear Clear all flags Cleared Couldn't clear flags for profile "%s". Couldn't flag profile "%d" for review. Flag Flag already exists. Flag profile for review. Flagged Flagged by %1$s and %2$d other Flagged by %1$s and %2$d others Flagged by %s Flagged for review Flagged profiles Flags cleared Moderate This plugin allows flagging of profiles for review and reviewing flagged profiles. Project-Id-Version: StatusNet - UserFlag
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:54+0000
Language-Team: French <http://translatewiki.net/wiki/Portal:fr>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 19:01:50+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: fr
X-Message-Group: #out-statusnet-plugin-userflag
Plural-Forms: nplurals=2; plural=(n > 1);
 Effacer Effacer tous les marquages Effacé Impossible de supprimer les marquages pour le profil « %s ». Impossible de marquer le profil « %d » pour vérification. Marquer Déjà marqué. Marquer le profil pour vérification. Marqué Marqué par %1$s et %2$d autre Marqué par %1$s et %2$d autres Marqué par %s Marqué pour vérification Profils marqués Marquages supprimés Modérer Cette extension permet de marquer des profils pour vérification et de vérifier des profils marqués. 