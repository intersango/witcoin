��          �      ,      �     �     �     �  &   �  &   �     �     �          (  >   0     o     }     �     �     �  R   �  M       Y     _     t  )   {  ,   �     �     �     �       >         _     n     �     �     �  M   �                                               	                           
    Clear Clear all flags Cleared Couldn't clear flags for profile "%s". Couldn't flag profile "%d" for review. Flag Flag already exists. Flag profile for review. Flagged Flagged by %1$s and %2$d other Flagged by %1$s and %2$d others Flagged by %s Flagged for review Flagged profiles Flags cleared Moderate This plugin allows flagging of profiles for review and reviewing flagged profiles. Project-Id-Version: StatusNet - UserFlag
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:54+0000
Language-Team: Interlingua <http://translatewiki.net/wiki/Portal:ia>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 19:01:50+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: ia
X-Message-Group: #out-statusnet-plugin-userflag
Plural-Forms: nplurals=2; plural=(n != 1);
 Rader Rader tote le marcas Radite Non poteva rader marcas pro profilo "%s". Non poteva marcar profilo "%d" pro revision. Rader tote le marcas Le marca ja existe. Marcar profilo pro revision. Marcate Marcate per %1$s e %2$d altere Marcate per %1$s e %2$d alteres Marcate per %s Marcate pro revision Profilos marcate Marcas radite Moderar Iste plugin permitte marcar profilos pro revision e revider profilos marcate. 