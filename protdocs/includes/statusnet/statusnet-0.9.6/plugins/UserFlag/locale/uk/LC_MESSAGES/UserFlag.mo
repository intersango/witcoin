��          �      ,      �     �     �     �  &   �  &   �     �     �          (  >   0     o     }     �     �     �  R   �  �    
   �  "   �  
   �  G   �  U   4     �  %   �  :   �     �  �        �  *   �  !        8     T  �   i                                               	                           
    Clear Clear all flags Cleared Couldn't clear flags for profile "%s". Couldn't flag profile "%d" for review. Flag Flag already exists. Flag profile for review. Flagged Flagged by %1$s and %2$d other Flagged by %1$s and %2$d others Flagged by %s Flagged for review Flagged profiles Flags cleared Moderate This plugin allows flagging of profiles for review and reviewing flagged profiles. Project-Id-Version: StatusNet - UserFlag
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:54+0000
Language-Team: Ukrainian <http://translatewiki.net/wiki/Portal:uk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 19:01:50+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: uk
X-Message-Group: #out-statusnet-plugin-userflag
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 Зняти Зняти всі позначки Знято Не можу зняти позначки для профілю «%s». Не вдалося відмітити профіль «%d» для розгляду. Відмітити Відмітка вже стоїть. Відмітити профіль для розгляду. Відмічені Відмічено %1$s та ще %2$d користувачем Відмічено %1$s та ще %2$d користувачами Відмічено %1$s та ще %2$d користувачами Відмічено %s Відмічені для розгляду Відмічені профілі Позначки знято Модерувати Цей додаток дозволяє відмічати профілі користувачів для подальшого розгляду та аналізу відмічених профілів. 