��          �      ,      �     �     �     �  &   �  &   �     �     �          (  >   0     o     }     �     �     �  R   �  G       S     Z  
   r  I   }  A   �     	          +  
   J  H   U     �     �     �     �  	   �  m                                                  	                           
    Clear Clear all flags Cleared Couldn't clear flags for profile "%s". Couldn't flag profile "%d" for review. Flag Flag already exists. Flag profile for review. Flagged Flagged by %1$s and %2$d other Flagged by %1$s and %2$d others Flagged by %s Flagged for review Flagged profiles Flags cleared Moderate This plugin allows flagging of profiles for review and reviewing flagged profiles. Project-Id-Version: StatusNet - UserFlag
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:54+0000
Language-Team: Dutch <http://translatewiki.net/wiki/Portal:nl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 19:01:50+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: nl
X-Message-Group: #out-statusnet-plugin-userflag
Plural-Forms: nplurals=2; plural=(n != 1);
 Wissen Alle markeringen wissen Verwijderd Het was niet mogelijk de markeringen van het profiel "%s" te verwijderen. Het was niet mogelijk het profiel "%d" voor controle te markeren. Markeren De markering bestaat al. Profiel voor controle markeren Gemarkeerd Gemarkeerd door %1$s en %2$d andere Gemarkeerd door %1$s en %2$d anderen Gemarkeerd door %s Gemarkeerd voor controle Gemarkeerde profielen Markeringen verwijderd Modereren Deze plugin maakt het markeren van profielen voor controle mogelijk en de controle van gemarkeerde profielen. 