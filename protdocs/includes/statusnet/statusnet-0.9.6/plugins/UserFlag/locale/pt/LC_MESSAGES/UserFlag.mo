��          �      ,      �     �     �     �  &   �  &   �     �     �          (  >   0     o     }     �     �     �  R   �  L       X     _     ~  ;   �  9   �     �     	     '  
   G  N   R     �     �     �     �     �  Q   �                                               	                           
    Clear Clear all flags Cleared Couldn't clear flags for profile "%s". Couldn't flag profile "%d" for review. Flag Flag already exists. Flag profile for review. Flagged Flagged by %1$s and %2$d other Flagged by %1$s and %2$d others Flagged by %s Flagged for review Flagged profiles Flags cleared Moderate This plugin allows flagging of profiles for review and reviewing flagged profiles. Project-Id-Version: StatusNet - UserFlag
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:54+0000
Language-Team: Portuguese <http://translatewiki.net/wiki/Portal:pt>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 19:01:50+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: pt
X-Message-Group: #out-statusnet-plugin-userflag
Plural-Forms: nplurals=2; plural=(n != 1);
 Limpar Limpar todas as sinalizações Limpas Não foi possível limpar as sinalizações do perfil "%s". Não foi possível sinalizar o perfil "%d" para análise. Sinalização Já existe uma sinalização. Sinalizar perfil para análise. Sinalizado Sinalizado por %1$s e mais %2$d pessoa Sinalizado por %1$s e mais %2$d pessoas Sinalizado por %s Sinalizado para análise Perfis sinalizados Sinalizações limpas Moderar Este plugin permite sinalizar perfis para análise e analisar perfis sinalizados. 