��          �      \      �     �     �     �  (   �  	   &     0     A     M     c     t  0   �  	   �     �     �     �     �  
             (  �  B  #   �             G   5     }     �  
   �     �  '   �  =   �  i   :     �  (   �     �  1   �  ;   '     c     t  "   |                                             
                      	                                Ad script URL AdSense AdSense configuration AdSense settings for this StatusNet site Client ID Google client ID Leaderboard Leaderboard slot code Medium rectangle Medium rectangle slot code Plugin to add Google Adsense to StatusNet sites. Rectangle Rectangle slot code Save Save AdSense settings Script URL (advanced) Skyscraper TITLEAdSense Wide skyscraper slot code Project-Id-Version: StatusNet - Adsense
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:09+0000
Language-Team: Ukrainian <http://translatewiki.net/wiki/Portal:uk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-20 17:58:21+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: uk
X-Message-Group: #out-statusnet-plugin-adsense
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 Адреса скрипту AdSense AdSense Конфігурація AdSense Налаштування AdSense на даному сайті StatusNet ІД клієнта ІД клієнта Google Банер Слот-код банеру Середній прямокутник Слот-код середнього прямокутника Додаток для відображення Google Adsense на сторінці сайту StatusNet. Прямокутник Слот-код прямокутника Зберегти Зберегти налаштування AdSense Адреса скрипту (розширена опція) Хмарочос AdSense Слот-код хмарочосу 