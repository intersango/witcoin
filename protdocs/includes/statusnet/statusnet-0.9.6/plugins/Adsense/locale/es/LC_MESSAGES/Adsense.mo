��          �      \      �     �     �     �  (   �  	   &     0     A     M     c     t  0   �  	   �     �     �     �     �  
             (  G  B     �     �     �  3   �     �          #  $   2     W  )   k  :   �     �  !   �     �  $        +     E     [  1   c                                             
                      	                                Ad script URL AdSense AdSense configuration AdSense settings for this StatusNet site Client ID Google client ID Leaderboard Leaderboard slot code Medium rectangle Medium rectangle slot code Plugin to add Google Adsense to StatusNet sites. Rectangle Rectangle slot code Save Save AdSense settings Script URL (advanced) Skyscraper TITLEAdSense Wide skyscraper slot code Project-Id-Version: StatusNet - Adsense
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:09+0000
Language-Team: Spanish <http://translatewiki.net/wiki/Portal:es>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-20 17:58:21+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: es
X-Message-Group: #out-statusnet-plugin-adsense
Plural-Forms: nplurals=2; plural=(n != 1);
 URL del script del anuncio AdSense Configuración de "AdSense" configuración de AdSense para este sitio StatusNet ID de cliente ID de cliente de Google Clasificación Código de espacio de clasificación Rectángulo mediano Código de espacio de rectángulo mediano Extensión para añadir Google Adsense a sitios StatusNet. Rectángulo Código de espacio de rectángulo Guardar Guardar la configuración de AdSense URL del script (avanzado) Banderola rascacielos AdSense Código de espacio de banderola rascacielos ancha 