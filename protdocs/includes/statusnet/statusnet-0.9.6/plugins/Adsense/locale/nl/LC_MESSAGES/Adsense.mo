��          �      \      �     �     �     �  (   �  	   &     0     A     M     c     t  0   �  	   �     �     �     �     �  
             (  E  B     �     �     �  0   �  	   �     �               <  "   R  ;   u  	   �     �     �     �     �  
             '                                             
                      	                                Ad script URL AdSense AdSense configuration AdSense settings for this StatusNet site Client ID Google client ID Leaderboard Leaderboard slot code Medium rectangle Medium rectangle slot code Plugin to add Google Adsense to StatusNet sites. Rectangle Rectangle slot code Save Save AdSense settings Script URL (advanced) Skyscraper TITLEAdSense Wide skyscraper slot code Project-Id-Version: StatusNet - Adsense
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:09+0000
Language-Team: Dutch <http://translatewiki.net/wiki/Portal:nl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-20 17:58:21+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: nl
X-Message-Group: #out-statusnet-plugin-adsense
Plural-Forms: nplurals=2; plural=(n != 1);
 URL voor advertentiescript AdSense AdSense-instellingen AdSense-instellingen voor deze StatusNet-website Client-ID Google client-ID Breedbeeldbanner Slotcode voor breedbeeldbanner Middelgrote rechthoek Slotcode voor gemiddelde rechthoek Plug-in om Google AdSense toe te voegen aan Statusnetsites. Rechthoek Slotcode voor rechthoek Opslaan AdSense-instellingen opslaan URL voor script (gevorderd) Skyscraper AdSense Slotcode voor brede skyscraper 