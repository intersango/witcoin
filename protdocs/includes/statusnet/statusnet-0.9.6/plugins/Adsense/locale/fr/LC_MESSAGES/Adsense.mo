��          �      \      �     �     �     �  (   �  	   &     0     A     M     c     t  0   �  	   �     �     �     �     �  
             (  E  B  !   �     �     �  *   �     �            '   2     Z  #   j  8   �  	   �     �     �  #   �           8     L  .   T                                             
                      	                                Ad script URL AdSense AdSense configuration AdSense settings for this StatusNet site Client ID Google client ID Leaderboard Leaderboard slot code Medium rectangle Medium rectangle slot code Plugin to add Google Adsense to StatusNet sites. Rectangle Rectangle slot code Save Save AdSense settings Script URL (advanced) Skyscraper TITLEAdSense Wide skyscraper slot code Project-Id-Version: StatusNet - Adsense
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:09+0000
Language-Team: French <http://translatewiki.net/wiki/Portal:fr>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-20 17:58:21+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: fr
X-Message-Group: #out-statusnet-plugin-adsense
Plural-Forms: nplurals=2; plural=(n > 1);
 Adresse URL du script d’annonce AdSense Configuration d’AdSense Paramètres Adsense pour ce site StatusNet Identifiant du client ID client Google Panneau de commande Code placé dans le panneau de commande Rectangle moyen Code placé dans un rectangle moyen Greffon pour ajouter Google Adsense aux sites StatusNet. Rectangle Codé placé dans le rectangle Sauvegarder Sauvegarder les paramètres AdSense URL du script (avancé) Bannière verticale AdSense Code placé dans une bannière verticale large 