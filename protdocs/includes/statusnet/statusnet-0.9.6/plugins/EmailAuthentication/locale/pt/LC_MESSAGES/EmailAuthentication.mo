��          ,      <       P   P   Q   b  �   l                        The Email Authentication plugin allows users to login using their email address. Project-Id-Version: StatusNet - EmailAuthentication
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:28+0000
Language-Team: Portuguese <http://translatewiki.net/wiki/Portal:pt>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:08+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: pt
X-Message-Group: #out-statusnet-plugin-emailauthentication
Plural-Forms: nplurals=2; plural=(n != 1);
 O plugin de autenticação por email permite aos utilizadores autenticar-se usando o seu endereço de email. 