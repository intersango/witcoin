��          �   %   �      P     Q     a  ,   p  -   �     �     �     �          .     ?     R     a     q     �  J   �  #   �  !     "   0     S     X  
   g  ?   r  '   �     �  Q   �  �  B  "   �  #     w   3  h   �  @   	  ?   U	  :   �	  H   �	  %   
  )   ?
     i
  '   �
  -   �
  a   �
  �   >  B   �  J     X   e     �  -   �     �  z     H   �  B   �  �                       	                                                                                              
           BUTTONAdd feed Bad form data. Can't mirror a StatusNet group at this time. Configure mirroring of posts from other feeds Could not subscribe to feed. Feed mirror settings Invalid feed URL. Invalid profile for mirroring. LABELLocal user LABELRemote feed: MENUMirroring Mirroring style Not logged in. Pull feeds into your timeline! Repeat: reference the original user's post (sometimes shows as 'RT @blah') Repost the content under my account Requested edit of missing mirror. Requested invalid profile to edit. Save Stop mirroring Subscribed There was a problem with your session token. Try again, please. This action only accepts POST requests. Web page or feed URL: You can mirror updates from many RSS and Atom feeds into your StatusNet timeline! Project-Id-Version: StatusNet - SubMirror
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:44+0000
Language-Team: Ukrainian <http://translatewiki.net/wiki/Portal:uk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:37:02+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: uk
X-Message-Group: #out-statusnet-plugin-submirror
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 Додати веб-стрічку Невірні дані форми. На даний момент не можу віддзеркалювати спільноту на сайті StatusNet. Конфігурація віддзеркалення дописів з інших веб-стрічок Не можу підписатися до веб-стрічки. Налаштування дзеркала веб-стрічки Помилкова URL-адреса веб-стрічки. Помилковий профіль для віддзеркалення. Тутешній користувач Віддалена веб-стрічка: Віддзеркалення Форма віддзеркалення Ви не увійшли до системи. Стягування веб-каналів до вашої стрічки повідомлень! Повторення: посилання до оригінального допису користувача (щось на зразок «RT @pupkin») Повторення змісту під моїм акаунтом Запитано редагування зниклого дзеркала. Було запитано невірний профіль для редагування. Зберегти Зупинити віддзеркалення Підписані Виникли певні проблеми з токеном сесії. Спробуйте знов, будь ласка. Ця дія приймає запити лише за формою POST. Веб-сторінка або ж URL-адреса стрічки: Ви маєте можливість віддзеркалювати оновлення багатьох веб-стрічок формату RSS або Atom одразу до стрічки своїх дописів на сайті StatusNet! 