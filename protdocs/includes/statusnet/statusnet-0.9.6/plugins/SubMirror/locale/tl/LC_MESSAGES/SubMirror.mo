��          �   %   �      P     Q     a  ,   p  -   �     �     �     �          .     ?     R     a     q     �  J   �  #   �  !     "   0     S     X  
   g  ?   r  '   �     �  Q   �  K  B     �     �  :   �  B   �  %   9  "   _     �  0   �     �     �     �     	     	  9   ,	  c   f	  6   �	  .   
  7   0
     h
     p
     �
  >   �
  ?   �
       y   8                    	                                                                                              
           BUTTONAdd feed Bad form data. Can't mirror a StatusNet group at this time. Configure mirroring of posts from other feeds Could not subscribe to feed. Feed mirror settings Invalid feed URL. Invalid profile for mirroring. LABELLocal user LABELRemote feed: MENUMirroring Mirroring style Not logged in. Pull feeds into your timeline! Repeat: reference the original user's post (sometimes shows as 'RT @blah') Repost the content under my account Requested edit of missing mirror. Requested invalid profile to edit. Save Stop mirroring Subscribed There was a problem with your session token. Try again, please. This action only accepts POST requests. Web page or feed URL: You can mirror updates from many RSS and Atom feeds into your StatusNet timeline! Project-Id-Version: StatusNet - SubMirror
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:44+0000
Language-Team: Tagalog <http://translatewiki.net/wiki/Portal:tl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:37:02+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: tl
X-Message-Group: #out-statusnet-plugin-submirror
Plural-Forms: nplurals=2; plural=(n != 1);
 Idagdag ang pakain Datong may masamang anyo. Hindi maisalamin sa ngayon ang isang pangkat ng StatusNet. Iayos ang pagsasalamin ng mga pagpapaskil mula sa ibang mga pakain Hindi magawang makatanggap ng pakain. Mga katakdaan ng salamin ng pakain Hindi tanggap na URL ng pakain. Hindi tanggap na balangkas para sa pagsasalamin. Katutubong tagagamit Pakaing malayo: Sinasalamin Estilo ng pagsasalamin Hindi nakalagda. Hilahin ang mga pakain papasok sa iyong guhit ng panahon! Ulitin: sangguniin ang orihinal na pagpapaskil ng tagagamit (minsang ipinapakita bilang 'RT @blah') Muling ipaskil ang nilalaman sa ilalim ng aking akawnt Hiniling na pagpatnugot ng nawawalang salamin. Hiniling na pamamatnugutang hindi tanggap na balangkas. Sagipin Ihinto ang pagsasalamin Tumanggap ng sipi May isang suliranin sa iyong token ng sesyon. Pakisubukan uli. Ang galaw na ito ay tumatanggap lamang ng mga kahilingang POST. URL ng pahina sa web o pakain: Maisasalamin mo ang mga pagsasapanahon mula sa maraming mga pakain ng RSS at Atom sa iyong guhit ng panahon ng StatusNet! 