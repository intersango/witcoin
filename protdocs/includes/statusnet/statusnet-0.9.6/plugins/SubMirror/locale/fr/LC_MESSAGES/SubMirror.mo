��          �   %   �      P     Q     a  ,   p  -   �     �     �     �          .     ?     R     a     q     �  J   �  #   �  !     "   0     S     X  
   g  ?   r  '   �     �  Q   �  I  B     �  !   �  @   �  B   �  #   B     f     �  '   �     �     �     �     �     	  5   !	  e   W	  #   �	  '   �	  %   	
     /
     ;
     N
  R   V
  8   �
  '   �
  i   
                    	                                                                                              
           BUTTONAdd feed Bad form data. Can't mirror a StatusNet group at this time. Configure mirroring of posts from other feeds Could not subscribe to feed. Feed mirror settings Invalid feed URL. Invalid profile for mirroring. LABELLocal user LABELRemote feed: MENUMirroring Mirroring style Not logged in. Pull feeds into your timeline! Repeat: reference the original user's post (sometimes shows as 'RT @blah') Repost the content under my account Requested edit of missing mirror. Requested invalid profile to edit. Save Stop mirroring Subscribed There was a problem with your session token. Try again, please. This action only accepts POST requests. Web page or feed URL: You can mirror updates from many RSS and Atom feeds into your StatusNet timeline! Project-Id-Version: StatusNet - SubMirror
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:44+0000
Language-Team: French <http://translatewiki.net/wiki/Portal:fr>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:37:02+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: fr
X-Message-Group: #out-statusnet-plugin-submirror
Plural-Forms: nplurals=2; plural=(n > 1);
 Ajouter le flux Données de formulaire erronées. Impossible de mettre en miroir un groupe StatusNet actuellement. Configurer la mise en miroir de messages provenant d’autres flux Impossible de vous abonner au flux. Paramètres de miroir de flux Adresse URL de flux invalide. Profil invalide pour la mise en miroir. Utilisateur local Flux distant : Mise en miroir Style de mise en miroir Non connecté. Importez des flux d’information dans votre agenda ! Répéter : référence le message de l’auteur d’origine (montré parfois comme « RT @blabla ») Reposter le contenu sous mon compte Miroir inexistant demandé à modifier. Profil invalide demandé à modifier. Sauvegarder Arrêter le miroir Abonné Un problème est survenu avec votre jeton de session. Veuillez essayer à nouveau. Cette action n’accepte que les requêtes de type POST. Adresse URL de la page Web ou du flux : Vous pouvez mettre en miroir dans votre agenda StatusNet les mises à jour de nombreux flux RSS et Atom ! 