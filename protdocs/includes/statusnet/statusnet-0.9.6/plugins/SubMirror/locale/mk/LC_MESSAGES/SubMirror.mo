��          �   %   �      P     Q     a  ,   p  -   �     �     �     �          .     ?     R     a     q     �  J   �  #   �  !     "   0     S     X  
   g  ?   r  '   �     �  Q   �  c  B     �  >   �  U   �  g   Q  C   �  E   �  8   C	  <   |	     �	      �	     �	  $   
     6
  f   T
  �   �
  G   h  `   �  J        \  ,   k     �  x   �  D   (  :   m  �   �                    	                                                                                              
           BUTTONAdd feed Bad form data. Can't mirror a StatusNet group at this time. Configure mirroring of posts from other feeds Could not subscribe to feed. Feed mirror settings Invalid feed URL. Invalid profile for mirroring. LABELLocal user LABELRemote feed: MENUMirroring Mirroring style Not logged in. Pull feeds into your timeline! Repeat: reference the original user's post (sometimes shows as 'RT @blah') Repost the content under my account Requested edit of missing mirror. Requested invalid profile to edit. Save Stop mirroring Subscribed There was a problem with your session token. Try again, please. This action only accepts POST requests. Web page or feed URL: You can mirror updates from many RSS and Atom feeds into your StatusNet timeline! Project-Id-Version: StatusNet - SubMirror
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:44+0000
Language-Team: Macedonian <http://translatewiki.net/wiki/Portal:mk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:37:02+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: mk
X-Message-Group: #out-statusnet-plugin-submirror
Plural-Forms: nplurals=2; plural=(n == 1 || n%10 == 1) ? 0 : 1;
 Додај канал Неисправни податоци за образецот. Моментално не можам да отсликам група од StatusNet. Нагодување на отсликувањето на објавите од други канали Не можев да Ве претплатам на каналот. Нагодувања на каналското отсликување Неважечка URL-адреса за каналот. Неважечки профил за отсликување. Локален корисник Далечински канал: Отсликување Стил на отсликување Не сте најавени. Повлекувајте каналски емитувања во Вашата хронологија! Повторување: наведете ја објавата на изворниот корисник (понекогаш се прикажува како „RT @blah“) Објави ја содржината под мојата сметка Побаравте уредување на отсликување што недостасува. Побаран е неважечки профил за уредување. Зачувај Престани со отсликување Претплатено Се појави проблем со жетонот на Вашата сесија. Обидете се подоцна. Оваа постапка прифаќа само POST-барања. Мреж. страница или URL на каналот: Можете да отсликувате поднови од многу RSS- и Atom-канали во Вашата хронологија на StatusNet! 