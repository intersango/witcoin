��          �   %   �      P     Q     a  ,   p  -   �     �     �     �          .     ?     R     a     q     �  J   �  #   �  !     "   0     S     X  
   g  ?   r  '   �     �  Q   �  O  B     �     �  =   �  ?   �  %   ;  '   a     �  #   �     �     �     �     �     	  )   .	  Y   X	  #   �	  2   �	  +   	
     5
     B
     Z
  F   f
  7   �
      �
  d                       	                                                                                              
           BUTTONAdd feed Bad form data. Can't mirror a StatusNet group at this time. Configure mirroring of posts from other feeds Could not subscribe to feed. Feed mirror settings Invalid feed URL. Invalid profile for mirroring. LABELLocal user LABELRemote feed: MENUMirroring Mirroring style Not logged in. Pull feeds into your timeline! Repeat: reference the original user's post (sometimes shows as 'RT @blah') Repost the content under my account Requested edit of missing mirror. Requested invalid profile to edit. Save Stop mirroring Subscribed There was a problem with your session token. Try again, please. This action only accepts POST requests. Web page or feed URL: You can mirror updates from many RSS and Atom feeds into your StatusNet timeline! Project-Id-Version: StatusNet - SubMirror
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:44+0000
Language-Team: Interlingua <http://translatewiki.net/wiki/Portal:ia>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:37:02+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: ia
X-Message-Group: #out-statusnet-plugin-submirror
Plural-Forms: nplurals=2; plural=(n != 1);
 Adder syndication Mal datos de formulario. Al presente il es impossibile republicar un gruppo StatusNet. Configurar le republication de messages de altere syndicationes Non poteva subscriber al syndication. Configuration de speculo de syndication URL de syndication invalide. Profilo invalide pro republication. Usator local Syndication remote: Republication Stilo de republication Tu non ha aperite un session. Importar syndicationes in tu chronologia! Repeter: referer al message del usator original (monstrate a vices como 'RT @pseudonymo') Republicar le contento sub mi conto Requestava le modification de un speculo mancante. Requestava un profilo invalide a modificar. Salveguardar Cessar le republication Subscribite Occurreva un problema con le indicio de tu session. Per favor reproba. Iste action accepta solmente le requestas de typo POST. URL de pagina web o syndication: Tu pote republicar actualisationes de multe syndicationes RSS e Atom in tu chronologia de StatusNet! 