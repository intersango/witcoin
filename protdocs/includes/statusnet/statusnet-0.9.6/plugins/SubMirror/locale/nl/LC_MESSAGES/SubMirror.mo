��          �   %   �      P     Q     a  ,   p  -   �     �     �     �          .     ?     R     a     q     �  J   �  #   �  !     "   0     S     X  
   g  ?   r  '   �     �  Q   �  I  B     �     �  9   �  3   �  $   %     J     h  !   �     �  	   �  	   �     �     �     �  i   	  )   m	  5   �	  5   �	     
     
     !
  Q   -
  0   
     �
  W   �
                    	                                                                                              
           BUTTONAdd feed Bad form data. Can't mirror a StatusNet group at this time. Configure mirroring of posts from other feeds Could not subscribe to feed. Feed mirror settings Invalid feed URL. Invalid profile for mirroring. LABELLocal user LABELRemote feed: MENUMirroring Mirroring style Not logged in. Pull feeds into your timeline! Repeat: reference the original user's post (sometimes shows as 'RT @blah') Repost the content under my account Requested edit of missing mirror. Requested invalid profile to edit. Save Stop mirroring Subscribed There was a problem with your session token. Try again, please. This action only accepts POST requests. Web page or feed URL: You can mirror updates from many RSS and Atom feeds into your StatusNet timeline! Project-Id-Version: StatusNet - SubMirror
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:44+0000
Language-Team: Dutch <http://translatewiki.net/wiki/Portal:nl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:37:02+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: nl
X-Message-Group: #out-statusnet-plugin-submirror
Plural-Forms: nplurals=2; plural=(n != 1);
 Feed toevoegen Onjuiste formuliergegevens. Het is niet mogelijk om een StatusNet-groep te spiegelen. Spiegelen instellen voor berichten van andere feeds Het abonneren op de feed is mislukt. Instellingen voor spiegelfeed Ongeldige URL voor feed. Ongeldig profiel om te spiegelen. Lokale gebruiker Bronfeed: Spiegelen Spiegelstijl Niet aangemeld. Neem feeds op in uw tijdlijn! Herhalen: refereer aan het bericht van de originele gebruiker (wordt soms weergegeven als "RT @blah ...") De inhoud herhalen alsof die van mij komt Er is een missende spiegel opgevraagd om te bewerken. Er is een ongeldig profiel opgevraagd om te bewerken. Opslaan Spiegelen beëindigen Geabonneerd Er is een probleem ontstaan met uw sessie. Probeer het nog een keer, alstublieft. Deze handeling accepteert alleen POST-verzoeken. URL van webpagina of feed: U kunt statusupdates vanuit veel RSS- en Atomfeeds spiegelen in uit StatusNet-tijdlijn. 