��    O      �  k         �     �  ;   �     �  c     3   e     �  )   �     �     �  \   �     T     n     �      �     �  #   �  #   �  9   	     A	  "   _	     �	  %   �	     �	  
   �	  "   �	  F   �	     7
     U
  !   k
     �
     �
    �
  l   �     B     ^  3   q  6   �     �  ]   �     X     x  y   �           3  )   @  D   j     �     �  /   �               #     /     =     W     \  "   r     �     �  %   �  v   �  ?   a  �   �     g     o     �     �     �     �     �     �          /     J     [  5   w  &   �     �  a  �     E  <   L     �  W   �  3   �       )   )     S     Z  c   m     �     �     �          &  !   6  $   X  9   }     �  $   �     �               .  '   ;  B   c  "   �     �  !   �              *  c   I     �     �  6   �  9        J  _   i     �  !   �  z   
     �  	   �  $   �  ?   �          &  ?   <     |     �     �     �     �     �     �  -   �            '   8  q   `  9   �  �        �     �     �     �  !   �           -      C      Y   !   n      �      �   *   �   %   �      !        &          !                                   4          .   E       
       /                        G   *      <   5       ,   I          %             =   >   N         H   O                     D   M   3   0   L   K   F      ?   1   B   (   '      7   6                            :   2      	                  #          +   @   C      A      J   -   $   ;   8   9       )   "             first. 1-64 lowercase letters or numbers, no punctuation or spaces Add Allow users to import their Twitter friends' timelines. Requires daemons to be manually configured. Allow users to login with their Twitter credentials Already logged in. Automatically send my notices to Twitter. Connect Connect existing account Connect your Twitter account to share your updates with your Twitter friends and vice-versa. Connected Twitter account Connection options Consumer key Consumer key assigned by Twitter Consumer secret Consumer secret assigned by Twitter Couldn't link your Twitter account. Couldn't link your Twitter account: oauth_token mismatch. Couldn't remove Twitter user. Couldn't save Twitter preferences. Create Create a new user with this nickname. Create new account Disconnect Disconnect my account from Twitter Disconnecting your Twitter could make it impossible to log in! Please  Enable "Sign-in with Twitter" Enable Twitter import Error connecting user to Twitter. Error registering user. Existing nickname Hi, %1$s. We're sorry to inform you that your link to Twitter has been disabled. We no longer seem to have permission to update your Twitter status. Did you maybe revoke %3$s's access?

You can re-enable your Twitter bridge by visiting your Twitter settings page:

	%2$s

Regards,
%3$s If you already have an account, login with your username and password to connect it to your Twitter account. Import my friends timeline. Integration source Invalid consumer key. Max length is 255 characters. Invalid consumer secret. Max length is 255 characters. Invalid username or password. Keep your %1$s account but disconnect from Twitter. You can use your %1$s password to log in. Login or register using Twitter Login with your Twitter account My text and files are available under %s except this private data: password, email address, IM address, and phone number. Name of your Twitter application New nickname Nickname already in use. Try another one. Nickname must have only lowercase letters and numbers and no spaces. Nickname not allowed. Not a valid invitation code. Note: a global consumer key and secret are set. Options Password Preferences RT @%1$s %2$s Registration not allowed. Save Save Twitter settings Send local "@" replies to Twitter. Sign in with Twitter Something weird happened. Subscribe to my Twitter friends here. The Twitter "bridge" plugin allows integration of a StatusNet instance with <a href="http://twitter.com/">Twitter</a>. There was a problem with your session token. Try again, please. This is the first time you've logged into %s so we must connect your Twitter account to a local account. You can either create a new account, or connect with your existing account, if you have one. Twitter Twitter Account Setup Twitter Login Twitter account Twitter account disconnected. Twitter application settings Twitter bridge configuration Twitter bridge settings Twitter integration options Twitter preferences saved. Twitter settings Unexpected form submission. You can't register if you don't agree to the license. Your Twitter bridge has been disabled. set a password Project-Id-Version: StatusNet - TwitterBridge
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:53+0000
Language-Team: Simplified Chinese <http://translatewiki.net/wiki/Portal:zh-hans>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 19:01:06+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: zh-hans
X-Message-Group: #out-statusnet-plugin-twitterbridge
Plural-Forms: nplurals=1; plural=0;
 先。 1 到 64 个小写字母或数字，不包含标点或空格 添加 允许用户导入他们 Twitter 好友的时间线。需要手动设置后台进程。 允许用户使用他们的 Twitter 帐号登录。 已登录。 自动将我的消息发送到 Twitter。 关联 关联现有账号 关联你的 Twitter 帐号并与你的 Twitter 好友分享你的更新和查看好友的更新。 已关联的 Twitter 帐号 连接选项 Consumer key Twitter 分配的 consumer key Consumer secret Twitter 分配的 consumer secret 无法连接你的 Twitter 帐号。 无法连接你的 Twitter 帐号：oauth_token 不符。 无法删除 Twitter 用户。 无法保存 Twitter 参数设置。 创建 以此昵称创建新帐户 创建新帐户 取消关联 取消我的帐号与 Twitter 的关联 取消关联你的 Twitter 帐号和能会导致无法登录！请 启用 “使用 Twitter 登录” 启用 Twitter 导入 关联用户到 Twitter 出错。 注册用户出错。 已存在的昵称 Hi, %1$s。我们很抱歉通知你，你与 Twitter 的连接已被禁用了。我们似乎没有更新你 Twitter 消息的权限了。或许你之前取消了 %3$ 的访问权限？

你可以通过更新你的 Twitter 设置重新恢复你的 Twitter 连接：

%2$s

祝好，
%3$s 如果你已有帐号，请输入用户名和密码登录并将其与你的 Twitter 账号关联。 导入我好友的时间线。 整合来源 无效的 consumer key。最大长度为 255 字符。 无效的 consumer secret。最大长度为 255 字符。 用户名或密码不正确。 保留你的 %1$s 帐号并取消关联 Twitter。你可以使用你的 %1$s 密码来登录。 使用 Twitter 登录或注册 使用你的 Twitter 帐号登录 我的文字和文件在%s下提供，除了如下隐私内容：密码、电子邮件地址、IM 地址和电话号码。 你的 Twitter 应用名称 新昵称 昵称已被使用，换一个吧。 昵称只能使用小写字母和数字且不能使用空格。 昵称不被允许。 无效的邀请码。 注意：已设置了一个全局的 consumer key 和 secret。 选项 密码 参数设置 RT @%1$s %2$s 不允许注册。 保存 保存 Twitter 设置 将本地的“@”回复发送到 Twitter。 使用 Twitter 登录 发生了很诡异的事情。 关注我在这里的 Twitter 好友。 Twitter "bridge" 是个可以让 StatusNet 账户与 <a href="http://twitter.com/">Twitter</a> 整合的插件。 你的 session token 出现了一个问题，请重试。 这是你第一次登录到 %s，我们需要将你的 Twitter 帐号与一个本地的帐号关联。你可以新建一个帐号，或者使用你在本站已有的帐号。 Twitter Twitter 帐号设置 Twitter 登录 Twitter 帐号 已取消 Twitter 帐号关联。 Twitter 应用设置 Twitter bridge 设置 Twitter bridge 设置 Twitter 整合选项 已保存 Twitter 参数设置。 Twitter 设置 未预料的表单提交。 你必须同意许可协议才能注册。 你的 Twitter bridge 已被禁用。 设置一个密码 