��          L      |       �      �      �      �      �   O   �   �  -     �     �      �       �   >                                         "%s" Share on %s Share on Facebook Share on Twitter This plugin allows sharing of notices to Twitter, Facebook and other platforms. Project-Id-Version: StatusNet - ShareNotice
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:36+0000
Language-Team: Ukrainian <http://translatewiki.net/wiki/Portal:uk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:34:10+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: uk
X-Message-Group: #out-statusnet-plugin-sharenotice
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 «%s» Розмістити в %s Розмістити в Facebook Розмістити в Twitter Цей додаток дозволяє ділитися дописами в Twitter, Facebook та інших сервісах, розміщуючи їх там. 