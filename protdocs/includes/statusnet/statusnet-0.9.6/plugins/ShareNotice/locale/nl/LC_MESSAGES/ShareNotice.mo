��          L      |       �      �      �      �      �   O   �   M  -     {     �     �     �  `   �                                         "%s" Share on %s Share on Facebook Share on Twitter This plugin allows sharing of notices to Twitter, Facebook and other platforms. Project-Id-Version: StatusNet - ShareNotice
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:36+0000
Language-Team: Dutch <http://translatewiki.net/wiki/Portal:nl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:34:10+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: nl
X-Message-Group: #out-statusnet-plugin-sharenotice
Plural-Forms: nplurals=2; plural=(n != 1);
 "%s" Delen op %s Delen op Facebook Delen op Twitter Deze plug-in maakt het mogelijk mededelingen te delen op Twitter, Facebook en andere platformen. 