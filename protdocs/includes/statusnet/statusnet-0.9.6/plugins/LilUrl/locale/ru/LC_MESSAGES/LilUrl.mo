��          4      L       `      a   ;   �   �  �   A   _  a   �                    A serviceUrl must be specified. Uses <a href="http://%1$s/">%1$s</a> URL-shortener service. Project-Id-Version: StatusNet - LilUrl
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:45+0000
Language-Team: Russian <http://translatewiki.net/wiki/Portal:ru>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:36:24+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: ru
X-Message-Group: #out-statusnet-plugin-lilurl
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 Должен быть указан URL-адрес сервису. Использование службы сокращения URL <a href="http://%1$s/">%1$s</a>. 