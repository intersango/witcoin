��          4      L       `      a   ;   �   T  �   !     >   4                    A serviceUrl must be specified. Uses <a href="http://%1$s/">%1$s</a> URL-shortener service. Project-Id-Version: StatusNet - LilUrl
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:45+0000
Language-Team: Norwegian (bokmål)‬ <http://translatewiki.net/wiki/Portal:no>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:36:24+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: no
X-Message-Group: #out-statusnet-plugin-lilurl
Plural-Forms: nplurals=2; plural=(n != 1);
 En tjeneste-Url må spesifiseres. Bruker URL-forkortertjenesten <a href="http://%1$s/">%1$s</a>. 