��          \      �       �      �      �   #        (     B  Y   H  l   �  ?    %   O  (   u  ,   �  '   �     �  [   �  �   T                                       A mailbox must be specified. A password must be specified. A poll_frequency must be specified. A user must be specified. Error ImapManager should be created using its constructor, not the using the static get method. The IMAP plugin allows for StatusNet to check a POP or IMAP mailbox for incoming mail containing user posts. Project-Id-Version: StatusNet - Imap
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:42+0000
Language-Team: Dutch <http://translatewiki.net/wiki/Portal:nl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:35:53+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: nl
X-Message-Group: #out-statusnet-plugin-imap
Plural-Forms: nplurals=2; plural=(n != 1);
 Er moet een postbus opgegeven worden. Er moet een wachtwoord opgegeven worden. Er moet een poll_frequency opgegeven worden. Er moet een gebruiker opgegeven worden. Fout ImapManager moet aangemaakt worden via de constructor en niet met de statische methode get. De plug-in IMAP maakt het mogelijk om StatusNet een POP- of IMAP-postbus te laten controleren op inkomende e-mail die berichten van gebruikers bevat. 