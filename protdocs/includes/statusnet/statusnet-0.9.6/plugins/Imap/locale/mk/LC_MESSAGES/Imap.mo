��          \      �       �      �      �   #        (     B  Y   H  l   �  Y    0   i  0   �  0   �  2   �     /  �   <  �                                          A mailbox must be specified. A password must be specified. A poll_frequency must be specified. A user must be specified. Error ImapManager should be created using its constructor, not the using the static get method. The IMAP plugin allows for StatusNet to check a POP or IMAP mailbox for incoming mail containing user posts. Project-Id-Version: StatusNet - Imap
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:42+0000
Language-Team: Macedonian <http://translatewiki.net/wiki/Portal:mk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:35:53+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: mk
X-Message-Group: #out-statusnet-plugin-imap
Plural-Forms: nplurals=2; plural=(n == 1 || n%10 == 1) ? 0 : 1;
 Мора да назначите сандаче. Мора да назначите лозинка. Мора да назначите poll_frequency. Мора да назначите корисник. Грешка ImapManager треба да се создаде користејќи го неговиот конструктор, а не преку статичниот метод на негово добивање. Приклучокот IMAP му овозможува на StatusNet да проверува дојдовни пораки со објави од корисници во POP или IMAP сандачиња . 