��          \      �       �      �      �   #        (     B  Y   H  l   �  A    '   Q     y  -   �  "   �     �  z   �  �   n                                       A mailbox must be specified. A password must be specified. A poll_frequency must be specified. A user must be specified. Error ImapManager should be created using its constructor, not the using the static get method. The IMAP plugin allows for StatusNet to check a POP or IMAP mailbox for incoming mail containing user posts. Project-Id-Version: StatusNet - Imap
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:42+0000
Language-Team: Tagalog <http://translatewiki.net/wiki/Portal:tl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:35:53+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: tl
X-Message-Group: #out-statusnet-plugin-imap
Plural-Forms: nplurals=2; plural=(n != 1);
 Dapat tukuyin ang isang kahon ng liham. Dapat tukuyin ang isang hudyat. Dapat tukuyin ang isang kadalasan ng botohan. Dapat tukuyin ang isang tagagamit. Kamalian Dapat na likhain ang ImapManager sa pamamagitan ng pambuo nito, hindi sa paggamit ng paraang hindi gumagalaw ang  pagkuha. Ang pamasak na IMAP ay nagpapahintulot na masuri ng StatusNet ang isang kahon ng liham ng POP o IMAP para sa papasok na liham na naglalaman ng mga pagpapaskil ng tagagamit. 