��          T      �       �      �      �   #   �           2  Y   8  O  �     �     �  !        <     X  Y   _                                        A mailbox must be specified. A password must be specified. A poll_frequency must be specified. A user must be specified. Error ImapManager should be created using its constructor, not the using the static get method. Project-Id-Version: StatusNet - Imap
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:42+0000
Language-Team: Simplified Chinese <http://translatewiki.net/wiki/Portal:zh-hans>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:35:53+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: zh-hans
X-Message-Group: #out-statusnet-plugin-imap
Plural-Forms: nplurals=1; plural=0;
 必须指定一个邮箱。 必须设定一个密码。 必须设定一个抓取频率。 必须设定一个用户。 错误 ImapManager 应该使用它的生成器来创建，而不是使用固定的 get 方法。 