��          \      �       �      �      �   #        (     B  Y   H  l   �  �    =   �  0   �  a     <   ~     �  �   �  �   a                                       A mailbox must be specified. A password must be specified. A poll_frequency must be specified. A user must be specified. Error ImapManager should be created using its constructor, not the using the static get method. The IMAP plugin allows for StatusNet to check a POP or IMAP mailbox for incoming mail containing user posts. Project-Id-Version: StatusNet - Imap
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:42+0000
Language-Team: Russian <http://translatewiki.net/wiki/Portal:ru>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:35:53+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: ru
X-Message-Group: #out-statusnet-plugin-imap
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 Должен быть указан почтовый ящик. Должен быть указан пароль. Периодичность проверки должна быть задана в poll_frequency. Должен быть указан пользователь. Ошибка ImapManager должен быть создан с помощью конструктора, а не получен статическим методом. Плагин IMAP позволяет StatusNet проверять почтовый ящик по протоколу POP или IMAP на предмет наличия во входящей почте сообщений от пользователей. 