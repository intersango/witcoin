��          \      �       �      �      �   #        (     B  Y   H  l   �  P       `       #   �     �     �  f   �  �   H                                       A mailbox must be specified. A password must be specified. A poll_frequency must be specified. A user must be specified. Error ImapManager should be created using its constructor, not the using the static get method. The IMAP plugin allows for StatusNet to check a POP or IMAP mailbox for incoming mail containing user posts. Project-Id-Version: StatusNet - Imap
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:42+0000
Language-Team: Norwegian (bokmål)‬ <http://translatewiki.net/wiki/Portal:no>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:35:53+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: no
X-Message-Group: #out-statusnet-plugin-imap
Plural-Forms: nplurals=2; plural=(n != 1);
 En postkasse må spesifiseres. Et passord må spesifiseres. En poll_frequency må spesifiseres. En bruker må spesifiseres. Feil ImapManager bør opprettes ved å bruke dets konstruktør, ikke ved å bruke dets statiske get-metode. Utvidelsen IMAP gjør det mulig for StatusNet å sjekke en POP- eller IMAP-postkasse for innkommende e-post som innholder brukerinnlegg. 