��          \      �       �      �      �   #        (     B  Y   H  l   �  ?    .   O  &   ~  8   �  -   �       z     �   �                                       A mailbox must be specified. A password must be specified. A poll_frequency must be specified. A user must be specified. Error ImapManager should be created using its constructor, not the using the static get method. The IMAP plugin allows for StatusNet to check a POP or IMAP mailbox for incoming mail containing user posts. Project-Id-Version: StatusNet - Imap
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:42+0000
Language-Team: French <http://translatewiki.net/wiki/Portal:fr>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:35:53+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: fr
X-Message-Group: #out-statusnet-plugin-imap
Plural-Forms: nplurals=2; plural=(n > 1);
 Une boîte aux lettres doit être spécifiée. Un mot de passe doit être spécifié. Une fréquence d’interrogation doit être spécifiée. Un nom d’utilisateur doit être spécifié. Erreur ImapManager devrait être instancié en utilisant son constructeur et non pas en utilisant la méthode statique « get ». L’extension IMAP permet à StatusNet de rechercher dans une boîte électronique POP ou IMAP les messages contenant des avis des utilisateurs. 