��    7      �  I   �      �     �  ;   �     �               3     H     W     e     q     }     �     �  %   �     �  "   �               5     O  "   e     �  �  �  e   @  �   �     )	     I	  2   g	     �	     �	      �	      �	      
  y   
     �
  )   �
  D   �
               7  
   D     O     X     r     �      �  +   �  @   �  ?     �   _       5   -     c  9     W  �       <   !     ^     e     r     �     �     �     �     �     �     �     �     �     �  )        1     D     Y     q     �     �  W  �  P     l   g     �     �  !     !   4     V     ]  "   }     �  {   �  	   %  $   /  ?   T     �  !   �  	   �     �     �     �     �     
  '     -   ?  8   m  +   �  �   �     �  *   �     �  9   �                          +   0   /         !          1             %      -       2   )   3          .      7           #   '   
   *                4      &   ,                   (                     	                 5      "                 6         $        %s and friends 1-64 lowercase letters or numbers, no punctuation or spaces After Already logged in. An unknown error has occured. Available characters BUTTONConnect BUTTONCreate BUTTONSend BUTTONSkip Before Connect existing account Connection options Create a new user with this nickname. Create new account Error connecting user to Facebook. Existing nickname Facebook Account Setup Facebook Connect Settings Facebook Connect User Facebook integration configuration Friends already using %s: Hi, %1$s. We're sorry to inform you that we are unable to update your Facebook status from %2$s, and have disabled the Facebook application for your account. This may be because you have removed the Facebook application's authorization, or have deleted your Facebook account.  You can re-enable the Facebook application and automatic status updating by re-installing the %2$s Facebook application.

Regards,

%2$s If you already have an account, login with your username and password to connect it to your Facebook. If you would like the %s app to automatically update your Facebook status with your latest notice, you need to give it permission. Incorrect username or password. Invalid username or password. Invitations have been sent to the following users: Invite your friends to use %s Login Login or register using Facebook Login with your Facebook Account MENUFacebook My text and files are available under %s except this private data: password, email address, IM address, and phone number. New nickname Nickname already in use. Try another one. Nickname must have only lowercase letters and numbers and no spaces. Nickname not allowed. Not a valid invitation code. Okay, do it! Pagination Password Registration not allowed. Send a notice Send invitations Server error: Couldn't get user! Thanks for inviting your friends to use %s. There is already a local user linked with this Facebook account. There was a problem with your session token. Try again, please. This is the first time you've logged into %s so we must connect your Facebook to a local account. You can either create a new account, or connect with your existing account, if you have one. What's up, %s? You can't register if you don't agree to the license. You have been invited to %s You must be logged into Facebook to use Facebook Connect. Project-Id-Version: StatusNet - Facebook
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:36+0000
Language-Team: Simplified Chinese <http://translatewiki.net/wiki/Portal:zh-hans>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:57:01+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: zh-hans
X-Message-Group: #out-statusnet-plugin-facebook
Plural-Forms: nplurals=1; plural=0;
 %s 和好友们 1 到 64 个小写字母或数字，不包含标点或空格 之后 已登录。 发生未知错误。 可用的字符 连接 创建 发送 跳过 之前 连接现有帐号 连接选项 以此昵称创建新帐户 创建新帐户 连接用户至Facebook时发生错误。 已存在的昵称 Facebook帐号设置 Facebook Connect 设置 Facebook Connect 用户 Facebook整合设置 已经使用 %s 的好友们： 你好，%1$s。我们很抱歉的通知你我们无法从%2$s更新你的Facebook状态，并已禁用你帐户的Facebook应用。这可能是因为你取消了Facebook应用的授权，或者你删除了你的Facebook帐户。通过重新安装Facebook应用你可以重新启用你的Facebook应用的自动状态更新。

祝好，

%2$s 如果你已有帐号，请输入用户名和密码登录并连接至Facebook。 如果你希望 %s 应用自动更新你最新的状态到Facebook状态上，你需要给它设置权限。 用户名或密码不正确。 用户名或密码不正确。 邀请已发给一些的用户： 邀请你的朋友们来使用 %s 登录 使用 Facebook 登陆或注册 使用你的 Facebook 帐号登录 Facebook  我的文字和文件在%s下提供，除了如下隐私内容：密码、电子邮件地址、IM 地址和电话号码。 新昵称 昵称已被使用，换一个吧。 昵称只能使用小写字母和数字且不能使用空格。 昵称不被允许。 对不起，无效的邀请码。 好的！ 分页 密码 不允许注册。 发送一个通知 发送邀请 服务器错误：无法获取用户。 谢谢你邀请你的朋友们来使用 %s。 这里已经有一个用户连接了此Facebook帐号。 你的session token出错了。请重试。  这是你第一次登录到 %s，我们需要将你的Facebook帐号与一个本地的帐号关联。你可以新建一个帐号，或者使用你在本站已有的帐号。 %s，最近怎么样？ 你必须同意许可协议才能注册。 你被邀请来到 %s 你必须使用Facebook Connect来登入Facebook帐号。 