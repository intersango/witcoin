��    O      �  k         �     �     �     �     �  ;   �     8     @     ]     }     �     �     �     �     �     �     �                    )     0     I  !   \     ~  %   �     �     �     �     �     	     "	     8	     G	     e	     	     �	     �	  2   �	     �	     �	     
       
      A
     b
     ~
  	   �
     �
     �
  ,   �
     �
     �
  )   �
          3     F  "   c     �     �  
   �     �     �     �     �     �     �     �     
          $  +   5  -   a  @   �  ?   �       5        U  9   q     �  G  �                     >  =   P     �  %   �  *   �  
   �     �     
     %     9     B  	   J     T  	   ]     g     k     w      ~     �  8   �  )   �  ,        D     Z     o     x  "   �     �     �      �          '  #   /  #   S  5   w     �  !   �     �  )   �     	  !   '     I     R     Z     a  .   n     �     �  0   �     �     �  !     3   .     b     i  
   w  
   �     �  !   �  	   �     �     �  
   �     �     
       /   4  3   d  <   �  >   �       Q   ,     ~  J   �     �     1   *       5   ;   M             &       !           =         )   (                           7       '   <   4   A          F       D   >   :                     0   E      G   O   6   K                     L   -      %   .      9   J           8   $      ?                        /      B             	   #      I   2      N       H       "      
       3   +   ,   @                     C     a new account.  first. %1$s and friends, page %2$d %s and friends 1-64 lowercase letters or numbers, no punctuation or spaces API key API key provided by Facebook API secret provided by Facebook After Already logged in. An unknown error has occured. Available characters BUTTONConnect BUTTONCreate BUTTONDisconnect BUTTONLogin BUTTONSave BUTTONSend BUTTONSkip Before Connect existing account Connection options Couldn't delete link to Facebook. Couldn't remove Facebook user. Create a new user with this nickname. Create new account Existing nickname Facebook Facebook Account Setup Facebook Connect Settings Facebook Connect User Facebook Login Facebook integration settings Friends already using %s: Home Incorrect username or password. Invalid username or password. Invitations have been sent to the following users: Invite Invite your friends to use %s Login Login or register using Facebook Login with your Facebook Account Lost or forgotten password? MENUFacebook MENUHome MENUInvite MENUSettings Manage how your account connects to Facebook New nickname Nickname Nickname already in use. Try another one. Nickname not allowed. No notice content! Not a valid invitation code. Not sure what you're trying to do. Notices Okay, do it! Pagination Password Register Registration not allowed. Save Secret Send "@" replies to Facebook. Send a notice Send invitations Settings Sync preferences Thanks for inviting your friends to use %s. That's too long. Max notice size is %d chars. There is already a local user linked with this Facebook account. There was a problem with your session token. Try again, please. What's up, %s? You can't register if you don't agree to the license. You have been invited to %s You must be logged into Facebook to use Facebook Connect. set a password Project-Id-Version: StatusNet - Facebook
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:34+0000
Language-Team: Breton <http://translatewiki.net/wiki/Portal:br>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:57:01+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: br
X-Message-Group: #out-statusnet-plugin-facebook
Plural-Forms: nplurals=2; plural=(n > 1);
 ur gont nevez. da gentañ. %1$s hag e vignoned, pajenn %2$d %s hag e vignoned 1 da 64 lizherenn vihan pe sifr, hep poentaouiñ nag esaouenn Alc'hwez API Alc'hwez API pourchaset gant Facebook Alc'hwez API kuzh pourchaset gant Facebook War-lerc'h Kevreet oc'h dija. Ur gudenn dizanv a zo bet. Arouezennoù a chom Kevreañ Krouiñ Digevrañ Kevreañ Enrollañ Kas Mont hebiou A-raok Kevreañ d'ur gont a zo dioutañ Dibarzhioù kevreañ N'eus ket bet gallet diverkañ al liamm war-du Facebook. Dibosupl eo dilemel an implijer Facebook. Krouiñ un implijer nevez gant al lesanv-se. Krouiñ ur gont nevez Lesanv a zo dioutañ Facebook Kefluniadur ar gont Facebook Arventennoù evit Facebook Connect Implijer Facebook Connect Kevreadenn Facebook Arventennoù enframmañ Facebook Mignoned hag a implij dija %s : Degemer Anv implijer pe ger-tremen direizh. Anv implijer pe ger-tremen direizh. Pedadennoù a zo bet kaset d'an implijerien da-heul : Pediñ Pedit ho mignoned da implijout %s Kevreañ Kevreañ pe en em enskrivañ dre Facebook Kevreit gant ho kont Facebook Ha kollet ho peus ho ker-tremen ? Facebook Degemer Pediñ Arventennoù Merañ an doare ma kevre ho kont ouzh Facebook Lesanv nevez Lesanv Implijet eo dija al lesanv-se. Klaskit unan all. Lesanv nann-aotreet. Danvez ebet en ali ! N'eo ket reizh ar c'hod pedadenn. N'omp ket sur eus ar pezh emaoc'h o klask ober aze. Alioù Ok, en ober ! Pajennadur Ger-tremen En em enskrivañ N'eo ket aotreet krouiñ kontoù. Enrollañ Kuzh Kas respontoù "@" da Facebook. Kas un ali Kas pedadennoù Penndibaboù Penndibaboù ar c'hempredañ Trugarez da bediñ ho mignoned da implijout %s. Re hir eo ! Ment hirañ an ali a zo a %d arouezenn. Un implijer lec'hel liammet d'ar gont Facebook-se a zo dija. Ur gudenn 'zo bet gant ho jedaouer dalc'h. Mar plij adklaskit. Penaos 'mañ kont, %s ? Rankout a rit bezañ a-du gant termenoù an aotre-implijout evit krouiñ ur gont. Pedet oc'h bet da %s Rankout a rit bezañ kevreet war Facebook evit implijout Facebook Connect. Termeniñ ur ger-tremen 