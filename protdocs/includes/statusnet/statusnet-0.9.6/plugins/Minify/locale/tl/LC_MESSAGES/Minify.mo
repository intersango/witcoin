��          D      l       �      �   \   �   &   �   *   &  E  Q  (   �  u   �  5   6  1   l                          File type not supported. The Minify plugin minifies StatusNet's CSS and JavaScript, removing whitespace and comments. The parameter "f" is not a valid path. The parameter "f" is required but missing. Project-Id-Version: StatusNet - Minify
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:50+0000
Language-Team: Tagalog <http://translatewiki.net/wiki/Portal:tl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:55+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: tl
X-Message-Group: #out-statusnet-plugin-minify
Plural-Forms: nplurals=2; plural=(n != 1);
 Hindi tinatangkilik ang uri ng talaksan. Ang pamasak na Minify ay nagpapaliit sa CSS at JavaScript ng StatusNet, na nagtatanggal ng puting puwang at mga puna. Ang parametrong "f" ay hindi isang tanggap na landas. Ang parametrong "f" ay kailangan ngunit nawawala. 