��          D      l       �      �   \   �   &   �   *   &  T  Q     �  ]   �  (     -   D                          File type not supported. The Minify plugin minifies StatusNet's CSS and JavaScript, removing whitespace and comments. The parameter "f" is not a valid path. The parameter "f" is required but missing. Project-Id-Version: StatusNet - Minify
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:49+0000
Language-Team: Norwegian (bokmål)‬ <http://translatewiki.net/wiki/Portal:no>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:55+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: no
X-Message-Group: #out-statusnet-plugin-minify
Plural-Forms: nplurals=2; plural=(n != 1);
 Filtype støttes ikke. Utvidelsen Minify minimerer StatusNets CSS og JavaScript og fjerner mellomrom og kommentarer. Parameteren «f» er ikke en gyldig sti. Parameteren «f» er nødvendig, men mangler. 