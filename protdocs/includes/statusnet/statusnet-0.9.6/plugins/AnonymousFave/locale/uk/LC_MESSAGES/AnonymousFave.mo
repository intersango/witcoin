��          �            h     i  *   z     �     �  M   �  J   )  '   t  0   �  0   �     �       ?     "   X     {  �  �      L  }   m  9   �  K   %  �   q  �   E  \   	  �   i	  �   
  "   �
     �
  z   �
  +   1  )   ]                   	                              
                               Add to favorites Allow anonymous users to favorite notices. Could not create favorite. Could not delete favorite. Could not disfavor notice! Please make sure your browser has cookies enabled. Could not favor notice! Please make sure your browser has cookies enabled. Couldn't create anonymous user session. Couldn't create favorite tally for notice ID %d. Couldn't update favorite tally for notice ID %d. Disfavor favorite Favored There was a problem with your session token. Try again, please. This notice is already a favorite! This notice is not a favorite! Project-Id-Version: StatusNet - AnonymousFave
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:11+0000
Language-Team: Ukrainian <http://translatewiki.net/wiki/Portal:uk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:56:48+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: uk
X-Message-Group: #out-statusnet-plugin-anonymousfave
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 Додати до обраних Дозволити анонімнім користувачам позначати повідомлення як обрані. Не вдалося позначити як обране. Не можу видалити допис зі списку обраних. Не вдалося видалити повідомлення зі списку обраних! Будь ласка, переконайтеся, що у вашому браузері увімкнено кукі. Не вдалося позначити повідомлення як обране! Будь ласка, переконайтеся, що у вашому браузері увімкнено кукі. Не вдалося створити сесію анонімного користувача. Не вдалося створити лічильник кількості позначок «обране» для допису за номером %d. Не вдалося оновити кількість позначок «обране» для допису за номером %d. Видалити з обраних Обране Виникли певні проблеми з токеном сесії. Спробуйте знов, будь ласка. Цей допис вже є обраним! Цей допис не є обраним! 