��          �            h     i  *   z     �     �  M   �  J   )  '   t  0   �  0   �     �       ?     "   X     {  Q  �     �  ;         <  "   \  q     m   �  9   _  ?   �  G   �     !  
   3  R   >  0   �      �                   	                              
                               Add to favorites Allow anonymous users to favorite notices. Could not create favorite. Could not delete favorite. Could not disfavor notice! Please make sure your browser has cookies enabled. Could not favor notice! Please make sure your browser has cookies enabled. Couldn't create anonymous user session. Couldn't create favorite tally for notice ID %d. Couldn't update favorite tally for notice ID %d. Disfavor favorite Favored There was a problem with your session token. Try again, please. This notice is already a favorite! This notice is not a favorite! Project-Id-Version: StatusNet - AnonymousFave
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:11+0000
Language-Team: French <http://translatewiki.net/wiki/Portal:fr>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:56:48+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: fr
X-Message-Group: #out-statusnet-plugin-anonymousfave
Plural-Forms: nplurals=2; plural=(n > 1);
 Ajouter aux favoris Autoriser les utilisateurs anonymes à préférer des avis. Impossible de créer le favori. Impossible de supprimer le favori. Impossible de marquer cet avis comme non favori ! Veuillez vous assurer que votre navigateur accepte les cookies. Impossible de marquer cet avis comme favori ! Veuillez vous assurer que votre navigateur accepte les cookies. Impossible de créer une session d’utilisateur anonyme. Impossible de créer le score de préférence pour l’avis %d. Impossible de mettre à jour le score de préférence pour l’avis %d. Retirer ce favori Préféré Un problème est survenu avec votre jeton de session. Veuillez essayer à nouveau. Cet avis a déjà été ajouté à vos favoris ! Cet avis n’est pas un favori ! 