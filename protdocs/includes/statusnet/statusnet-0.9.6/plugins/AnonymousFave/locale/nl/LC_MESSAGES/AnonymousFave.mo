��          �            h     i  *   z     �     �  M   �  J   )  '   t  0   �  0   �     �       ?     "   X     {  Q  �     �  F     0   L  L   }  l   �  l   7  A   �  b   �  c   I     �     �  Q   �  /   7	  1   g	                   	                              
                               Add to favorites Allow anonymous users to favorite notices. Could not create favorite. Could not delete favorite. Could not disfavor notice! Please make sure your browser has cookies enabled. Could not favor notice! Please make sure your browser has cookies enabled. Couldn't create anonymous user session. Couldn't create favorite tally for notice ID %d. Couldn't update favorite tally for notice ID %d. Disfavor favorite Favored There was a problem with your session token. Try again, please. This notice is already a favorite! This notice is not a favorite! Project-Id-Version: StatusNet - AnonymousFave
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:11+0000
Language-Team: Dutch <http://translatewiki.net/wiki/Portal:nl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:56:48+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: nl
X-Message-Group: #out-statusnet-plugin-anonymousfave
Plural-Forms: nplurals=2; plural=(n != 1);
 Aan favorieten toevoegen Staat anonieme gebruikers toe mededelingen als favoriet aan te merken. Het was niet mogelijk een favoriet aan te maken. Het was niet mogelijk deze mededeling van uw favorietenlijst te verwijderen. De mededeling kon niet als favoriet verwijderd worden. Zorg dat uw browser het gebruik van cookies toestaat. De mededeling kon niet als favoriet aangemerkt worden. Zorg dat uw browser het gebruik van cookies toestaat. Het was niet mogelijk een anonieme gebruikerssessie aan te maken. Het was niet mogelijk de telling voor aantal favorieten aan te maken voor de mededeling met ID %d. Het was niet mogelijk de telling voor aantal favorieten bij te werken voor de mededeling met ID %d. Van favorietenlijst verwijderen Als favoriet aangemerkt Er is een probleem ontstaan met uw sessie. Probeer het nog een keer, alstublieft. Deze mededeling staat al in uw favorietenlijst. Deze mededeling staat niet op uw favorietenlijst. 