��          �            h     i  *   z     �     �  M   �  J   )  '   t  0   �  0   �     �       ?     "   X     {  Q  �     �  =   	  %   G  %   m  s   �  n     .   v  =   �  >   �     "     6  >   ?  :   ~  '   �                   	                              
                               Add to favorites Allow anonymous users to favorite notices. Could not create favorite. Could not delete favorite. Could not disfavor notice! Please make sure your browser has cookies enabled. Could not favor notice! Please make sure your browser has cookies enabled. Couldn't create anonymous user session. Couldn't create favorite tally for notice ID %d. Couldn't update favorite tally for notice ID %d. Disfavor favorite Favored There was a problem with your session token. Try again, please. This notice is already a favorite! This notice is not a favorite! Project-Id-Version: StatusNet - AnonymousFave
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:10+0000
Language-Team: Breton <http://translatewiki.net/wiki/Portal:br>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:56:48+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: br
X-Message-Group: #out-statusnet-plugin-anonymousfave
Plural-Forms: nplurals=2; plural=(n > 1);
 Ouzhpennañ d'ar pennrolloù Aotreañ an implijerien dizanv da gavout gwelloc'h kemennoù. Diposupl eo krouiñ ar pennroll-mañ. Diposupl eo dilemel ar pennroll-mañ. Dibosupl eo merkañ ar c'hemenn-mañ evel nann-pennroll ! Mar plij gwiriekait e tegemer ho urzhiataer an toupinoù. Dibosupl eo merkañ ar c'hemenn-mañ evel pennroll ! Mar plij gwiriekait e tegemer ho urzhiataer an toupinoù. Dibosupl eo krouiñ un dalc'h implijer dizanv. Dibosupl eo krouiñ ar skor penndibaboù evit ar c'hemenn %d. Dibosupl eo hizivaat ar skor penndibaboù evit ar c'hemenn %d. Tennañ ar pennroll Karetañ Ur gudenn 'zo bet gant ho jedaouer dalc'h. Mar plij adklaskit. Ouzhpennet eo bet ar c'hemenn-mañ d'ho pennrolloù dija ! N'eo ket ar c'hemenn-mañ ur pennroll ! 