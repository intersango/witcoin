��          �            h     i  *   z     �     �  M   �  J   )  '   t  0   �  0   �     �       ?     "   X     {  S  �     �  =        @     [  �   z  |     -   �  >   �  E   �  "   3     V  I   _  %   �  !   �                   	                              
                               Add to favorites Allow anonymous users to favorite notices. Could not create favorite. Could not delete favorite. Could not disfavor notice! Please make sure your browser has cookies enabled. Could not favor notice! Please make sure your browser has cookies enabled. Couldn't create anonymous user session. Couldn't create favorite tally for notice ID %d. Couldn't update favorite tally for notice ID %d. Disfavor favorite Favored There was a problem with your session token. Try again, please. This notice is already a favorite! This notice is not a favorite! Project-Id-Version: StatusNet - AnonymousFave
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:11+0000
Language-Team: Spanish <http://translatewiki.net/wiki/Portal:es>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:56:48+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: es
X-Message-Group: #out-statusnet-plugin-anonymousfave
Plural-Forms: nplurals=2; plural=(n != 1);
 Añadir a favoritos Permitir a usuarios anónimos marcar mensajes como favoritos. No se pudo crear favorito. No se pudo borrar el favorito. ¡No es posible eliminar el mensaje de entre los favoritos! Por favor, asegúrate de que las cookies estén habilitadas en tu navegador. No fue posible marcar el mensaje como favorito. Por favor, asegúrate de que las cookies están habilitadas en tu navegador. No se pudo crear sesión de usuario anónimo. No se pudo crear una cuenta favorita para el mensaje de ID %d. No se pudo actualizar el la cuenta favorita para el mensaje de ID %d. Eliminar de la lista de favoritos. Favorito Hubo un problema con tu token de sesión. Por favor, inténtalo de nuevo. ¡Este mensaje ya está en favoritos! ¡Este mensaje no es un favorito! 