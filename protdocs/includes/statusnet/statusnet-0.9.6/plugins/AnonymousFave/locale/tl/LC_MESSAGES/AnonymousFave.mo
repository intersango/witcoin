��          �            h     i  *   z     �     �  M   �  J   )  '   t  0   �  0   �     �       ?     "   X     {  S  �     �  G        N     j  j   �  ]   �  9   N  H   �  M   �       	   :  K   D  %   �  +   �                   	                              
                               Add to favorites Allow anonymous users to favorite notices. Could not create favorite. Could not delete favorite. Could not disfavor notice! Please make sure your browser has cookies enabled. Could not favor notice! Please make sure your browser has cookies enabled. Couldn't create anonymous user session. Couldn't create favorite tally for notice ID %d. Couldn't update favorite tally for notice ID %d. Disfavor favorite Favored There was a problem with your session token. Try again, please. This notice is already a favorite! This notice is not a favorite! Project-Id-Version: StatusNet - AnonymousFave
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:11+0000
Language-Team: Tagalog <http://translatewiki.net/wiki/Portal:tl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:56:48+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: tl
X-Message-Group: #out-statusnet-plugin-anonymousfave
Plural-Forms: nplurals=2; plural=(n != 1);
 Idagdag sa mga paborito Payagan ang hindi nakikilalang mga tagagamit sa paboritong mga pabatid. Hindi malikha ang paborito. Hindi mabura ang paborito. Hindi magawang hindi paborito ang pabatid!  Pakitiyak na gumagana ang mga  otap ng iyong pantingin-tingin. Hindi maipaborito ang pabatid!  Pakitiyak na gumagana ang mga otap ng iyong pantingin-tingin. Hindi malikha ang sesyon ng hindi nakikilalang tagagamit. Hindi malikha ang talang-bilang ng paborito para sa ID na %d ng pabatid. Hindi maisapanahon ang talang-bilang ng paborito para sa ID na %d ng pabatid. Huwag paboran ang paborito Pinaboran Nagkaroon ng isang suliranin sa iyong token ng sesyon.  Paki subukang muli. Isa nang paborito ang pabatid na ito! Ang pabatid na ito ay hindi isang paborito! 