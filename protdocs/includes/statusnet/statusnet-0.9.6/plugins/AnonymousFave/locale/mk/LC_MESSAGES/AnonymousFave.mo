��          �            h     i  *   z     �     �  M   �  J   )  '   t  0   �  0   �     �       ?     "   X     {  k  �  "     b   )  L   �  U   �  �   /  �   �  Y   �  q   �  y   _	  '   �	     
  x   
  D   �
  <   �
                   	                              
                               Add to favorites Allow anonymous users to favorite notices. Could not create favorite. Could not delete favorite. Could not disfavor notice! Please make sure your browser has cookies enabled. Could not favor notice! Please make sure your browser has cookies enabled. Couldn't create anonymous user session. Couldn't create favorite tally for notice ID %d. Couldn't update favorite tally for notice ID %d. Disfavor favorite Favored There was a problem with your session token. Try again, please. This notice is already a favorite! This notice is not a favorite! Project-Id-Version: StatusNet - AnonymousFave
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:11+0000
Language-Team: Macedonian <http://translatewiki.net/wiki/Portal:mk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:56:48+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: mk
X-Message-Group: #out-statusnet-plugin-anonymousfave
Plural-Forms: nplurals=2; plural=(n == 1 || n%10 == 1) ? 0 : 1;
 Додај во бендисани Дозволи анонимни корисници да бендисуваат забелешки. Не можев да создадам бендисана забелешка. Не можев да ја избришам бендисаната забелешка. Не можев да ја одбендисам забелешката! Проверете дали имате овозможено колачиња во прелистувачот. Не можев да ја бендисам заблешката. Проверете дали имате овозможено колачиња во прелистувачот. Не можев да создадам анонимна корисничка сесија. Не можев создадам бројач на бендисувања за забелешката со ID %d. Не можев да го поновам бројот на бендисувања за забелешката со ID %d. Одбендисај бендисана Бендисано Се појави проблем со жетонот на Вашата сесија. Обидете се подоцна. Веќе сте ја бендисале оваа забелешка! Оваа забелешка не Ви е бендисана! 