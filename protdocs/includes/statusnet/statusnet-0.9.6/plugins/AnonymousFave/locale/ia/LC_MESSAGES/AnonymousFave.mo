��          �            h     i  *   z     �     �  M   �  J   )  '   t  0   �  0   �     �       ?     "   X     {  W  �     �  .        4     R  ]   p  Z   �  +   )  =   U  B   �     �     �  F   �     :     T                   	                              
                               Add to favorites Allow anonymous users to favorite notices. Could not create favorite. Could not delete favorite. Could not disfavor notice! Please make sure your browser has cookies enabled. Could not favor notice! Please make sure your browser has cookies enabled. Couldn't create anonymous user session. Couldn't create favorite tally for notice ID %d. Couldn't update favorite tally for notice ID %d. Disfavor favorite Favored There was a problem with your session token. Try again, please. This notice is already a favorite! This notice is not a favorite! Project-Id-Version: StatusNet - AnonymousFave
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:11+0000
Language-Team: Interlingua <http://translatewiki.net/wiki/Portal:ia>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:56:48+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: ia
X-Message-Group: #out-statusnet-plugin-anonymousfave
Plural-Forms: nplurals=2; plural=(n != 1);
 Adder al favorites Permitter a usatores anonyme de favorir notas. Non poteva crear le favorite. Non poteva deler le favorite. Non poteva disfavorir le nota! Per favor assecura te que tu navigator ha le cookies activate. Non poteva favorir le nota! Per favor assecura te que tu navigator ha le cookies activate. Non poteva crear session de usator anonyme. Non poteva crear un numero de favorites pro le ID de nota %d. Non poteva actualisar le numero de favorites pro le ID de nota %d. Disfavorir favorite Favorite Occurreva un problema con le indicio de tu session. Per favor reproba. Iste nota es ja favorite! Iste nota non es favorite! 