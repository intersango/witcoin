��          |      �             !  *   2     ]     x  M   �  J   �  '   ,     T  ?   f  "   �     �  R  �     ;  ;   T  "   �      �  w   �  h   L  0   �     �  F   �  (   E  !   n            	                                       
        Add to favorites Allow anonymous users to favorite notices. Could not create favorite. Could not delete favorite. Could not disfavor notice! Please make sure your browser has cookies enabled. Could not favor notice! Please make sure your browser has cookies enabled. Couldn't create anonymous user session. Disfavor favorite There was a problem with your session token. Try again, please. This notice is already a favorite! This notice is not a favorite! Project-Id-Version: StatusNet - AnonymousFave
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:10+0000
Language-Team: German <http://translatewiki.net/wiki/Portal:de>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:56:48+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: de
X-Message-Group: #out-statusnet-plugin-anonymousfave
Plural-Forms: nplurals=2; plural=(n != 1);
 Zu Favoriten hinzufügen Ermöglicht anonymen Benutzern Nachrichten zu favorisieren. Konnte keinen Favoriten erstellen. Konnte Favoriten nicht löschen. Konnte Nachricht nicht aus den Favoriten entfernen! Bitte stelle sicher, dass Cookies in deinem Browser aktiviert sind. Konnte Nachricht nicht favorisieren! Bitte stelle sicher, dass Cookies in deinem Browser aktiviert sind. Konnte keine anonyme Benutzer-Sitzung erstellen. Aus Favoriten entfernen Es gab ein Problem mit deinem Sitzungstoken. Bitte versuche es erneut. Diese Nachricht ist bereits ein Favorit! Diese Nachricht ist kein Favorit! 