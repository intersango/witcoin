��          <      \       p   3   q   .   �   4   �   {  	  V   �  p   �  �   M                   Disables posting without a validated email address. You must provide an email address to register. You must validate your email address before posting. Project-Id-Version: StatusNet - RequireValidatedEmail
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:26+0000
Language-Team: Macedonian <http://translatewiki.net/wiki/Portal:mk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-20 17:54:48+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: mk
X-Message-Group: #out-statusnet-plugin-requirevalidatedemail
Plural-Forms: nplurals=2; plural=(n == 1 || n%10 == 1) ? 0 : 1;
 Оневозможува објавување без потврдена е-пошта. За да се регистрирате, ќе мора да наведете е-поштенска адреса. Пред да почнете да објавувате ќе мора да ја потврдите Вашата е-поштенска адреса. 