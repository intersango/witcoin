��          <      \       p   3   q   .   �   4   �   a  	  Q   k  G   �  ?                      Disables posting without a validated email address. You must provide an email address to register. You must validate your email address before posting. Project-Id-Version: StatusNet - RequireValidatedEmail
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:26+0000
Language-Team: French <http://translatewiki.net/wiki/Portal:fr>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-20 17:54:48+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: fr
X-Message-Group: #out-statusnet-plugin-requirevalidatedemail
Plural-Forms: nplurals=2; plural=(n > 1);
 Désactive le postage pour ceux qui n’ont pas d’adresse électronique valide. Vous devez fournir une adresse électronique avant de vous enregistrer. Vous devez valider votre adresse électronique avant de poster. 