��          4      L       `   .   a   4   �   b  �   >   (  D   g                    You must provide an email address to register. You must validate your email address before posting. Project-Id-Version: StatusNet - RequireValidatedEmail
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:26+0000
Language-Team: German <http://translatewiki.net/wiki/Portal:de>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-20 17:54:48+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: de
X-Message-Group: #out-statusnet-plugin-requirevalidatedemail
Plural-Forms: nplurals=2; plural=(n != 1);
 Du musst eine E-Mail-Adresse angeben, um dich zu registrieren. Du musst deine E-Mail-Adresse validieren, bevor du beitragen kannst. 