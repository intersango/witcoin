��          <      \       p   3   q   .   �   4   �   g  	  D   q  =   �  6   �                   Disables posting without a validated email address. You must provide an email address to register. You must validate your email address before posting. Project-Id-Version: StatusNet - RequireValidatedEmail
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:26+0000
Language-Team: Interlingua <http://translatewiki.net/wiki/Portal:ia>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-20 17:54:48+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: ia
X-Message-Group: #out-statusnet-plugin-requirevalidatedemail
Plural-Forms: nplurals=2; plural=(n != 1);
 Disactiva le publication de messages sin adresse de e-mail validate. Tu debe fornir un adresse de e-mail pro poter crear un conto. Tu debe validar tu adresse de e-mail ante de publicar. 