��          �      �       H     I  &   U     |  (   �  
   �     �     �     �     �  (   �  D   $  	   i  &   s  G  �     �  =   �     (  ?   5     u     �  +   �     �     �  ?   �  d   )     �  1   �                                         	              
          BUTTONSave Bing Webmaster Tools verification key. Bing key Google Webmaster Tools verification key. Google key MENUSitemap Save sitemap settings. Sitemap Sitemap configuration Sitemap settings for this StatusNet site This plugin allows creation of sitemaps for Bing, Yahoo! and Google. Yahoo key Yahoo! Site Explorer verification key. Project-Id-Version: StatusNet - Sitemap
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:41+0000
Language-Team: Tagalog <http://translatewiki.net/wiki/Portal:tl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:34:11+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: tl
X-Message-Group: #out-statusnet-plugin-sitemap
Plural-Forms: nplurals=2; plural=(n != 1);
 Sagipin Susing pantiyak ng mga Kasangkapan ng Maestro ng Web ng Bing. Susi ng Bing Susing pantiyak ng mga Kasangkapan ng Maestro ng Web ng Google. Susi ng Google Mapa ng sityo Sagipin ang mga katakdaan ng mapa ng sityo. Mapa ng sityo Pagkakaayos ng mapa ng sityo Mga katakdaan ng mapa ng sityo para sa sityong ito ng StatusNet Nagpapahintulot ang pampasak na ito ng paglikha ng mga mapa ng sityo para sa Bing, Yahoo! at Google. Susi ng Yahoo Susing pantiyak ng Panggalugad ng Sityo ng Yahoo! 