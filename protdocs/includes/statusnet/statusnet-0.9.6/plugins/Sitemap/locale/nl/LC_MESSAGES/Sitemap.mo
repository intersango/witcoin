��          �      �       H     I  &   U     |  (   �  
   �     �     �     �     �  (   �  D   $  	   i  &   s  E  �     �  -   �       /   "     R     `  "   h     �     �  .   �  Q   �     .  -   ;                                         	              
          BUTTONSave Bing Webmaster Tools verification key. Bing key Google Webmaster Tools verification key. Google key MENUSitemap Save sitemap settings. Sitemap Sitemap configuration Sitemap settings for this StatusNet site This plugin allows creation of sitemaps for Bing, Yahoo! and Google. Yahoo key Yahoo! Site Explorer verification key. Project-Id-Version: StatusNet - Sitemap
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:40+0000
Language-Team: Dutch <http://translatewiki.net/wiki/Portal:nl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:34:11+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: nl
X-Message-Group: #out-statusnet-plugin-sitemap
Plural-Forms: nplurals=2; plural=(n != 1);
 Opslaan Verificatiesleutel voor Bing Webmaster Tools. Bingsleutel Verificatiesleutel voor Google Webmaster Tools. Googlesleutel Sitemap Instellingen voor sitemap opslaan. Sitemap Instellingen voor sitemap Sitemapinstellingen voor deze StatusNetwebsite Deze plugin maakt het mogelijk sitemaps aan te maken voor Bing, Yahoo! en Google. Yahoosleutel Verificatiesleutel voor Yahoo! Site Explorer. 