��          �      �       H     I  &   U     |  (   �  
   �     �     �     �     �  (   �  D   $  	   i  &   s  K  �     �  .   �  
   "  0   -     ^     k  ,   y     �     �  6   �  N   
     Y  -   e                                         	              
          BUTTONSave Bing Webmaster Tools verification key. Bing key Google Webmaster Tools verification key. Google key MENUSitemap Save sitemap settings. Sitemap Sitemap configuration Sitemap settings for this StatusNet site This plugin allows creation of sitemaps for Bing, Yahoo! and Google. Yahoo key Yahoo! Site Explorer verification key. Project-Id-Version: StatusNet - Sitemap
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:40+0000
Language-Team: Interlingua <http://translatewiki.net/wiki/Portal:ia>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:34:11+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: ia
X-Message-Group: #out-statusnet-plugin-sitemap
Plural-Forms: nplurals=2; plural=(n != 1);
 Salveguardar Clave de verification de Bing Webmaster Tools. Clave Bing Clave de verification de Google Webmaster Tools. Clave Google Mappa de sito Salveguardar configuration de mappa de sito. Mappa de sito Configuration de mappa de sito Configuration de mappa de sito pro iste sito StatusNet Iste plug-in permitte le creation de mappas de sito pro Bing, Yahoo! e Google. Clave Yahoo Clave de verification de Yahoo Site Explorer. 