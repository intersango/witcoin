��          �      �       H     I  &   U     |  (   �  
   �     �     �     �     �  (   �  D   $  	   i  &   s  �  �     @  8   Q     �  :   �     �     �  @   �     :  .   P  U     r   �     H  8   W                                         	              
          BUTTONSave Bing Webmaster Tools verification key. Bing key Google Webmaster Tools verification key. Google key MENUSitemap Save sitemap settings. Sitemap Sitemap configuration Sitemap settings for this StatusNet site This plugin allows creation of sitemaps for Bing, Yahoo! and Google. Yahoo key Yahoo! Site Explorer verification key. Project-Id-Version: StatusNet - Sitemap
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:41+0000
Language-Team: Ukrainian <http://translatewiki.net/wiki/Portal:uk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:34:11+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: uk
X-Message-Group: #out-statusnet-plugin-sitemap
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 Зберегти Ключ підтвердження Bing Webmaster Tools Ключ Bing Ключ підтвердження Google Webmaster Tools Ключ Google Карта сайту Зберегти налаштування карти сайту. Карта сайту Конфігурація карти сайту Налаштування карти сайту на даному сайті StatusNet Цей додаток дозволяє створювати карти сайту для Bing, Yahoo! та Google. Ключ Yahoo Ключ підтвердження Yahoo! Site Explorer 