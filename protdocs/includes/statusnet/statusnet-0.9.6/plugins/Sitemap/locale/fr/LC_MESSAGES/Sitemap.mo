��          �      �       H     I  &   U     |  (   �  
   �     �     �     �     �  (   �  D   $  	   i  &   s  E  �     �  5   �  	   "  7   ,     d     p  ,   }     �     �  2   �  O     
   X  9   c                                         	              
          BUTTONSave Bing Webmaster Tools verification key. Bing key Google Webmaster Tools verification key. Google key MENUSitemap Save sitemap settings. Sitemap Sitemap configuration Sitemap settings for this StatusNet site This plugin allows creation of sitemaps for Bing, Yahoo! and Google. Yahoo key Yahoo! Site Explorer verification key. Project-Id-Version: StatusNet - Sitemap
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:40+0000
Language-Team: French <http://translatewiki.net/wiki/Portal:fr>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:34:11+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: fr
X-Message-Group: #out-statusnet-plugin-sitemap
Plural-Forms: nplurals=2; plural=(n > 1);
 Sauvegarder Clé de vérification pour les outils Bing Webmaster. Clé Bing Clé de vérification pour les outils Google Webmaster. Clé Google Plan du site Sauvegarder les paramètres de plan du site. Plan du site Configuration du plan du site Paramètres de plan du site pour ce site StatusNet Cette extension permet de créer des plans du site pour Bing, Yahoo! et Google. Clé Yahoo Clé de vérification pour l’explorateur de site Yahoo! 