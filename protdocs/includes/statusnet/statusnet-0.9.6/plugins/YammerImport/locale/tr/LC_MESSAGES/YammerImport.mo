��             +         �     �     �     �  
          /        F     K     b     v     �     �     �  
   �  %   �     �     
  !        >     K     R  !   k     �     �     �     �     �  
   �     �     �       J       a     }     �     �     �  1   �     �     �       &   5     \  &   s  %   �     �  +   �  -   	     3	  '   Q	     y	     �	     �	  8   �	  "   �	      
  &   '
     N
     i
     z
     �
     �
     �
                                      
                                 	                                                                         Abort import Awaiting authorization... Connect to Yammer Connected. Continue Copy the verification code you are given below: Done Encountered error "%s" Import is complete! Import public notices Import status Import user accounts Import user groups Initialize Initiated Yammer server connection... Invalid avatar URL %s. No import running Open Yammer authentication window Pause import Paused Paused from admin panel. Prepare public notices for import Reset import state Save Save code and begin import Start authentication Verification code: Waiting... Yammer Yammer Import Yammer import Project-Id-Version: StatusNet - YammerImport
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:48:00+0000
Language-Team: Turkish <http://translatewiki.net/wiki/Portal:tr>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:34:40+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: tr
X-Message-Group: #out-statusnet-plugin-yammerimport
Plural-Forms: nplurals=1; plural=0;
 İçeri aktarmayı iptal et Yetkilendirme bekleniyor... Yammer'a Bağlan Bağlandı. Devam et Aşağıda verilen doğrulama kodunu kopyalayın: Tamamlandı Karşılaşılan hata "%s" İçeri aktarma tamamlandı! Genel durum mesajlarını içeri aktar İçeri aktarma durumu Kullanıcı hesaplarını içeri aktar Kullanıcı gruplarını içeri aktar İlk kullanıma hazırla Başlatılan Yammer sunucu bağlantısı... Geçersiz kullanıcı resmi bağlantısı %s. Çalışan içeri aktarma yok Open Yammer kimlik doğrulama penceresi İçeri aktarmayı duraklat Duraklatıldı Yönetim panelinden durduruldu. Genel durum mesajlarını içeri aktarmak için hazırla İçeri aktarma durumunu sıfırla Kaydet Kodu kaydet ve içeri aktarmaya başla Kimlik doğrulamaya başla Doğrulama kodu: Bekleniyor... Yammer Yammer İçeri Aktar Yammer içeri aktarma 