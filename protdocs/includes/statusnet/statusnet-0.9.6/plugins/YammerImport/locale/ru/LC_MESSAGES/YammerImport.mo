��          �      �           	       
   0     ;     D     I     `     t     �     �  $   �  ,   �  *   �  
   '     2     D     Q     X     ]     x     �  
   �  �  �     Y  *   w     �     �     �  (   �       ,   &  E   S  2   �  �   �  @   t  �   �     =	      X	  '   y	     �	     �	  6   �	  4   
  "   =
     `
                          
                                       	                                       Abort import Awaiting authorization... Connected. Continue Done Encountered error "%s" Import is complete! Import status Import user accounts Import user groups Imported %d user. Imported %d users. Importing %d group... Importing %d groups... Importing %d user... Importing %d users... Initialize No import running Pause import Paused Save Save code and begin import Start authentication Verification code: Waiting... Project-Id-Version: StatusNet - YammerImport
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:48:00+0000
Language-Team: Russian <http://translatewiki.net/wiki/Portal:ru>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:34:40+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: ru
X-Message-Group: #out-statusnet-plugin-yammerimport
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 Прервать импорт Ожидание авторизации… Подсоединено. Продолжить Готово Обнаружена ошибка «%s» Импорт завершён! Статус процесса импорта Импорт учётных записей пользователей Импорт групп пользователей Импортирован %d пользователь. Импортировано %d пользователя. Импортировано %d пользователей. Импорт %d группы… Импорт %d группы…  Импорт %d пользователя… Импорт %d пользователей… Импорт %d пользователей… Инициализация Импорт не запущен Приостановить импорт Приостановлено Сохранить Сохранить код и начать импорт Начало проверки подлинности Код подтверждения: Ожидание… 