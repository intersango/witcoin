��          t      �         J     +   \  8   �     �  N   �  �   &  @   �  #     $   2  Q   W  �  �  �   Q  y   �  k   e  *   �  �   �    �  T   �	  7   �	  8   ,
  �   e
                          
                              	    Feed subscription failed - notification handler doesn't respond correctly. Feed subscription failed: Not a valid feed. Only http-post notifications are supported at this time. Request must be POST. Thanks for the subscription. When the feed(s) update(s), you will be notified. The RSSCloud plugin enables your StatusNet instance to publish real-time updates for profile RSS feeds using the <a href="http://rsscloud.org/">RSSCloud protocol</a>. The following parameters were missing from the request body: %s. This resource requires an HTTP GET. This resource requires an HTTP POST. You must provide at least one valid profile feed url (url1, url2, url3 ... urlN). Project-Id-Version: StatusNet - RSSCloud
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:31+0000
Language-Team: Ukrainian <http://translatewiki.net/wiki/Portal:uk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:36:54+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: uk
X-Message-Group: #out-statusnet-plugin-rsscloud
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 Підписатися до веб-стрічки не вдалося: обробник сповіщень відреагував неправильно. Підписатися до веб-стрічки не вдалося: ця веб-стрічка не є дійсною. На даний момент підтримуються лише сповіщення форми http-post. Запит вимагає форми POST. Дякуємо за підписку. Коли веб-стрічки оновляться, вас буде поінформовано. Додаток RSSCloud дозволяє вашому StatusNet-сумісному сайту публікувати оновлення з веб-стрічок у реальному часі, використовуючи протокол <a href="http://rsscloud.org/">RSSCloud</a>. У формі запиту відсутні наступні параметри: %s. Цей ресурс вимагає форми HTTP GET. Цей ресурс вимагає форми HTTP POST. Ви повинні зазначити щонайменше один дійсний профіль для URL-адреси веб-стрічки (URL-адреси через кому та пробіл). 