��          t      �         J     +   \  8   �     �  N   �  �   &  @   �  #     $   2  Q   W  G  �  e   �  F   W  W   �  6   �  T   -  �   �  T   @  1   �  2   �  a   �                          
                              	    Feed subscription failed - notification handler doesn't respond correctly. Feed subscription failed: Not a valid feed. Only http-post notifications are supported at this time. Request must be POST. Thanks for the subscription. When the feed(s) update(s), you will be notified. The RSSCloud plugin enables your StatusNet instance to publish real-time updates for profile RSS feeds using the <a href="http://rsscloud.org/">RSSCloud protocol</a>. The following parameters were missing from the request body: %s. This resource requires an HTTP GET. This resource requires an HTTP POST. You must provide at least one valid profile feed url (url1, url2, url3 ... urlN). Project-Id-Version: StatusNet - RSSCloud
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:30+0000
Language-Team: French <http://translatewiki.net/wiki/Portal:fr>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:36:54+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: fr
X-Message-Group: #out-statusnet-plugin-rsscloud
Plural-Forms: nplurals=2; plural=(n > 1);
 L’abonnement au flux RSS a échoué : le gestionnaire de notifications ne répond pas correctement. L’abonnement au flux a échoué : ce n’est pas un flux RSS valide. Seules les notifications HTTP-POST sont prises en charge en ce moment sur le nuage RSS. La requête HTTP au nuage RSS doit être de type POST. Merci pour l’abonnement. Vous serez informé lors des mises à jour de ce(s) flux. L’extension RSSCloud permet à votre instance StatusNet de publier des mises à jour en temps réel sur des flux RSS en utilisant le protocole <a href="http://rsscloud.org/">RSSCloud</a>. Les paramètres suivants étaient absents du corps de la requête au nuage RSS : %s. Cette ressource nécessite une requête HTTP GET. Cette ressource nécessite une requête HTTP POST. Vous devez fournir au moins une adresse URL de flux de profil valide (url1, url2, url3 ... urlN). 