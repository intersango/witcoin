��          t      �         J     +   \  8   �     �  N   �  �   &  @   �  #     $   2  Q   W  G  �  S   �  3   E  A   y     �  d   �  �   <  6   �  #     $   5  ]   Z                          
                              	    Feed subscription failed - notification handler doesn't respond correctly. Feed subscription failed: Not a valid feed. Only http-post notifications are supported at this time. Request must be POST. Thanks for the subscription. When the feed(s) update(s), you will be notified. The RSSCloud plugin enables your StatusNet instance to publish real-time updates for profile RSS feeds using the <a href="http://rsscloud.org/">RSSCloud protocol</a>. The following parameters were missing from the request body: %s. This resource requires an HTTP GET. This resource requires an HTTP POST. You must provide at least one valid profile feed url (url1, url2, url3 ... urlN). Project-Id-Version: StatusNet - RSSCloud
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:30+0000
Language-Team: Dutch <http://translatewiki.net/wiki/Portal:nl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:36:54+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: nl
X-Message-Group: #out-statusnet-plugin-rsscloud
Plural-Forms: nplurals=2; plural=(n != 1);
 Abonneren op de feed is mislukt: het verwerkingsproces heeft niet juist geantwoord. Abonneren op de feed is mislukt: geen geldige feed. Op dit moment worden alle mededelingen via HTTP POST ondersteund. Het verzoek moet POST zijn. Dank u wel voor het abonneren. Als de feed of feeds bijgewerkt worden, wordt u op de hoogte gesteld. De plug-in RSSCloud laat StatusNet real-time updates publiceren voor de RSS-feeds van profielen met het protocol <a href="http://rsscloud.org/">RSSCloud</a>. De volgende parameters missen in de verzoekinhoud: %s. Deze bron heeft een HTTP GET nodig. Deze bron heeft een HTTP POST nodig. U moet tenminste een geldige URL voor een profielfeed opgeven ( url1, url2, url3, ..., urlN). 