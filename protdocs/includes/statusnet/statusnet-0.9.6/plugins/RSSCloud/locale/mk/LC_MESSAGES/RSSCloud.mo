��          t      �         J     +   \  8   �     �  N   �  �   &  @   �  #     $   2  Q   W  a  �  �     b   �  g     -   n  �   �    D  q   b	  (   �	  )   �	  �   '
                          
                              	    Feed subscription failed - notification handler doesn't respond correctly. Feed subscription failed: Not a valid feed. Only http-post notifications are supported at this time. Request must be POST. Thanks for the subscription. When the feed(s) update(s), you will be notified. The RSSCloud plugin enables your StatusNet instance to publish real-time updates for profile RSS feeds using the <a href="http://rsscloud.org/">RSSCloud protocol</a>. The following parameters were missing from the request body: %s. This resource requires an HTTP GET. This resource requires an HTTP POST. You must provide at least one valid profile feed url (url1, url2, url3 ... urlN). Project-Id-Version: StatusNet - RSSCloud
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:30+0000
Language-Team: Macedonian <http://translatewiki.net/wiki/Portal:mk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:36:54+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: mk
X-Message-Group: #out-statusnet-plugin-rsscloud
Plural-Forms: nplurals=2; plural=(n == 1 || n%10 == 1) ? 0 : 1;
 Претплаќањето на каналот не успеа - ракувачот со известувања не одговара правилно. Претплаќањето на каналот не успеа: Не е важечки канал. Моментално се поддржани само известувања од типот http-post. Барањето мора да биде POST. Ви благодариме шт осе претплативте. Ќе бидете известени за сите подновувања на каналот/ите. Приклучокот RSSCloud овозможува Вашиот примерок на StatusNet да објавува поднови во живо за профилни RSS-емитувања користејќи го протоколот <a href="http://rsscloud.org/">RSSCloud</a>. Следниве параметри недостасуваа од содржината на барањето: %s. Овој ресурс бара HTTP GET. Овој ресурс бара HTTP POST. Мора да наведете барем едеа важечка URL-адреса за профилен канал (url1, url2, url3 ... urlN). 