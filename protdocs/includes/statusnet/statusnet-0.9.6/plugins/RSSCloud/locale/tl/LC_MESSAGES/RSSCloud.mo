��          t      �         J     +   \  8   �     �  N   �  �   &  @   �  #     $   2  Q   W  I  �  U   �  ?   I  B   �     �  N   �  �   8  F   1  3   x  6   �  g   �                          
                              	    Feed subscription failed - notification handler doesn't respond correctly. Feed subscription failed: Not a valid feed. Only http-post notifications are supported at this time. Request must be POST. Thanks for the subscription. When the feed(s) update(s), you will be notified. The RSSCloud plugin enables your StatusNet instance to publish real-time updates for profile RSS feeds using the <a href="http://rsscloud.org/">RSSCloud protocol</a>. The following parameters were missing from the request body: %s. This resource requires an HTTP GET. This resource requires an HTTP POST. You must provide at least one valid profile feed url (url1, url2, url3 ... urlN). Project-Id-Version: StatusNet - RSSCloud
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:31+0000
Language-Team: Tagalog <http://translatewiki.net/wiki/Portal:tl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:36:54+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: tl
X-Message-Group: #out-statusnet-plugin-rsscloud
Plural-Forms: nplurals=2; plural=(n != 1);
 Nabigo ang pagtanggap ng pakain - hindi tama ang pagtugon ng tagapaghawak ng pabatid. Nabigo ang pagtanggap ng pakain: Hindi isang tanggap na pakain. Tanging mga pabatid na http-post lang ang tinatangkilik sa ngayon. Ang hiling ay dapat na POST. Salamat sa pagtatanggap.  Kapag nagsapanahon ang (mga) pakain, pababatiran ka. Ang pamasak ng RSSCloud ay nagpapahintulot sa iyong pagkakataon ng StatusNet na maglathala ng mga pagsasapanahong pangtunay na oras para sa mga balangkas ng mga pakain ng RSS na gumagamit ng <a href="http://rsscloud.org/">protokolo ng RSSCloud</a>. Nawawala ang sumusunod na mga parametro mula sa katawan ng hiling: %s. Ang pinagkunang ito ay nangangailangan ng HTTP GET. Ang pinagkukunang ito ay nangangailangan ng HTTP POST. Dapat kang magbigay ng kahit na isang url ng pakain ng tanggap na talaksan (url1, url2, url3 ... urlN). 