��          t      �         J     +   \  8   �     �  N   �  �   &  @   �  #     $   2  Q   W  M  �  a   �  E   Y  A   �     �  ^   �  �   Z  <     #   Z  $   ~  \   �                          
                              	    Feed subscription failed - notification handler doesn't respond correctly. Feed subscription failed: Not a valid feed. Only http-post notifications are supported at this time. Request must be POST. Thanks for the subscription. When the feed(s) update(s), you will be notified. The RSSCloud plugin enables your StatusNet instance to publish real-time updates for profile RSS feeds using the <a href="http://rsscloud.org/">RSSCloud protocol</a>. The following parameters were missing from the request body: %s. This resource requires an HTTP GET. This resource requires an HTTP POST. You must provide at least one valid profile feed url (url1, url2, url3 ... urlN). Project-Id-Version: StatusNet - RSSCloud
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:30+0000
Language-Team: Interlingua <http://translatewiki.net/wiki/Portal:ia>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:36:54+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: ia
X-Message-Group: #out-statusnet-plugin-rsscloud
Plural-Forms: nplurals=2; plural=(n != 1);
 Le subscription al syndication ha fallite - le gestor de notification non responde correctemente. Le subscription al syndication ha fallite: Non un syndication valide. Solmente le notificationes http-post es presentemente supportate. Requesta debe esser POST. Gratias pro le subscription. Quando le syndication(es) se actualisa, tu recipera notification. Le plug-in RSSCloud permitte que tu installation de SatusNet publica actualisationes in directo de syndicationes RSS de profilos usante le <a href="http://rsscloud.org/">protocollo RSSCloud</a>. Le sequente parametros mancava del corpore del requesta: %s. Iste ressource require un HTTP GET. Iste ressource require un HTTP POST. Tu debe fornir al minus un URL de syndication de profilo valide (url1, url2, url3 ... urlN). 