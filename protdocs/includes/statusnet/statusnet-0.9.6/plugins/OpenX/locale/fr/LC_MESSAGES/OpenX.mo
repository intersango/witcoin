��          �      <      �     �     �     �     �     �     �            &   )  ?   P  	   �     �     �  
   �  
   �     �     �  A  �  !   6     X     d     x     �  $   �     �     �  (   �  _     	   s     }  !   �     �     �     �     �         
                      	                                                                      Ad script URL BUTTONSave Leaderboard Leaderboard zone Medium rectangle Medium rectangle zone OpenX OpenX configuration OpenX settings for this StatusNet site Plugin for <a href="http://www.openx.org/">OpenX Ad Server</a>. Rectangle Rectangle zone Save OpenX settings Script URL Skyscraper TITLEOpenX Wide skyscraper zone Project-Id-Version: StatusNet - OpenX
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:01+0000
Language-Team: French <http://translatewiki.net/wiki/Portal:fr>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 19:00:18+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: fr
X-Message-Group: #out-statusnet-plugin-openx
Plural-Forms: nplurals=2; plural=(n > 1);
 Adresse URL du script d’annonce Sauvegarder Panneau de commande Zone de format tableau de bord Rectangle moyen Zone rectangulaire de taille moyenne OpenX Configuration d’OpenX Paramètres OpenX pour ce site StatusNet Module complémentaire pour le <a href="http://www.openx.org/">serveur de publicité OpenX</a>. Rectangle Zone de forme rectangulaire Sauvegarder les paramètres OpenX Adresse URL du script Bannière verticale OpenX Zone de format vertical large 