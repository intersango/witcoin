��          ,      <       P   �   Q   Q  "  :  t                     The autocomplete plugin allows users to autocomplete screen names in @ replies. When an "@" is typed into the notice text area, an autocomplete box is displayed populated with the user's friend' screen names. Project-Id-Version: StatusNet - Autocomplete
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:12+0000
Language-Team: Tagalog <http://translatewiki.net/wiki/Portal:tl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:04+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: tl
X-Message-Group: #out-statusnet-plugin-autocomplete
Plural-Forms: nplurals=2; plural=(n != 1);
 Ang pamasak na pangkusang pagkukumpleto ay nagpapahintulot sa mga tagagamit na kusang makumpleto ang mga pangalang bansag sa mga tugong @.  Kapag iminakinilya ang "@" sa lugar ng teksto ng pabatid, isang kahong na pangkusang pagkukumpleto ang ipapakita na nilagyan ng mga pangalang bansag ng kaibigan ng tagagamit. 