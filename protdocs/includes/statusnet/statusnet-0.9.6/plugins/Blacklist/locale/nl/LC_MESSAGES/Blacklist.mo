��          �      |      �  &   �  &     	   ?     I  !   h  9   �     �  	   �  '   �  ,     $   2     W     \     o     �     �  (   �  (   �  $   �     $     C  I  b  1   �  7   �       5   #  +   Y  A   �     �     �  0   �  :     +   P     |     �     �     �  %   �  .   �  4   	  1   O	  '   �	  -   �	                       	                                                             
             Add this homepage pattern to blacklist Add this nickname pattern to blacklist Blacklist Blacklisted URLs and nicknames Can't subscribe to nickname "%s". Keeps a blacklist of forbidden nickname and URL patterns. MENUBlacklist Nicknames Patterns of URLs to block, one per line Patterns of nicknames to block, one per line Posts from nickname "%s" disallowed. Save Save site settings TOOLTIPBlacklist configuration URLs Users from "%s" blocked. You may not register with homepage "%s". You may not register with nickname "%s". You may not use URL "%s" in notices. You may not use homepage "%s". You may not use nickname "%s". Project-Id-Version: StatusNet - Blacklist
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-28 23:09:57+0000
Language-Team: Dutch <http://translatewiki.net/wiki/Portal:nl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-28 00:09:54+0000
X-Generator: MediaWiki 1.17alpha (r75629); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: nl
X-Message-Group: #out-statusnet-plugin-blacklist
Plural-Forms: nplurals=2; plural=(n != 1);
 Dit homepagepatroon aan de zwarte lijst toevoegen Dit gebruikersnaampatroon aan de zwarte lijst toevoegen Zwarte lijst URL's en gebruikersnamen die op de zwarte lijst staan U kunt niet abonneren op de gebruiker "%s". Houdt een lijst bij van verboden gebruikersnamen en URL-patronen. Zwarte lijst Gebruikersnamen Patronen van te blokkeren URL's. Eén per regel. Patronen van te blokkeren gebruikersnamen. Eén per regel. Gebruiker "%s" mag geen berichten plaatsen. Opslaan Websiteinstellingen opslaan Instellingen voor zwarte lijst URL's Gebruikers van "%s" zijn geblokkeerd. U kunt niet registreren met "%s" als homepage. U kunt niet registreren met "%s" als gebruikersnaam. U mag de URL "%s" niet gebruiken in mededelingen. U mag "%s" niet als homepage instellen. U mag "%s" niet als gebruikersnaam gebruiken. 