��          �      �       0  &   1  &   X  	        �  9   �  	   �  '   �  ,        A     F     Y  $   ^  J  �  6   �  ;        A  .   P  F        �  0   �  :     	   @     J     j  :   o                          
                  	              Add this homepage pattern to blacklist Add this nickname pattern to blacklist Blacklist Blacklisted URLs and nicknames Keeps a blacklist of forbidden nickname and URL patterns. Nicknames Patterns of URLs to block, one per line Patterns of nicknames to block, one per line Save Save site settings URLs You may not use URL "%s" in notices. Project-Id-Version: StatusNet - Blacklist
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:17+0000
Language-Team: German <http://translatewiki.net/wiki/Portal:de>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:05+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: de
X-Message-Group: #out-statusnet-plugin-blacklist
Plural-Forms: nplurals=2; plural=(n != 1);
 Dieses Homepage-Muster zur schwarzen Liste hinzufügen Dieses Benutzernamen-Muster zur schwarzen Liste hinzufügen Schwarze Liste URLs und Benutzernamen auf der schwarzen Liste Hält eine schwarze Liste der verbotenen Benutzernamen und URL-Muster. Benutzernamen Muster der zu blockierenden URLS, eine pro Zeile Muster der zu blockierenden Benutzernamen, einer pro Zeile Speichern Website-Einstellungen speichern URLs Du darfst nicht die URL „%s“ in Nachrichten verwenden. 