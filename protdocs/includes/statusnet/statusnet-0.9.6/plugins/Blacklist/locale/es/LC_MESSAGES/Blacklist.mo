��          �      �       0  &   1  &   X  	        �  9   �  	   �  '   �  ,        A     F     Y  $   ^  K  �  :   �  :   
     E  4   Q  L   �     �  *   �  9        K  #   S     w  /   |                          
                  	              Add this homepage pattern to blacklist Add this nickname pattern to blacklist Blacklist Blacklisted URLs and nicknames Keeps a blacklist of forbidden nickname and URL patterns. Nicknames Patterns of URLs to block, one per line Patterns of nicknames to block, one per line Save Save site settings URLs You may not use URL "%s" in notices. Project-Id-Version: StatusNet - Blacklist
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:17+0000
Language-Team: Spanish <http://translatewiki.net/wiki/Portal:es>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:05+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: es
X-Message-Group: #out-statusnet-plugin-blacklist
Plural-Forms: nplurals=2; plural=(n != 1);
 Añadir este patrón de página principal a la lista negra Añadir este patrón de nombre de usuario a la lista negra Lista negra URL y nombres de usuario incluidos en la lista negra Mantiene una lista negra de patrones de nombres de usuario y URL prohibidos. Nombres de usuario Patrones de URL a bloquear, uno por línea Patrones de nombres de usuario a bloquear, uno por línea Guardar Guardar la configuración del sitio URLs No puedes utilizar el URL "%s" en los mensajes. 