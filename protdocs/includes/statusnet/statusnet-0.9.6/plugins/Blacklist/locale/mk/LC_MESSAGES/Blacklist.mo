��          �      |      �  &   �  &     	   ?     I  !   h  9   �     �  	   �  '   �  ,     $   2     W     \     o     �     �  (   �  (   �  $   �     $     C  c  b  _   �  K   &     r  A   �  R   �  j        �     �  [   �  Y   	  M   _	     �	  9   �	  /   �	     &
  =   7
  g   u
  T   �
  e   2  \   �  I   �                       	                                                             
             Add this homepage pattern to blacklist Add this nickname pattern to blacklist Blacklist Blacklisted URLs and nicknames Can't subscribe to nickname "%s". Keeps a blacklist of forbidden nickname and URL patterns. MENUBlacklist Nicknames Patterns of URLs to block, one per line Patterns of nicknames to block, one per line Posts from nickname "%s" disallowed. Save Save site settings TOOLTIPBlacklist configuration URLs Users from "%s" blocked. You may not register with homepage "%s". You may not register with nickname "%s". You may not use URL "%s" in notices. You may not use homepage "%s". You may not use nickname "%s". Project-Id-Version: StatusNet - Blacklist
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-28 23:09:57+0000
Language-Team: Macedonian <http://translatewiki.net/wiki/Portal:mk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-28 00:09:54+0000
X-Generator: MediaWiki 1.17alpha (r75629); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: mk
X-Message-Group: #out-statusnet-plugin-blacklist
Plural-Forms: nplurals=2; plural=(n == 1 || n%10 == 1) ? 0 : 1;
 Додај го овој вид домашна страница  во црниот список Додај го овој вид прекар во црниот список Црн список URL-адреси и прекари на црниот список Не можете да се претплатите на прекарот „%s“. Води црн список на забранети видови на прекари и URL-адреси. Црн список Прекари Видови URL-адреси за блокирање, по една во секој ред Видови прекари за блокирање, по еден во секој ред Објавите од прекарот „%s“ не се дозволени. Зачувај Зачувај поставки на мреж. место Поставки за црниот список URL-адреси Корисниците од „%s“ се блокирани. Не можете да се регистрирате со домашната страница „%s“. Не можете да се регистрирате со прекарот „%s“. Не можете да ја користите URL-адресата „%s“ во забелешки. Не можете да ја користите домашната страница „%s“. Не можете да го користите прекарот „%s“. 