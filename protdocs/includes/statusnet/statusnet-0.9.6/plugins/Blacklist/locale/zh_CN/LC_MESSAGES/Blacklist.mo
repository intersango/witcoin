��          �      �       0  &   1  &   X  	        �  9   �  	   �  '   �  ,        A     F     Y  $   ^  Y  �  !   �  !   �  	   !     +  3   G     {  !   �  $   �     �     �     �  &   �                          
                  	              Add this homepage pattern to blacklist Add this nickname pattern to blacklist Blacklist Blacklisted URLs and nicknames Keeps a blacklist of forbidden nickname and URL patterns. Nicknames Patterns of URLs to block, one per line Patterns of nicknames to block, one per line Save Save site settings URLs You may not use URL "%s" in notices. Project-Id-Version: StatusNet - Blacklist
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:17+0000
Language-Team: Simplified Chinese <http://translatewiki.net/wiki/Portal:zh-hans>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:05+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: zh-hans
X-Message-Group: #out-statusnet-plugin-blacklist
Plural-Forms: nplurals=1; plural=0;
 向黑名单添加此主页规则 向黑名单添加此昵称规则 黑名单 黑名单中的URL和昵称 为被禁止的昵称和URL模板创建黑名单。 昵称 禁止的URL规则，每行一个 禁止的昵称规则，每行一个 保存 保存网站设置 URL 你不能在提醒中使用URL '%s'。 