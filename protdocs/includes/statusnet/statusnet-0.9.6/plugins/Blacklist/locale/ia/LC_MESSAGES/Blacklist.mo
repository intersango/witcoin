��          �      |      �  &   �  &     	   ?     I  !   h  9   �     �  	   �  '   �  ,     $   2     W     \     o     �     �  (   �  (   �  $   �     $     C  O  b  4   �  /   �       !   #  '   E  G   m     �     �  /   �  .   �  )   ,     V  %   c     �     �     �  5   �  0   	  &   5	  )   \	  $   �	                       	                                                             
             Add this homepage pattern to blacklist Add this nickname pattern to blacklist Blacklist Blacklisted URLs and nicknames Can't subscribe to nickname "%s". Keeps a blacklist of forbidden nickname and URL patterns. MENUBlacklist Nicknames Patterns of URLs to block, one per line Patterns of nicknames to block, one per line Posts from nickname "%s" disallowed. Save Save site settings TOOLTIPBlacklist configuration URLs Users from "%s" blocked. You may not register with homepage "%s". You may not register with nickname "%s". You may not use URL "%s" in notices. You may not use homepage "%s". You may not use nickname "%s". Project-Id-Version: StatusNet - Blacklist
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-28 23:09:57+0000
Language-Team: Interlingua <http://translatewiki.net/wiki/Portal:ia>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-28 00:09:54+0000
X-Generator: MediaWiki 1.17alpha (r75629); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: ia
X-Message-Group: #out-statusnet-plugin-blacklist
Plural-Forms: nplurals=2; plural=(n != 1);
 Adder iste patrono de pagina personal al lista nigre Adder iste patrono de pseudonymo al lista nigre Lista nigre URLs e pseudonymos in lista nigre Non pote subscriber al pseudonymo "%s". Tene un lista nigre de pseudonymos e patronos de adresse URL prohibite. Lista nigre Pseudonymos Patronos de adresses URL a blocar, un per linea Patronos de pseudonymos a blocar, un per linea Notas del pseudonymo "%s" non permittite. Salveguardar Salveguardar configurationes del sito Configuration del lista nigre Adresses URL Usatores de "%s" blocate. Tu non pote registrar te con le pagina personal "%s". Tu non pote registrar te con le pseudonymo "%s". Tu non pote usar le URL "%s" in notas. Tu non pote usar le pagina personal "%s". Tu non pote usar le pseudonymo "%s". 