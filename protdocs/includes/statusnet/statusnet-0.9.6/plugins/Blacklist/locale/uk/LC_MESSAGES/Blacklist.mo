��          �      |      �  &   �  &     	   ?     I  !   h  9   �     �  	   �  '   �  ,     $   2     W     \     o     �     �  (   �  (   �  $   �     $     C  �  b  U     C   b     �  Z   �  H     o   d     �     �  q   �  u   q	  G   �	     /
  4   @
  4   u
     �
  :   �
  h   �
  f   _  w   �  Q   >  L   �                       	                                                             
             Add this homepage pattern to blacklist Add this nickname pattern to blacklist Blacklist Blacklisted URLs and nicknames Can't subscribe to nickname "%s". Keeps a blacklist of forbidden nickname and URL patterns. MENUBlacklist Nicknames Patterns of URLs to block, one per line Patterns of nicknames to block, one per line Posts from nickname "%s" disallowed. Save Save site settings TOOLTIPBlacklist configuration URLs Users from "%s" blocked. You may not register with homepage "%s". You may not register with nickname "%s". You may not use URL "%s" in notices. You may not use homepage "%s". You may not use nickname "%s". Project-Id-Version: StatusNet - Blacklist
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-28 23:09:57+0000
Language-Team: Ukrainian <http://translatewiki.net/wiki/Portal:uk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-28 00:09:54+0000
X-Generator: MediaWiki 1.17alpha (r75629); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: uk
X-Message-Group: #out-statusnet-plugin-blacklist
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 Додати цей шаблон веб-адреси до чорного списку Додати цей нікнейм до чорного списку Чорний список URL-адреси і нікнеми, що містяться в чорному списку Не можу підписатися до користувача «%s». Зберігає чорний список заборонених нікнеймів та URL-шаблонів. Чорний список Нікнейми Шаблони URL-адрес, котрі будуть блокуватися (по одному на рядок) Шаблони нікнеймів, котрі будуть блокуватися (по одному на рядок) Дописи від користувача «%s» заборонені. Зберегти Зберегти налаштування сайту Конфігурація чорного списку URL-адреси Користувачів з «%s» заблоковано. Ви не можете зареєструватися, вказавши «%s» як веб-адресу. Ви не можете зареєструватися, використавши нікнейм «%s». Ви не можете використовувати URL-адресу «%s» в своїх повідомленнях. Ви не можете використовувати веб-адресу «%s». Ви не можете використовувати нікнейм «%s». 