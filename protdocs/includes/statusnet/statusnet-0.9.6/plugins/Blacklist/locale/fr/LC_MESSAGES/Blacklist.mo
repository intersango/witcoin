��          �      �       0  &   1  &   X  	        �  9   �  	   �  '   �  ,        A     F     Y  $   ^  I  �  7   �  1        7  %   C  F   i     �  0   �  *   �       #   $     H  ;   U                          
                  	              Add this homepage pattern to blacklist Add this nickname pattern to blacklist Blacklist Blacklisted URLs and nicknames Keeps a blacklist of forbidden nickname and URL patterns. Nicknames Patterns of URLs to block, one per line Patterns of nicknames to block, one per line Save Save site settings URLs You may not use URL "%s" in notices. Project-Id-Version: StatusNet - Blacklist
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:17+0000
Language-Team: French <http://translatewiki.net/wiki/Portal:fr>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:05+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: fr
X-Message-Group: #out-statusnet-plugin-blacklist
Plural-Forms: nplurals=2; plural=(n > 1);
 Ajouter ce motif de pages d’accueil à la liste noire Ajouter ce motif de pseudonymes à la liste noire Liste noire Liste noire d’URL et de pseudonymes Maintient une liste noire des pseudonymes et motifs d’URL interdits. Pseudonymes Motifs d’adresses URL à bloquer, un par ligne Motifs de surnoms à bloquer, un par ligne Sauvegarder Sauvegarder les paramètres du site Adresses URL Vous ne pouvez pas utiliser l’URL « %s » dans les avis. 