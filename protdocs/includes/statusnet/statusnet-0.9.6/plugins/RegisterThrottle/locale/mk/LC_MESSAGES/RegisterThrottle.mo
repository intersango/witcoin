��          L      |       �   /   �      �   /   �   :   !  9   \  q  �  Y     A   b  s   �  ]     m   v                                         A banned user has registered from this address. Cannot find IP address. Cannot find user after successful registration. Throttles excessive registration from a single IP address. Too many registrations. Take a break and try again later. Project-Id-Version: StatusNet - RegisterThrottle
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:25+0000
Language-Team: Macedonian <http://translatewiki.net/wiki/Portal:mk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 19:00:59+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: mk
X-Message-Group: #out-statusnet-plugin-registerthrottle
Plural-Forms: nplurals=2; plural=(n == 1 || n%10 == 1) ? 0 : 1;
 Од оваа адреса се регистрирал забранет корисник. Не можам да ја пронајдам IP-адресата. Не можам да го пронајдам корисникот по успешната регистрација. Истиснува прекумерни регистрации од една IP-адреса. Премногу регистрации. Направете пауза и обидете се подоцна. 