��          D      l       �      �   /   �   :   �   9     X  F     �  <   �  >   �  N   :                          Cannot find IP address. Cannot find user after successful registration. Throttles excessive registration from a single IP address. Too many registrations. Take a break and try again later. Project-Id-Version: StatusNet - RegisterThrottle
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:25+0000
Language-Team: German <http://translatewiki.net/wiki/Portal:de>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 19:00:59+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: de
X-Message-Group: #out-statusnet-plugin-registerthrottle
Plural-Forms: nplurals=2; plural=(n != 1);
 Kann IP-Addresse nicht finden. Kann Benutzer nach erfolgreicher Registrierung nicht finden. Drosselt exzessive Registrierungen einer einzelnen IP-Adresse. Zu viele Registrierungen. Mach eine Pause and versuche es später noch einmal. 