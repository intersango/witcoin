��          L      |       �   /   �      �   /   �   :   !  9   \  W  �  :   �  %   )  G   O  ?   �  O   �                                         A banned user has registered from this address. Cannot find IP address. Cannot find user after successful registration. Throttles excessive registration from a single IP address. Too many registrations. Take a break and try again later. Project-Id-Version: StatusNet - RegisterThrottle
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:25+0000
Language-Team: French <http://translatewiki.net/wiki/Portal:fr>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 19:00:59+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: fr
X-Message-Group: #out-statusnet-plugin-registerthrottle
Plural-Forms: nplurals=2; plural=(n > 1);
 Un utilisateur banni s’est inscrit depuis cette adresse. Impossible de trouver l’adresse IP. Impossible de trouver l’utilisateur après un enregistrement réussi. Évite les inscriptions excessives depuis une même adresse IP. Inscriptions trop nombreuses. Faites une pause et essayez à nouveau plus tard. 