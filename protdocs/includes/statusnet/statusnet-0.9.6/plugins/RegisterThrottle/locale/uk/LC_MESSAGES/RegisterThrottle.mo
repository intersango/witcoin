��          L      |       �   /   �      �   /   �   :   !  9   \  �  �  k   N  3   �  k   �  m   Z  x   �                                         A banned user has registered from this address. Cannot find IP address. Cannot find user after successful registration. Throttles excessive registration from a single IP address. Too many registrations. Take a break and try again later. Project-Id-Version: StatusNet - RegisterThrottle
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:25+0000
Language-Team: Ukrainian <http://translatewiki.net/wiki/Portal:uk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 19:00:59+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: uk
X-Message-Group: #out-statusnet-plugin-registerthrottle
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 Заблокований користувач був зареєстрований з цієї адреси. Не вдається знайти IP-адресу. Не вдається знайти користувача після успішної реєстрації. Цей додаток обмежує кількість реєстрацій з певної IP-адреси. Забагато реєстрацій. Випийте поки що кави і повертайтесь пізніше. 