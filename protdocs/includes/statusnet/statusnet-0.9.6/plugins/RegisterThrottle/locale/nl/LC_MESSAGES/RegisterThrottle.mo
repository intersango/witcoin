��          L      |       �   /   �      �   /   �   :   !  9   \  W  �  F   �  &   5  <   \  ?   �  >   �                                         A banned user has registered from this address. Cannot find IP address. Cannot find user after successful registration. Throttles excessive registration from a single IP address. Too many registrations. Take a break and try again later. Project-Id-Version: StatusNet - RegisterThrottle
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:25+0000
Language-Team: Dutch <http://translatewiki.net/wiki/Portal:nl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 19:00:59+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: nl
X-Message-Group: #out-statusnet-plugin-registerthrottle
Plural-Forms: nplurals=2; plural=(n != 1);
 Er is een geblokkeerde gebruiker die vanaf dit adres is geregistreerd. Het IP-adres kon niet gevonden worden. Het was niet mogelijk de gebruiker te vinden na registratie. Beperkt excessieve aantallen registraties vanaf één IP-adres. Te veel registraties. Wacht even en probeer het later opnieuw. 