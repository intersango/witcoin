��          <      \       p   !   q       �   4   �   C  �   4   -  -   b  @   �                   Couldn't connect to %1$s on %2$s. Error adding meteor message "%s" Plugin to do "real time" updates using Comet/Bayeux. Project-Id-Version: StatusNet - Meteor
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:49+0000
Language-Team: Dutch <http://translatewiki.net/wiki/Portal:nl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:36:30+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: nl
X-Message-Group: #out-statusnet-plugin-meteor
Plural-Forms: nplurals=2; plural=(n != 1);
 Het was niet mogelijk te verbinden met %1$s op %2$s. Fout bij het toevoegen van meteorbericht "%s" Plug-in voor het maken van "real time" updates via Comet/Bayeux. 