��          <      \       p   !   q       �   4   �   T  �   #   >  +   b  D   �                   Couldn't connect to %1$s on %2$s. Error adding meteor message "%s" Plugin to do "real time" updates using Comet/Bayeux. Project-Id-Version: StatusNet - Meteor
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:49+0000
Language-Team: Norwegian (bokmål)‬ <http://translatewiki.net/wiki/Portal:no>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-09 14:36:30+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: no
X-Message-Group: #out-statusnet-plugin-meteor
Plural-Forms: nplurals=2; plural=(n != 1);
 Kunne ikke koble til %1$s på %2$s. Mislyktes å legge til meteormelding «%s» Utvidelse for å gjøre «sanntids»-oppdateringer med Comet/Bayeux. 