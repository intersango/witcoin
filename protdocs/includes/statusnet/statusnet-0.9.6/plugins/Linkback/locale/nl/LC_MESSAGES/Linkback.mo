��          ,      <       P   �   Q   G  <  �   �                     Notify blog authors when their posts have been linked in microblog notices using <a href="http://www.hixie.ch/specs/pingback/pingback">Pingback</a> or <a href="http://www.movabletype.org/docs/mttrackback.html">Trackback</a> protocols. Project-Id-Version: StatusNet - Linkback
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:45+0000
Language-Team: Dutch <http://translatewiki.net/wiki/Portal:nl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:54+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: nl
X-Message-Group: #out-statusnet-plugin-linkback
Plural-Forms: nplurals=2; plural=(n != 1);
 Blogauteurs laten weten wanneer naar hun berichten wordt verwezen in mededelingen met behulp van de protocollen <a href="http://www.hixie.ch/specs/pingback/pingback">Pingback</a> of <a href="http://www.movabletype.org/docs/mttrackback.html">Trackback</a>. 