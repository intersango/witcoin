��          ,      <       P   �   Q   E  <  �   �                     Notify blog authors when their posts have been linked in microblog notices using <a href="http://www.hixie.ch/specs/pingback/pingback">Pingback</a> or <a href="http://www.movabletype.org/docs/mttrackback.html">Trackback</a> protocols. Project-Id-Version: StatusNet - Linkback
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:45+0000
Language-Team: Indonesian <http://translatewiki.net/wiki/Portal:id>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:54+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: id
X-Message-Group: #out-statusnet-plugin-linkback
Plural-Forms: nplurals=1; plural=0;
 Memberitahu penulis blog apabila kiriman mereka telah terhubung dengan mikroblog menggunakan protokol <a href="http://www.hixie.ch/specs/pingback/pingback">Pingback</a> atau <a href="http://www.movabletype.org/docs/mttrackback.html">Trackback</a> . 