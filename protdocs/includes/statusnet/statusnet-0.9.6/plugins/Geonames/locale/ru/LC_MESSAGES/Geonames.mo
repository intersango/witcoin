��          ,      <       P   �   Q   �  �     �                     Uses <a href="http://geonames.org/">Geonames</a> service to get human-readable names for locations based on user-provided lat/long pairs. Project-Id-Version: StatusNet - Geonames
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:38+0000
Language-Team: Russian <http://translatewiki.net/wiki/Portal:ru>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:51+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: ru
X-Message-Group: #out-statusnet-plugin-geonames
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 Использование сервиса <a href="http://geonames.org/">GeoNames</a> для получения понятных для человека названий по указанным пользователем географическим координатам. 