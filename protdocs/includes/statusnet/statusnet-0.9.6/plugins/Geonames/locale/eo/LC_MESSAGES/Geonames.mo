��          ,      <       P   �   Q   K  �   �   '                     Uses <a href="http://geonames.org/">Geonames</a> service to get human-readable names for locations based on user-provided lat/long pairs. Project-Id-Version: StatusNet - Geonames
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:37+0000
Language-Team: Esperanto <http://translatewiki.net/wiki/Portal:eo>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:51+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: eo
X-Message-Group: #out-statusnet-plugin-geonames
Plural-Forms: nplurals=2; plural=(n != 1);
 Uziĝas <a href="http://geonames.org/">Geonames</a> servo por akiri kompreneblan nomon de lokoj baze de latituda-longituda paro donita de uzanto. 