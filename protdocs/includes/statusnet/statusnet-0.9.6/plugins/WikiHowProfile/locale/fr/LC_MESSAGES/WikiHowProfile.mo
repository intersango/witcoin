��          <      \       p   e   q      �      �   S    ~   b  )   �  5                      Fetches avatar and other profile information for WikiHow users when setting up an account via OpenID. Invalid avatar URL %s. Unable to fetch avatar from %s. Project-Id-Version: StatusNet - WikiHowProfile
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:47:56+0000
Language-Team: French <http://translatewiki.net/wiki/Portal:fr>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:34:39+0000
X-Generator: MediaWiki 1.17alpha (r75596); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: fr
X-Message-Group: #out-statusnet-plugin-wikihowprofile
Plural-Forms: nplurals=2; plural=(n > 1);
 Récupère l’avatar et les autres informations de profil des utilisateurs WikiHow lorsqu’ils créent un compte via OpenID. Adresse URL d’avatar « %s » invalide. Impossible de récupérer l’avatar depuis « %s ». 