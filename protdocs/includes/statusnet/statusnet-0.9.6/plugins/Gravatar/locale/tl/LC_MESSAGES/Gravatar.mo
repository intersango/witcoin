��          �      �       0     1  (   5     ^     n  :   �  4   �     �     �       m     2   �  5   �  I  �     5  9   =     w     �  H   �  M   �     ;     B     V  �   j  J      =   K           	                   
                            Add Failed to save Gravatar to the database. Gravatar added. Gravatar removed. If you want to remove your Gravatar image, click "Remove". If you want to use your Gravatar image, click "Add". Remove Remove Gravatar Set Gravatar The Gravatar plugin allows users to use their <a href="http://www.gravatar.com/">Gravatar</a> with StatusNet. To use a Gravatar first enter in an email address. You do not have an email address set in your profile. Project-Id-Version: StatusNet - Gravatar
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:40+0000
Language-Team: Tagalog <http://translatewiki.net/wiki/Portal:tl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:57:04+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: tl
X-Message-Group: #out-statusnet-plugin-gravatar
Plural-Forms: nplurals=2; plural=(n != 1);
 Idagdag Nabigong sagipin ang Gravatar sa iyong kalipunan ng dato. Idinagdag ang Gravatar. Inalis ang Gravatar. Kung nais mong alisin ang larawan mo ng Gravatar, pindutin ang "Alisin". Kung nais mong gamitin ang iyong larawan ng Gravatar, pindutin ang "Idagdag". Alisin Alisin ang Gravatar Itakda ang Gravatar Ang pamasak na Gravatar ay nagpapahintulot sa mga tagagamit na gamitin ang kanilang <a href="http://www.gravatar.com/">Gravatar</a> na may StatusNet. Upang gamitin ang isang Gravatar ipasok muna ang isang tirahan ng e-liham. Wala kang tirahan ng e-liham na nakatakda sa iyong balangkas. 