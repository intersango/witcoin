��          �      �       0     1  (   5     ^     n  :   �  4   �     �     �       m     2   �  5   �  M  �     9  8   ?     x     �  9   �  4   �               $  s   6  7   �  6   �           	                   
                            Add Failed to save Gravatar to the database. Gravatar added. Gravatar removed. If you want to remove your Gravatar image, click "Remove". If you want to use your Gravatar image, click "Add". Remove Remove Gravatar Set Gravatar The Gravatar plugin allows users to use their <a href="http://www.gravatar.com/">Gravatar</a> with StatusNet. To use a Gravatar first enter in an email address. You do not have an email address set in your profile. Project-Id-Version: StatusNet - Gravatar
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:40+0000
Language-Team: Interlingua <http://translatewiki.net/wiki/Portal:ia>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:57:04+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: ia
X-Message-Group: #out-statusnet-plugin-gravatar
Plural-Forms: nplurals=2; plural=(n != 1);
 Adder Falleva de salveguardar le Gravatar in le base de datos. Gravatar addite. Gravatar removite. Si tu vole remover tu imagine Gravatar, clicca "Remover". Si tu vole usar tu imagine Gravatar, clicca "Adder". Remover Remover Gravatar Stabilir Gravatar Le plug-in Gravatar permitte al usatores de usar lor <a href="http://www.gravatar.com/">Gravatar</a> con StatusNet. Pro usar un Gravatar, entra primo un adresse de e-mail. Tu non ha un adresse de e-mail definite in tu profilo. 