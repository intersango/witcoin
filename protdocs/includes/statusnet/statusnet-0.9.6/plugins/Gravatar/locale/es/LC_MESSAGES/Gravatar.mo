��          �      �       0     1  (   5     ^     n  :   �  4   �     �     �       m     2   �  5   �  I  �     5  .   =     l       A   �  <   �               .  x   B  S   �  I              	                   
                            Add Failed to save Gravatar to the database. Gravatar added. Gravatar removed. If you want to remove your Gravatar image, click "Remove". If you want to use your Gravatar image, click "Add". Remove Remove Gravatar Set Gravatar The Gravatar plugin allows users to use their <a href="http://www.gravatar.com/">Gravatar</a> with StatusNet. To use a Gravatar first enter in an email address. You do not have an email address set in your profile. Project-Id-Version: StatusNet - Gravatar
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:40+0000
Language-Team: Spanish <http://translatewiki.net/wiki/Portal:es>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:57:04+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: es
X-Message-Group: #out-statusnet-plugin-gravatar
Plural-Forms: nplurals=2; plural=(n != 1);
 Añadir Error al guardar Gravatar en la base de datos. Gravatar agregado. Gravatar eliminado. Si desesa eliminar tu imagen de Gravatar, haz clic en "Eliminar". Si deseas utilizar tu imagen Gravatar, haz clic en "Agregar" Borrar Eliminar el Gravatar Definir un Gravatar La extensión Gravatar permite a los usuarios utilizar su <a href="http://www.gravatar.com/">Gravatar</a> con StatusNet. Para utilizar un Gravatar, primero introduce una dirección de correo electrónico. No tienes una dirección de correo electrónico establecida en tu perfil. 