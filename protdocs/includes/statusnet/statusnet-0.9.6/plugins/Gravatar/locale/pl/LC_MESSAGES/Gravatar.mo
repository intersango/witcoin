��          �      �       0     1  (   5     ^     n  :   �  4   �     �     �       m     2   �  5   �  �  �     ~  ;   �     �     �  A   �  B   3     v     |     �  y   �  A     &   V           	                   
                            Add Failed to save Gravatar to the database. Gravatar added. Gravatar removed. If you want to remove your Gravatar image, click "Remove". If you want to use your Gravatar image, click "Add". Remove Remove Gravatar Set Gravatar The Gravatar plugin allows users to use their <a href="http://www.gravatar.com/">Gravatar</a> with StatusNet. To use a Gravatar first enter in an email address. You do not have an email address set in your profile. Project-Id-Version: StatusNet - Gravatar
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:40+0000
Language-Team: Polish <http://translatewiki.net/wiki/Portal:pl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:57:04+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: pl
X-Message-Group: #out-statusnet-plugin-gravatar
Plural-Forms: nplurals=3; plural=(n == 1) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 Dodaj Zapisanie obrazu Gravatar w bzie danych nie powiodło się. Dodano obraz Gravatar. Usunięto obraz Gravatar. Aby usunąć obraz Gravatar, należy nacisnąć przycisk "Usuń". Aby używać obrazu Gravatar, należy nacisnąć przycisk "Dodaj". Usuń Usuń Gravatar Ustaw Gravatar Wtyczka Gravatar umożliwia użytkownikom używanie obrazów <a href="http://www.gravatar.com/">Gravatar</a> w StatusNet. Aby użyć obrazu Gravatar, należy najpierw podać adres e-mail. Nie ustawiono adresu e-mail w profilu. 