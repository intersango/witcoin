��          �      �       0     1  (   5     ^     n  :   �  4   �     �     �       m     2   �  5   �  a  �  
   M  a   X      �  $   �  �         �               3  �   K  T   	  D   ^           	                   
                            Add Failed to save Gravatar to the database. Gravatar added. Gravatar removed. If you want to remove your Gravatar image, click "Remove". If you want to use your Gravatar image, click "Add". Remove Remove Gravatar Set Gravatar The Gravatar plugin allows users to use their <a href="http://www.gravatar.com/">Gravatar</a> with StatusNet. To use a Gravatar first enter in an email address. You do not have an email address set in your profile. Project-Id-Version: StatusNet - Gravatar
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:40+0000
Language-Team: Macedonian <http://translatewiki.net/wiki/Portal:mk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:57:04+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: mk
X-Message-Group: #out-statusnet-plugin-gravatar
Plural-Forms: nplurals=2; plural=(n == 1 || n%10 == 1) ? 0 : 1;
 Додај Не успеав да го зачувам Gravatar-от во базата на податоци. Gravatar-от е додаден. Gravatar-от е отстранет. Ако сакате да ја отстраните Вашата слика од Gravatar, кликнете на „Отстрани“. Ако сакате да ја користите Вашата слика од Gravatar, кликнете на „Додај“. Отстрани Отстрани Gravatar Постави Gravatar Приклучокот Gravatar им овозможува на корисниците да го користат својот <a href="http://www.gravatar.com/">Gravatar</a> со StatusNet. За да користите Gravatar најпрвин внесете е-пошта. Во профилот немате назначено е-пошта. 