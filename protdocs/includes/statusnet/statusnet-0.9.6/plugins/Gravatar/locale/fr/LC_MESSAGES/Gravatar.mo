��          �      �       0     1  (   5     ^     n  :   �  4   �     �     �       m     2   �  5   �  G  �     3  ?   ;     {     �  N   �  K   �     <     D     Z  �   o  J   �  @   >           	                   
                            Add Failed to save Gravatar to the database. Gravatar added. Gravatar removed. If you want to remove your Gravatar image, click "Remove". If you want to use your Gravatar image, click "Add". Remove Remove Gravatar Set Gravatar The Gravatar plugin allows users to use their <a href="http://www.gravatar.com/">Gravatar</a> with StatusNet. To use a Gravatar first enter in an email address. You do not have an email address set in your profile. Project-Id-Version: StatusNet - Gravatar
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:40+0000
Language-Team: French <http://translatewiki.net/wiki/Portal:fr>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:57:04+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: fr
X-Message-Group: #out-statusnet-plugin-gravatar
Plural-Forms: nplurals=2; plural=(n > 1);
 Ajouter Impossible de sauvegarder le Gravatar dans la base de données. Gravatar ajouté. Gravatar supprimé. Si vous souhaitez supprimer votre image Gravatar, cliquez sur « Supprimer ». Si vous souhaitez utiliser votre image Gravatar, cliquez sur « Ajouter ». Enlever Supprimer le Gravatar Définir un Gravatar Le greffon Gravatar permet aux utilisateurs d’utiliser leur image <a href="http://www.gravatar.com/">Gravatar</a> avec StatusNet. Pour utiliser un Gravatar, veuillez d’abord saisir une adresse courriel. Vous n'avez pas d’adresse courriel définie dans votre profil. 