��          �      �       0     1  (   5     ^     n  :   �  4   �     �     �       m     2   �  5   �  W  �     C  (   J     s     �  @   �  =   �          %     5  t   E  5   �  (   �           	                   
                            Add Failed to save Gravatar to the database. Gravatar added. Gravatar removed. If you want to remove your Gravatar image, click "Remove". If you want to use your Gravatar image, click "Add". Remove Remove Gravatar Set Gravatar The Gravatar plugin allows users to use their <a href="http://www.gravatar.com/">Gravatar</a> with StatusNet. To use a Gravatar first enter in an email address. You do not have an email address set in your profile. Project-Id-Version: StatusNet - Gravatar
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:41+0000
Language-Team: Simplified Chinese <http://translatewiki.net/wiki/Portal:zh-hans>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:57:04+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: zh-hans
X-Message-Group: #out-statusnet-plugin-gravatar
Plural-Forms: nplurals=1; plural=0;
 添加 将 Gravatar 保存到数据库失败。 Gravatar 已添加。 Gravatar 已删除。 如果你想删除你的 Gravatar 图像，点击“删除”。 如果你想使用你的 Gravatar 图像，点击“添加” 删除 删除 Gravatar 设置 Gravatar Gravatar 插件可以让用户在 StatusNet 站点使用自己的 <a href="http://www.gravatar.com/">Gravatar</a>。 要使用 Gravatar 先要填写一个 email 地址。 你的账号没有设置 email 地址。 