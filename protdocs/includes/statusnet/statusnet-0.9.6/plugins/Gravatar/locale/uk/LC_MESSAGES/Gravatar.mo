��          �      �       0     1  (   5     ^     n  :   �  4   �     �     �       m     2   �  5   �  �  �     �  G   �     �     �  �     u   �          #     =  �   [  {     d   �           	                   
                            Add Failed to save Gravatar to the database. Gravatar added. Gravatar removed. If you want to remove your Gravatar image, click "Remove". If you want to use your Gravatar image, click "Add". Remove Remove Gravatar Set Gravatar The Gravatar plugin allows users to use their <a href="http://www.gravatar.com/">Gravatar</a> with StatusNet. To use a Gravatar first enter in an email address. You do not have an email address set in your profile. Project-Id-Version: StatusNet - Gravatar
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:41+0000
Language-Team: Ukrainian <http://translatewiki.net/wiki/Portal:uk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:57:04+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: uk
X-Message-Group: #out-statusnet-plugin-gravatar
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 Додати Не вдалося зберегти Gravatar до бази даних. Gravatar додано. Gravatar вилучено. Якщо ви бажаєте видалити свою аватару надану Gravatar, тисніть «Видалити». Якщо ви бажаєте використовувати аватари Gravatar, тисніть «Додати». Видалити Видалити Gravatar Встановити Gravatar Додаток Gravatar дозволяє користувачам встановлювати аватарки з <a href="http://www.gravatar.com/">Gravatar</a> для сайту StatusNet. Щоб використовувати Gravatar, спершу введіть адресу електронної пошти. У вашому профілі не вказано жодної електронної адреси. 