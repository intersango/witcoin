��    
      l      �       �      �      �        :     4   R     �     �  m   �  2     H  ?     �     �     �  E   �  G     	   L     V  y   h  M   �                     	       
                 Add Gravatar added. Gravatar removed. If you want to remove your Gravatar image, click "Remove". If you want to use your Gravatar image, click "Add". Remove Remove Gravatar The Gravatar plugin allows users to use their <a href="http://www.gravatar.com/">Gravatar</a> with StatusNet. To use a Gravatar first enter in an email address. Project-Id-Version: StatusNet - Gravatar
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:40+0000
Language-Team: German <http://translatewiki.net/wiki/Portal:de>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:57:04+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: de
X-Message-Group: #out-statusnet-plugin-gravatar
Plural-Forms: nplurals=2; plural=(n != 1);
 Hinzufügen Gravatar hinzugefügt. Gravatar entfernt. Falls Sie Ihr Gravatar Bild entfernen wollen, klicken sie "Entfernen" Falls Sie Ihr Gravatar Bild verwenden wollen, klicken sie "Hinzufügen" Entfernen Gravatar löschen Das Gravatar-Plugin erlaubt es Benutzern, ihr <a href="http://www.gravatar.com/">Gravatar</a> mit StatusNet zu verwenden. Um einen Gravatar zuverwenden geben Sie zunächst in eine E-Mail-Adresse ein. 