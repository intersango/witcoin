��          �      �       0     1  (   5     ^     n  :   �  4   �     �     �       m     2   �  5   �  L  �  	   8  ,   B     o     �  >   �  =   �               -  y   >  @   �  1   �           	                   
                            Add Failed to save Gravatar to the database. Gravatar added. Gravatar removed. If you want to remove your Gravatar image, click "Remove". If you want to use your Gravatar image, click "Add". Remove Remove Gravatar Set Gravatar The Gravatar plugin allows users to use their <a href="http://www.gravatar.com/">Gravatar</a> with StatusNet. To use a Gravatar first enter in an email address. You do not have an email address set in your profile. Project-Id-Version: StatusNet - Gravatar
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:40+0000
Language-Team: Portuguese <http://translatewiki.net/wiki/Portal:pt>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:57:04+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: pt
X-Message-Group: #out-statusnet-plugin-gravatar
Plural-Forms: nplurals=2; plural=(n != 1);
 Adicionar Erro ao guardar o Gravatar na base de dados. Gravatar adicionado. Gravatar removido. Se desejar remover a sua imagem Gravatar, clique em "Remover". Se desejar usar a sua imagem Gravatar, clique em "Adicionar". Remover Remover Gravatar Definir Gravatar O plugin Gravatar permite que os utilizadores usem o seu <a href="http://www.gravatar.com/">Gravatar</a> com o StatusNet. Para usar um Gravatar, primeiro introduza um endereço de email. Não definiu um endereço de email no seu perfil. 