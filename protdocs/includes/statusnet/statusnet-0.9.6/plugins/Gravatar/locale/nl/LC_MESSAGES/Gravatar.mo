��          �      �       0     1  (   5     ^     n  :   �  4   �     �     �       m     2   �  5   �  G  �  	   3  >   =     |     �  J   �  <   �     :     F     [  �   n  >   �  0   .           	                   
                            Add Failed to save Gravatar to the database. Gravatar added. Gravatar removed. If you want to remove your Gravatar image, click "Remove". If you want to use your Gravatar image, click "Add". Remove Remove Gravatar Set Gravatar The Gravatar plugin allows users to use their <a href="http://www.gravatar.com/">Gravatar</a> with StatusNet. To use a Gravatar first enter in an email address. You do not have an email address set in your profile. Project-Id-Version: StatusNet - Gravatar
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:40+0000
Language-Team: Dutch <http://translatewiki.net/wiki/Portal:nl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-23 18:57:04+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: nl
X-Message-Group: #out-statusnet-plugin-gravatar
Plural-Forms: nplurals=2; plural=(n != 1);
 Toevoegen Het was niet mogelijk de Gravatar in the database op te slaan. De Gravatar is toegevoegd. De Gravatar is verwijderd. Klik "Verwijderen" om uw afbeelding van Gravatar niet langer te gebruiken. Klik "Toevoegen" om uw afbeelding van Gravatar te gebruiken. Verwijderen Gravatar verwijderen Gravatar instellen De plug-in Gravatar maak het mogelijk dat gebruikers hun <a href="http://www.gravatar.com/">Gravatar</a> gebruiken in StatusNet. Voer eerst een e-mailadres in om Gravatar te kunnen gebruiken. U hebt geen e-mailadres ingesteld in uw profiel. 