��          4      L       `   e   a   '   �   I  �   y   9  5   �                    Allows forced group memberships and forces all notices to appear in groups that users were forced in. Could not join user %1$s to group %2$s. Project-Id-Version: StatusNet - ForceGroup
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:37+0000
Language-Team: Indonesian <http://translatewiki.net/wiki/Portal:id>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:51+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: id
X-Message-Group: #out-statusnet-plugin-forcegroup
Plural-Forms: nplurals=1; plural=0;
 Memungkinkan keanggotaan grup secara paksa dan memaksa semua pemberitahuan muncul di grup yang penggunanya dipaksa masuk. Tidak dapat menggabungkan pengguna %1$s ke grup %2$s. 