��          4      L       `   e   a   '   �   M  �   �   =  4   �                    Allows forced group memberships and forces all notices to appear in groups that users were forced in. Could not join user %1$s to group %2$s. Project-Id-Version: StatusNet - ForceGroup
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:37+0000
Language-Team: Spanish <http://translatewiki.net/wiki/Portal:es>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:51+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: es
X-Message-Group: #out-statusnet-plugin-forcegroup
Plural-Forms: nplurals=2; plural=(n != 1);
 Permite forzar inclusiones en grupos y fuerza la aparición de todos los mensajes en los grupos en los que los usuarios fueron incluidos a la fuerza. No se pudo incluir al usuario %1$s en el grupo %2$s. 