��          4      L       `   e   a   '   �   Q  �   z   A  6   �                    Allows forced group memberships and forces all notices to appear in groups that users were forced in. Could not join user %1$s to group %2$s. Project-Id-Version: StatusNet - ForceGroup
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:37+0000
Language-Team: Interlingua <http://translatewiki.net/wiki/Portal:ia>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:51+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: ia
X-Message-Group: #out-statusnet-plugin-forcegroup
Plural-Forms: nplurals=2; plural=(n != 1);
 Permitte fortiar le inscription in gruppos e fortia que tote le notas appare in gruppos in que usatores esseva inscribite. Non poteva inscriber le usator %1$s in le gruppo %2$s. 