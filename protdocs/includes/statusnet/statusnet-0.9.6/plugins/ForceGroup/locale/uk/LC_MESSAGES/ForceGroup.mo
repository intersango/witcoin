��          4      L       `   e   a   '   �   �  �   2  �  ^   �                    Allows forced group memberships and forces all notices to appear in groups that users were forced in. Could not join user %1$s to group %2$s. Project-Id-Version: StatusNet - ForceGroup
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:37+0000
Language-Team: Ukrainian <http://translatewiki.net/wiki/Portal:uk>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:51+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: uk
X-Message-Group: #out-statusnet-plugin-forcegroup
Plural-Forms: nplurals=3; plural=(n%10 == 1 && n%100 != 11) ? 0 : ( (n%10 >= 2 && n%10 <= 4 && (n%100 < 10 || n%100 >= 20)) ? 1 : 2 );
 Дозволяє примусове долучення користувачів до спільнот разом із дописами, котрі відповідають темі той чи іншої спільноти, до якої було примусово долучено користувача. Не вдалось долучити користувача %1$s до спільноти %2$s. 