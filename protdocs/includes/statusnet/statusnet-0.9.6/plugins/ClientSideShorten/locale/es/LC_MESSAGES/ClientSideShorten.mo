��          4      L       `   "   a   �   �   [    (   l  �   �                    'text' argument must be specified. ClientSideShorten causes the web interface's notice form to automatically shorten URLs as they entered, and before the notice is submitted. Project-Id-Version: StatusNet - ClientSideShorten
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:23+0000
Language-Team: Spanish <http://translatewiki.net/wiki/Portal:es>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:51+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: es
X-Message-Group: #out-statusnet-plugin-clientsideshorten
Plural-Forms: nplurals=2; plural=(n != 1);
 Debe especificarse el argumento 'texto'. ClientSideShorten hace que el formulario de mensaje de la interfaz web acorte las URL automáticamente conforme se introducen t antes de que se envíe el mensaje. 