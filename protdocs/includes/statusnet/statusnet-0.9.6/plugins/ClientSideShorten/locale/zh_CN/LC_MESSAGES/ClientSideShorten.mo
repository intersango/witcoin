��          4      L       `   "   a   �   �   i       z  o   �                    'text' argument must be specified. ClientSideShorten causes the web interface's notice form to automatically shorten URLs as they entered, and before the notice is submitted. Project-Id-Version: StatusNet - ClientSideShorten
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:24+0000
Language-Team: Simplified Chinese <http://translatewiki.net/wiki/Portal:zh-hans>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:51+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: zh-hans
X-Message-Group: #out-statusnet-plugin-clientsideshorten
Plural-Forms: nplurals=1; plural=0;
 需要定义'text' 变量。 客户端短网址（ClientSideShorten ）将在消息发布前自动在网页界面下缩短输入的网址。 