��          4      L       `   "   a   �   �   [    (   l  �   �                    'text' argument must be specified. ClientSideShorten causes the web interface's notice form to automatically shorten URLs as they entered, and before the notice is submitted. Project-Id-Version: StatusNet - ClientSideShorten
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-10-27 23:43+0000
PO-Revision-Date: 2010-10-27 23:46:23+0000
Language-Team: Tagalog <http://translatewiki.net/wiki/Portal:tl>
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-POT-Import-Date: 2010-10-18 20:29:51+0000
X-Generator: MediaWiki 1.17alpha (r75590); Translate extension (2010-09-17)
X-Translation-Project: translatewiki.net at http://translatewiki.net
X-Language-Code: tl
X-Message-Group: #out-statusnet-plugin-clientsideshorten
Plural-Forms: nplurals=2; plural=(n != 1);
 dapat tukuyin ang argumento ng 'teksto'. Ang ClientSideShorten ay nakapagsasanhi sa pormularyo ng pabatid ng ugnayang-mukha ng web na kusang paiksiin ang mga URL habang ipinapasok sila, at bago ipasa ang pabatid. 