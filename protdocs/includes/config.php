<?php
	global $costs, $domain, $site;

	$db = array(); $domain = array();

	// Extract port to distinguish between production and development environment
	$port = str_replace($_SERVER["SERVER_NAME"], "", $_SERVER["HTTP_HOST"]);
	if ($_SERVER["SERVER_NAME"] == "witcoin.com") { $domain["cat"] = "*"; $domain["address"] = "witcoin.com".$port; $site = "default"; }
	else if ($_SERVER["SERVER_NAME"] == "witcoin.i2p") { $domain["cat"] = "*"; $domain["address"] = "witcoin.i2p".$port; $site = "i2p"; }
	else if (preg_match("/^([a-zA-Z0-9.-]*)\.(witcoin\.(?:com|i2p))/", $_SERVER["HTTP_HOST"], $matches)) {
		if (preg_match("/^.*\.i2p/", $_SERVER["HTTP_HOST"])) $site = "i2p"; else $site = "default";
		$domain["cat"] = $matches[1]; $domain["address"] = $matches[2].$port;
	}
	else {
		if (is_dir("../vhosts/domains/".$_SERVER["SERVER_NAME"])) {
			$domain["address"] = $_SERVER["SERVER_NAME"].$port;
			$domain["cat"] = file_get_contents("../vhosts/domains/".$_SERVER["SERVER_NAME"]."/cat");
			$domain["logo"] = file_get_contents("../vhosts/domains/".$_SERVER["SERVER_NAME"]."/logo");
			$site = "3rd";
		} else { header("Location: "."http" . (isset($_SERVER["HTTPS"]) ? "s" : "") . "://witcoin.com/"); exit; }
	}

	$db["host"] = apache_getenv("pgh");
	$db["db"] = apache_getenv("pgd");
	$db["user"] = apache_getenv("pgu");
	$db["pass"] = apache_getenv("pgp");

	$costs = array(); // Store costs as strings to preserve accurate decimals for bcmath
	$costs["edit"] = "0.0001";
	$costs["new category"] = "0.1";
	$costs["new charity"] = "0.001";
	$costs["post"] = "0.0001";
	$costs["reply"] = "0.0001";
	$costs["upvote"] = "0.001";
?>