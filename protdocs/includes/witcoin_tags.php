<?php
	$tags = array();

	if ($category["categoryid"] != 0) $sql = "WITH RECURSIVE hcategories(categoryid, categorypid, depth, path) AS (
			(SELECT categoryid, categorypid, 0, array[categoryid]
			FROM categories
			WHERE categorypid = %d)
			UNION ALL
			(SELECT c.categoryid, c.categorypid, hcategories.depth+1, hcategories.path || c.categoryid
			FROM categories c
			JOIN hcategories ON (c.categorypid = hcategories.categoryid))
		) ";
	else $sql = "";

	$sql .= "SELECT posts.taglist FROM posts";

	if ($category["categoryid"] != 0) $sql .= " WHERE category = %d OR category IN (SELECT categoryid FROM hcategories)";

	$sql .= " ORDER BY posts.posted DESC";

	$result = db_query($sql, $category["categoryid"], $category["categoryid"]);

	while ($tag = db_fetch_array($result)) { if ($tag["taglist"] != "{}") $tags = array_merge($tags, tags_delimited(pg_array_to_str($tag["taglist"]))); }
	$tags = array_slice(array_unique($tags), 0, 100);
?>