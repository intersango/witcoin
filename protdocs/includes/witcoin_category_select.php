<?php
	// This was provided courtesy of RhodiumToad of #postgresql
	$sql = "WITH RECURSIVE t(categoryid, depth, path, category, categorysd) AS (select categoryid, 0, array[category], category, categorysd from categories where categorypid is null
			UNION ALL SELECT c.categoryid, t.depth+1, t.path || c.category, c.category, c.categorysd FROM categories c join t ON (c.categorypid=t.categoryid))
			SELECT * from t order by path;";
	$result = db_query($sql);

	$lastdepth = 0;
	$category_select = array();
	while ($row = db_fetch_array($result)) $category_select[] = $row;
?>