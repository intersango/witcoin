<?php
	$head = array();

	$head["script"] = array();
	$head["script"][] = "/js/jquery-1.5.1.min.js";
	// jQuery UI - Theme: Sunny, Interactions: Draggable, Widgets: Datepicker, Dialog, Slider, Tabs, Effects: NONE
	$head["script"][] = "/js/jquery-ui-1.8.10.custom.min.js";
	$head["script"][] = "/js/js.js";
	$head["script"][] = "/js/jquery.textarearesizer.compressed.js";
	$head["script"][] = "/js/jquery.wmd.min.js";
	$head["script"][] = "/js/jquery.colorbox-min.js";
	$head["script"][] = "/js/jquery.cookie.js";
	$head["script"][] = "/js/jquery.jqModal.js";

	$head["style"] = array();
	$head["style"]["default"] = "/css/style.css";
	$head["style"][] = "/css/sunny/jquery-ui-1.8.10.custom.css";
?>