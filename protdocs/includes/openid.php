<?php
	function get_openid_provider($url) {
		//https://www.google.com/accounts/o8/id?id=AItOawl26Vxc1Kq1h8szVbEeHdtViyjfMHGn4sk
		if (preg_match("/^http:\/\/openid.anonymity.com\/[a-z0-9]*$/", $url)) return "anonymity";
		else if (preg_match("/^https:\/\/www.google.com\/accounts\/o8\/id\?id=.*$/", $url)) return "google";
		else if (preg_match("/^https:\/\/launchpad.net\/~[a-z-]*/", $url)) return "launchpad";
		else if (preg_match("/^http:\/\/[a-zA-Z0-9]*.livejournal.com\/$/", $url)) return "livejournal";
		else if (preg_match("/^http:\/\/[a-z0-9-]*.myid.net\/$/", $url)) return "myid";
		else if (preg_match("/^http:\/\/[a-zA-Z0-9-]*.myopenid.com\/$/", $url)) return "myopenid";
		else if (preg_match("/^http:\/\/myspace.com\/[a-zA-z0-9_]*$/", $url)) return "myspace";
		else if (preg_match("/^http:\/\/steamcommunity.com\/openid\/id\/[0-9]*$/", $url)) return "steam";
		else if (preg_match("/^https:\/\/[a-z]*.pip.verisignlabs.com\/$/", $url)) return "verisign";
		else if (preg_match("/^http:\/\/[a-zA-Z0-9]*.wordpress.com\/$/", $url)) return "wordpress";
		else if (preg_match("/^https:\/\/me.yahoo.com\/a\/[a-zA-Z0-9.#_-]*$/", $url)) return "yahoo";
		else if ($_SERVER["REMOTE_ADDR"] == "65.30.35.48") {
			echo $url."<br>";
			return "";
		} else return "";
	}

	function get_user_by_openid_attributes($openid) {
		$attributes = $openid->getAttributes();
		if (get_openid_provider($openid->identity) == "google") {
			return db_fetch_array(db_query("SELECT * FROM users_openid WHERE openid_provider_name = 'google' AND openid_user_email = '%s'", $attributes["contact/email"]));
		}
		return FALSE;
	}

	function get_user_by_openid_identity($openid) {
		return db_fetch_array(db_query("SELECT * FROM users_openid WHERE openid_provider_identity = '%s'", $openid->identity));
	}
?>