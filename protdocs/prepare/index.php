<?php
	global $blocks, $category, $posts, $sort, $title;

	$blocks[] = "category"; $blocks[] = "description"; $blocks[] = "links"; $blocks[] = "prices"; $blocks[] = "tags";

	$sort = "toprank"; if (isset($_GET["sort"])) $sort = $_GET["sort"];
	$status = "all"; if (isset($_GET["status"]) && in_array($_GET["status"], explode(",", $category["status"]))) $status = $_GET["status"];
	if (isset($_GET["a"]) && is_numeric(base64_decode($_GET["a"]))) $after = $_GET["a"];
	$limit = 60;

	if ($category["categoryid"] != 0) $title[] = $category["category"];
	else if ($sort == "toprank") $title[] = "top content";
	else if ($sort == "lastactivity") $title[] = "latest activity";
	else if ($sort == "newest") $title[] = "newest content";
	else if ($sort == "oldest") $title[] = "oldest content";
	else if ($sort == "topreplies") $title[] = "top replies";
	else if ($sort == "topvotes") $title[] = "top votes";

	if (isset($after)) {
		$after = db_fetch_array(db_query("SELECT posts.*, (SELECT COUNT(*) FROM replies WHERE postid = posts.postid) AS posts_replies FROM POSTS WHERE postid = %d", base64_decode($after)));
		if (!$after) { require_once "../protdocs/templates/_error.php"; exit; }
	}

	if ($category["categoryid"] != 0) $sql = "WITH RECURSIVE hcategories(categoryid, categorypid, depth) AS (
			(SELECT categoryid, categorypid, 0 FROM categories WHERE categoryid = %d)
			UNION ALL (SELECT c.categoryid, c.categorypid, hcategories.depth+1 FROM categories c JOIN hcategories ON (c.categorypid = hcategories.categoryid))
		), pcategories(category, categoryid, categorypid, depth) AS (
			(SELECT category, categoryid, categorypid, 0 FROM categories WHERE categoryid = %d)
			UNION ALL (SELECT c.category, c.categoryid, c.categorypid, pcategories.depth+1 FROM categories c JOIN pcategories ON (c.categoryid = pcategories.categorypid))
		) ";
	else $sql = "";

	$sql .= "SELECT posts.category, posts.lastactivity, posts.lastevent, posts.lastuserid, posts.post, posts.posted, posts.postid AS posts_postid, posts.status AS posts_status, posts.taglist, posts.title, posts.userid AS posts_userid,
		posts.views, posts.votes AS posts_votes, posts_thumbs.thumb, users.displayname, categories.category AS category_name, categories.categorysd AS category_sd, categories.status AS category_status,
		(SELECT lang FROM categories WHERE categoryid = posts.category) AS category_lang,
		(SELECT rankfresh(1+posts.votes+(posts.support / 0.01)::int, 0, EXTRACT(EPOCH FROM now() - posts.lastactivity))) AS posts_rank,
		(SELECT COUNT(*) FROM replies WHERE postid = posts.postid) AS posts_replies
		FROM posts
		LEFT OUTER JOIN posts_thumbs ON (posts.postid = posts_thumbs.postid)
		LEFT OUTER JOIN users ON (posts.lastuserid = users.userid)
		LEFT OUTER JOIN categories ON (posts.category = categories.categoryid)
		WHERE TRUE";

	if ($category["categoryid"] != 0) $sql .= " AND posts.category IN (SELECT categoryid FROM hcategories)";
	else {
		$sql .= " AND posts.category != 88"; // Not in sandbox
		$sql .= " AND (WITH RECURSIVE pcategories(category, categoryid, categorypid, depth) AS (
                        (SELECT category, categoryid, categorypid, 0 FROM categories WHERE categoryid = posts.category)
                        UNION ALL (SELECT c.category, c.categoryid, c.categorypid, pcategories.depth+1 FROM categories c JOIN pcategories ON (c.categoryid = pcategories.categorypid))
                ) SELECT categoryid FROM pcategories WHERE categorypid IS NULL) != 124"; // Not in nsfw hierarchy
	}

	if (isset($after)) {
		if ($sort == "lastactivity") $sql .= " AND posts.lastactivity < '%s'";
		else if ($sort == "newest") $sql .= " AND posts.posted < '%s'";
		else if ($sort == "oldest") $sql .= " AND posts.posted > '%s'";
		else if ($sort == "toprank") $sql .= " AND (rankfresh(1+posts.votes+(posts.support / 0.01)::int, 0, EXTRACT(EPOCH FROM now() - posts.lastactivity)), posts.lastactivity) < (rankfresh(1+%d+%d, 0, EXTRACT(EPOCH FROM now() - '%s')), '%s')";
		else if ($sort == "topreplies") $sql .= " AND ((SELECT COUNT(*) FROM replies WHERE postid = posts.postid), posts.lastactivity) < (%d, '%s')";
		else if ($sort == "topvotes") $sql .= " AND (posts.votes, posts.lastactivity) < (%d, '%s')";
	}

	if ($status != "all") $sql .= " AND posts.status = '" . db_escape_string($status) . "'";

	if ($sort == "lastactivity") $sql .= " ORDER BY posts.lastactivity DESC";
	else if ($sort == "newest") $sql .= " ORDER BY posts.posted DESC";
	else if ($sort == "oldest") $sql .= " ORDER BY posts.posted";
	else if ($sort == "toprank") $sql .= " ORDER BY rankfresh(1+posts.votes+(posts.support / 0.01)::int, 0, EXTRACT(EPOCH FROM now() - posts.lastactivity)) DESC, posts.lastactivity DESC";
	else if ($sort == "topreplies") $sql .= " ORDER BY posts_replies DESC, posts.lastactivity DESC";
	else if ($sort == "topvotes") $sql .= " ORDER BY posts.votes DESC, posts.lastactivity DESC";

	$sql .= " LIMIT ".strval($limit);

	if ($category["categoryid"] != 0) {
		if (!isset($after)) $posts = db_prepare_posts(db_query($sql, $category["categoryid"]));
		else if ($sort == "lastactivity") $posts = db_prepare_posts(db_query($sql, $category["categoryid"], $category["categoryid"], $after["lastactivity"]));
		else if ($sort == "newest") $posts = db_prepare_posts(db_query($sql, $category["categoryid"], $category["categoryid"], $after["posted"]));
		else if ($sort == "oldest") $posts = db_prepare_posts(db_query($sql, $category["categoryid"], $category["categoryid"], $after["posted"]));
		else if ($sort == "toprank") $posts = db_prepare_posts(db_query($sql, $category["categoryid"], $category["categoryid"], $after["votes"], $after["support"], $after["lastactivity"], $after["lastactivity"]));
		else if ($sort == "topreplies") $posts = db_prepare_posts(db_query($sql, $category["categoryid"], $category["categoryid"], $after["posts_replies"], $after["lastactivity"]));
		else if ($sort == "topvotes") $posts = db_prepare_posts(db_query($sql, $category["categoryid"], $category["categoryid"], $after["votes"], $after["lastactivity"]));
	} else {
		if (!isset($after)) $posts = db_prepare_posts(db_query($sql));
		else if ($sort == "lastactivity") $posts = db_prepare_posts(db_query($sql, $after["lastactivity"]));
		else if ($sort == "newest") $posts = db_prepare_posts(db_query($sql, $after["posted"]));
		else if ($sort == "oldest") $posts = db_prepare_posts(db_query($sql, $after["posted"]));
		else if ($sort == "toprank") $posts = db_prepare_posts(db_query($sql, $after["votes"], $after["support"], $after["lastactivity"], $after["lastactivity"]));
		else if ($sort == "topreplies") $posts = db_prepare_posts(db_query($sql, $after["posts_replies"], $after["lastactivity"]));
		else if ($sort == "topvotes") $posts = db_prepare_posts(db_query($sql, $after["votes"], $after["lastactivity"]));
	}

	if (isset($category["status"])) {
		$statuses = explode(",", $category["status"]); sort($statuses);
		$status_filter = "<select id='status_filter'>";
		$status_filter .= "<option ".($status == "all" ? "selected='selected' " : "")."value='all'>all</option>";
		foreach ($statuses as $s) $status_filter .= "<option ".($status == $s ? "selected='selected' " : "")."value='$s'>$s</option>";
		$status_filter .= "</select>";
	}
?>