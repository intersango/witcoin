<?php
	global $bodyclass, $costs, $err, $errmsg, $title;

	if (!isset($_SESSION["userid"])) { header("Location: ".getdomain()); exit; }

	$bodyclass[] = "p_charity"; $title[] = "add your charitable organization";

	$newcharity = array("bitcoin_address_charity" => "", "email" => "", "logo" => "", "name" => "", "website" => "");
	$err = array(); $errmsg = array();
	if (count($_POST) > 0) {
		$witizen = db_fetch_array(db_query("SELECT * FROM users WHERE userid = %d", $_SESSION["userid"]));

		$newcharity["bitcoin_address_charity"] = trim($_POST["bitcoin_address"]);
		$newcharity["email"] = trim($_POST["email"]);
		$newcharity["logo"] = trim($_POST["logo"]);
		$newcharity["name"] = trim($_POST["name"]);
		$newcharity["website"] = trim($_POST["website"]);

		// Check for errors
		if ($witizen["witcoins"] < $costs["new charity"]) { $err["witcoins"] = TRUE; $errmsg[] = "not enough witcoins"; }
		if ($newcharity["bitcoin_address_charity"] != "" && !checkAddress($newcharity["bitcoin_address_charity"])) { $err["bitcoin_address_charity"] = TRUE; $errmsg[] = "invalid bitcoin address"; }
		if (strlen($newcharity["name"]) < 3) { $err["name"] = TRUE; $errmsg[] = "charity name is too short"; }
		if (strlen($newcharity["name"]) > 64) { $err["name"] = TRUE; $errmsg[] = "charity name is too long"; }
		if (!preg_match("/^[-A-Z0-9+*&@#\/%^?=~_|()!:,.;'\" ]*$/i", $newcharity["name"])) { $err["name"] = TRUE; $errmsg[] = "charity name must contain only a-zA-Z0-9+*&@#/%^?=~-_|()!:,.;'\" "; }
		if ($newcharity["email"] != "" && !preg_match("/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/", strtoupper($newcharity["email"]))) { $err["email"] = TRUE; $errmsg[] = "invalid email address"; }
		if (preg_match("/^javascript:/", $newcharity["logo"])) { $err["logo"] = TRUE; $errmsg[] = "invalid logo url"; }
		if (preg_match("/^javascript:/", $newcharity["website"])) { $err["website"] = TRUE; $errmsg[] = "invalid website"; }
		if (db_result(db_query("SELECT charityid FROM charities WHERE path = '%s'", urlfriendly($newcharity["name"])))) { $err["name"] = TRUE; $errmsg[] = "charity already exists"; }
		if (db_result(db_query("SELECT charityid FROM charities WHERE bitcoin_address_charity = '%s'", $newcharity["bitcoin_address_charity"]))) { $err["bitcoin_address_charity"] = TRUE; $errmsg[] = "bitcoin address already used"; }

		if (!$err) {
			// Distribute funds
			if ($_SESSION["rank"] != "moderator") {
				$payees = array(0);
				foreach (explode(",", $witizen["charities"]) as $charity) $payees[] = "charity_".$charity;
				payment($_SESSION["userid"], $payees, $costs["new charity"]);
			}

			db_query("INSERT INTO charities (userid, charity, website, logo, email, bitcoin_address_charity, path) VALUES(%d, '%s', '%s', '%s', '%s', '%s', '%s')",
				$_SESSION["userid"], $newcharity["name"], $newcharity["website"], $newcharity["logo"], $newcharity["email"], $newcharity["bitcoin_address_charity"], urlfriendly($newcharity["name"]));
			$charityid = db_last_insert_id("charities", "charityid");

			header("Location: /charity/".urlfriendly($newcharity["name"])); exit;
		}
	}
?>