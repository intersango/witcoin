<?php
	global $categories, $category, $posts, $sort, $title;

	$title[] = "categories";

	$sort = "topcategories"; if (isset($_GET["sort"])) $sort = $_GET["sort"];

	$sql = "SELECT categories.allowposts, categories.category, categories.categoryid as categories_categoryid, categories.categorysd, categories.created, categories.description, categories.lang,
			categories.renter, costs.edit AS cost_edit, costs.post AS cost_post, costs.reply AS cost_reply, costs.upvote AS cost_upvote,
			(SELECT COUNT(*) FROM posts WHERE categories.categoryid = posts.category) AS categories_posts
			FROM categories LEFT OUTER JOIN costs ON (categories.categoryid = costs.categoryid)";

	if ($sort == "newest") $sql .= " ORDER BY categories.created DESC";
	else if ($sort == "oldest") $sql .= " ORDER BY categories.created";
	else if ($sort == "topcategories") $sql .= " ORDER BY categories_posts DESC";

	$categories = db_prepare_categories(db_query($sql));
?>