<?php
	global $path, $reply, $title;

	preg_match("/^\/r\/([0-9]*)\/revs?$/", $path, $matches);

	$sql = "SELECT posts.status AS post_status, posts.title AS post_title, replies.postid AS reply_postid, replies.replied, replies.reply, replies.replyid AS reply_replyid, replies.userid AS reply_userid, reply_revs.revid,
		reply_revs.reply as replyrev_reply, reply_revs.replied as replyrev_replied,users.displayname, users.email_address, categories.categorysd AS category_sd, categories.status AS category_status
		FROM replies
		LEFT OUTER JOIN users ON (replies.userid = users.userid)
		LEFT OUTER JOIN reply_revs ON (replies.replyid = reply_revs.replyid)
		LEFT OUTER JOIN posts ON (posts.postid = replies.postid)
		LEFT OUTER JOIN categories ON (posts.category = categories.categoryid)
		WHERE replies.replyid = %d";

	$replies = db_prepare_replies(db_query($sql, $matches[1]));
	if ($replies) {
		require_once "../protdocs/includes/diff.php";
		$keys = array_keys($replies);
		$reply = array("replyid" => $keys[0]);
		foreach ($replies[$keys[0]] as $key => $value) $reply[$key] = $value;
		$reply["replyrevs"] = array_reverse($reply["replyrevs"], true);
		
		$title[] = $reply["post_title"];
	} else { require_once "../protdocs/templates/_error.php"; exit; }
?>