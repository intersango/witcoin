<?php
	global $blocks, $bodyclass, $err, $errmsg, $title, $witizen, $witizenfrom;

	preg_match("/^\/witizens\/donate\/([0-9]*)$/", $path, $matches);
	$userid = $matches[1];

	$blocks[] = "profile"; $bodyclass[] = "p_donate";

	if (!isset($_SESSION["userid"]) || $userid != $_SESSION["userid"]) {
		if (isset($_SESSION["userid"])) $witizenfrom = db_fetch_array(db_query("SELECT * FROM users WHERE userid = '%d'", $_SESSION["userid"]));
		$witizen = db_fetch_array(db_query("SELECT * FROM users WHERE userid = '%d'", $userid));
		if (!$witizen) { require_once "../protdocs/templates/_error.php"; exit; }

		// Duplicated from _witizen.php, maybe put in a function call sometime?
		$witizen["gravatar"] = $witizen["email_address"] == "" ? "" : "https://secure.gravatar.com/avatar/".md5(strtolower($witizen["email_address"]))."?s=168&d=monsterid&r=x";
		$created = db_fetch_array(db_query("SELECT EXTRACT(EPOCH FROM TIMESTAMP WITH TIME ZONE '%s')", $witizen["created"]));
		$access = db_fetch_array(db_query("SELECT EXTRACT(EPOCH FROM TIMESTAMP WITH TIME ZONE '%s')", $witizen["access"]));
		$witizen["member_for"] = time_length($created["date_part"]);
		$witizen["seen"] = time_duration($access["date_part"]);
		if ($witizen["website"] != "" && !preg_match("/^https?:\/\/.*$/", $witizen["website"])) $witizen["website"] = "http://" . $witizen["website"];

		$title[] = "donate to <a href='/witizens/".$userid."'>".filter(htmlspecialchars($witizen["displayname"]), FILTER_WORDS)."</a>";

		if (count($_POST) > 0) {
			// Check for errors
			$err = array(); $errmsg = array();
			if (!isset($_SESSION["userid"])) $err["user"] = TRUE;
			if (strlen($_POST["subject"]) < 3) { $err["subject"] = TRUE; $errmsg[] = "not enough subject"; }
			else if (strlen($_POST["subject"]) > 64) { $err["subject"] = TRUE; $errmsg[] = "too much subject"; }
			if (!is_numeric($_POST["witcoins"]) || $_POST["witcoins"] < 0.00000001) { $err["witcoins"] = TRUE; $errmsg[] = "invalid witcoin amount"; }
			else if ($_POST["witcoins"] > $witizenfrom["witcoins"]) { $err["witcoins"] = TRUE; $errmsg[] = "not enough witcoins"; }

			if (!$err) {
				$_POST["message"] = trim($_POST["message"]);
				$_POST["subject"] = trim($_POST["subject"]);

				db_result(db_query("UPDATE users SET witcoins = witcoins - '%s' WHERE userid = %d", $_POST["witcoins"], $_SESSION["userid"]));
				db_result(db_query("UPDATE users SET witcoins = witcoins + '%s' WHERE userid = %d", $_POST["witcoins"], $userid));
				db_result(db_query("INSERT INTO messages (touser, fromuser, type, amount, subject, message) VALUES(%d, %d, '%s', '%s', '%s', '%s')",
					$userid, $_SESSION["userid"], "donation", $_POST["witcoins"], $_POST["subject"], $_POST["message"]));
			}
		}
	} else {
		$title[] = "donate witcoins to yourself";
	}
?>
