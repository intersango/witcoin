<?php
	global $blocks, $category, $path, $posts, $site, $sort, $title;

	$blocks[] = "category"; $blocks[] = "prices"; $blocks[] = "tags";

	$sort = "toprank"; if (isset($_GET["sort"])) $sort = $_GET["sort"];
	$status = "all"; if (isset($_GET["status"]) && in_array($_GET["status"], explode(",", $category["status"]))) $status = $_GET["status"];
	if (isset($_GET["a"]) && is_numeric(base64_decode($_GET["a"]))) $after = $_GET["a"];
	$limit = 60;

	// Change in includes.php, p.php, editpost.php, _tagged.php
	preg_match("/^\/p\/t\/([a-zA-Z0-9 #+.,_-]*)$/", $path, $matches);

	if ($matches) {
		$title[] = "\"" . htmlspecialchars($matches[1]) . "\" tagged posts";
		if (isset($after)) {
			$after = db_fetch_array(db_query("SELECT posts.*, (SELECT COUNT(*) FROM replies WHERE postid = posts.postid) AS posts_replies FROM POSTS WHERE postid = %d", base64_decode($after)));
			if (!$after) { require_once "../protdocs/templates/_error.php"; exit; }
		}

		if ($category["categoryid"] != 0) $sql = "WITH RECURSIVE hcategories(categoryid, categorypid, depth) AS (
				(SELECT categoryid, categorypid, 0
				FROM categories
				WHERE categoryid = $1)
				UNION ALL
				(SELECT c.categoryid, c.categorypid, hcategories.depth+1
				FROM categories c
				JOIN hcategories ON (c.categorypid = hcategories.categoryid))
			) ";
		else $sql = "";

		// <nanotube> nicez0r! :)
		// <nanotube> i totally want credit for my mad sql skillz :D
		$sql .= "SELECT posts.category, posts.lastactivity, posts.lastevent, posts.lastuserid, posts.post, posts.posted, posts.postid AS posts_postid, posts.status AS posts_status, posts.taglist, posts.title,
			posts.userid AS posts_userid, posts.views, posts.votes AS posts_votes, posts_thumbs.thumb, users.displayname, categories.category AS category_name, categories.categorysd AS category_sd,
			categories.status AS category_status,
			(SELECT lang FROM categories WHERE categoryid = posts.category) AS category_lang,
			(SELECT rankfresh(1+posts.votes+(posts.support / 0.01)::int, 0, EXTRACT(EPOCH FROM now() - posts.lastactivity))) AS posts_rank,
			(SELECT COUNT(*) FROM replies WHERE postid = posts.postid) AS posts_replies
			FROM posts
			LEFT OUTER JOIN posts_thumbs ON (posts.postid = posts_thumbs.postid)
			LEFT OUTER JOIN users ON (posts.userid = users.userid)
			LEFT OUTER JOIN categories ON (posts.category = categories.categoryid)
			WHERE taglist @> $2";

		if ($category["categoryid"] != 0) $sql .= " AND posts.category IN (SELECT categoryid FROM hcategories)";
		else $sql .= " AND $1 = $1 AND posts.category != 88"; // Not in sandbox

		if (isset($after)) {
			if ($sort == "lastactivity") $sql .= " AND posts.lastactivity < $3";
			else if ($sort == "newest") $sql .= " AND posts.posted < $3";
			else if ($sort == "oldest") $sql .= " AND posts.posted > $3";
			else if ($sort == "toprank") $sql .= " AND (rankfresh(1+posts.votes+(posts.support / 0.01)::int, 0, EXTRACT(EPOCH FROM now() - posts.lastactivity)), posts.lastactivity) < (rankfresh(1+$3+$4, 0, EXTRACT(EPOCH FROM now() - $5)), $5)";
			else if ($sort == "topreplies") $sql .= " AND ((SELECT COUNT(*) FROM replies WHERE postid = posts.postid), posts.lastactivity) < ($3, $4)";
			else if ($sort == "topvotes") $sql .= " AND (posts.votes, posts.lastactivity) < ($3, $4)";
		}

		if ($status != "all") $sql .= " AND posts.status = '" . db_escape_string($status) . "'";

		if ($sort == "lastactivity") $sql .= " ORDER BY posts.lastactivity DESC";
		else if ($sort == "newest") $sql .= " ORDER BY posts.posted DESC";
		else if ($sort == "oldest") $sql .= " ORDER BY posts.posted";
		else if ($sort == "topreplies") $sql .= " ORDER BY posts_replies DESC, posts.lastactivity DESC";
		else if ($sort == "toprank") $sql .= " ORDER BY rankfresh(1+posts.votes+(posts.support / 0.01)::int, 0, EXTRACT(EPOCH FROM now() - posts.lastactivity)) DESC, posts.lastactivity DESC";
		else if ($sort == "topvotes") $sql .= " ORDER BY posts.votes DESC, posts.lastactivity DESC";

		$sql .= " LIMIT ".strval($limit);

		if ($category["categoryid"] != 0) {
			if (!isset($after)) $posts = db_prepare_posts(db_query_params($sql, array($category["categoryid"], pg_str_to_array($matches[1]))));
			else if ($sort == "lastactivity") $posts = db_prepare_posts(db_query_params($sql, array($category["categoryid"], pg_str_to_array($matches[1]), $after["lastactivity"])));
			else if ($sort == "newest") $posts = db_prepare_posts(db_query_params($sql, array($category["categoryid"], pg_str_to_array($matches[1]), $after["posted"])));
			else if ($sort == "oldest") $posts = db_prepare_posts(db_query_params($sql, array($category["categoryid"], pg_str_to_array($matches[1]), $after["posted"])));
			else if ($sort == "toprank") $posts = db_prepare_posts(db_query_params($sql, array($category["categoryid"], pg_str_to_array($matches[1]), $after["votes"], $after["support"], $after["lastactivity"])));
			else if ($sort == "topreplies") $posts = db_prepare_posts(db_query_params($sql, array($category["categoryid"], pg_str_to_array($matches[1]), $after["posts_replies"], $after["lastactivity"])));
			else if ($sort == "topvotes") $posts = db_prepare_posts(db_query_params($sql, array($category["categoryid"], pg_str_to_array($matches[1]), $after["votes"], $after["lastactivity"])));
		} else {
			if (!isset($after)) $posts = db_prepare_posts(db_query_params($sql, array("", pg_str_to_array($matches[1]))));
			else if ($sort == "lastactivity") $posts = db_prepare_posts(db_query_params($sql, array("", pg_str_to_array($matches[1]), $after["lastactivity"])));
			else if ($sort == "newest") $posts = db_prepare_posts(db_query_params($sql, array("", pg_str_to_array($matches[1]), $after["posted"])));
			else if ($sort == "oldest") $posts = db_prepare_posts(db_query_params($sql, array("", pg_str_to_array($matches[1]), $after["posted"])));
			else if ($sort == "toprank") $posts = db_prepare_posts(db_query_params($sql, array("", pg_str_to_array($matches[1]), $after["votes"], $after["support"], $after["lastactivity"])));
			else if ($sort == "topreplies") $posts = db_prepare_posts(db_query_params($sql, array("", pg_str_to_array($matches[1]), $after["posts_replies"], $after["lastactivity"])));
			else if ($sort == "topvotes") $posts = db_prepare_posts(db_query_params($sql, array("", pg_str_to_array($matches[1]), $after["votes"], $after["lastactivity"])));
		}

		if (isset($category["status"])) {
			$statuses = explode(",", $category["status"]); sort($statuses);
			$status_filter = "<select id='status_filter'>";
			$status_filter .= "<option ".($status == "all" ? "selected='selected' " : "")."value='all'>all</option>";
			foreach ($statuses as $s) $status_filter .= "<option ".($status == $s ? "selected='selected' " : "")."value='$s'>$s</option>";
			$status_filter .= "</select>";
		}
	} else require_once "../protdocs/templates/_error.php";
?>