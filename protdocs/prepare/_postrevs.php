<?php
	global $category, $path, $post, $title;

	preg_match("/^\/p\/([0-9]*)\/revs?$/", $path, $matches);

	$sql = "SELECT  posts.category, posts.lastactivity, posts.post, posts.posted, posts.postid AS post_postid, posts.status AS post_status, posts.syndicateupto, posts.taglist, posts.title, posts.userid AS post_userid,
		posts.views, post_revs.revid, post_revs.status AS postrev_status, post_revs.syndicateupto AS postrev_syndicateupto, post_revs.title AS postrev_title, post_revs.post AS postrev_post, post_revs.taglist AS postrev_taglist,
		post_revs.posted AS postrev_posted, post_revs.userid AS postrev_userid, users.displayname, users.email_address, categories.categorysd AS category_sd, categories.status AS category_status,
		(SELECT displayname FROM users WHERE userid = post_revs.userid) AS postrev_displayname,
		(SELECT email_address FROM users WHERE userid = post_revs.userid) AS postrev_emailaddress
		FROM posts
		LEFT OUTER JOIN users ON (posts.userid = users.userid)
		LEFT OUTER JOIN post_revs ON (posts.postid = post_revs.postid)
		LEFT OUTER JOIN categories ON (posts.category = categories.categoryid)
		WHERE posts.category = %d AND posts.postid = %d";

	$posts = db_prepare_posts(db_query($sql, $category["categoryid"], $matches[1]));
	if ($posts) {
		require_once "../protdocs/includes/diff.php";
		$keys = array_keys($posts);
		$post = array("postid" => $keys[0]);
		foreach ($posts[$keys[0]] as $key => $value) $post[$key] = $value;
		$post["postrevs"] = array_reverse($post["postrevs"], true);

		$title[] = $post["title"];
	} else { require_once "../protdocs/templates/_error.php"; exit; }
?>