<?php
	global $bitcoin_address_witcoin, $btc, $btcconn, $domain, $rcvd0, $rcvd1, $witcoins;

	require_once "../protdocs/includes/bitcoin.php";

	if (!isset($_SESSION["userid"])) { require_once "../protdocs/templates/_error.php"; exit; }
	else if (!$btc) { require_once "../protdocs/templates/_error_btc.php"; exit; }

	// duplicated in transactions page
	if ($domain["address"] != "witcoin.com:2980" && $domain["address"] != "witcoin.com:2981" && $domain["address"] != "witcoin.com:2982") {
		$bitcoin_address_witcoin = db_result(db_query("SELECT bitcoin_address_witcoin FROM users WHERE userid = %d", $_SESSION["userid"]));
		if (!$bitcoin_address_witcoin) {
			try { $bitcoin_address_witcoin = $btcconn->getnewaddress("witcoin.com"); } catch (Exception $e) { btcerr($e); require_once "../protdocs/templates/_error_btc.php"; exit; }
			db_query("UPDATE users SET bitcoin_address_witcoin = '%s' WHERE userid = %d", $bitcoin_address_witcoin, $_SESSION["userid"]);
			$rcvd0 = 0;
		}
		else {
			try {
				$rcvd0 = number_format($btcconn->getreceivedbyaddress($bitcoin_address_witcoin, 0), 16, ".", "");
				$rcvd1 = number_format($btcconn->getreceivedbyaddress($bitcoin_address_witcoin, 1), 16, ".", "");
			} catch (Exception $e) { btcerr($e); require_once "../protdocs/templates/_error_btc.php"; exit; }

			if ($rcvd0 > 0) {
				$transaction = db_fetch_array(db_query("SELECT * FROM transactions WHERE userid = %d AND type = 'deposit' AND bitcoin_address_witcoin = '%s'", $_SESSION["userid"], $bitcoin_address_witcoin));
				if ($rcvd1 > 0) {
					if (!$transaction) db_query("INSERT INTO transactions (userid, type, witcoins, bitcoin_address_witcoin, confirmations, txed) VALUES(%d, '%s', '%s', '%s', %d, %s)",
						$_SESSION["userid"], "deposit", $rcvd1, $bitcoin_address_witcoin, 1, "to_timestamp(".time().")");
					else
						db_query("UPDATE transactions SET confirmations = 1 WHERE userid = %d AND type = 'deposit' AND bitcoin_address_witcoin = '%s'", $_SESSION["userid"], $bitcoin_address_witcoin);
					$witcoins = db_result(db_query("UPDATE users SET witcoins = witcoins + '%s', bitcoin_address_witcoin = DEFAULT WHERE userid = %d RETURNING witcoins", $rcvd1, $_SESSION["userid"]));
				} else if (!$transaction) db_query("INSERT INTO transactions (userid, type, witcoins, bitcoin_address_witcoin, confirmations, txed) VALUES(%d, '%s', %s, '%s', %d, %s)",
					$_SESSION["userid"], "deposit", $rcvd0, $bitcoin_address_witcoin, 0, "to_timestamp(".time().")");
			}
		}
	} else {
		$witupped = FALSE;
		if (count($_POST) > 0) {
			$witupped = TRUE;
			db_query("UPDATE users SET witcoins = 99999999.99999999 WHERE userid = %d", $_SESSION["userid"]);
		}
	}
?>