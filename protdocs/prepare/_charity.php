<?php
	global $title;

	if (!preg_match("/^\/charity\/([-a-zA-Z0-9]*)$/", $path, $matches)) { require_once "../protdocs/templates/_error.php"; exit; }

	$charity = db_fetch_array(db_query("SELECT * FROM charities WHERE path = '%s'", $matches[1]));

	if (!$charity) { require_once "../protdocs/templates/_error.php"; exit; }
	
	$title[] = "charity"; $title[] = filter(htmlspecialchars($charity["charity"]), FILTER_WORDS);
?>