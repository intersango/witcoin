<?php
	global $blocks, $bodyclass, $category, $err, $errmsg, $head, $path, $post, $title, $witizen;

	if ($category["categoryid"] == 0 || $category["allowposts"] == "f") { header("Location: /"); exit; }

	$blocks[] = "category"; $blocks[] = "description"; $blocks[] = "prices"; $blocks[] = "tags"; $bodyclass[] = "p_post"; $title[] = filter(htmlspecialchars($category["lang"]["verb_post"]["present"]), FILTER_WORDS);

	$err = array(); $errmsg = array();

	if (isset($_SESSION["userid"])) {
		$witizen = db_fetch_array(db_query("SELECT * FROM users WHERE userid = %d", $_SESSION["userid"]));

		$post = array(
			"taglist" => isset($_GET["tags"]) ? trim($_GET["tags"]) : "",
			"title" => isset($_GET["title"]) ? trim($_GET["title"]) : "",
			"post" => isset($_GET["post"]) ? trim($_GET["post"]) : "",
			"post_status" => isset($_GET["post_status"]) ? trim($_GET["post_status"]) : "",
			"upvote" => isset($_GET["upvote"]) ? "t" : ""
		);

		if (count($_POST) > 0) {
			$post["title"] = trim($_POST["title"]);
			$post["post"] = trim($_POST["post"]);
			$post["taglist"] = strtolower(trim($_POST["tags"], ", \t\n\r\0\x0B"));
			if (isset($category["status"])) $post["post_status"] = $_POST["status"];

			if (count(explode(" ", $post["taglist"])) > 3) {
				$tags = array();
				foreach (explode(",", $post["taglist"]) as $key => $tag) { if (count(explode(" ", $tag)) > 3) foreach (explode(" ", $tag) as $tag2) $tags[] = trim($tag2, ", \t\n\r\0\x0B"); else $tags[] = trim($tag, ", \t\n\r\0\x0B"); }
				$post["taglist"] = implode(",", $tags);
			}

			// Check for errors
			if (isset($_POST["upvote"]) && $witizen["witcoins"] < $category["costs"]["post"] + $category["costs"]["upvote"]) { $err["witcoins"] = TRUE; $errmsg[] = "not enough witcoins"; }
			else if ($witizen["witcoins"] < $category["costs"]["post"]) { $err["witcoins"] = TRUE; $errmsg[] = "not enough witcoins"; }
			if (strlen($post["title"]) < 3) { $err["title"] = TRUE; $errmsg[] = "title is too short"; }
			if (strlen($post["title"]) > 300) { $err["title"] = TRUE; $errmsg[] = "title is too long"; }
			if (strlen($post["post"]) < 10) { $err["post"] = TRUE; $errmsg[] = "not enough ".$category["lang"]["noun_post"]["singular"]; }
			if ($post["taglist"] != "" && strlen($post["taglist"]) < 3) { $err["tags"] = TRUE; $errmsg[] = "not enough tag"; }
			// Change in includes.php, p.php, editpost.php, _tagged.php
			else if (!preg_match("/^[a-zA-Z0-9 #+.,_-]*$/", $post["taglist"])) { $err["tags"] = TRUE; $errmsg[] = "tags must contain only a-zA-Z0-9 #+._-"; }
			else if (preg_match("/(?:^.?,|,.?,|,.?$)/", $post["taglist"])) { $err["tags"] = TRUE; $errmsg[] = "one or more tags are too short"; }
			else if (preg_match("/(?:^ *[^ ,]{17,} *$|^ *[^ ,]{17,} *,|, *[^ ,]{17,} *,|, *[^ ,]{17,} *$)/", $post["taglist"])) { $err["tags"] = TRUE; $errmsg[] = "one or more tags are too long"; }
			if (isset($category["status"]) && !in_array($post["post_status"], explode(",", $category["status"]))) { $err["status"] = TRUE; $errmsg[] = "invalid status"; }
			if ($category["p_uniquetitles"] == "t") {
				if (db_result(db_query("SELECT postid FROM posts WHERE category = %d AND title = '%s'", $category["categoryid"], $post["title"]))) { $err["title"] = TRUE; $errmsg[] = "title already exists"; }
			}

			if (!$err) {
				if ($category["costs"]["post"] > 0) {
					// Distribute funds
					$payees = array(0, $category["renter"]);
					foreach (explode(",", $witizen["charities"]) as $charity) $payees[] = "charity_".$charity;
					payment($_SESSION["userid"], $payees, $category["costs"]["post"]);
				}

				if (isset($_POST["upvote"])) {
					if ($category["costs"]["upvote"] > 0) {
						$payees[] = $_SESSION["userid"];
						foreach (explode(",", $witizen["charities"]) as $charity) $payees[] = "charity_".$charity;
						payment($_SESSION["userid"], $payees, $category["costs"]["upvote"]);
					}

					if (isset($category["status"])) db_query("INSERT INTO posts (userid, lastuserid, title, post, status, taglist, category, posted, lastevent, lastactivity, balance, votes)
						VALUES(%d, %d, '%s', '%s', '%s', '%s', %d, 'now()', 'posted', 'now()', '%s', 1)",
						$_SESSION["userid"], $_SESSION["userid"], $post["title"], $post["post"], $post["post_status"], pg_str_to_array($post["taglist"]), $category["categoryid"],
						bcsub(bcdiv($category["costs"]["upvote"], 3, 64), $category["costs"]["post"], 64));
					else db_query("INSERT INTO posts (userid, lastuserid, title, post, taglist, category, posted, lastevent, lastactivity, balance, votes)
						VALUES(%d, %d, '%s', '%s', '%s', %d, 'now()', 'posted', 'now()', '%s', 1)",
						$_SESSION["userid"], $_SESSION["userid"], $post["title"], $post["post"], pg_str_to_array($post["taglist"]), $category["categoryid"],
						bcsub(bcdiv($category["costs"]["upvote"], 3, 64), $category["costs"]["post"], 64));
					$postid = db_last_insert_id("posts", "postid");

					db_query("INSERT INTO votes (postid, userid, vote, voted, balance) VALUES(%d, %d, 'upvote', 'now()', '%s')", $postid, $_SESSION["userid"], bcsub(0, $category["costs"]["upvote"]), 64);
				} else {
					if (isset($category["status"])) db_query("INSERT INTO posts (userid, lastuserid, title, post, status, taglist, category, posted, lastevent, lastactivity, balance, votes)
					VALUES(%d, %d, '%s', '%s', '%s', '%s', %d, 'now()', 'posted', 'now()', '%s', 0)",
						$_SESSION["userid"], $_SESSION["userid"], $post["title"], $post["post"], $post["post_status"], pg_str_to_array($post["taglist"]), $category["categoryid"],
						bcsub(0, $category["costs"]["post"], 64));
					else db_query("INSERT INTO posts (userid, lastuserid, title, post, taglist, category, posted, lastevent, lastactivity, balance, votes)
					VALUES(%d, %d, '%s', '%s', '%s', %d, 'now()', 'posted', 'now()', '%s', 0)",
						$_SESSION["userid"], $_SESSION["userid"], $post["title"], $post["post"], pg_str_to_array($post["taglist"]), $category["categoryid"], bcsub(0, $category["costs"]["post"], 64));
					$postid = db_last_insert_id("posts", "postid");
				}

				$image = FALSE;
				$html = filter($post["post"]);
				if (preg_match("/<img[^>]* src=[\"']([^\"]*)[\"'][^>]*>/i", $html, $matches)) $image = $matches[1];
				else if (preg_match("/<param[^>]* name=[\"']movie[\"'][^>]*>/i", $html, $matches)) {
					if (preg_match("`value=[\"'](https?://[^\"' >]*)[\"']`i", $matches[0], $matches)) {
						if (preg_match("`https?://(?:www\.)?youtube\.com/v/([a-z0-9]*)`i", $matches[1], $matches)) {
							$image = "http://i1.ytimg.com/vi/".$matches[1]."/default.jpg";
						}
					}
				}
				if ($image) db_query("INSERT INTO posts_thumbs (postid, thumb) VALUES(%d, '%s')", $postid, $image);

				$tag = strtok($post["taglist"], ",");
				while ($tag !== false) {
					$tag = trim(strtolower($tag));
					if (preg_match("/^[a-z0-9 #+._-]*$/", $tag) && !db_fetch_array(db_query("SELECT * FROM tags WHERE tag = '%s'", $tag))) db_query("INSERT INTO tags (tag) VALUES('%s')", $tag);
					$tag = strtok(",");
				}

				header("Location: /p/".$postid."/".urlfriendly($post["title"])); exit;
			}

		}

		if (isset($category["status"])) {
			$select_status = '<select name="status">';
			foreach (explode(",", $category["status"]) as $option) $select_status .= '<option '.($post["post_status"] == $option ? "selected='selected' " : "").'value="'.$option.'">'.$option.'</option>';
			$select_status .= '</select>';
		}
	}
?>