<?php
	global $bodyclass, $category, $err, $errmsg, $head, $path, $post, $title, $witizen;

	if (!isset($_SESSION["userid"])) { require_once "../protdocs/templates/_error.php"; exit; }

	$blocks[] = "category"; $blocks[] = "prices"; $blocks[] = "tags"; $bodyclass[] = "p_editpost";

	preg_match("/^\/p\/([0-9]*)\/edit?$/", $path, $matches);

	$sql = "SELECT posts.category, posts.lastactivity, posts.post, posts.posted, posts.postid AS post_postid, posts.status AS post_status, posts.taglist, posts.title, posts.userid AS post_userid, posts.views,
		posts_thumbs.thumb, users.displayname, users.email_address
		FROM posts
		LEFT OUTER JOIN posts_thumbs ON (posts.postid = posts_thumbs.postid)
		LEFT OUTER JOIN users ON (posts.userid = users.userid)
		WHERE posts.category = %d AND posts.postid = %d";

	$posts = db_prepare_posts(db_query($sql, $category["categoryid"], $matches[1]));
	if ($posts) {
		$keys = array_keys($posts);
		$post = array("postid" => $keys[0]);
		foreach ($posts[$keys[0]] as $key => $value) $post[str_replace("posts_", "post_", $key)] = $value;

		// Make sure post is by user
		if ($post["post_userid"] != $_SESSION["userid"]) { require_once "../protdocs/templates/_error.php"; exit; }

		$title[] = "edit ".filter(htmlspecialchars($category["lang"]["verb_post"]["present"]), FILTER_WORDS);

		$witizen = db_fetch_array(db_query("SELECT * FROM users WHERE userid = %d", $_SESSION["userid"]));

		$err = array(); $errmsg = array();

		if (count($_POST) > 0) {
			// If no changes, go back to post
			if ($post["title"] == $_POST["title"] && $post["post"] == $_POST["post"] && $post["taglist"] == $_POST["tags"] && (!isset($_POST["status"]) || $post["post_status"] == $_POST["status"])) {
				header("Location: /p/".$matches[1]."/".urlfriendly($post["title"])); exit;
			}

			// Preserve current post for inserting into post_revs, entry in posts table will be overwritten with changes
			$post_last = $post;

			$post["title"] = trim($_POST["title"]);
			$post["post"] = trim($_POST["post"]);
			$post["taglist"] = strtolower(trim($_POST["tags"], ", \t\n\r\0\x0B"));
			if (isset($category["status"])) $post["post_status"] = $_POST["status"];

			if (count(explode(" ", $post["taglist"])) > 3) {
				$tags = array();
				foreach (explode(",", $post["taglist"]) as $key => $tag) { if (count(explode(" ", $tag)) > 3) foreach (explode(" ", $tag) as $tag2) $tags[] = trim($tag2, ", \t\n\r\0\x0B"); else $tags[] = trim($tag, ", \t\n\r\0\x0B"); }
				$post["taglist"] = implode(",", $tags);
			}

			// Check for errors
			if ($witizen["witcoins"] < $category["costs"]["edit"]) { $errmsg[] = "not enough witcoins"; }
			if (strlen($post["title"]) < 3) { $err["title"] = TRUE; $errmsg[] = "title is too short"; }
			if (strlen($post["title"]) > 300) { $err["title"] = TRUE; $errmsg[] = "title is too long"; }
			if (strlen($post["post"]) < 10) { $err["post"] = TRUE; $errmsg[] = "not enough ".$category["lang"]["noun_post"]["singular"]; }
			if ($post["taglist"] != "" && strlen($post["taglist"]) < 2) { $err["tags"] = TRUE; $errmsg[] = "not enough tag"; }
			// Change in includes.php, p.php, editpost.php, _tagged.php
			else if (!preg_match("/^[a-zA-Z0-9 #+.,_-]*$/", $post["taglist"])) { $err["tags"] = TRUE; $errmsg[] = "tags must contain only a-zA-Z0-9 #+._-"; }
			else if (preg_match("/(?:^.?,|,.?,|,.?$)/", $post["taglist"])) { $err["tags"] = TRUE; $errmsg[] = "one or more tags are too short"; }
			else if (preg_match("/(?:^ *[^ ,]{17,} *$|^ *[^ ,]{17,} *,|, *[^ ,]{17,} *,|, *[^ ,]{17,} *$)/", $post["taglist"])) { $err["tags"] = TRUE; $errmsg[] = "one or more tags are too long"; }
			if (isset($category["status"]) && !in_array($post["post_status"], explode(",", $category["status"]))) { $err["status"] = TRUE; $errmsg[] = "invalid status"; }
			if ($category["p_uniquetitles"] == "t") {
				if (db_result(db_query("SELECT postid FROM posts WHERE category = %d AND title = '%s' AND postid != %d", $category["categoryid"], $post["title"], $post["postid"]))) { $err["title"] = TRUE; $errmsg[] = "title already exists"; }
			}

			if (!$err) {
				if ($category["costs"]["edit"] > 0) {
					// Distribute funds
					$payees = array(0, $category["renter"]);
					foreach (explode(",", $witizen["charities"]) as $charity) $payees[] = "charity_".$charity;
					$balance = bcsub(payment($_SESSION["userid"], $payees, $category["costs"]["edit"]), $witizen["witcoins"], 64);
				} else $balance = "0";

				if (isset($category["status"])) {
					// If adding columns, update protdocs/prepare/_post.php also
					db_query("INSERT INTO post_revs (postid, title, post, taglist, posted, status, userid) VALUES(%d, '%s', '%s', '%s', '%s', '%s', %d)",
						$post_last["postid"], $post_last["title"], $post_last["post"], pg_str_to_array($post_last["taglist"]), $post_last["posted"], $post_last["post_status"], $_SESSION["userid"]);
					db_query("UPDATE posts SET title = '%s', post = '%s', taglist = '%s', posted = 'now()', lastevent = 'post edited', lastactivity = 'now()', status = '%s', lastuserid = %d, balance = balance + '%s'
						WHERE postid = %d",
						$post["title"], $post["post"], pg_str_to_array($post["taglist"]), $post["post_status"], $_SESSION["userid"], $balance, $post["postid"]);
				} else {
					db_query("INSERT INTO post_revs (postid, title, post, taglist, posted, userid) VALUES(%d, '%s', '%s', '%s', '%s', %d)",
						$post_last["postid"], $post_last["title"], $post_last["post"], pg_str_to_array($post_last["taglist"]), $post_last["posted"], $_SESSION["userid"]);
					db_query("UPDATE posts SET title = '%s', post = '%s', taglist = '%s', posted = 'now()', lastevent = 'post edited', lastactivity = 'now()', lastuserid = %d, balance = balance + '%s' WHERE postid = %d",
						$post["title"], $post["post"], pg_str_to_array($post["taglist"]), $_SESSION["userid"], $balance, $post["postid"]);
				}

				$image = FALSE;
				$html = filter($post["post"]);
				if (preg_match("/<img[^>]* src=[\"']([^\"]*)[\"'][^>]*>/i", $html, $matches)) $image = $matches[1];
				else if (preg_match("/<param[^>]* name=[\"']movie[\"'][^>]*>/i", $html, $matches)) {
					if (preg_match("`value=[\"'](https?://[^\"' >]*)[\"']`i", $matches[0], $matches)) {
						if (preg_match("`https?://(?:www\.)?youtube\.com/v/([a-z0-9]*)`i", $matches[1], $matches)) {
							$image = "http://i1.ytimg.com/vi/".$matches[1]."/default.jpg";
						}
					}
				}
				if (isset($post["thumb"]) && !$image) db_query("DELETE FROM posts_thumbs WHERE postid = %d", $post["postid"]);
				else if (isset($post["thumb"]) && $post["thumb"] != $image) db_QUERY("UPDATE posts_thumbs SET thumb = '%s' WHERE postid = %d", $image, $post["postid"]);
				else if (!isset($post["thumb"]) && $image) db_query("INSERT INTO posts_thumbs (postid, thumb) VALUES(%d, '%s')", $post["postid"], $image);

				$newtags = array(); $oldtags = array();
				foreach (explode(",", $post_last["taglist"]) as $tag) {
					//if (count(explode(" ", $tag)) > 3) $oldtags = array_merge($oldtags, explode(" ", $tag)); else 
					$oldtags[] = $tag;
				}
				foreach (explode(",", $post["taglist"]) as $tag) {
					if (count(explode(" ", $tag)) > 3) $newtags = array_merge($newtags, explode(" ", $tag)); else $newtags[] = $tag;
				}
				$tags2del = array_diff($oldtags, $newtags);
				$tags2add = array_diff($newtags, $oldtags);

				foreach ($tags2add as $tag) {
					if (preg_match("/^[a-z0-9 -]*$/", $tag) && !db_fetch_array(db_query("SELECT * FROM tags WHERE tag = '%s'", trim($tag, ", \t\n\r\0\x0B")))) db_query("INSERT INTO tags (tag) VALUES('%s')", $tag);
				}

				header("Location: /p/".$post["postid"]."/".urlfriendly($post["title"])); exit;
			}
		}

		if (isset($category["status"])) {
			$select_status = '<select name="status">';
			foreach (explode(",", $category["status"]) as $option) $select_status .= '<option '.($post["post_status"] == $option ? "selected='selected' " : "").'value="'.$option.'">'.$option.'</option>';
			$select_status .= '</select>';
		}
	} else { require_once "../protdocs/templates/_error.php"; exit; }
?>