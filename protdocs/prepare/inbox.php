<?php
	global $title, $witizen;

	if (!isset($_SESSION["userid"])) { require_once "../protdocs/templates/_error.php"; exit; }

	$title[] = "inbox";

	$messages = array();
	$result = db_query("SELECT messages.*, (SELECT displayname FROM users WHERE userid = messages.fromuser) AS fromuserdisplayname,
		(SELECT displayname FROM users WHERE userid = messages.touser) AS touserdisplayname
		FROM messages WHERE touser = %d ORDER BY date DESC", $_SESSION["userid"]);
	while ($message = db_fetch_array($result)) $messages[] = $message;
?>