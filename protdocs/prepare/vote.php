<?php
	global $category, $return;
	$return = array("error" => FALSE);
	if (!isset($_SESSION["userid"])) { $return["error"] = "not logged in"; }
	else {
		$witizen = db_fetch_array(db_query("SELECT * FROM users WHERE userid = %d", $_SESSION["userid"]));

		if ($witizen["witcoins"] < $category["costs"]["upvote"]) { $return["error"] = "not enough witcoins<br>voting costs ".bitcoins($category["costs"]["upvote"]); }
		if ($return["error"] === FALSE) {
			if (isset($_POST["postid"])) {
				$voted = db_result(db_query("SELECT userid FROM votes WHERE postid = %d AND vote = 'upvote' AND userid = %d", $_POST["postid"], $_SESSION["userid"]));
				if (!$voted) {
					if ($category["costs"]["upvote"] > 0) {
						// Distribute funds
						$payees = array(0, $category["renter"]);
						$payees[] = db_result(db_query("SELECT userid FROM posts WHERE postid = %d", $_POST["postid"]));
						$result = db_query("SELECT userid FROM votes WHERE postid = %d AND vote = 'upvote'", $_POST["postid"]);
						while ($payee = db_fetch_array($result)) $payees[] = $payee["userid"];
						foreach (explode(",", $witizen["charities"]) as $charity) $payees[] = "charity_".$charity;
						$return["witcoins"] = payment($_SESSION["userid"], $payees, $category["costs"]["upvote"]);
						$balance = bcsub($return["witcoins"], $witizen["witcoins"], 64);

						$amount = bcdiv($category["costs"]["upvote"], count($payees), 64);

						// Update balance for poster
						$return["balance"] = db_result(db_query("UPDATE posts SET balance = balance + '%s', votes = votes + 1 WHERE postid = %d RETURNING BALANCE", $amount, $_POST["postid"]));
						if ($return["balance"] < "0.00000001") unset($return["balance"]); else $return["balance"] = clean_num($return["balance"]);

						// Update balance for previous upvoters
						db_query("UPDATE votes SET balance = balance + '%s' WHERE postid = %d", $amount, $_POST["postid"]);
					} else {
						$balance = "0";
						db_query("UPDATE posts SET votes = votes + 1 WHERE postid = %d", $_POST["postid"]);
					}

					db_query("INSERT INTO votes (postid, userid, vote, voted, balance) VALUES(%d, %d, 'upvote', 'now()', '%s')", $_POST["postid"], $_SESSION["userid"], $balance);
					$return["upvotes"] = db_result(db_query("SELECT COUNT(*) FROM votes WHERE postid = %d AND vote = 'upvote'", $_POST["postid"]));
				} else $return["witcoins"] = db_result(db_query("SELECT witcoins FROM users WHERE userid = %d", $_SESSION["userid"]));
			}
			else if (isset($_POST["replyid"])) {
				$voted = db_result(db_query("SELECT userid FROM votes WHERE replyid = %d AND vote = 'upvote' AND userid = %d", $_POST["replyid"], $_SESSION["userid"]));
				if (!$voted) {
					if ($category["costs"]["upvote"] > 0) {
						// Distribute funds
						$result = db_query("SELECT userid FROM votes WHERE replyid = %d AND vote = 'upvote'", $_POST["replyid"]);
						$payees = array(0, $category["renter"]);
						$payees[] = db_result(db_query("SELECT userid FROM replies WHERE replyid = %d", $_POST["replyid"]));
						while ($payee = db_fetch_array($result)) $payees[] = $payee["userid"];
						foreach (explode(",", $witizen["charities"]) as $charity) $payees[] = "charity_".$charity;
						$return["witcoins"] = payment($_SESSION["userid"], $payees, $category["costs"]["upvote"]);
						$balance = bcsub($return["witcoins"], $witizen["witcoins"], 64);

						$amount = bcdiv($category["costs"]["upvote"], count($payees), 64);

						// Update balance for replier
						$return["balance"] = db_result(db_query("UPDATE replies SET balance = balance + '%s', votes = votes + 1 WHERE replyid = %d RETURNING balance", $amount, $_POST["replyid"]));
						if ($return["balance"] < "0.00000001") unset($return["balance"]); else $return["balance"] = clean_num($return["balance"]);

						// Update balance for previous upvoters
						db_query("UPDATE votes SET balance = balance + '%s' WHERE replyid = %d", $amount, $_POST["replyid"]);
					} else {
						$balance = "0";
						db_query("UPDATE replies SET votes = votes + 1 WHERE replyid = %d", $_POST["replyid"]);
					}

					db_query("UPDATE posts SET votes = votes + 1 WHERE postid = (SELECT postid FROM replies WHERE replyid = %d)", $_POST["replyid"]);
					db_query("INSERT INTO votes (replyid, userid, vote, voted, balance) VALUES(%d, %d, 'upvote', 'now()', '%s')", $_POST["replyid"], $_SESSION["userid"], $balance);
					$return["upvotes"] = db_result(db_query("SELECT COUNT(*) FROM votes WHERE replyid = %d AND vote = 'upvote'", $_POST["replyid"]));
				} else $return["witcoins"] = db_result(db_query("SELECT witcoins FROM users WHERE userid = %d", $_SESSION["userid"]));
			}
			$return["witcoins"] = clean_num($return["witcoins"]);
		}
	}
?>