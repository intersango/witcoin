<?php
	global $blocks, $bodyclass, $category, $err, $path, $reply, $title, $witizen;

	$blocks[] = "category"; $blocks[] = "prices"; $blocks[] = "tags"; $bodyclass[] = "p_reply";

	$title[] = filter(htmlspecialchars($category["lang"]["verb_reply"]["present"]), FILTER_WORDS);

	preg_match("/^\/r\/([0-9]*)\/reply$/", $path, $matches);

	$sql = "SELECT replies.reply, replies.replied, replies.replyid AS reply_replyid, replies.userid AS reply_userid, replies.votes AS reply_votes,
		categories.categorysd AS category_sd, posts.postid AS reply_postid, users.displayname, users.email_address,
		(SELECT COUNT(*) FROM reply_revs WHERE replyid = replies.replyid) AS reply_revs,
		(SELECT COUNT(*) FROM votes WHERE replyid = replies.replyid AND userid = %d) AS reply_voted
		FROM replies
		LEFT OUTER JOIN users ON (replies.userid = users.userid)
		LEFT OUTER JOIN posts ON (replies.postid = posts.postid)
		LEFT OUTER JOIN categories ON (posts.category = categories.categoryid)
		WHERE replies.replyid = %d";

	if (isset($_SESSION["userid"])) {
		$replies = db_prepare_replies(db_query($sql, $_SESSION["userid"], $matches[1]));

		if ($replies) {
			$witizen = db_fetch_array(db_query("SELECT * FROM users WHERE userid = %d", $_SESSION["userid"]));
			$keys = array_keys($replies);
			$reply = array("replyid" => $keys[0]);
			foreach ($replies[$keys[0]] as $key => $value) $reply[$key] = $value;

			$preply = array("reply" => "", "upvote" => "");
			if (count($_POST) > 0) {
				if (isset($_POST["json"])) $return = array("error" => FALSE);

				$preply["reply"] = trim($_POST["reply"]);
				if (isset($_POST["upvote"])) $preply["upvote"] = $_POST["upvote"];

				$err = array(); $errmsg = array();
				if (isset($_POST["upvote"]) && $witizen["witcoins"] < $category["costs"]["reply"] + $category["costs"]["upvote"]) {
					if (isset($_POST["json"])) { echo json_encode(array("error" => "not enough witcoins")); exit; }
					$err["witcoins"] = TRUE; $errmsg[] = "not enough witcoins";
				}
				else if ($witizen["witcoins"] < $category["costs"]["reply"]) {
					if (isset($_POST["json"])) { echo json_encode(array("error" => "not enough witcoins")); exit; }
					$err["witcoins"] = TRUE; $errmsg[] = "not enough witcoins";
				}
				if (strlen($preply["reply"]) < 5) {
					if (isset($_POST["json"])) { echo json_encode(array("error" => lang($category["lang"], "noun_reply", "singular")." is too short")); exit; }
					$err["reply"] = TRUE; $errmsg[] = lang($category["lang"], "noun_reply", "singular")." is too short";
				}

				if (!$err) {
					if ($category["costs"]["reply"] > 0) {
						// Distribute funds
						$payees = array(0, $category["renter"], $reply["reply_userid"]);
						foreach (explode(",", $witizen["charities"]) as $charity) $payees[] = "charity_".$charity;
						payment($_SESSION["userid"], $payees, $category["costs"]["reply"]);

						$amount = bcdiv($category["costs"]["reply"], count($payees), 64);
						db_query("UPDATE replies SET balance = balance + '%s' WHERE replyid = %d", $amount, $reply["replyid"]);
					}

					if (isset($_POST["upvote"])) {
						$payees = array(0, $category["renter"], $_SESSION["userid"]);
						foreach (explode(",", $witizen["charities"]) as $charity) $payees[] = "charity_".$charity;
						payment($_SESSION["userid"], $payees, $category["costs"]["upvote"]);

						$now = db_result(db_query("INSERT INTO replies (replypid, postid, userid, reply, replied, balance, votes) VALUES(%d, %d, %d, '%s', 'now()', '%s', %d) RETURNING replied",
							$reply["replyid"], $reply["reply_postid"], $_SESSION["userid"], $preply["reply"], bcsub(bcdiv($category["costs"]["upvote"], 3, 64), $category["costs"]["reply"], 64), 1));
						$replyid = db_last_insert_id("replies", "replyid");

						db_query("UPDATE posts SET lastactivity = '%s', lastevent = 'replied', lastuserid = %d, votes = votes + 1 WHERE postid = %d", $now, $_SESSION["userid"], $reply["reply_postid"]);

						db_query("INSERT INTO votes (replyid, userid, vote, voted, balance) VALUES(%d, %d, 'upvote', '%s', '%s')", $replyid, $_SESSION["userid"], $now, bcsub(0, $category["costs"]["upvote"], 64));
					} else {
						$now = db_result(db_query("INSERT INTO replies (replypid, postid, userid, reply, replied, balance, votes) VALUES(%d, %d, %d, '%s', 'now()', '%s', %d) RETURNING replied",
							$reply["replyid"], $reply["reply_postid"], $_SESSION["userid"], $preply["reply"], bcsub(0, $category["costs"]["reply"], 64), 0));

						db_query("UPDATE posts SET lastactivity = '%s', lastevent = 'replied', lastuserid = %d WHERE postid = %d", $now, $_SESSION["userid"], $reply["reply_postid"]);
					}

					if (isset($_POST["json"])) { header("Location: /p/".$reply["reply_postid"]."?json"); exit; }

					header("Location: /p/".$reply["reply_postid"]); exit;
				}
			} else { // Not a form submission
				// Filter reply
				$reply["reply"] = filter($reply["reply"]);
			}
		} else { require_once "../protdocs/templates/_error.php"; exit; }
	} else if (count($_POST) > 0) {
		if (isset($_POST["json"])) { echo json_encode(array("error" => "not logged in")); exit; }
	}

?>