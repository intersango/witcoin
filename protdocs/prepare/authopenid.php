<?php
	global $errmsg, $head, $title;

	if (isset($_SESSION["userid"])) { if (isset($_GET["dest"])) header("Location: ".$_GET["dest"]); else header("Location: /"); exit; }

	require_once "../protdocs/includes/lightopenid.php";
	require_once "../protdocs/includes/openid.php";

	$head["script"][] = "/js/openid-jquery.js";
	$head["style"][] = "/css/openid.css";

	$query = array();
	if (isset($_GET["dest"])) $query[] = "dest=" . strip_tags($_GET["dest"]);

	$errmsg = "";
	$title[] = "wit up";
	try {
		$openid = new LightOpenID;
		if(!$openid->mode) {
			if(isset($_POST["openid_identifier"])) {
//	if ($_SERVER["REMOTE_ADDR"] == "65.30.35.48") { var_dump($_POST);exit; }
				// send login request to openid processor
				$openid->identity = $_POST["openid_identifier"];

				// see http://www.axschema.org/types/
				$openid->required = array("contact/email", "namePerson", "namePerson/first", "namePerson/friendly", "namePerson/last");

				header("Location: " . $openid->authUrl()); exit;
			}
//	if ($_SERVER["REMOTE_ADDR"] == "65.30.35.48") { echo 1;exit; }
		}
		elseif ($openid->mode == "cancel") {
			$blah = (array)$openid;
			$errmsg = "unable to log in with specified openid provider: cancel[l]ed.";
		}
		else if ($openid->mode == "error") {
			// login request error
			$errmsg = "unable to log in with specified openid provider: ".$openid->mode;
		}
		else if ($openid->mode == "id_res") {
			// login request successful
			if ($openid->validate()) {
				if ($openid_user = get_user_by_openid_identity($openid)) {
					// Successfully logged in
					$_SESSION["userid"] = $openid_user["userid"];
					$row = db_fetch_array(db_query("SELECT displayname FROM users WHERE userid = %d", $_SESSION["userid"]));
					$_SESSION["displayname"] = $row["displayname"];
					db_query("UPDATE users SET access = 'now()', login = 'now()' WHERE userid = %d", $_SESSION["userid"]);
					if (isset($_GET["dest"])) header("Location: ".$_GET["dest"]);
					else header("Location: /");
					exit;
				} else if ($openid_user = get_user_by_openid_attributes($openid)) {
					// New OpenID provider identity but existing OpenID account.  How or why does this happen?
					// http://blog.wekeroad.com/thoughts/open-id-is-a-party-that-happened
					$_SESSION["userid"] = $openid_user["userid"];
					$row = db_fetch_array(db_query("SELECT displayname FROM users WHERE userid = %d", $_SESSION["userid"]));
					$_SESSION["displayname"] = $row["displayname"];

					// Associate additional OpenID provider identity with existing account
					db_query("INSERT INTO users_openid (userid, openid_provider_name, openid_provider_identity, openid_user_email) VALUES(%d, '%s', '%s', '%s')",
						$_SESSION["userid"], get_openid_provider($openid->identity), $openid->identity, $openid_user["openid_user_email"]);

					db_query("UPDATE users SET access = 'now()', login = 'now()' WHERE userid = %d", $_SESSION["userid"]);
					if (isset($_GET["dest"])) header("Location: ".$_GET["dest"]);
					else header("Location: /");
					exit;
				} else {
					$user = $openid->getAttributes();
					$openid_user_email = isset($user["contact/email"]) ? $user["contact/email"] : "";
					$openid_user_firstname = isset($user["namePerson/first"]) ? $user["namePerson/first"] : "";
					$openid_user_lastname = isset($user["namePerson/last"]) ? $user["namePerson/last"] : "";
					$openid_user_name = isset($user["namePerson"]) ? $user["namePerson"] : "";
					$openid_user_nickname = isset($user["namePerson/friendly"]) ? $user["namePerson/friendly"] : "";
					$openid_provider_name = get_openid_provider($openid->identity);

					db_query("INSERT INTO users (displayname, email_address, created, access, login, rank) VALUES('%s', '%s', 'now()', 'now()', 'now()', '%s')",
						"witizen", $openid_user_email, "registered");
					$userid = db_last_insert_id("users", "userid");

					db_query("INSERT INTO users_openid (userid, openid_provider_name, openid_provider_identity, openid_user_email) VALUES('%d', '%s', '%s', '%s')",
						$userid, $openid_provider_name, $openid->identity, $openid_user_email);

					$_SESSION["userid"] = db_last_insert_id("users", "userid");
					$_SESSION["displayname"] = "witizen ".$_SESSION["userid"];
					db_query("UPDATE users SET displayname = '%s' WHERE userid = %d", $_SESSION["displayname"], $_SESSION["userid"]);

					if (isset($_GET["dest"])) header("Location: ".$_GET["dest"]);
					else header("Location: /");
					exit;
				}
			} else {
				// login request unsuccessful
				$errmsg = "unable to log in with specified openid provider: validation error";
			}
		}
		else {
			// login request unknown
			$errmsg = "unable to log in with specified openid provider: ".$openid->mode;
		}
	} catch(ErrorException $e) {
		$login_error = $e->getMessage();
		if ($login_error == "No servers found!") {
			$errmsg = "unable to log in with specified openid provider: no openid endpoint found.";
		}
		else {
			//echo $login_error;
			//$blah = (array)$e;
			//echo "<pre>"; var_dump($e); echo "</pre>";
			$errmsg = "unable to log in with specified openid provider: $login_error";
		}
	}
?>