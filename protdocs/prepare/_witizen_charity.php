<?php
	global $bodyclass, $err, $errmsg, $head, $path, $title, $witizen;

	$blocks[] = "profile"; $bodyclass[] = "witizens_charity";
	preg_match("/^\/witizens\/charities\/([0-9]*)$/", $path, $matches);
	$userid = $matches[1];

	if (count($_POST) > 0) {
		$charities = array();
		foreach ($_POST as $charity => $val) if ($val == "on" && preg_match("/^charity_([0-9]+)$/", $charity, $matches)) $charities[] = $matches[1];
		implode(",", $charities);
		db_query("UPDATE users SET charities = '%s' WHERE userid = %d", implode(",", $charities), $_SESSION["userid"]);
		//header("Location: /witizens/$userid"); exit;
	}

	$result = db_query("SELECT * FROM users WHERE userid = '%d'", $userid);
	$witizen = db_fetch_array($result);
	$title[] = "user"; $title[] = filter(htmlspecialchars($witizen["displayname"]), FILTER_WORDS); $title[] = "charitable organizations";
	$witizen["gravatar"] = $witizen["email_address"] == "" ? "" : "https://secure.gravatar.com/avatar/".md5(strtolower($witizen["email_address"]))."?s=168&d=monsterid&r=x";
	$created = db_fetch_array(db_query("SELECT EXTRACT(EPOCH FROM TIMESTAMP WITH TIME ZONE '%s')", $witizen["created"]));
	$access = db_fetch_array(db_query("SELECT EXTRACT(EPOCH FROM TIMESTAMP WITH TIME ZONE '%s')", $witizen["access"]));
	$witizen["member_for"] = time_length($created["date_part"]);
	$witizen["seen"] = time_duration($access["date_part"]);

	$charities = db_prepare_charities(db_query("SELECT * FROM charities WHERE status = 'verified' ORDER BY donated DESC"));
?>