<?php
	global $bodyclass, $blocks, $category, $err, $path, $post, $replies, $title, $witizen;

	$bodyclass[] = "p_viewpost";

	$blocks[] = "category"; $blocks[] = "prices"; $blocks[] = "	";
	preg_match("/^\/p\/([0-9]*)(?:\/.*)?$/", $path, $matches);

	$raw = FALSE; if (isset($_GET["raw"])) $raw = TRUE;
	$sort = "topvotes"; if (isset($_GET["sort"])) $sort = $_GET["sort"];

	$sql = "SELECT posts.balance AS post_balance, posts.lastactivity, posts.post, posts.posted, posts.postid AS post_postid, posts.status AS post_status, posts.taglist, posts.title, posts.userid AS post_userid, posts.views,
		users.displayname, users.email_address, categories.category AS category_name, categories.categorysd AS category_sd, categories.status AS category_status,
		(SELECT lang FROM categories WHERE categoryid = posts.category) AS category_lang,
		(SELECT COUNT(*) FROM post_revs WHERE postid = posts.postid) AS post_revs,
		(SELECT COUNT(*) FROM votes WHERE postid = posts.postid) AS post_votes";
	if (isset($_SESSION["userid"])) $sql .= ", (SELECT COUNT(*) FROM votes WHERE postid = posts.postid AND userid = %d) AS post_voted";
	$sql .= " FROM posts
		LEFT OUTER JOIN users ON (posts.userid = users.userid)
		LEFT OUTER JOIN categories ON (posts.category = categories.categoryid)
		WHERE posts.category = %d AND posts.postid = %d";

	if (isset($_SESSION["userid"])) $posts = db_prepare_posts(db_query($sql, $_SESSION["userid"], $category["categoryid"], $matches[1]));
	else $posts = db_prepare_posts(db_query($sql, $category["categoryid"], $matches[1]));

	if ($posts) {
		$keys = array_keys($posts);
		$post = array("postid" => $keys[0]);
		foreach ($posts[$keys[0]] as $key => $value) $post[$key] = $value;

		$title[] = $post["title"];

		$preply = array("reply" => "", "upvote" => "");
		if (count($_POST) > 0) {
			if (isset($_POST["reply"])) {
				if (isset($_POST["json"])) $return = array("error" => FALSE);

				$preply["reply"] = trim($_POST["reply"]);
				if (isset($_POST["upvote"])) $preply["upvote"] = $_POST["upvote"];

				$err = array(); $errmsg = array();
				if (!isset($_SESSION["userid"])) {
					if (isset($_POST["json"])) { echo json_encode(array("error" => "not logged in")); exit; }
					$err["userid"] = TRUE; $errmsg[] = "not logged in";
				} else {
					$witizen = db_fetch_array(db_query("SELECT * FROM users WHERE userid = %d", $_SESSION["userid"]));
					if (isset($_POST["upvote"]) && $witizen["witcoins"] < $category["costs"]["reply"] + $category["costs"]["upvote"]) {
						if (isset($_POST["json"])) { echo json_encode(array("error" => "not enough witcoins")); exit; }
						$err["witcoins"] = TRUE; $errmsg[] = "not enough witcoins";
					}
					else if ($witizen["witcoins"] < $category["costs"]["reply"]) {
						if (isset($_POST["json"])) { echo json_encode(array("error" => "not enough witcoins")); exit; }
						$err["witcoins"] = TRUE; $errmsg[] = "not enough witcoins";
					}
				}
				if (strlen($preply["reply"]) < 5) {
					if (isset($_POST["json"])) { echo json_encode(array("error" => lang($category["lang"], "noun_reply", "singular")." is too short")); exit; }
					$err["reply"] = TRUE; $errmsg[] = lang($category["lang"], "noun_reply", "singular")." is too short";
				}

				if (!$err) {
					if ($category["costs"]["reply"] > 0) {
						// Distribute funds
						$payees = array(0, $category["renter"], $post["post_userid"]);
						foreach (explode(",", $witizen["charities"]) as $charity) $payees[] = "charity_".$charity;
						payment($_SESSION["userid"], $payees, $category["costs"]["reply"]);
						$amount = bcdiv($category["costs"]["reply"], count($payees), 64);
					} else $amount = "0";


					if (isset($_POST["upvote"])) {
						if ($category["costs"]["upvote"] > 0) {
							$payees = array(0, $category["renter"], $_SESSION["userid"]);
							foreach (explode(",", $witizen["charities"]) as $charity) $payees[] = "charity_".$charity;
							payment($_SESSION["userid"], $payees, $category["costs"]["upvote"]);
						}

						$now = db_result(db_query("INSERT INTO replies (postid, userid, reply, replied, balance, votes) VALUES(%d, %d, '%s', 'now()', '%s', %d) RETURNING replied",
							$post["postid"], $_SESSION["userid"], $preply["reply"], bcsub(bcdiv($category["costs"]["upvote"], 3, 64), $category["costs"]["reply"], 64), 1));
						$replyid = db_last_insert_id("replies", "replyid");

						db_query("UPDATE posts SET balance = balance + '%s', lastactivity = '%s', lastevent = 'replied', lastuserid = %d, votes = votes + 1 WHERE postid = %d",
							$amount, $now, $_SESSION["userid"], $post["postid"]);

						db_query("INSERT INTO votes (replyid, userid, vote, voted, balance) VALUES(%d, %d, 'upvote', '%s', '%s')", $replyid, $_SESSION["userid"], $now, bcsub(0, $category["costs"]["upvote"], 64));
					} else {
						$now = db_result(db_query("INSERT INTO replies (postid, userid, reply, replied, balance, votes) VALUES(%d, %d, '%s', 'now()', '%s', %d) RETURNING replied",
							$post["postid"], $_SESSION["userid"], $preply["reply"], bcsub(0, $category["costs"]["reply"], 64), 0));

						db_query("UPDATE posts SET balance = balance + '%s', lastactivity = '%s', lastevent = 'replied', lastuserid = %d WHERE postid = %d", $amount, $now, $_SESSION["userid"], $post["postid"]);
					}

					if (isset($_POST["json"])) { header("Location: $path?json"); exit; }

					header("Location: $path"); exit;
				} else if (isset($_POST["json"])) { $return["error"] = implode(".  ", $errmsg); echo json_encode($return); exit; }
			} else if (isset($_POST["status"])) {
				if (isset($post["category_status"]) && is_rank("moderator") && $post["post_status"] != $_POST["status"] && in_array($_POST["status"], explode(",", $post["category_status"]))) {
					// If adding columns, update protdocs/prepare/editpost.php also
					// Maybe create a wrapper function to call for both pages later?
					db_query("INSERT INTO post_revs (postid, title, post, taglist, posted, status, userid) VALUES(%d, '%s', '%s', '%s', '%s', '%s', %d)",
						$post["postid"], $post["title"], $post["post"], pg_str_to_array($post["taglist"]), $post["posted"], $post["post_status"], $_SESSION["userid"]);
					db_query("UPDATE posts SET title = '%s', post = '%s', taglist = '%s', lastevent = 'status updated', lastactivity = 'now()', lastuserid = %d, status = '%s' WHERE postid = %d",
						$post["title"], $post["post"], pg_str_to_array($post["taglist"]), $_SESSION["userid"], $_POST["status"], $post["postid"]);
				}
				header("Location: $path"); exit;
			}
		} else { // Not a form submission
			// Add to view count
			$userid = isset($_SESSION["userid"]) ? $_SESSION["userid"] : -1;
			$view = db_fetch_array(db_query("SELECT * FROM views WHERE postid = %d AND userid = %d AND ipaddress = '%s' ORDER BY viewid DESC",
				$post["postid"], $userid, $_SERVER["REMOTE_ADDR"]));
			if ($view) $viewed = db_fetch_array(db_query("SELECT EXTRACT(EPOCH FROM TIMESTAMP WITH TIME ZONE '%s')", $view["viewed"]));
			if ($view && intval($viewed["date_part"]) > (time() - 1200)) { }
			else {
				// Remove old views
				db_query("DELETE FROM views WHERE viewed < to_timestamp(".(time() - 1200).")");

				db_query("UPDATE posts SET views = views + 1 WHERE postid = %d", $post["postid"]);
				db_query("INSERT INTO views (postid, userid, ipaddress, viewed) VALUES(%d, %d, '%s', 'now()')",
					$post["postid"],
					isset($_SESSION["userid"]) ? $_SESSION["userid"] : -1,
					$_SERVER["REMOTE_ADDR"]);
				//$viewid = db_last_insert_id("views", "viewid");
			}

			// Filter post
			$post["post"] = filter($post["post"]);

			if (isset($_SESSION["userid"])) {
				$s_col1 = ", (SELECT COUNT(*) FROM votes WHERE replyid = replies.replyid AND userid = %d) AS reply_voted";
				$s_col2 = ", (SELECT COUNT(*) FROM votes WHERE replyid = r.replyid AND userid = %d)";
			} else { $s_col1 = ""; $s_col2 = ""; }

			if ($sort == "newest") { $s_arr1 = "-extract(epoch from replied)::integer"; $s_arr2 = " || -extract(epoch from r.replied)::integer"; }
			else if ($sort == "oldest") { $s_arr1 = "extract(epoch from replied)::integer"; $s_arr2 = " || extract(epoch from r.replied)::integer"; }
			else if ($sort == "toprank" || $sort == "topvotes") { $s_arr1 = "-votes"; $s_arr2 = " || -r.votes"; }
			else { $s_arr1 = ""; $s_arr2 = ""; }

			$sql = "WITH RECURSIVE t(replyid, replypid, depth, path, reply, replied, reply_userid, reply_balance) AS (
					(SELECT replyid, replypid, 0, array[$s_arr1,replyid], reply, replied, replies.userid, replies.balance, u.displayname, u.email_address,
						(SELECT COUNT(*) FROM reply_revs WHERE replyid = replies.replyid) AS reply_revs,
						(SELECT COUNT(*) FROM votes WHERE replyid = replies.replyid) AS reply_votes
						$s_col1
					FROM replies
					LEFT OUTER JOIN users u ON (replies.userid = u.userid)
					WHERE replypid is NULL AND postid = %d)
					UNION ALL
					(SELECT r.replyid, r.replypid, t.depth+1, t.path$s_arr2 || r.replyid, r.reply, r.replied, r.userid, r.balance, u.displayname, u.email_address,
						(SELECT COUNT(*) FROM reply_revs WHERE replyid = r.replyid) AS reply_revs,
						(SELECT COUNT(*) FROM votes WHERE replyid = r.replyid) AS reply_votes
						$s_col2
					FROM replies r
					JOIN t ON (r.replypid = t.replyid)
					LEFT OUTER JOIN users u ON (r.userid = u.userid))
				) SELECT * FROM t ORDER BY path";

			if (isset($_SESSION["userid"])) $replies = db_prepare_replies(db_query($sql, $_SESSION["userid"], $matches[1], $_SESSION["userid"]));
			else $replies = db_prepare_replies(db_query($sql, $matches[1]));

			// Filter replies
			foreach ($replies as $replyid => $reply) $replies[$replyid]["reply"] = filter($replies[$replyid]["reply"]);

			if (isset($post["category_status"]) && is_rank("moderator")) {
				$select_status = '<select name="status"><option>status</option>';
				foreach (explode(",", $post["category_status"]) as $option) $select_status .= '<option '.(isset($post["post_status"]) && $post["post_status"] == $option ? "selected='selected' " : "").'value="'.$option.'">'.$option.'</option>';
				$select_status .= '</select>';
			}

			if (isset($_GET["json"])) {
				$return = array("post" => $post, "replies" => array());
				if ($post["post_balance"] >= "0.00000001") $return["post"]["profit"] = clean_num($post["post_balance"]);
				$return["post"]["duration"] = time_duration(strtotime($post["posted"]));
				foreach ($replies as $replyid => $reply) {
					$merge = array("duration" => time_duration(strtotime($replies[$replyid]["replied"])), "profit" => "", "replyid" => $replyid);
					if ($reply["reply_balance"] >= "0.00000001") $merge["profit"] = clean_num($reply["reply_balance"]);
					$return["replies"][] = array_merge($reply, $merge);
				}
				$return["num_replies"] = count($replies) . " " . ($category["lang"]["noun_reply"][count($replies) == 1 ? "singular" : "plural"]);

				if (loggedin()) {
					$return["userid"] = $_SESSION["userid"];
					$return["witcoins"] = clean_num(getbalance($_SESSION["userid"]));
				}
				echo json_encode($return); exit;
			}
		}
	} else { require_once "../protdocs/templates/_error.php"; exit; }
?>