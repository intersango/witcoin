<?php
	global $blocks, $category, $posts, $title;

	$blocks[] = "category"; $blocks[] = "description"; $blocks[] = "prices"; $blocks[] = "tags";

	$status = "all"; if (isset($_GET["status"]) && in_array($_GET["status"], explode(",", $category["status"]))) $status = $_GET["status"];
	$limit = 60;

	if ($category["categoryid"] != 0) $sql = "WITH RECURSIVE hcategories(categoryid, categorypid, depth) AS (
			(SELECT categoryid, categorypid, 0 FROM categories WHERE categoryid = %d)
			UNION ALL (SELECT c.categoryid, c.categorypid, hcategories.depth+1 FROM categories c JOIN hcategories ON (c.categorypid = hcategories.categoryid))
		), pcategories(category, categoryid, categorypid, depth) AS (
			(SELECT category, categoryid, categorypid, 0 FROM categories WHERE categoryid = %d)
			UNION ALL (SELECT c.category, c.categoryid, c.categorypid, pcategories.depth+1 FROM categories c JOIN pcategories ON (c.categoryid = pcategories.categorypid))
		) ";
	else $sql = "";

	$sql .= "SELECT posts.category, posts.lastactivity, posts.lastevent, posts.lastuserid, posts.post, posts.posted, posts.postid AS posts_postid, posts.status AS posts_status, posts.taglist, posts.title, posts.userid AS posts_userid,
		posts.views, posts.votes AS posts_votes, users.displayname, categories.category AS category_name, categories.categorysd AS category_sd, categories.status AS category_status,
		(SELECT lang FROM categories WHERE categoryid = posts.category) AS category_lang,
		(SELECT rankfresh(1+posts.votes+(posts.support / 0.01)::int, 0, EXTRACT(EPOCH FROM now() - posts.lastactivity))) AS posts_rank,
		(SELECT COUNT(*) FROM replies WHERE postid = posts.postid) AS posts_replies
		FROM posts
		LEFT OUTER JOIN users ON (posts.lastuserid = users.userid)
		LEFT OUTER JOIN categories ON (posts.category = categories.categoryid)
		WHERE TRUE";

	if ($category["categoryid"] != 0) $sql .= " AND posts.category IN (SELECT categoryid FROM hcategories)";
	else $sql .= " AND posts.category != 88"; // Not in sandbox

	if ($status != "all") $sql .= " AND posts.status = '" . db_escape_string($status) . "'";

	$sql .= " ORDER BY posts.posted DESC LIMIT ".strval($limit);

	if ($category["categoryid"] != 0) $posts = db_prepare_posts(db_query($sql, $category["categoryid"]));
	else $posts = db_prepare_posts(db_query($sql));

	$lastactivity = array("date" => 0, "ts" => 0);
	foreach ($posts as $postid => $post) {
		// Filter posts
		$posts[$postid]["post"] = filter($posts[$postid]["post"]);
		if (strtotime($post["lastactivity"]) > $lastactivity["ts"]) $lastactivity = array("ts" => strtotime($post["lastactivity"]), "date" => $post["lastactivity"]);
	}
?>