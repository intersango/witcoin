<?php
	global $blocks, $bodyclass, $path, $site, $title, $witizen;

	$blocks[] = "profile"; $bodyclass[] = "witizens";
	preg_match("/^\/witizens\/([0-9]*)$/", $path, $matches);
	$userid = $matches[1];
	$witizen = db_fetch_array(db_query("SELECT * FROM users WHERE userid = '%d'", $userid));
	$title[] = "user - ". filter(htmlspecialchars($witizen["displayname"]), FILTER_WORDS);
	$witizen["gravatar"] = $witizen["email_address"] == "" ? "" : "https://secure.gravatar.com/avatar/".md5(strtolower($witizen["email_address"]))."?s=168&d=monsterid&r=x";
	$created = db_fetch_array(db_query("SELECT EXTRACT(EPOCH FROM TIMESTAMP WITH TIME ZONE '%s')", $witizen["created"]));
	$access = db_fetch_array(db_query("SELECT EXTRACT(EPOCH FROM TIMESTAMP WITH TIME ZONE '%s')", $witizen["access"]));
	$witizen["member_for"] = time_length($created["date_part"]);
	$witizen["seen"] = time_duration($access["date_part"]);
	if ($witizen["website"] != "" && !preg_match("/^https?:\/\/.*$/", $witizen["website"])) $witizen["website"] = "http://" . $witizen["website"];

	$sort = "toprank"; if (isset($_GET["sort"])) $sort = $_GET["sort"];
	if (isset($_GET["a"]) && is_numeric(base64_decode($_GET["a"]))) $after = $_GET["a"];
	$limit = 60;

	if (isset($after)) {
		$after = db_fetch_array(db_query("SELECT posts.*, (SELECT COUNT(*) FROM replies WHERE postid = posts.postid) AS posts_replies
			FROM POSTS WHERE category = %d AND postid = %d", $category["categoryid"], base64_decode($after)));
		if (!$after) { require_once "../protdocs/templates/_error.php"; exit; }
	}

	$sql = "SELECT posts.category, posts.lastactivity, posts.lastevent, posts.lastuserid, posts.post, posts.posted, posts.postid AS posts_postid, posts.status AS posts_status, posts.taglist, posts.title, posts.userid AS posts_userid,
		posts.views, posts.votes AS posts_votes, posts_thumbs.thumb, users.displayname, categories.category AS category_name, categories.categorysd AS category_sd, categories.status AS category_status,
		(SELECT lang FROM categories WHERE categoryid = posts.category) AS category_lang,
		(SELECT rankfresh(1+posts.votes+(posts.support / 0.01)::int, 0, EXTRACT(EPOCH FROM now() - posts.lastactivity))) AS posts_rank,
		(SELECT COUNT(*) FROM replies WHERE postid = posts.postid) AS posts_replies
		FROM posts
		LEFT OUTER JOIN posts_thumbs ON (posts.postid = posts_thumbs.postid)
		LEFT OUTER JOIN users ON (posts.userid = users.userid)
		LEFT OUTER JOIN categories ON (posts.category = categories.categoryid)
		WHERE posts.userid = %d AND posts.category != 88";

	if (isset($after)) {
		if ($sort == "lastactivity") $sql .= " AND posts.lastactivity < '%s'";
		else if ($sort == "newest") $sql .= " AND posts.posted < '%s'";
		else if ($sort == "oldest") $sql .= " AND posts.posted > '%s'";
		else if ($sort == "toprank") $sql .= " AND (rankfresh(1+posts.votes+(posts.support / 0.01)::int, 0, EXTRACT(EPOCH FROM now() - posts.lastactivity)), posts.lastactivity) < (rankfresh(1+%d+%d, 0, EXTRACT(EPOCH FROM now() - '%s')), '%s')";
		else if ($sort == "topreplies") $sql .= " AND ((SELECT COUNT(*) FROM replies WHERE postid = posts.postid), posts.lastactivity) < (%d, '%s')";
		else if ($sort == "topvotes") $sql .= " AND (posts.votes, posts.lastactivity) < (%d, '%s')";

	}

	if ($sort == "lastactivity") $sql .= " ORDER BY posts.lastactivity DESC";
	else if ($sort == "newest") $sql .= " ORDER BY posts.posted DESC";
	else if ($sort == "oldest") $sql .= " ORDER BY posts.posted";
	else if ($sort == "topreplies") $sql .= " ORDER BY posts_replies DESC, posts.lastactivity DESC";
	else if ($sort == "toprank") $sql .= " ORDER BY rankfresh(1+posts.votes+(posts.support / 0.01)::int, 0, EXTRACT(EPOCH FROM now() - posts.lastactivity)) DESC, posts.lastactivity DESC";
	else if ($sort == "topvotes") $sql .= " ORDER BY posts.votes DESC, posts.lastactivity DESC";

	$sql .= " LIMIT ".strval($limit);

	if (!isset($after)) $posts = db_prepare_posts(db_query($sql, $userid));
	else if ($sort == "lastactivity") $posts = db_prepare_posts(db_query($sql, $userid, $after["lastactivity"]));
	else if ($sort == "newest") $posts = db_prepare_posts(db_query($sql, $userid, $after["posted"]));
	else if ($sort == "oldest") $posts = db_prepare_posts(db_query($sql, $userid, $after["posted"]));
	else if ($sort == "toprank") $posts = db_prepare_posts(db_query($sql, $userid, $after["votes"], $after["support"], $after["lastactivity"], $after["lastactivity"]));
	else if ($sort == "topreplies") $posts = db_prepare_posts(db_query($sql, $userid, $after["posts_replies"], $after["lastactivity"]));
	else if ($sort == "topvotes") $posts = db_prepare_posts(db_query($sql, $userid, $after["votes"], $after["lastactivity"]));
?>