<?php
	global $btc, $btcconn, $domain, $err, $success, $title, $witizen;

	require_once "../protdocs/includes/bitcoin.php";

	if (!isset($_SESSION["userid"])) { require_once "../protdocs/templates/_error.php"; exit; }
	else if (!$btc) { require_once "../protdocs/templates/_error_btc.php"; exit; }

	$title[] = "witdraw";

	$witizen = db_fetch_array(db_query("SELECT * FROM users WHERE userid = %d", $_SESSION["userid"]));

	$err = array(); $errmsg = array(); $success = FALSE; $successmsg = array();

	if (!checkAddress($witizen["bitcoin_address_user"])) { $err["bitcoin_address_user"] = TRUE; $errmsg[] = "invalid bitcoin address"; }

	if (count($_POST) > 0) {
		// Check for errors
		if (!is_numeric($_POST["witcoins"]) || $_POST["witcoins"] <= 0) { $err["witcoins"] = TRUE; $errmsg[] = "invalid amount of witcoins"; }
		else if ($_POST["witcoins"] < 0.01) { $err["witcoins"] = TRUE; $errmsg[] = "you must witdraw 0.01 or greater amount of witcoins"; }
		else if ($_POST["witcoins"] > $witizen["witcoins"]) { $err["witcoins"] = TRUE; $errmsg[] = "you do not have that many witcoins"; }

		if (!$err) {
			if ($btc && $domain["address"] != "witcoin.com:2980" && $domain["address"] != "witcoin.com:2981" && $domain["address"] != "witcoin.com:2982") {
				try { $btcconn->sendfrom("witcoin.com", $witizen["bitcoin_address_user"], (float)$_POST["witcoins"], 1, "", ""); }
				catch (Exception $e) { btcerr($e); require_once "../protdocs/templates/_error_btc.php"; exit; }
			}
			$witdraw = number_format($_POST["witcoins"], 2, ".", "");
			$witcoins = bcsub($witizen["witcoins"], $witdraw, 64);
			db_query("UPDATE users SET witcoins = '%s' WHERE userid = %d", $witcoins, $_SESSION["userid"]);
			db_query("INSERT INTO transactions (userid, type, witcoins, bitcoin_address_user, txed) VALUES(%d, '%s', '%s', '%s', 'now()')",
				$_SESSION["userid"], "withdraw", $witdraw, $witizen["bitcoin_address_user"]);
			$success = TRUE; $successmsg[] = $witdraw . " witcoin".($witdraw == "1.00" ? " has" : "s have")." been sent to " . $witizen["bitcoin_address_user"];
			unset($_POST);
		}
	}
?>