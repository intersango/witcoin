<?php
	global $err, $errmsg, $head, $title;

	if (isset($_SESSION["userid"])) {
		if (isset($_GET["dest"])) header("Location: ".$_GET["dest"]); else header("Location: /");
		exit;
	}

	$title[] = "get wit it";
	$head["script"][] = "/js/openid-jquery.js";
	$head["style"][] = "/css/openid.css";
	require_once "../protdocs/includes/passwordhash.php";

	$query = array();
	if (isset($_GET["dest"])) $query[] = "dest=" . strip_tags($_GET["dest"]);

	$err = array(); $errmsg = array();
	if (count($_POST) > 0) {
		if (strlen($_POST["username"]) < 3 || strlen($_POST["username"]) > 43 || !preg_match("/^[a-zA-Z0-9_-]*$/", $_POST["username"])) {
			$err["username"] = TRUE; $errmsg[] = "invalid username";
		}
		if (strlen($_POST["password"]) < 12) { $err["password"] = TRUE; $errmsg[] = "invalid password"; }

		if (!$err) {
			$hasher = new PasswordHash(12, FALSE);
			$user = db_fetch_array(db_query("SELECT * FROM users WHERE username = '%s'", $_POST["username"]));
			if ($hasher->CheckPassword($_POST["password"], $user["password_hash"])) {
				$_SESSION["userid"] = $user["userid"];
				$_SESSION["displayname"] = $user["displayname"];
				if (isset($_GET["dest"])) header("Location: ".$_GET["dest"]);
				else header("Location: /");
				exit;
			} else { $err["password"] = TRUE; $errmsg[] = "wrong password"; }
		}
	}
?>