<?php
	global $title, $witizen;

	if (!isset($_SESSION["userid"])) { require_once "../protdocs/templates/_error.php"; exit; }

	preg_match("|^/inbox/msg/([0-9]*)$|", $path, $matches);

	$messages = array();
	$sql = "SELECT messages.*, (SELECT displayname FROM users WHERE userid = messages.fromuser) AS fromuserdisplayname,
		(SELECT displayname FROM users WHERE userid = messages.touser) AS touserdisplayname
		FROM messages WHERE (touser = %d OR fromuser = %d) AND messageid = %d";
	$message = db_fetch_array(db_query($sql, $_SESSION["userid"], $_SESSION["userid"], $matches[1]));

	if (!$message) { require_once "../protdocs/templates/_error.php"; exit; }

	$raw = FALSE; if (isset($_GET["raw"])) $raw = TRUE;

	if ($message["type"] == "donation") {
		$title[] = $message["amount"] . " witcoin" . ($message["amount"] == 1 ? "" : "s") . " donated "
			. ($_SESSION["userid"] == $message["touser"] ? "from" : "to") . " <a href='/witizens/".($_SESSION["userid"] == $message["touser"] ? $message["fromuser"] : $message["touser"]) . "' title='"
			. filter(htmlspecialchars($message[$_SESSION["userid"] == $message["touser"] ? "fromuserdisplayname" : "touserdisplayname"]), FILTER_WORDS)."'>"
			. filter(htmlspecialchars($message[$_SESSION["userid"] == $message["touser"] ? "fromuserdisplayname" : "touserdisplayname"]), FILTER_WORDS)."</a>";
	}

	if ($message["touser"] == $_SESSION["userid"]) db_query("UPDATE messages SET read = TRUE WHERE touser = %d AND messageid = %d", $_SESSION["userid"], $matches[1]);
?>