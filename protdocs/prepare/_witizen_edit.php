<?php
	global $bodyclass, $err, $errmsg, $head, $path, $title, $witizen;

	$bodyclass[] = "witizens_edit";
	preg_match("/^\/witizens\/edit\/([0-9]*)$/", $path, $matches);
	$userid = $matches[1];

	if (count($_POST) > 0) {
		foreach ($_POST as $k => $v) $_POST[$k] = trim($v);

		// Check for errors
		$err = array(); $errmsg = array();
		if (strlen($_POST["displayname"]) < 3) { $err["displayname"] = TRUE; $errmsg[] = "not enough name"; }
		else if (strlen($_POST["displayname"]) > 43) { $err["displayname"] = TRUE; $errmsg[] = "too much name"; }
		else if (!preg_match("/^[a-zA-Z0-9 '_-]*$/", $_POST["displayname"])) { $err["displayname"] = TRUE; $errmsg[] = "display name must contain only a-zA-Z0-9 '_-"; }
		if ($_POST["bitcoinaddress"] != "" && !checkAddress($_POST["bitcoinaddress"])) { $err["bitcoinaddress"] = TRUE; $errmsg[] = "invalid bitcoin address"; }
		if ($_POST["emailaddress"] != "" && !preg_match("/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/", strtoupper($_POST["emailaddress"]))) { $err["emailaddress"] = TRUE; $errmsg[] = "email address is invalid"; }
		if (preg_match("/^javascript:/", $_POST["website"])) { $err["website"] = TRUE; $errmsg[] = "invalid website"; }

		if (!$err && db_result(db_query("SELECT displayname FROM users WHERE userid != %d AND displayname = '%s'", $_SESSION["userid"], $_POST["displayname"]))) { $err["displayname"] = TRUE; $errmsg[] = "display name already exists"; }

		if (!$err) {
			$displayname = $_POST["displayname"];
			$bitcoinaddress = $_POST["bitcoinaddress"];
			$emailaddress = $_POST["emailaddress"];
			$website = htmlspecialchars(strip_tags($_POST["website"]));
			$location = filter(htmlspecialchars($_POST["location"]), FILTER_WORDS);
			$aboutme = $_POST["aboutme"];
			$query = "UPDATE users SET displayname = '%s', bitcoin_address_user = '%s', email_address = '%s', website = '%s', location = '%s', aboutme = '%s' WHERE userid = %d";
			db_query($query, $displayname, $bitcoinaddress, $emailaddress, $website, $location, $aboutme, $userid);
			$_SESSION["displayname"] = $displayname;
			header("Location: /witizens/$userid"); exit;
		}
	}

	$result = db_query("SELECT * FROM users WHERE userid = '%d'", $userid);
	$witizen = db_fetch_array($result);
	$title[] = "user"; $title[] = filter(htmlspecialchars($witizen["displayname"]), FILTER_WORDS); $title[] = "edit";
	$witizen["gravatar"] = $witizen["email_address"] == "" ? "" : "https://secure.gravatar.com/avatar/".md5(strtolower($witizen["email_address"]))."?s=168&d=monsterid&r=x";
	//	$created = db_fetch_array(db_query("SELECT EXTRACT(EPOCH FROM TIMESTAMP WITH TIME ZONE '%s')", $witizen["created"]));
	//	$access = db_fetch_array(db_query("SELECT EXTRACT(EPOCH FROM TIMESTAMP WITH TIME ZONE '%s')", $witizen["access"]));
	//	$witizen["member_for"] = time_length($created["date_part"]);
	//	$witizen["seen"] = time_duration($access["date_part"]);
?>