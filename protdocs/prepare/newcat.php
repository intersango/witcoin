<?php
	global $blocks, $bodyclass, $costs, $lang, $newcat, $title, $witizen;

	$bodyclass[] = "p_newcat";

	if (!isset($_SESSION["userid"])) { require_once "../protdocs/templates/_error.php"; exit; }
	if (isset($_SESSION["rank"]) && $_SESSION["rank"] != "moderator" && in_array($category["categoryid"], array(2,3,4,5,6,88,198))) { require_once "../protdocs/templates/_error.php"; exit; }
	if (isset($_SESSION["rank"]) && $_SESSION["rank"] != "moderator" && $category["categoryid"] == "0") { require_once "../protdocs/templates/_error.php"; exit; }
	if ($category["categoryid"] != 0 && !db_result(db_query("SELECT categoryid FROM categories WHERE categoryid = %d", $category["categoryid"]))) { require_once "../protdocs/templates/_error.php"; exit; }
	if ($category["categoryid"] == "0" && $_SESSION["rank"] != "moderator") { require_once "../protdocs/templates/_error.php"; exit; }

	$blocks[] = "category"; $blocks[] = "description"; $blocks[] = "prices"; $blocks[] = "tags";
	if ($category["categoryid"] == "0") $title[] = "new category";
	else $title[] = "new subcategory";

	$err = array(); $errmsg = array();

	$witizen = db_fetch_array(db_query("SELECT * FROM users WHERE userid = %d", $_SESSION["userid"]));

	$newcat = array("category" => "", "categorysd" => "");
	if (count($_POST) > 0) {
		$newcat["category"] = strtolower(trim($_POST["category"]));
		$newcat["categorysd"] = strtolower(trim($_POST["categorysd"]));

		// Check for errors
		if ($_SESSION["rank"] != "moderator" && $witizen["witcoins"] < $costs["new category"]) { $err["witcoins"] = TRUE; $errmsg[] = "not enough witcoins"; }
		if (strlen($newcat["category"]) < 3) { $err["category"] = TRUE; $errmsg[] = "category name is too short"; }
		else if (!preg_match("/^[a-zA-Z0-9' .,!&?-]*$/", $newcat["category"])) { $err["category"] = TRUE; $errmsg[] = "category name must contain only a-zA-Z0-9' .,!&?-"; }
		if (strlen($newcat["categorysd"]) < 3) { $err["categorysd"] = TRUE; $errmsg[] = "category subdomain is too short"; }
		else if (!preg_match("/^[a-zA-Z0-9-]*$/", $newcat["categorysd"])) { $err["categorysd"] = TRUE; $errmsg[] = "category subdomain must contain only a-zA-Z0-9-"; }

		// Restricted categories
		$restricted = array("admin", "www");
		if (in_array($newcat["category"], $restricted)) { $err["category"] = TRUE; $errmsg[] = "category name is restricted"; }
		$restricted = array("admin", "www");
		if (in_array($newcat["categorysd"], $restricted)) { $err["categorysd"] = TRUE; $errmsg[] = "category subdomain is restricted"; }

		if (!$err) {
			// Check if category exists
			$cats = array();
			$result = db_query("SELECT * FROM categories WHERE categorysd = '%s'", $newcat["categorysd"]);
			if (db_fetch_array($result)) { $err["categorysd"] = TRUE; $errmsg[] = "category subdomain already exists"; }
		}

		if (!$err) {
			// Distribute funds
			if ($_SESSION["rank"] != "moderator") {
				$payees = array(0);
				foreach (explode(",", $witizen["charities"]) as $charity) $payees[] = "charity_".$charity;
				payment($_SESSION["userid"], $payees, $costs["new category"]);
			}

			if ($category["categoryid"] == "0")
				db_query("INSERT INTO categories (category, categorysd, creator, renter, created, lang, description, faq, categorypid, allowposts, value)
					VALUES('%s', '%s', %d, %d, 'now()', '%s', '', '', NULL, FALSE, '%s')",
					$newcat["category"], $newcat["categorysd"], $_SESSION["userid"], $_SESSION["userid"], serialize($lang), $costs["new category"]);
			else
				db_query("INSERT INTO categories (category, categorysd, creator, renter, created, lang, description, faq, categorypid, allowposts, value)
					VALUES('%s', '%s', %d, %d, 'now()', '%s', '', '', %d, TRUE, '%s')",
					$newcat["category"], $newcat["categorysd"], $_SESSION["userid"], $_SESSION["userid"], serialize($lang), $category["categoryid"], $costs["new category"]);

			$categoryid = db_last_insert_id("categories", "categoryid");
			db_query("INSERT INTO costs (categoryid, post, reply, edit, upvote) VALUES(%d, '%s', '%s', '%s', '%s')",
				$categoryid, $costs["post"], $costs["reply"], $costs["edit"], $costs["upvote"]);

			header("Location: ".getdomain($newcat["categorysd"])); exit;
		}
	}
?>