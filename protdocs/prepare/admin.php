<?php
	global $bodyclass, $category, $domain, $err, $errmsg, $path, $title, $witizen;

	$bodyclass[] = "p_catman";

	if (!isset($_SESSION["userid"])) { header("Location: ".getdomain()); exit; }
	if ($_SESSION["userid"] != $category["renter"]) { header("Location: ".getdomain()); exit; }
	$blocks[] = "category"; $blocks[] = "description";

	$title[] = "edit category";
	$admincat = $category;
	//$admincat["cost_edit"] = $admincat["costs"]["edit"]; $admincat["cost_post"] = $admincat["costs"]["post"]; $admincat["cost_reply"] = $admincat["costs"]["reply"]; $admincat["cost_upvote"] = $admincat["costs"]["upvote"];

	$err = array(); $errmsg = array(); $successmsg = array();
	if (count($_POST) > 0) {
		$admincat = $_POST;
		// Check for errors
		//if (!is_numeric($admincat["cost_post"]) || $admincat["cost_post"] < 0.00000001 || $admincat["cost_post"] > 1000) { $err["cost_post"] = TRUE; $errmsg[] = "invalid post cost"; }
		//if (!is_numeric($admincat["cost_reply"]) || $admincat["cost_reply"] < 0.00000001 || $admincat["cost_reply"] > 1000) { $err["cost_reply"] = TRUE; $errmsg[] = "invalid reply cost"; }
		//if (!is_numeric($admincat["cost_edit"]) || $admincat["cost_edit"] < 0.00000001 || $admincat["cost_edit"] > 1000) { $err["cost_edit"] = TRUE; $errmsg[] = "invalid edit cost"; }
		//if (!is_numeric($admincat["cost_upvote"]) || $admincat["cost_upvote"] < 0.00000001 || $admincat["cost_upvote"] > 1000) { $err["cost_upvote"] = TRUE; $errmsg[] = "invalid upvote cost"; }
		if ($admincat["description"] != "" && strlen($admincat["description"]) < 6) { $err["description"] = TRUE; $errmsg[] = "description is too short"; }
		if (strlen($admincat["description"]) > 600) { $err["description"] = TRUE; $errmsg[] = "description is too long"; }
		if ($admincat["faq"] != "" && strlen($admincat["faq"]) < 10) { $err["faq"] = TRUE; $errmsg[] = "faq is too short"; }
		if (!preg_match("/^[A-Z ]{3,}$/i", $admincat["adj_newest"])) { $err["lang_adj_newest"] = TRUE; $errmsg[] = "invalid language"; }
		if (!preg_match("/^[A-Z ]{3,}$/i", $admincat["adj_oldest"])) { $err["lang_adj_oldest"] = TRUE; $errmsg[] = "invalid language"; }
		if (!preg_match("/^[A-Z ]{3,}$/i", $admincat["noun_post_singular"])) { $err["lang_noun_post"] = TRUE; $errmsg[] = "invalid language"; }
		if (!preg_match("/^[A-Z ]{3,}$/i", $admincat["noun_post_plural"])) { $err["lang_noun_post"] = TRUE; $errmsg[] = "invalid language"; }
		if (!preg_match("/^[A-Z ]{3,}$/i", $admincat["noun_recenttags"])) { $err["lang_noun_recenttags"] = TRUE; $errmsg[] = "invalid language"; }
		if (!preg_match("/^[A-Z ]{3,}$/i", $admincat["noun_reply_singular"])) { $err["lang_noun_reply"] = TRUE; $errmsg[] = "invalid language"; }
		if (!preg_match("/^[A-Z ]{3,}$/i", $admincat["noun_reply_plural"])) { $err["lang_noun_reply"] = TRUE; $errmsg[] = "invalid language"; }
		if (!preg_match("/^[A-Z ]{3,}$/i", $admincat["noun_vote_singular"])) { $err["lang_noun_vote"] = TRUE; $errmsg[] = "invalid language"; }
		if (!preg_match("/^[A-Z ]{3,}$/i", $admincat["noun_vote_plural"])) { $err["lang_noun_vote"] = TRUE; $errmsg[] = "invalid language"; }
		if (!preg_match("/^[A-Z ]{3,}$/i", $admincat["verb_post_present"])) { $err["lang_verb_post"] = TRUE; $errmsg[] = "invalid language"; }
		if (!preg_match("/^[A-Z ]{3,}$/i", $admincat["verb_post_past"])) { $err["lang_verb_post"] = TRUE; $errmsg[] = "invalid language"; }
		if (!preg_match("/^[A-Z ]{3,}$/i", $admincat["verb_post_participle"])) { $err["lang_verb_post"] = TRUE; $errmsg[] = "invalid language"; }
		if (!preg_match("/^[A-Z ]{3,}$/i", $admincat["verb_reply_present"])) { $err["lang_verb_reply"] = TRUE; $errmsg[] = "invalid language"; }
		if (!preg_match("/^[A-Z ]{3,}$/i", $admincat["verb_reply_past"])) { $err["lang_verb_reply"] = TRUE; $errmsg[] = "invalid language"; }
		if (!preg_match("/^[A-Z ]{3,}$/i", $admincat["verb_reply_participle"])) { $err["lang_verb_reply"] = TRUE; $errmsg[] = "invalid language"; }
		if (!preg_match("/^[A-Z ]{3,}$/i", $admincat["verb_tag_present"])) { $err["lang_verb_tag"] = TRUE; $errmsg[] = "invalid language"; }
		if (!preg_match("/^[A-Z ]{3,}$/i", $admincat["verb_tag_past"])) { $err["lang_verb_tag"] = TRUE; $errmsg[] = "invalid language"; }
		if (!preg_match("/^[A-Z ]{3,}$/i", $admincat["verb_tag_participle"])) { $err["lang_verb_tag"] = TRUE; $errmsg[] = "invalid language"; }
		if ($admincat["status"] != "") {
			if (strlen($admincat["status"]) < 9 || strpos($admincat["status"], ",") === FALSE) { $err["status"] = TRUE; $errmsg[] = "not enough status"; }
			else if (!preg_match("/^[a-z ,-]*$/", $admincat["status"])) { $err["status"] = TRUE; $errmsg[] = "statuses must contain only lowercase letters, hyphens and spaces"; }
			else if (preg_match("/(?:^.?.?.?,|,.?.?.?,|,.?.?.?$)/", $admincat["status"])) { $err["status"] = TRUE; $errmsg[] = "one or more statuses are too short"; }
			else if (preg_match("/(?:^ *[^ ,]{13,} *$|^ *[^ ,]{13,} *,|, *[^ ,]{13,} *,|, *[^ ,]{13,} *$)/", $admincat["status"])) { $err["status"] = TRUE; $errmsg[] = "one or more statuses are too long"; }
		}

		if (!$err) {
			$l = array(
				"adj_newest" => $admincat["adj_newest"],
				"adj_oldest" => $admincat["adj_oldest"],
				"noun_post" => array("singular" => $admincat["noun_post_singular"], "plural" => $admincat["noun_post_plural"]),
				"noun_recenttags" => $admincat["noun_recenttags"],
				"noun_reply" => array("singular" => $admincat["noun_reply_singular"], "plural" => $admincat["noun_reply_plural"]),
				"noun_vote" => array("singular" => $admincat["noun_vote_singular"], "plural" => $admincat["noun_vote_plural"]),
				"verb_post" => array("present" => $admincat["verb_post_present"], "past" => $admincat["verb_post_past"], "participle" => $admincat["verb_post_participle"]),
				"verb_reply" => array("present" => $admincat["verb_reply_present"], "past" => $admincat["verb_reply_past"], "participle" => $admincat["verb_reply_participle"]),
				"verb_tag" => array("present" => $admincat["verb_tag_present"], "past" => $admincat["verb_tag_past"], "participle" => $admincat["verb_tag_participle"])
			);

			if (isset($admincat["p_uniquetitles"])) $p_ut = "TRUE"; else $p_ut = "FALSE";

			$successmsg[] = "Updated!";
			if ($admincat["status"] == "")
				db_query("UPDATE categories SET description = '%s', faq = '%s', lang = '%s', status = NULL, p_uniquetitles = '%s' WHERE categoryid = %d",
					$admincat["description"], $admincat["faq"], serialize($l), $p_ut, $category["categoryid"]);
			else
				db_query("UPDATE categories SET description = '%s', faq = '%s', lang = '%s', status = '%s', p_uniquetitles = '%s' WHERE categoryid = %d",
					$admincat["description"], $admincat["faq"], serialize($l), $admincat["status"], $p_ut, $category["categoryid"]);

			/*if (db_affected_rows(db_query("UPDATE costs SET post = '%s', reply = '%s', edit = '%s', upvote = '%s' WHERE categoryid = %d",
				$admincat["cost_post"], $admincat["cost_reply"], $admincat["cost_edit"], $admincat["cost_upvote"], $category["categoryid"])) == 0)
				db_query("INSERT INTO costs (categoryid, post, reply, edit, upvote) VALUES(%d, '%s', '%s', '%s', '%s')",
					$category["categoryid"], $admincat["cost_post"], $admincat["cost_reply"], $admincat["cost_edit"], $admincat["cost_upvote"]);*/

			$category = getcategorydata($domain["cat"]);
		}
		$admincat["lang"] = $category["lang"];
	}
?>