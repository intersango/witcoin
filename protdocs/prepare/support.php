<?php
	global $bodyclass, $err, $errmsg, $post, $title, $witizen, $witizenfrom;

	$blocks[] ="profile"; $bodyclass[] = "p_support"; $query = array();

	if (isset($_SESSION["userid"])) {
		if (isset($_GET["p"]) && is_numeric($_GET["p"])) {
			$query[] = "p=" . $_GET["p"];

			$sql = "SELECT posts.post, posts.postid AS post_postid, posts.taglist, posts.title, posts.userid AS post_userid, users.displayname, categories.categorysd AS category_sd
				FROM posts
				LEFT OUTER JOIN users ON (posts.userid = users.userid)
				LEFT OUTER JOIN categories ON (posts.category = categories.categoryid)
				WHERE posts.category = %d AND posts.postid = %d";

			$posts = db_prepare_posts(db_query($sql, $category["categoryid"], $_GET["p"]));
			if (!$posts) { require_once "../protdocs/templates/_error.php"; exit; }

			$keys = array_keys($posts);
			$post = array("postid" => $keys[0]);
			foreach ($posts[$keys[0]] as $key => $value) $post[$key] = $value;

			$witizen = db_fetch_array(db_query("SELECT * FROM users WHERE userid = '%d'", $post["post_userid"]));

			// Duplicated from _witizen.php, maybe put in a function call sometime?
			$witizen["gravatar"] = $witizen["email_address"] == "" ? "" : "https://secure.gravatar.com/avatar/".md5(strtolower($witizen["email_address"]))."?s=168&d=monsterid&r=x";
			$created = db_fetch_array(db_query("SELECT EXTRACT(EPOCH FROM TIMESTAMP WITH TIME ZONE '%s')", $witizen["created"]));
			$access = db_fetch_array(db_query("SELECT EXTRACT(EPOCH FROM TIMESTAMP WITH TIME ZONE '%s')", $witizen["access"]));
			$witizen["member_for"] = time_length($created["date_part"]);
			$witizen["seen"] = time_duration($access["date_part"]);
			if ($witizen["website"] != "" && !preg_match("/^https?:\/\/.*$/", $witizen["website"])) $witizen["website"] = "http://" . $witizen["website"];

			$title[] = "support <a href='".getdomain($post["category_sd"])."p/".$post["postid"]."/".urlfriendly($post["title"])."' title='" . $post["title"] . "'>" . $post["title"] . "</a> by <a href='/witizens/".$post["post_userid"]."' title='".$post["displayname"]."'>".$post["displayname"]."</a>";
		} else if (isset($_GET["r"]) && is_numeric($_GET["r"])) {
			$query[] = "r=" . $_GET["r"];

			$sql = "SELECT replies.reply, replies.replyid AS reply_replyid, replies.userid AS reply_userid, posts.postid AS reply_postid, users.displayname, categories.categorysd AS category_sd
				FROM replies
				LEFT OUTER JOIN users ON (replies.userid = users.userid)
				LEFT OUTER JOIN posts ON (replies.postid = posts.postid)
				LEFT OUTER JOIN categories ON (posts.category = categories.categoryid)
				WHERE posts.category = %d AND replies.replyid = %d";

			$replies = db_prepare_replies(db_query($sql, $category["categoryid"], $_GET["r"]));
			if (!$replies) { require_once "../protdocs/templates/_error.php"; exit; }

			$keys = array_keys($replies);
			$reply = array("replyid" => $keys[0]);
			foreach ($replies[$keys[0]] as $key => $value) $reply[$key] = $value;

			$title[] = "support <a href='".getdomain($reply["category_sd"])."p/".$reply["reply_postid"]."#r-".$reply["replyid"]."'>reply</a> by <a href='/witizens/".$reply["reply_userid"]."' title='".$reply["displayname"]."'>".$reply["displayname"]."</a>";
		} else { require_once "../protdocs/templates/_error.php"; exit; }

		$witizenfrom = db_fetch_array(db_query("SELECT * FROM users WHERE userid = '%d'", $_SESSION["userid"]));

		if (count($_POST) > 0 && isset($_POST["witcoins"])) {
			$_POST["witcoins"] = trim($_POST["witcoins"]);

			// Check for errors
			$err = array(); $errmsg = array();
			if (!is_numeric($_POST["witcoins"]) || $_POST["witcoins"] < 0.00000001) { $err["witcoins"] = TRUE; $errmsg[] = "invalid witcoin amount"; }
			else if ($_POST["witcoins"] > $witizenfrom["witcoins"]) { $err["witcoins"] = TRUE; $errmsg[] = "not enough witcoins"; }

			if (!$err) {
				if (isset($_GET["p"])) {
					// Distribute funds
					// 95% to poster
					$share = bcmul($_POST["witcoins"], "0.95", 64);
					db_result(db_query("UPDATE users SET witcoins = witcoins - '%s' WHERE userid = %d", $share, $_SESSION["userid"]));
					db_result(db_query("UPDATE users SET witcoins = witcoins + '%s' WHERE userid = %d", $share, $post["post_userid"]));

					// Update balance for post
					db_query("UPDATE posts SET balance = balance + '%s', support = support + '%s' WHERE postid = %d", $share, $_POST["witcoins"], $post["postid"]);

					// 5% to site, category renter, upvoters
					$share = bcmul($_POST["witcoins"], "0.05", 64);
					$payees = array(0, $category["renter"]);
					$result = db_query("SELECT userid FROM votes WHERE postid = %d AND vote = 'upvote'", $post["postid"]);
					while ($payee = db_fetch_array($result)) $payees[] = $payee["userid"];
					foreach (explode(",", $witizenfrom["charities"]) as $charity) $payees[] = "charity_".$charity;
					payment($_SESSION["userid"], $payees, $share);

					// Update balances for votes
					$amount = bcdiv($share, count($payees), 64);
					db_query("UPDATE votes SET balance = balance + '%s' WHERE postid = %d", $amount, $post["postid"]);
				} else {
					// Distribute funds
					// 95% to replier
					$share = bcmul($_POST["witcoins"], "0.95", 64);
					db_result(db_query("UPDATE users SET witcoins = witcoins - '%s' WHERE userid = %d", $share, $_SESSION["userid"]));
					db_result(db_query("UPDATE users SET witcoins = witcoins + '%s' WHERE userid = %d", $share, $reply["reply_userid"]));

					// Update balance for reply
					db_query("UPDATE replies SET balance = balance + '%s', support = support + '%s' WHERE replyid = %d", $share, $_POST["witcoins"], $reply["replyid"]);

					// 5% to site, category renter, upvoters
					$share = bcmul($_POST["witcoins"], "0.05", 64);
					$payees = array(0, $category["renter"]);
					$result = db_query("SELECT userid FROM votes WHERE replyid = %d AND vote = 'upvote'", $reply["replyid"]);
					while ($payee = db_fetch_array($result)) $payees[] = $payee["userid"];
					foreach (explode(",", $witizenfrom["charities"]) as $charity) $payees[] = "charity_".$charity;
					payment($_SESSION["userid"], $payees, $share);

					// Update balances for votes
					$amount = bcdiv($share, count($payees), 64);
					db_query("UPDATE votes SET balance = balance + '%s' WHERE replyid = %d", $amount, $reply["replyid"]);
				}
			}
		}
	} else {
		$title[] = "support";
	}
?>