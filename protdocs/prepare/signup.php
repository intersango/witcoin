<?php
	global $bitcoin_address_witcoin, $btc, $btcconn, $rcvd0, $rcvd1, $title;

	require_once "../protdocs/includes/bitcoin.php";

	if (!$btc) { require_once "../protdocs/templates/_error_btc.php"; exit; }
	else if (isset($_SESSION["userid"]) && !isset($_GET["get"])) {
		if (isset($_GET["dest"])) header("Location: ".$_GET["dest"]); else header("Location: /");
		exit;
	} else if (isset($_SESSION["userid"])) {
		$witizen = db_fetch_array(db_query("SELECT * FROM users WHERE userid = %d", $_SESSION["userid"]));
		$title[] = "create account";
	} else if (!isset($_SESSION["userid"]) && isset($_GET["get"])) {
		require_once "../protdocs/templates/_error.php"; exit;
	} else $title[] = "sign up";

	require_once "../protdocs/includes/passwordhash.php";

	$query = array();
	if (isset($_GET["dest"])) $query[] = "dest=" . strip_tags($_GET["dest"]);
	if (isset($_GET["get"])) $query[] = "get";

	if (isset($_SESSION["userid"])) {
		$reg = db_fetch_array(db_query("SELECT * FROM users_bitcoin WHERE userid = %d", $_SESSION["userid"]));
		if (!$reg) {
			try { $bitcoin_address_witcoin = $btcconn->getnewaddress("witcoin.com"); }
			catch (Exception $e) { btcerr($e); require_once "../protdocs/templates/_error_btc.php"; exit; }
			db_query("INSERT INTO users_bitcoin (userid, bitcoin_address_witcoin) VALUES(%d, '%s')", $_SESSION["userid"], $bitcoin_address_witcoin);
			$rcvd0 = 0;
		} else {
			$bitcoin_address_witcoin = $reg["bitcoin_address_witcoin"];
			try {
				$rcvd0 = number_format($btcconn->getreceivedbyaddress($bitcoin_address_witcoin, 0), 16, ".", "");
				$rcvd1 = number_format($btcconn->getreceivedbyaddress($bitcoin_address_witcoin, 1), 16, ".", "");
			} catch (Exception $e) { btcerr($e); require_once "../protdocs/templates/_error_btc.php"; exit; }
		}
	} else {
		$reg = db_fetch_array(db_query("SELECT * FROM users_bitcoin WHERE ipaddress = '%s'", $_SERVER["REMOTE_ADDR"]));
		if (!$reg) {
			try { $bitcoin_address_witcoin = $btcconn->getnewaddress("witcoin.com"); }
			catch (Exception $e) { btcerr($e); require_once "../protdocs/templates/_error_btc.php"; exit; }
			db_query("INSERT INTO users_bitcoin (ipaddress, bitcoin_address_witcoin) VALUES('%s', '%s')", $_SERVER["REMOTE_ADDR"], $bitcoin_address_witcoin);
			$rcvd0 = 0;
		} else {
			$bitcoin_address_witcoin = $reg["bitcoin_address_witcoin"];
			try {
				$rcvd0 = number_format($btcconn->getreceivedbyaddress($bitcoin_address_witcoin, 0), 16, ".", "");
				$rcvd1 = number_format($btcconn->getreceivedbyaddress($bitcoin_address_witcoin, 1), 16, ".", "");
			} catch (Exception $e) { btcerr($e); require_once "../protdocs/templates/_error_btc.php"; exit; }
		}
	}

	$err = array(); $errmsg = array();
	if (!isset($_GET["ne"])) {
		// Check for errors
		if ($rcvd1 > 0 && count($_POST) > 0) {
			if (!isset($_POST["username"]) || strlen($_POST["username"]) < 3) { $err["username"] = TRUE; $errmsg[] = "not enough username"; }
			else if (strlen($_POST["username"]) > 43) { $err["username"] = TRUE; $errmsg[] = "too much username"; }
			else if (!preg_match("/^[a-zA-Z0-9_-]*$/", $_POST["username"])) { $err["username"] = TRUE; $errmsg[] = "username must contain only a-zA-Z0-9_-"; }
			if (!isset($_GET["get"])) {
				if (!isset($_POST["display_name"]) || strlen($_POST["display_name"]) < 3) { $err["display_name"] = TRUE; $errmsg[] = "not enough display name"; }
				else if (strlen($_POST["display_name"]) > 43) { $err["display_name"] = TRUE; $errmsg[] = "too much display name"; }
				else if (!preg_match("/^[a-zA-Z0-9 '_-]*$/", $_POST["display_name"])) { $err["display_name"] = TRUE; $errmsg[] = "display name must contain only a-zA-Z0-9 '_-"; }
				// http://www.regular-expressions.info/email.html
				if ($_POST["email_address"] != "" && !preg_match("/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/", strtoupper($_POST["email_address"]))) {
					$err["email_address"] = TRUE; $errmsg[] = "invalid email address.";
				}
			}

			// Make sure display name and username are available
			if (!$err && db_result(db_query("SELECT displayname FROM users WHERE displayname = '%s'", $_POST["display_name"]))) { $err["display_name"] = TRUE; $errmsg[] = "display name already exists"; }
			if (!$err && db_result(db_query("SELECT username FROM users WHERE username = '%s'", $_POST["username"]))) { $err["username"] = TRUE; $errmsg[] = "username already exists"; }

			if (strlen($_POST["password"]) < 12) { $err["password"] = TRUE; $err["password_confirm"] = TRUE; $errmsg[] = "not enough password"; }
			if ($_POST["password_confirm"] != $_POST["password"]) { $err["password"] = TRUE; $err["password_confirm"] = TRUE; $errmsg[] = "passwords do not match."; }

			if (!$err) {
				$hasher = new PasswordHash(12, FALSE);
				$hash = $hasher->HashPassword($_POST["password"]);

				if (isset($_GET["get"]))
					$userid = db_result(db_query("UPDATE users SET username = '%s', password_hash = '%s', witcoins = witcoins + '%s' WHERE userid = %d RETURNING userid",
						$_POST["username"], $hash, $rcvd1, $_SESSION["userid"]));
				else
					$userid = db_result(db_query("INSERT INTO users (username, displayname, password_hash, email_address, created, access, login, witcoins)
						VALUES('%s', '%s', '%s', '%s', 'now()', 'now()', 'now()', '%s') RETURNING userid",
						$_POST["username"], $_POST["display_name"], $hash, $_POST["email_address"], $rcvd1));

				if ($userid) {
					if (!isset($_GET["get"])) {
						$_SESSION["userid"] = $userid;
						$_SESSION["displayname"] = $_POST["display_name"];
					}
					db_query("INSERT INTO transactions (userid, type, witcoins, bitcoin_address_witcoin, confirmations, txed) VALUES(%d, '%s', '%s', '%s', %d, %s)",
						$userid, "deposit", $rcvd1, $bitcoin_address_witcoin, 1, "to_timestamp(".time().")");
					db_query("DELETE FROM users_bitcoin WHERE entryid = %d", $reg["entryid"]);
					if (isset($_GET["dest"])) header("Location: ".$_GET["dest"]);
					else header("Location: /");
					exit;
				}
			}
		}
	}
?>