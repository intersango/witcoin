<?php
	global $bodyclass, $category, $err, $errmsg, $head, $path, $reply, $title, $witizen;

	if (!isset($_SESSION["userid"])) { require_once "../protdocs/templates/_error.php"; exit; }

	$blocks[] = "category"; $blocks[] = "prices"; $blocks[] = "tags"; $bodyclass[] = "p_editreply";

	preg_match("/^\/r\/([0-9]*)\/edit?$/", $path, $matches);

	$sql = "SELECT posts.userid AS post_userid, preplies.userid AS preply_userid, replies.postid AS replies_postid, replies.replyid AS replies_replyid, replies.replypid, replies.userid AS replies_userid, replies.reply, replies.replied,
		users.displayname, users.email_address
		FROM replies
		LEFT OUTER JOIN users ON (replies.userid = users.userid)
		LEFT OUTER JOIN replies AS preplies ON (preplies.replyid = replies.replypid)
		LEFT OUTER JOIN posts ON (posts.postid = replies.postid)
		WHERE replies.userid = %d AND replies.replyid = %d";

	$replies = db_prepare_replies(db_query($sql, $_SESSION["userid"], $matches[1]));
	if ($replies) {
		$keys = array_keys($replies);
		$reply = array("replyid" => $keys[0]);
		foreach ($replies[$keys[0]] as $key => $value) $reply[str_replace("replies_", "reply_", $key)] = $value;

		// Make sure reply is by user
		if ($reply["reply_userid"] != $_SESSION["userid"]) { require_once "../protdocs/templates/_error.php"; exit; }

		$title[] = "edit ".filter(htmlspecialchars($category["lang"]["verb_reply"]["present"]), FILTER_WORDS);

		$witizen = db_fetch_array(db_query("SELECT * FROM users WHERE userid = %d", $_SESSION["userid"]));

		$err = array(); $errmsg = array();

		if (count($_POST) > 0) {
			// If no changes, go back to related post
			if ($reply["reply"] == $_POST["reply"]) { header("Location: /p/".$reply["reply_postid"]."/"); exit; }

			// Preserve current reply for inserting into reply_revs, entry in replies table will be overwritten with changes
			$reply_last = $reply;

			$reply["reply"] = $_POST["reply"];

			// Check for errors
			if ($witizen["witcoins"] < $category["costs"]["edit"]) { $err["witcoins"] = TRUE; $errmsg[] = "not enough witcoins"; }
			if (strlen($reply["reply"]) < 5) { $err["reply"] = TRUE; $errmsg[] = "not enough ".$category["lang"]["noun_reply"]["singular"]; }

			if (!$err) {
				if ($category["costs"]["edit"] > 0) {
					// Distribute funds
					$payees = array(0, $category["renter"]);
					if (isset($reply["preply_userid"])) $payees[] = $reply["preply_userid"]; else $payees[] = $reply["post_userid"];
					foreach (explode(",", $witizen["charities"]) as $charity) $payees[] = "charity_".$charity;
					$balance = bcsub(payment($_SESSION["userid"], $payees, $category["costs"]["edit"]), $witizen["witcoins"], 64);
				} else $balance = "0";

				$amount = bcdiv($category["costs"]["edit"], count($payees), 64);
				if (isset($reply["replypid"])) db_query("UPDATE replies SET balance = balance + '%s' WHERE replyid = %d", $amount, $reply["replypid"]);
				else db_query("UPDATE posts SET balance = balance + '%s' WHERE postid = %d", $amount, $reply["reply_postid"]);

				db_query("INSERT INTO reply_revs (replyid, reply, replied, userid) VALUES(%d, '%s', '%s', %d)", $reply_last["replyid"], $reply_last["reply"], $reply_last["replied"], $_SESSION["userid"]);
				$now = db_result(db_query("UPDATE replies SET reply = '%s', replied = 'now()', balance = balance + '%s' WHERE replyid = %d RETURNING replied", $reply["reply"], $balance, $reply["replyid"]));
				db_query("UPDATE posts SET balance = balance + '%s', lastactivity = '%s', lastevent = 'reply edited', lastuserid = %d WHERE postid = %d", $amount, $now, $_SESSION["userid"], $reply["reply_postid"]);

				header("Location: /p/".$reply["reply_postid"]."/"); exit;
			}
		}
	} else { require_once "../protdocs/templates/_error.php"; exit; }
?>