<?php
	global $bodyclass, $category, $domain, $head, $path, $title, $witizen;

	if (!is_rank("moderator")) { header("Location: ".getdomain()); exit; }

	$bodyclass[] = "p_admincat"; $head["style"][] = "/css/admin.css";

	$sort = "category"; if (isset($_GET["sort"])) $sort = $_GET["sort"];

	$title[] = "administrate categories";

	$sql = "SELECT categories.allowposts, categories.category, categories.categoryid AS category_categoryid, categories.categorypid, categories.categorysd, categories.created, categories.creator,
		categories.description, categories.faq, categories.lang, categories.p_uniquetitles, categories.privacy, categories.renter, categories.status,
	costs.edit AS cost_edit, costs.post AS cost_post, costs.reply AS cost_reply, costs.upvote AS cost_upvote
	FROM categories LEFT OUTER JOIN costs ON (categories.categoryid = costs.categoryid)";

	if ($sort == "category") $sql .= " ORDER BY category";
	else if ($sort == "edit") $sql .= " ORDER BY cost_edit, category";
	else if ($sort == "post") $sql .= " ORDER BY cost_post, category";
	else if ($sort == "reply") $sql .= " ORDER BY cost_reply, category";
	else if ($sort == "upvote") $sql .= " ORDER BY cost_upvote, category";

	$categories = db_prepare_categories(db_query($sql));

	$err = array(); $errmsg = array(); $successmsg = array();
	if (count($_POST) > 0) {
		// Check for errors
		foreach ($_POST as $var => $val) {
			if (preg_match("/^cat_([0-9]*)_cost_edit$/", $var, $matches)) {
				if (!is_numeric($val) || $val < 0 || $val > 1) { $err["cat_".$matches[1]."_cost_edit"] = TRUE; $errmsg["prices"] = isset($errmsg["prices"]) ? "invalid prices" : "invalid price"; }
			}
			if (preg_match("/^cat_([0-9]*)_cost_post$/", $var, $matches)) {
				if (!is_numeric($val) || $val < 0 || $val > 1) { $err["cat_".$matches[1]."_cost_post"] = TRUE; $errmsg["prices"] = isset($errmsg["prices"]) ? "invalid prices" : "invalid price"; }
			}
			if (preg_match("/^cat_([0-9]*)_cost_reply$/", $var, $matches)) {
				if (!is_numeric($val) || $val < 0 || $val > 1) { $err["cat_".$matches[1]."_cost_reply"] = TRUE; $errmsg["prices"] = isset($errmsg["prices"]) ? "invalid prices" : "invalid price"; }
			}
			if (preg_match("/^cat_([0-9]*)_cost_upvote$/", $var, $matches)) {
				if (!is_numeric($val) || $val < 0 || $val > 1) { $err["cat_".$matches[1]."_cost_upvote"] = TRUE; $errmsg["prices"] = isset($errmsg["prices"]) ? "invalid prices" : "invalid price"; }
			}
		}

		if (!$err) {
			$catconfig = array();
			foreach ($_POST as $var => $val) {
				if (preg_match("/^cat_([0-9]*)_cost_edit$/", $var, $matches)) {
					if (!isset($catconfig[$matches[1]])) $catconfig[$matches[1]] = array();
					$catconfig[$matches[1]]["cost_edit"] = $val;
				}
				if (preg_match("/^cat_([0-9]*)_cost_post$/", $var, $matches)) {
					if (!isset($catconfig[$matches[1]])) $catconfig[$matches[1]] = array();
					$catconfig[$matches[1]]["cost_post"] = $val;
				}
				if (preg_match("/^cat_([0-9]*)_cost_reply$/", $var, $matches)) {
					if (!isset($catconfig[$matches[1]])) $catconfig[$matches[1]] = array();
					$catconfig[$matches[1]]["cost_reply"] = $val;
				}
				if (preg_match("/^cat_([0-9]*)_cost_upvote$/", $var, $matches)) {
					if (!isset($catconfig[$matches[1]])) $catconfig[$matches[1]] = array();
					$catconfig[$matches[1]]["cost_upvote"] = $val;
				}
			}

			foreach ($catconfig as $cat => $config) {
				if ($config["cost_edit"] != $categories[$cat]["costs"]["edit"]
				  || $config["cost_post"] != $categories[$cat]["costs"]["post"]
				  || $config["cost_reply"] != $categories[$cat]["costs"]["reply"]
				  || $config["cost_upvote"] != $categories[$cat]["costs"]["upvote"]) {
					if (db_affected_rows(db_query("UPDATE costs SET edit = '%s', post = '%s', reply = '%s', upvote = '%s' WHERE categoryid = %d",
						$config["cost_edit"], $config["cost_post"], $config["cost_reply"], $config["cost_upvote"], $cat)) == 0)
						db_query("INSERT INTO costs (categoryid, edit, post, reply, upvote) VALUES(%d, '%s', '%s', '%s', '%s')",
							$cat, $config["cost_edit"], $config["cost_post"], $config["cost_reply"], $config["cost_upvote"]);
				}
			}

			$successmsg[] = "Categories updated!";

			$sql = "SELECT categories.allowposts, categories.category, categories.categoryid AS category_categoryid, categories.categorypid, categories.categorysd, categories.created, categories.creator,
				categories.description, categories.faq, categories.lang, categories.p_uniquetitles, categories.privacy, categories.renter, categories.status,
			costs.edit AS cost_edit, costs.post AS cost_post, costs.reply AS cost_reply, costs.upvote AS cost_upvote
			FROM categories LEFT OUTER JOIN costs ON (categories.categoryid = costs.categoryid)";

			if ($sort == "category") $sql .= " ORDER BY category";
			else if ($sort == "edit") $sql .= " ORDER BY cost_edit, category";
			else if ($sort == "post") $sql .= " ORDER BY cost_post, category";
			else if ($sort == "reply") $sql .= " ORDER BY cost_reply, category";
			else if ($sort == "upvote") $sql .= " ORDER BY cost_upvote, category";

			$categories = db_prepare_categories(db_query($sql));
		}
	}
?>